Pod::Spec.new do |s|
       s.name         = "KDS"
       s.version      = '1.9.0'
       s.authors      = "Usercentrics"
       s.summary      = "Usercentrics SDK"
       s.description  = "Usercentrics Framework for iOS"
       s.homepage     = "https://docs.usercentrics.com/#/sdk?id=ios-swift-doc"
       s.license = { :type => 'Copyright', :text => "
                      Copyright 2020
                      Permission is granted to Usercentrics
                    "
                   }
       s.source       = { :git => "https://gitlab.com/fnoack10/pod.git", :tag => "#{s.version}" }
       s.public_header_files = "UsercentricsSDK.framework/Headers/*.h"
       s.source_files = "UsercentricsSDK.framework/Headers/*.h"
       s.vendored_frameworks = "UsercentricsSDK.framework"
       s.platform = :ios
       s.swift_version = "4.2"
       s.ios.deployment_target  = '10.3.2'
   end
