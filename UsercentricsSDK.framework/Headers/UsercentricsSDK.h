#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class USDKCategory, USDKCategoryProps, USDKTCFPurpose, USDKPurposeProps, USDKTCFSpecialFeature, USDKSpecialFeatureProps, USDKTCFStack, USDKStackProps, USDKTCFData, USDKVendorProps, USDKTCFVendor, USDKTCF_DECISION_UI_LAYER, USDKUserDecision, USDKTCFUserDecisions, USDKConsentHandlers, USDKResourcesFileResource, USDKResourcesImageResource, USDKUserOptions, USDKUCError, USDKConsentType, USDKExtendedSettings, UIViewController, USDKCustomAssets, USDKService, USDKSettings, USDKCCPAData, USDKInitialUIValues, USDKUsercentricsLoggerDelegate, USDKEvent, USDKKotlinThrowable, UIFont, USDKUsercentrics, USDKUIVariant, USDKUISettings, USDKCCPAUISettings, USDKTCFUISettings, USDKViewData, USDKViewHandlers, USDKLayer, UIView, USDKServiceCategory, USDKKotlinEnum, USDKTCFTab, USDKHistoryLabels, USDKFontTheme, USDKServiceSection, UIColor, USDKTheme, USDKConsentAction, USDKConsent, USDKVersions, USDKConsentHistory, USDKDataTransferObjectConsent, USDKDataTransferObjectSettings, USDKDataTransferObjectService, USDKDataTransferObject, USDKEssentialServices, USDKMergedServicesSettings, USDKCacheEntry, USDKStorageCCPA, USDKStorageCache, USDKStorageKeys, USDKStorageService, USDKStorageSettings, USDKStorageTCF, USDKKotlinArray, USDKCountryData, USDKLocationData, USDKUserCountry, USDKDataDistribution, USDKLanguage, USDKProcessingCompany, USDKURLs, USDKBaseService, USDKButton, USDKConsent_, USDKConsentStatus, USDKCustomizationColor, USDKCustomizationFont, USDKCustomization, USDKDataDistributionTitle, USDKDataExchangeSetting, USDKDataExchangeType, USDKDescriptionTitle, USDKCCPAOptions, USDKTCFOptions, USDKGeneralLabels, USDKLabel, USDKLink, USDKLinks, USDKPoweredBy, USDKURLsTitle, USDKServiceLabels, USDKSettingsVersion, USDKThemeSettings, USDKLabelOnOff, USDKTCFButtons, USDKTCFChangedPurposes, USDKTCFFirstLayerDescription, USDKTCFFirstLayer, USDKTCFGeneralLabels, USDKLabels, USDKVendor, USDKTCFLabels, USDKTCFScope, USDKTCFTabs, USDKTCFSecondLayer, USDKToggles, USDKApiAggregatorService, USDKApiAggregatorTemplates, USDKApiBackgroundOverlay, USDKApiBackgroundOverlayTarget, USDKApiBaseService, USDKFirstLayerVariant, USDKCCPARegion, USDKSecondLayerVariant, USDKApiCCPA, USDKApiCategory, USDKApiCustomizationColor, USDKApiCustomizationFont, USDKApiCustomization, USDKApiDataExchangeSetting, USDKApiDataExchangeType, USDKApiLabels, USDKApiService, USDKApiTCF2, USDKApiSettings, USDKApiSettingsVersion, USDKApiStringToListProperties, USDKApiTCF2ChangedPurposes, USDKConsentData, USDKConsentsList, USDKGetConsentsVariables, USDKGraphQLConsent, USDKGraphQLConsentString, USDKGraphQLQuery, USDKSaveConsentsVariables, USDKGraphQLQueryMutation, USDKLanguageData, USDKOptionalOptions, USDKSaveConsentsData, USDKSaveConsentsResponse, USDKServiceGenerateData, USDKKotlinx_serialization_runtimeJsonElement, USDKKotlinx_serialization_runtimeJsonTransformingSerializer, USDKUserConsentResponse, USDKInitialView, USDKUserAgentInfo, USDKCCPAButtons, USDKFirstLayerDescription, USDKCCPAFirstLayer, USDKTabs, USDKCCPASecondLayer, USDKWarningMessages, USDKButtons, USDKFirstLayer, USDKSecondLayer, USDKLanguageApi, USDKDeviceStorage, USDKUsercentricsLogger, USDKAPIArgs, USDKCustomCommands, USDKCmpStatus, USDKDisplayStatus, USDKEventListenerQueue, USDKEventStatus, USDKTCModel, USDKKotlinUnit, USDKEventItem, USDKValidType, USDKCommandRespType, USDKCommand, USDKGetTCDataCommand, USDKAddEventListenerCommand, USDKGetInAppTCDataCommand, USDKPingCommand, USDKRemoveEventListenerCommand, USDKGetVendorListCommand, USDKResponse, USDKVendorList, USDKVendor_, USDKFeature, USDKPurpose, USDKStack, USDKGVLData, USDKTCFCommand, USDKBooleanVectorOrString, USDKConsentData_, USDKOutOfBand, USDKPublisherData, USDKTCData, USDKRestrictions, USDKRestrictionType, USDKStringOrNumber, USDKGVL, USDKTCModelPropType, USDKConsentLanguages, USDKVector, USDKPurposeRestrictionVector, USDKFields, USDKLocation, USDKUserCountryApi, USDKSettingsApi, USDKServicesApi, USDKStorage, USDKIABTCF_KEYS, USDKConsentStatusObject, USDKEventManager, USDKHttpRequests, USDKSettingsService, USDKConsentsApi, USDKTCF_VENDOR_PURPOSE_TYPE, USDKTCF_WARN_MESSAGES, USDKIdAndName, USDKTCFFeature, USDKTCFSpecialPurpose, USDKBasePurpose, USDKBaseTCFUserDecision, USDKTCFUserDecisionOnPurpose, USDKTCFUserDecisionOnSpecialFeature, USDKTCFUserDecisionOnVendor, USDKTCModelInitOptions, USDKCCPA, USDKKotlinx_serialization_runtimeJson, USDKTimeTimer, USDKKtor_client_coreHttpClient, USDKEventDispatcher, USDKKotlinException, USDKCCPAException, USDKDeclarations, USDKGVLError, USDKGVLParsedBaseParams, USDKGVLParsedFeatures, USDKGVLParsedPurposes, USDKGVLParsedSpecialFeatures, USDKGVLParsedSpecialPurposes, USDKGVLParsedStacks, USDKGVLParsedVendors, USDKEncodingOptions, USDKBitLength, USDKEncodedTCFKeys, USDKEncodingOptionsVersion, USDKSegment, USDKFieldSequence, USDKSequenceVersionMapType, USDKSequenceVersionMap, USDKSequenceVersionMapTypeSVMItem, USDKIntEncoderCompanion, USDKLangEncoderCompanion, USDKDateEncoderCompanion, USDKBooleanEncoderCompanion, USDKFixedVectorEncoderCompanion, USDKPurposeRestrictionVectorEncoderCompanion, USDKVendorVectorEncoderCompanion, USDKVectorEncodingType, USDKTreeNode, USDKPurposeRestriction, USDKSingleIDOrCollection, USDKByPurposeVendorMap, USDKOverflow, USDKKotlinx_serialization_runtimeJsonBuilder, USDKKotlinx_serialization_runtimeJsonConfiguration, USDKKotlinx_coroutines_coreCoroutineDispatcher, UISwitch, UIStackView, UISegmentedControl, NSLayoutConstraint, UIImage, USDKKotlinCValue, UIImageView, NSURL, NSBundle, USDKKotlinx_serialization_runtimeSerialKind, USDKKotlinNothing, USDKKotlinx_serialization_runtimeUpdateMode, USDKKotlinx_serialization_runtimeJsonNull, USDKKotlinx_serialization_runtimeJsonPrimitive, USDKKtor_client_coreHttpClientConfig, USDKKtor_client_coreHttpClientEngineConfig, USDKKtor_client_coreHttpReceivePipeline, USDKKtor_client_coreHttpRequestPipeline, USDKKtor_client_coreHttpResponsePipeline, USDKKtor_client_coreHttpSendPipeline, USDKKotlinAbstractCoroutineContextElement, USDKKotlinAutofreeScope, USDKKotlinCValuesRef, USDKKotlinCValues, USDKKtor_utilsAttributeKey, USDKKtor_client_coreProxyConfig, USDKKtor_utilsPipelinePhase, USDKKtor_utilsPipeline, USDKKotlinDeferScope, USDKKtor_httpUrl, USDKKtor_httpURLProtocol;

@protocol USDKResultCallback, USDKKotlinComparable, USDKConsentInterface, USDKKotlinx_serialization_runtimeKSerializer, USDKUISettingsInterface, USDKApiBaseServiceInterface, USDKKotlinx_serialization_runtimeEncoder, USDKKotlinx_serialization_runtimeSerialDescriptor, USDKKotlinx_serialization_runtimeSerializationStrategy, USDKKotlinx_serialization_runtimeDecoder, USDKKotlinx_serialization_runtimeDeserializationStrategy, USDKParcelizeParcelable, USDKJsonHttpClient, USDKCCPAStorage, USDKKotlinIterator, USDKKotlinIterable, USDKGVLMapItem, USDKKotlinx_serialization_runtimeSerialModule, USDKKotlinx_serialization_runtimeSerialFormat, USDKKotlinx_serialization_runtimeStringFormat, USDKKotlinx_serialization_runtimeCompositeEncoder, USDKKotlinAnnotation, USDKKotlinx_serialization_runtimeCompositeDecoder, USDKKotlinCoroutineContext, USDKKotlinx_coroutines_coreCoroutineScope, USDKKtor_ioCloseable, USDKKtor_client_coreHttpClientEngine, USDKKtor_client_coreHttpClientEngineCapability, USDKKtor_utilsAttributes, USDKKotlinx_serialization_runtimeSerialModuleCollector, USDKKotlinKClass, USDKKotlinCoroutineContextKey, USDKKotlinCoroutineContextElement, USDKKotlinContinuation, USDKKotlinContinuationInterceptor, USDKKotlinx_coroutines_coreRunnable, USDKKtor_client_coreHttpClientFeature, USDKKotlinSuspendFunction2, USDKKotlinKDeclarationContainer, USDKKotlinKAnnotatedElement, USDKKotlinKClassifier, USDKKotlinNativePlacement, USDKKotlinFunction, USDKKtor_httpParameters, USDKKotlinMapEntry, USDKKtor_utilsStringValues;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wnullability"

__attribute__((swift_name("KotlinBase")))
@interface USDKBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface USDKBase (USDKBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface USDKMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface USDKMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorUSDKKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface USDKNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface USDKByte : USDKNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface USDKUByte : USDKNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface USDKShort : USDKNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface USDKUShort : USDKNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface USDKInt : USDKNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface USDKUInt : USDKNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface USDKLong : USDKNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface USDKULong : USDKNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface USDKFloat : USDKNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface USDKDouble : USDKNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface USDKBoolean : USDKNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CategoryProps")))
@interface USDKCategoryProps : USDKBase
- (instancetype)initWithCategory:(USDKCategory *)category checked:(BOOL)checked __attribute__((swift_name("init(category:checked:)"))) __attribute__((objc_designated_initializer));
- (USDKCategoryProps *)doCopyCategory:(USDKCategory *)category checked:(BOOL)checked __attribute__((swift_name("doCopy(category:checked:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKCategory *category __attribute__((swift_name("category")));
@property BOOL checked __attribute__((swift_name("checked")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PurposeProps")))
@interface USDKPurposeProps : USDKBase
- (instancetype)initWithChecked:(BOOL)checked legitimateInterestChecked:(BOOL)legitimateInterestChecked purpose:(USDKTCFPurpose *)purpose __attribute__((swift_name("init(checked:legitimateInterestChecked:purpose:)"))) __attribute__((objc_designated_initializer));
- (USDKPurposeProps *)doCopyChecked:(BOOL)checked legitimateInterestChecked:(BOOL)legitimateInterestChecked purpose:(USDKTCFPurpose *)purpose __attribute__((swift_name("doCopy(checked:legitimateInterestChecked:purpose:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL checked __attribute__((swift_name("checked")));
@property BOOL legitimateInterestChecked __attribute__((swift_name("legitimateInterestChecked")));
@property USDKTCFPurpose *purpose __attribute__((swift_name("purpose")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SpecialFeatureProps")))
@interface USDKSpecialFeatureProps : USDKBase
- (instancetype)initWithChecked:(BOOL)checked specialFeature:(USDKTCFSpecialFeature *)specialFeature __attribute__((swift_name("init(checked:specialFeature:)"))) __attribute__((objc_designated_initializer));
- (USDKSpecialFeatureProps *)doCopyChecked:(BOOL)checked specialFeature:(USDKTCFSpecialFeature *)specialFeature __attribute__((swift_name("doCopy(checked:specialFeature:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL checked __attribute__((swift_name("checked")));
@property USDKTCFSpecialFeature *specialFeature __attribute__((swift_name("specialFeature")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StackProps")))
@interface USDKStackProps : USDKBase
- (instancetype)initWithChecked:(BOOL)checked stack:(USDKTCFStack *)stack __attribute__((swift_name("init(checked:stack:)"))) __attribute__((objc_designated_initializer));
- (USDKStackProps *)doCopyChecked:(BOOL)checked stack:(USDKTCFStack *)stack __attribute__((swift_name("doCopy(checked:stack:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL checked __attribute__((swift_name("checked")));
@property USDKTCFStack *stack __attribute__((swift_name("stack")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsercentricsMaps")))
@interface USDKUsercentricsMaps : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsercentricsMaps.Companion")))
@interface USDKUsercentricsMapsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (NSArray<USDKCategoryProps *> *)mapCategoriesCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("mapCategories(categories:)")));
- (NSArray<USDKPurposeProps *> *)mapPurposesTcfData:(USDKTCFData *)tcfData __attribute__((swift_name("mapPurposes(tcfData:)")));
- (NSArray<USDKSpecialFeatureProps *> *)mapSpecialFeaturesTcfData:(USDKTCFData *)tcfData __attribute__((swift_name("mapSpecialFeatures(tcfData:)")));
- (NSArray<USDKStackProps *> *)mapStacksTcfData:(USDKTCFData *)tcfData __attribute__((swift_name("mapStacks(tcfData:)")));
- (NSArray<USDKVendorProps *> *)mapVendorsTcfData:(USDKTCFData *)tcfData __attribute__((swift_name("mapVendors(tcfData:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VendorProps")))
@interface USDKVendorProps : USDKBase
- (instancetype)initWithChecked:(BOOL)checked legitimateInterestChecked:(BOOL)legitimateInterestChecked vendor:(USDKTCFVendor *)vendor __attribute__((swift_name("init(checked:legitimateInterestChecked:vendor:)"))) __attribute__((objc_designated_initializer));
- (USDKVendorProps *)doCopyChecked:(BOOL)checked legitimateInterestChecked:(BOOL)legitimateInterestChecked vendor:(USDKTCFVendor *)vendor __attribute__((swift_name("doCopy(checked:legitimateInterestChecked:vendor:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL checked __attribute__((swift_name("checked")));
@property BOOL legitimateInterestChecked __attribute__((swift_name("legitimateInterestChecked")));
@property USDKTCFVendor *vendor __attribute__((swift_name("vendor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentHandlers")))
@interface USDKConsentHandlers : USDKBase
- (instancetype)initWithAcceptAllServices:(void (^)(USDKTCF_DECISION_UI_LAYER * _Nullable, USDKBoolean * _Nullable, id<USDKResultCallback>))acceptAllServices denyAllServices:(void (^)(USDKTCF_DECISION_UI_LAYER * _Nullable, USDKBoolean * _Nullable, id<USDKResultCallback>))denyAllServices updateServices:(void (^)(USDKTCF_DECISION_UI_LAYER * _Nullable, NSArray<USDKUserDecision *> * _Nullable, USDKTCFUserDecisions * _Nullable, id<USDKResultCallback>))updateServices __attribute__((swift_name("init(acceptAllServices:denyAllServices:updateServices:)"))) __attribute__((objc_designated_initializer));
- (USDKConsentHandlers *)doCopyAcceptAllServices:(void (^)(USDKTCF_DECISION_UI_LAYER * _Nullable, USDKBoolean * _Nullable, id<USDKResultCallback>))acceptAllServices denyAllServices:(void (^)(USDKTCF_DECISION_UI_LAYER * _Nullable, USDKBoolean * _Nullable, id<USDKResultCallback>))denyAllServices updateServices:(void (^)(USDKTCF_DECISION_UI_LAYER * _Nullable, NSArray<USDKUserDecision *> * _Nullable, USDKTCFUserDecisions * _Nullable, id<USDKResultCallback>))updateServices __attribute__((swift_name("doCopy(acceptAllServices:denyAllServices:updateServices:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property void (^acceptAllServices)(USDKTCF_DECISION_UI_LAYER * _Nullable, USDKBoolean * _Nullable, id<USDKResultCallback>) __attribute__((swift_name("acceptAllServices")));
@property void (^denyAllServices)(USDKTCF_DECISION_UI_LAYER * _Nullable, USDKBoolean * _Nullable, id<USDKResultCallback>) __attribute__((swift_name("denyAllServices")));
@property void (^updateServices)(USDKTCF_DECISION_UI_LAYER * _Nullable, NSArray<USDKUserDecision *> * _Nullable, USDKTCFUserDecisions * _Nullable, id<USDKResultCallback>) __attribute__((swift_name("updateServices")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR")))
@interface USDKMR : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)mR __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.files")))
@interface USDKMRFiles : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)files __attribute__((swift_name("init()")));
@property (readonly) USDKResourcesFileResource *check __attribute__((swift_name("check")));
@property (readonly) USDKResourcesFileResource *close __attribute__((swift_name("close")));
@property (readonly) USDKResourcesFileResource *cog __attribute__((swift_name("cog")));
@property (readonly) USDKResourcesFileResource *collapse __attribute__((swift_name("collapse")));
@property (readonly, getter=doCopy) USDKResourcesFileResource *copy __attribute__((swift_name("copy")));
@property (readonly) USDKResourcesFileResource *cross __attribute__((swift_name("cross")));
@property (readonly) USDKResourcesFileResource *expand __attribute__((swift_name("expand")));
@property (readonly) USDKResourcesFileResource *help __attribute__((swift_name("help")));
@property (readonly) USDKResourcesFileResource *link __attribute__((swift_name("link")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.fonts")))
@interface USDKMRFonts : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)fonts __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.images")))
@interface USDKMRImages : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)images __attribute__((swift_name("init()")));
@property (readonly) USDKResourcesImageResource *check __attribute__((swift_name("check")));
@property (readonly) USDKResourcesImageResource *close __attribute__((swift_name("close")));
@property (readonly) USDKResourcesImageResource *cog __attribute__((swift_name("cog")));
@property (readonly) USDKResourcesImageResource *collapse __attribute__((swift_name("collapse")));
@property (readonly, getter=doCopy) USDKResourcesImageResource *copy __attribute__((swift_name("copy")));
@property (readonly) USDKResourcesImageResource *cross __attribute__((swift_name("cross")));
@property (readonly) USDKResourcesImageResource *expand __attribute__((swift_name("expand")));
@property (readonly) USDKResourcesImageResource *help __attribute__((swift_name("help")));
@property (readonly) USDKResourcesImageResource *link __attribute__((swift_name("link")));
@property (readonly) USDKResourcesImageResource *logo __attribute__((swift_name("logo")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.plurals")))
@interface USDKMRPlurals : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)plurals __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.strings")))
@interface USDKMRStrings : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)strings __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Storage")))
@interface USDKStorage : USDKBase
- (instancetype)initWithName:(NSString * _Nullable)name context:(id _Nullable)context __attribute__((swift_name("init(name:context:)"))) __attribute__((objc_designated_initializer));
- (void)deleteKeyKey:(NSString *)key __attribute__((swift_name("deleteKey(key:)")));
- (NSString * _Nullable)getValueKey:(NSString *)key defaultValue:(NSString * _Nullable)defaultValue __attribute__((swift_name("getValue(key:defaultValue:)")));
- (void)putValueKey:(NSString *)key value:(NSString *)value __attribute__((swift_name("putValue(key:value:)")));
@end;

__attribute__((swift_name("Usercentrics")))
@interface USDKUsercentrics : USDKBase
- (instancetype)initWithSettingsId:(NSString *)settingsId options:(USDKUserOptions * _Nullable)options __attribute__((swift_name("init(settingsId:options:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithSettingsId:(NSString *)settingsId options:(USDKUserOptions * _Nullable)options appContext:(id _Nullable)appContext __attribute__((swift_name("init(settingsId:options:appContext:)"))) __attribute__((objc_designated_initializer));
- (void)acceptAllForTCFFromLayer:(USDKTCF_DECISION_UI_LAYER *)fromLayer onSuccess:(void (^)(void))onSuccess onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("acceptAllForTCF(fromLayer:onSuccess:onFailure:)")));
- (void)acceptAllServicesConsentType:(USDKConsentType *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("acceptAllServices(consentType:callback:onFailure:)")));
- (void)changeLanguageLanguage:(NSString *)language callback:(void (^)(void))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("changeLanguage(language:callback:onFailure:)")));
- (void)denyAllForTCFFromLayer:(USDKTCF_DECISION_UI_LAYER *)fromLayer onSuccess:(void (^)(void))onSuccess onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("denyAllForTCF(fromLayer:onSuccess:onFailure:)")));
- (void)denyAllServicesConsentType:(USDKConsentType *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("denyAllServices(consentType:callback:onFailure:)")));
- (NSArray<USDKCategory *> * _Nullable)getCategories __attribute__((swift_name("getCategories()")));
- (NSString * _Nullable)getControllerId __attribute__((swift_name("getControllerId()")));
- (USDKExtendedSettings * _Nullable)getExtendedSettings __attribute__((swift_name("getExtendedSettings()")));
- (UIViewController * _Nullable)getPredefinedUIViewContext:(id _Nullable)viewContext customAssets:(USDKCustomAssets * _Nullable)customAssets dismissView:(void (^)(void))dismissView __attribute__((swift_name("getPredefinedUI(viewContext:customAssets:dismissView:)")));
- (UIViewController * _Nullable)getPredefinedUICustomAssets:(USDKCustomAssets * _Nullable)customAssets dismissView:(void (^)(void))dismissView __attribute__((swift_name("getPredefinedUI(customAssets:dismissView:)")));
- (NSArray<USDKService *> * _Nullable)getServices __attribute__((swift_name("getServices()")));
- (USDKSettings * _Nullable)getSettings __attribute__((swift_name("getSettings()")));
- (USDKTCFData *)getTCFData __attribute__((swift_name("getTCFData()")));
- (USDKCCPAData *)getUSPData __attribute__((swift_name("getUSPData()")));
- (void)initializeCallback:(void (^)(USDKInitialUIValues *))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("initialize(callback:onFailure:)")));
- (void)resetUserSessionCallback:(void (^)(USDKInitialUIValues *))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("resetUserSession(callback:onFailure:)")));
- (void)restoreUserSessionControllerId:(NSString *)controllerId callback:(void (^)(USDKInitialUIValues *))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("restoreUserSession(controllerId:callback:onFailure:)")));
- (void)saveOptOutForCCPAIsOptedOut:(BOOL)isOptedOut consentType:(USDKConsentType *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("saveOptOutForCCPA(isOptedOut:consentType:callback:onFailure:)")));
- (void)setCMPIDId:(NSString *)id onSuccess:(void (^)(void))onSuccess onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("setCMPID(id:onSuccess:onFailure:)")));
- (void)setLoggerDelegateLoggerDelegate:(USDKUsercentricsLoggerDelegate * _Nullable)loggerDelegate __attribute__((swift_name("setLoggerDelegate(loggerDelegate:)")));
- (void)setTCFUIAsClosedOnSuccess:(void (^)(void))onSuccess onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("setTCFUIAsClosed(onSuccess:onFailure:)")));
- (void)setTCFUIAsOpenOnSuccess:(void (^)(void))onSuccess onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("setTCFUIAsOpen(onSuccess:onFailure:)")));
- (void)subscribeEventEventName:(NSString *)eventName action:(void (^)(USDKEvent *))action __attribute__((swift_name("subscribeEvent(eventName:action:)"))) __attribute__((deprecated("Please do not use this anymore, use [getServices] instead")));
- (void)updateChoicesForTCFDecisions:(USDKTCFUserDecisions *)decisions fromLayer:(USDKTCF_DECISION_UI_LAYER *)fromLayer onSuccess:(void (^)(void))onSuccess onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("updateChoicesForTCF(decisions:fromLayer:onSuccess:onFailure:)")));
- (void)updateServicesDecisions:(NSArray<USDKUserDecision *> *)decisions consentType:(USDKConsentType *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("updateServices(decisions:consentType:callback:onFailure:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsercentricsLogger")))
@interface USDKUsercentricsLogger : USDKBase
- (instancetype)initWithIsDebugMode:(BOOL)isDebugMode logger:(USDKUsercentricsLoggerDelegate *)logger __attribute__((swift_name("init(isDebugMode:logger:)"))) __attribute__((objc_designated_initializer));
- (void)debugMessage:(NSString *)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("debug(message:cause:)")));
- (void)errorMessage:(NSString *)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("error(message:cause:)")));
- (void)warningMessage:(NSString *)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("warning(message:cause:)")));
@property BOOL isDebugMode __attribute__((swift_name("isDebugMode")));
@property USDKUsercentricsLoggerDelegate *logger __attribute__((swift_name("logger")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsercentricsLoggerDelegate")))
@interface USDKUsercentricsLoggerDelegate : USDKBase
- (instancetype)initWithLogDebug:(void (^)(NSString *, USDKKotlinThrowable * _Nullable))logDebug logWarning:(void (^)(NSString *, USDKKotlinThrowable * _Nullable))logWarning logError:(void (^)(NSString *, USDKKotlinThrowable * _Nullable))logError __attribute__((swift_name("init(logDebug:logWarning:logError:)"))) __attribute__((objc_designated_initializer));
@property (readonly) void (^logDebug)(NSString *, USDKKotlinThrowable * _Nullable) __attribute__((swift_name("logDebug")));
@property (readonly) void (^logError)(NSString *, USDKKotlinThrowable * _Nullable) __attribute__((swift_name("logError")));
@property (readonly) void (^logWarning)(NSString *, USDKKotlinThrowable * _Nullable) __attribute__((swift_name("logWarning")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsercentricsView")))
@interface USDKUsercentricsView : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (UIViewController * _Nullable)getViewViewContext:(id _Nullable)viewContext customFont:(NSDictionary<NSString *, UIFont *> * _Nullable)customFont dismissView:(void (^)(void))dismissView __attribute__((swift_name("getView(viewContext:customFont:dismissView:)")));
- (void)initializeUc:(USDKUsercentrics *)uc variant:(USDKUIVariant *)variant __attribute__((swift_name("initialize(uc:variant:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ViewData")))
@interface USDKViewData : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId services:(NSArray<USDKService *> *)services categories:(NSArray<USDKCategory *> *)categories gdprUI:(USDKUISettings * _Nullable)gdprUI ccpaUI:(USDKCCPAUISettings * _Nullable)ccpaUI tcfUI:(USDKTCFUISettings * _Nullable)tcfUI ccpaData:(USDKCCPAData *)ccpaData tcfData:(USDKTCFData *)tcfData uiVariant:(USDKUIVariant *)uiVariant __attribute__((swift_name("init(controllerId:services:categories:gdprUI:ccpaUI:tcfUI:ccpaData:tcfData:uiVariant:)"))) __attribute__((objc_designated_initializer));
- (USDKViewData *)doCopyControllerId:(NSString *)controllerId services:(NSArray<USDKService *> *)services categories:(NSArray<USDKCategory *> *)categories gdprUI:(USDKUISettings * _Nullable)gdprUI ccpaUI:(USDKCCPAUISettings * _Nullable)ccpaUI tcfUI:(USDKTCFUISettings * _Nullable)tcfUI ccpaData:(USDKCCPAData *)ccpaData tcfData:(USDKTCFData *)tcfData uiVariant:(USDKUIVariant *)uiVariant __attribute__((swift_name("doCopy(controllerId:services:categories:gdprUI:ccpaUI:tcfUI:ccpaData:tcfData:uiVariant:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKCategory *> *categories __attribute__((swift_name("categories")));
@property USDKCCPAData *ccpaData __attribute__((swift_name("ccpaData")));
@property USDKCCPAUISettings * _Nullable ccpaUI __attribute__((swift_name("ccpaUI")));
@property NSString *controllerId __attribute__((swift_name("controllerId")));
@property USDKUISettings * _Nullable gdprUI __attribute__((swift_name("gdprUI")));
@property NSArray<USDKService *> *services __attribute__((swift_name("services")));
@property USDKTCFData *tcfData __attribute__((swift_name("tcfData")));
@property USDKTCFUISettings * _Nullable tcfUI __attribute__((swift_name("tcfUI")));
@property USDKUIVariant *uiVariant __attribute__((swift_name("uiVariant")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ViewHandlers")))
@interface USDKViewHandlers : USDKBase
- (instancetype)initWithDismissView:(void (^)(void))dismissView updateLanguage:(void (^)(NSString *, id<USDKResultCallback>))updateLanguage __attribute__((swift_name("init(dismissView:updateLanguage:)"))) __attribute__((objc_designated_initializer));
- (USDKViewHandlers *)doCopyDismissView:(void (^)(void))dismissView updateLanguage:(void (^)(NSString *, id<USDKResultCallback>))updateLanguage __attribute__((swift_name("doCopy(dismissView:updateLanguage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property void (^dismissView)(void) __attribute__((swift_name("dismissView")));
@property void (^updateLanguage)(NSString *, id<USDKResultCallback>) __attribute__((swift_name("updateLanguage")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("BottomBar")))
@interface USDKBottomBar : NSObject
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Content")))
@interface USDKContent : USDKBase
- (instancetype)initWithViewController:(UIViewController *)viewController data:(USDKViewData *)data __attribute__((swift_name("init(viewController:data:)"))) __attribute__((objc_designated_initializer));
- (void)changeLayerCurrentLayer:(USDKLayer *)currentLayer __attribute__((swift_name("changeLayer(currentLayer:)")));
- (void)setConstraintsCurrentLayer:(USDKLayer *)currentLayer __attribute__((swift_name("setConstraints(currentLayer:)")));
- (void)updateUINewData:(USDKViewData *)newData currentLayer:(USDKLayer *)currentLayer __attribute__((swift_name("updateUI(newData:currentLayer:)")));
@property (readonly) USDKViewData *data __attribute__((swift_name("data")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("Header")))
@interface USDKHeader : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("HeaderContainer")))
@interface USDKHeaderContainer : NSObject
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFHeaderLinks")))
@interface USDKTCFHeaderLinks : USDKBase
- (instancetype)initWithHeader:(UIViewController *)header __attribute__((swift_name("init(header:)"))) __attribute__((objc_designated_initializer));
@property UIView *container __attribute__((swift_name("container")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("CategoryCardView")))
@interface USDKCategoryCardView : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("CollapsedContainer")))
@interface USDKCollapsedContainer : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("IABCardView")))
@interface USDKIABCardView : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("IABHeaderOnlyCardView")))
@interface USDKIABHeaderOnlyCardView : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("PurposesScrollView")))
@interface USDKPurposesScrollView : NSObject
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceCategory")))
@interface USDKServiceCategory : USDKBase
- (instancetype)initWithService:(USDKService *)service category:(USDKCategory *)category __attribute__((swift_name("init(service:category:)"))) __attribute__((objc_designated_initializer));
- (USDKServiceCategory *)doCopyService:(USDKService *)service category:(USDKCategory *)category __attribute__((swift_name("doCopy(service:category:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKCategory *category __attribute__((swift_name("category")));
@property USDKService *service __attribute__((swift_name("service")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("TCFSecondLayerContent")))
@interface USDKTCFSecondLayerContent : NSObject
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol USDKKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface USDKKotlinEnum : USDKBase <USDKKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(USDKKotlinEnum *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFTab")))
@interface USDKTCFTab : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKTCFTab *purposes __attribute__((swift_name("purposes")));
@property (class, readonly) USDKTCFTab *vendors __attribute__((swift_name("vendors")));
- (int32_t)compareToOther:(USDKTCFTab *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("VendorsCardView")))
@interface USDKVendorsCardView : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("VendorsScrollView")))
@interface USDKVendorsScrollView : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("VendorsTableView")))
@interface USDKVendorsTableView : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("TCFFirstLayerContent")))
@interface USDKTCFFirstLayerContent : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("CCPAContent")))
@interface USDKCCPAContent : NSObject
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HistoryLabels")))
@interface USDKHistoryLabels : USDKBase
- (instancetype)initWithHistoryTitle:(NSString *)historyTitle historyDescription:(NSString *)historyDescription historyDecisionLabel:(NSString *)historyDecisionLabel historyDateLabel:(NSString *)historyDateLabel historyConsentGivenLabel:(NSString *)historyConsentGivenLabel historyConsentNotGivenLabel:(NSString *)historyConsentNotGivenLabel __attribute__((swift_name("init(historyTitle:historyDescription:historyDecisionLabel:historyDateLabel:historyConsentGivenLabel:historyConsentNotGivenLabel:)"))) __attribute__((objc_designated_initializer));
- (USDKHistoryLabels *)doCopyHistoryTitle:(NSString *)historyTitle historyDescription:(NSString *)historyDescription historyDecisionLabel:(NSString *)historyDecisionLabel historyDateLabel:(NSString *)historyDateLabel historyConsentGivenLabel:(NSString *)historyConsentGivenLabel historyConsentNotGivenLabel:(NSString *)historyConsentNotGivenLabel __attribute__((swift_name("doCopy(historyTitle:historyDescription:historyDecisionLabel:historyDateLabel:historyConsentGivenLabel:historyConsentNotGivenLabel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *historyConsentGivenLabel __attribute__((swift_name("historyConsentGivenLabel")));
@property (readonly) NSString *historyConsentNotGivenLabel __attribute__((swift_name("historyConsentNotGivenLabel")));
@property (readonly) NSString *historyDateLabel __attribute__((swift_name("historyDateLabel")));
@property (readonly) NSString *historyDecisionLabel __attribute__((swift_name("historyDecisionLabel")));
@property (readonly) NSString *historyDescription __attribute__((swift_name("historyDescription")));
@property (readonly) NSString *historyTitle __attribute__((swift_name("historyTitle")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("GPDRTabsContent")))
@interface USDKGPDRTabsContent : NSObject
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Layer")))
@interface USDKLayer : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKLayer *first __attribute__((swift_name("first")));
@property (class, readonly) USDKLayer *secondPurpose __attribute__((swift_name("secondPurpose")));
@property (class, readonly) USDKLayer *secondVendorlist __attribute__((swift_name("secondVendorlist")));
- (int32_t)compareToOther:(USDKLayer *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("MainViewController")))
@interface USDKMainViewController : NSObject
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAGlobalState")))
@interface USDKCCPAGlobalState : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)cCPAGlobalState __attribute__((swift_name("init()")));
- (USDKCCPAData * _Nullable)getGlobalState __attribute__((swift_name("getGlobalState()")));
- (void)handleLanguageChangeNewData:(USDKViewData *)newData __attribute__((swift_name("handleLanguageChange(newData:)")));
- (void)initializeCCPADataCcpaData:(USDKCCPAData *)ccpaData __attribute__((swift_name("initializeCCPAData(ccpaData:)")));
- (void)initializeCategoriesCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("initializeCategories(categories:)")));
- (void)initializeControllerIdControllerId:(NSString *)controllerId __attribute__((swift_name("initializeControllerId(controllerId:)")));
- (void)initializeUISettingsCcpaUISettings:(USDKCCPAUISettings * _Nullable)ccpaUISettings __attribute__((swift_name("initializeUISettings(ccpaUISettings:)")));
@property NSArray<USDKCategory *> * _Nullable categories __attribute__((swift_name("categories")));
@property USDKCCPAData * _Nullable ccpaData __attribute__((swift_name("ccpaData")));
@property USDKCCPAUISettings * _Nullable ccpaUI __attribute__((swift_name("ccpaUI")));
@property NSString * _Nullable controllerId __attribute__((swift_name("controllerId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FontTheme")))
@interface USDKFontTheme : USDKBase
- (instancetype)initWithRegularTitle:(UIFont *)regularTitle regularBody:(UIFont *)regularBody regularSmall:(UIFont *)regularSmall regularTiny:(UIFont *)regularTiny boldTitle:(UIFont *)boldTitle boldBody:(UIFont *)boldBody boldSmall:(UIFont *)boldSmall boldTiny:(UIFont *)boldTiny __attribute__((swift_name("init(regularTitle:regularBody:regularSmall:regularTiny:boldTitle:boldBody:boldSmall:boldTiny:)"))) __attribute__((objc_designated_initializer));
- (USDKFontTheme *)doCopyRegularTitle:(UIFont *)regularTitle regularBody:(UIFont *)regularBody regularSmall:(UIFont *)regularSmall regularTiny:(UIFont *)regularTiny boldTitle:(UIFont *)boldTitle boldBody:(UIFont *)boldBody boldSmall:(UIFont *)boldSmall boldTiny:(UIFont *)boldTiny __attribute__((swift_name("doCopy(regularTitle:regularBody:regularSmall:regularTiny:boldTitle:boldBody:boldSmall:boldTiny:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property UIFont *boldBody __attribute__((swift_name("boldBody")));
@property UIFont *boldSmall __attribute__((swift_name("boldSmall")));
@property UIFont *boldTiny __attribute__((swift_name("boldTiny")));
@property UIFont *boldTitle __attribute__((swift_name("boldTitle")));
@property UIFont *regularBody __attribute__((swift_name("regularBody")));
@property UIFont *regularSmall __attribute__((swift_name("regularSmall")));
@property UIFont *regularTiny __attribute__((swift_name("regularTiny")));
@property UIFont *regularTitle __attribute__((swift_name("regularTitle")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceSection")))
@interface USDKServiceSection : USDKBase
- (instancetype)initWithCategory:(NSString *)category type:(NSString *)type title:(NSString *)title description:(NSString * _Nullable)description content:(id)content __attribute__((swift_name("init(category:type:title:description:content:)"))) __attribute__((objc_designated_initializer));
- (USDKServiceSection *)doCopyCategory:(NSString *)category type:(NSString *)type title:(NSString *)title description:(NSString * _Nullable)description content:(id)content __attribute__((swift_name("doCopy(category:type:title:description:content:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *category __attribute__((swift_name("category")));
@property id content __attribute__((swift_name("content")));
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property NSString *title __attribute__((swift_name("title")));
@property NSString *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFGlobalState")))
@interface USDKTCFGlobalState : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFGlobalState.Companion")))
@interface USDKTCFGlobalStateCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKTCFData * _Nullable)getGlobalState __attribute__((swift_name("getGlobalState()")));
- (void)handleLanguageChangeNewData:(USDKViewData *)newData __attribute__((swift_name("handleLanguageChange(newData:)")));
- (void)initializeCategoriesCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("initializeCategories(categories:)")));
- (void)initializeControllerIdControllerId:(NSString *)controllerId __attribute__((swift_name("initializeControllerId(controllerId:)")));
- (void)initializeTcfDataTcfData:(USDKTCFData *)tcfData __attribute__((swift_name("initializeTcfData(tcfData:)")));
- (void)initializeUISettingsTcfuiSettings:(USDKTCFUISettings * _Nullable)tcfuiSettings __attribute__((swift_name("initializeUISettings(tcfuiSettings:)")));
@property NSArray<USDKCategory *> * _Nullable categories __attribute__((swift_name("categories")));
@property NSString * _Nullable controllerId __attribute__((swift_name("controllerId")));
@property USDKTCFData * _Nullable tcfData __attribute__((swift_name("tcfData")));
@property USDKTCFUISettings * _Nullable tcfUI __attribute__((swift_name("tcfUI")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Theme")))
@interface USDKTheme : USDKBase
- (instancetype)initWithBackgroundColor:(UIColor *)backgroundColor brandColor:(UIColor *)brandColor brandSecondaryColor:(UIColor *)brandSecondaryColor brandTertiaryColor:(UIColor *)brandTertiaryColor brandQuaternaryColor:(UIColor *)brandQuaternaryColor fontColor:(UIColor *)fontColor fontSecondaryColor:(UIColor *)fontSecondaryColor fontTertiaryColor:(UIColor *)fontTertiaryColor fontQuaternaryColor:(UIColor *)fontQuaternaryColor __attribute__((swift_name("init(backgroundColor:brandColor:brandSecondaryColor:brandTertiaryColor:brandQuaternaryColor:fontColor:fontSecondaryColor:fontTertiaryColor:fontQuaternaryColor:)"))) __attribute__((objc_designated_initializer));
- (USDKTheme *)doCopyBackgroundColor:(UIColor *)backgroundColor brandColor:(UIColor *)brandColor brandSecondaryColor:(UIColor *)brandSecondaryColor brandTertiaryColor:(UIColor *)brandTertiaryColor brandQuaternaryColor:(UIColor *)brandQuaternaryColor fontColor:(UIColor *)fontColor fontSecondaryColor:(UIColor *)fontSecondaryColor fontTertiaryColor:(UIColor *)fontTertiaryColor fontQuaternaryColor:(UIColor *)fontQuaternaryColor __attribute__((swift_name("doCopy(backgroundColor:brandColor:brandSecondaryColor:brandTertiaryColor:brandQuaternaryColor:fontColor:fontSecondaryColor:fontTertiaryColor:fontQuaternaryColor:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property UIColor *backgroundColor __attribute__((swift_name("backgroundColor")));
@property UIColor *brandColor __attribute__((swift_name("brandColor")));
@property UIColor *brandQuaternaryColor __attribute__((swift_name("brandQuaternaryColor")));
@property UIColor *brandSecondaryColor __attribute__((swift_name("brandSecondaryColor")));
@property UIColor *brandTertiaryColor __attribute__((swift_name("brandTertiaryColor")));
@property UIColor *fontColor __attribute__((swift_name("fontColor")));
@property UIColor *fontQuaternaryColor __attribute__((swift_name("fontQuaternaryColor")));
@property UIColor *fontSecondaryColor __attribute__((swift_name("fontSecondaryColor")));
@property UIColor *fontTertiaryColor __attribute__((swift_name("fontTertiaryColor")));
@end;

__attribute__((swift_name("ConsentInterface")))
@protocol USDKConsentInterface
@required
@property USDKConsentAction *action __attribute__((swift_name("action")));
@property BOOL status __attribute__((swift_name("status")));
@property USDKConsentType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Consent")))
@interface USDKConsent : USDKBase <USDKConsentInterface>
- (instancetype)initWithAction:(USDKConsentAction *)action status:(BOOL)status type:(USDKConsentType *)type __attribute__((swift_name("init(action:status:type:)"))) __attribute__((objc_designated_initializer));
- (USDKConsent *)doCopyAction:(USDKConsentAction *)action status:(BOOL)status type:(USDKConsentType *)type __attribute__((swift_name("doCopy(action:status:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKConsentAction *action __attribute__((swift_name("action")));
@property BOOL status __attribute__((swift_name("status")));
@property USDKConsentType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Consent.Companion")))
@interface USDKConsentCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentAction")))
@interface USDKConsentAction : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKConsentAction *acceptAllServices __attribute__((swift_name("acceptAllServices")));
@property (class, readonly) USDKConsentAction *denyAllServices __attribute__((swift_name("denyAllServices")));
@property (class, readonly) USDKConsentAction *essentialChange __attribute__((swift_name("essentialChange")));
@property (class, readonly) USDKConsentAction *initialPageLoad __attribute__((swift_name("initialPageLoad")));
@property (class, readonly) USDKConsentAction *nonEuRegion __attribute__((swift_name("nonEuRegion")));
@property (class, readonly) USDKConsentAction *sessionRestored __attribute__((swift_name("sessionRestored")));
@property (class, readonly) USDKConsentAction *updateServices __attribute__((swift_name("updateServices")));
@property (class, readonly) USDKConsentAction *tcfStringChange __attribute__((swift_name("tcfStringChange")));
@property (class, readonly) USDKConsentAction *noneAction __attribute__((swift_name("noneAction")));
- (int32_t)compareToOther:(USDKConsentAction *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentAction.Companion")))
@interface USDKConsentActionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKConsentAction *)fromS:(NSString *)s __attribute__((swift_name("from(s:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentHistory")))
@interface USDKConsentHistory : USDKBase <USDKConsentInterface>
- (instancetype)initWithAction:(USDKConsentAction *)action status:(BOOL)status type:(USDKConsentType *)type language:(NSString *)language timestampInSeconds:(double)timestampInSeconds versions:(USDKVersions *)versions __attribute__((swift_name("init(action:status:type:language:timestampInSeconds:versions:)"))) __attribute__((objc_designated_initializer));
- (USDKConsentHistory *)doCopyAction:(USDKConsentAction *)action status:(BOOL)status type:(USDKConsentType *)type language:(NSString *)language timestampInSeconds:(double)timestampInSeconds versions:(USDKVersions *)versions __attribute__((swift_name("doCopy(action:status:type:language:timestampInSeconds:versions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKConsentAction *action __attribute__((swift_name("action")));
@property (readonly) NSString *language __attribute__((swift_name("language")));
@property BOOL status __attribute__((swift_name("status")));
@property (readonly) double timestampInSeconds __attribute__((swift_name("timestampInSeconds")));
@property USDKConsentType *type __attribute__((swift_name("type")));
@property (readonly) USDKVersions *versions __attribute__((swift_name("versions")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentHistory.Companion")))
@interface USDKConsentHistoryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentType")))
@interface USDKConsentType : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKConsentType *explicit_ __attribute__((swift_name("explicit_")));
@property (class, readonly) USDKConsentType *implicit __attribute__((swift_name("implicit")));
@property (class, readonly) USDKConsentType *noneType __attribute__((swift_name("noneType")));
- (int32_t)compareToOther:(USDKConsentType *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentType.Companion")))
@interface USDKConsentTypeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKConsentType *)fromS:(NSString *)s __attribute__((swift_name("from(s:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObject")))
@interface USDKDataTransferObject : USDKBase
- (instancetype)initWithApplicationVersion:(NSString *)applicationVersion consent:(USDKDataTransferObjectConsent *)consent settings:(USDKDataTransferObjectSettings *)settings services:(NSArray<USDKDataTransferObjectService *> *)services timestampInSeconds:(double)timestampInSeconds __attribute__((swift_name("init(applicationVersion:consent:settings:services:timestampInSeconds:)"))) __attribute__((objc_designated_initializer));
- (USDKDataTransferObject *)doCopyApplicationVersion:(NSString *)applicationVersion consent:(USDKDataTransferObjectConsent *)consent settings:(USDKDataTransferObjectSettings *)settings services:(NSArray<USDKDataTransferObjectService *> *)services timestampInSeconds:(double)timestampInSeconds __attribute__((swift_name("doCopy(applicationVersion:consent:settings:services:timestampInSeconds:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *applicationVersion __attribute__((swift_name("applicationVersion")));
@property (readonly) USDKDataTransferObjectConsent *consent __attribute__((swift_name("consent")));
@property (readonly) NSArray<USDKDataTransferObjectService *> *services __attribute__((swift_name("services")));
@property (readonly) USDKDataTransferObjectSettings *settings __attribute__((swift_name("settings")));
@property (readonly) double timestampInSeconds __attribute__((swift_name("timestampInSeconds")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObject.Companion")))
@interface USDKDataTransferObjectCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("DataTransferObjectConsent")))
@interface USDKDataTransferObjectConsent : USDKBase
- (instancetype)initWithAction:(USDKConsentAction *)action type:(USDKConsentType *)type __attribute__((swift_name("init(action:type:)"))) __attribute__((objc_designated_initializer));
@property (readonly) USDKConsentAction *action __attribute__((swift_name("action")));
@property (readonly) USDKConsentType *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectConsent.Companion")))
@interface USDKDataTransferObjectConsentCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectService")))
@interface USDKDataTransferObjectService : USDKBase
- (instancetype)initWithId:(NSString *)id name:(NSString *)name status:(BOOL)status version:(NSString *)version processorId:(NSString *)processorId __attribute__((swift_name("init(id:name:status:version:processorId:)"))) __attribute__((objc_designated_initializer));
- (USDKDataTransferObjectService *)doCopyId:(NSString *)id name:(NSString *)name status:(BOOL)status version:(NSString *)version processorId:(NSString *)processorId __attribute__((swift_name("doCopy(id:name:status:version:processorId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectService.Companion")))
@interface USDKDataTransferObjectServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectSettings")))
@interface USDKDataTransferObjectSettings : USDKBase
- (instancetype)initWithId:(NSString *)id controllerId:(NSString *)controllerId referrerControllerId:(NSString *)referrerControllerId language:(NSString *)language version:(NSString *)version __attribute__((swift_name("init(id:controllerId:referrerControllerId:language:version:)"))) __attribute__((objc_designated_initializer));
- (USDKDataTransferObjectSettings *)doCopyId:(NSString *)id controllerId:(NSString *)controllerId referrerControllerId:(NSString *)referrerControllerId language:(NSString *)language version:(NSString *)version __attribute__((swift_name("doCopy(id:controllerId:referrerControllerId:language:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *language __attribute__((swift_name("language")));
@property (readonly) NSString *referrerControllerId __attribute__((swift_name("referrerControllerId")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectSettings.Companion")))
@interface USDKDataTransferObjectSettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EssentialServices")))
@interface USDKEssentialServices : USDKBase
- (instancetype)initWithMergedEssentialServices:(NSArray<USDKService *> *)mergedEssentialServices updatedEssentialServices:(NSArray<USDKService *> *)updatedEssentialServices __attribute__((swift_name("init(mergedEssentialServices:updatedEssentialServices:)"))) __attribute__((objc_designated_initializer));
- (USDKEssentialServices *)doCopyMergedEssentialServices:(NSArray<USDKService *> *)mergedEssentialServices updatedEssentialServices:(NSArray<USDKService *> *)updatedEssentialServices __attribute__((swift_name("doCopy(mergedEssentialServices:updatedEssentialServices:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKService *> *mergedEssentialServices __attribute__((swift_name("mergedEssentialServices")));
@property (readonly) NSArray<USDKService *> *updatedEssentialServices __attribute__((swift_name("updatedEssentialServices")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MergedServicesSettings")))
@interface USDKMergedServicesSettings : USDKBase
- (instancetype)initWithMergedServices:(NSArray<USDKService *> *)mergedServices mergedSettings:(USDKExtendedSettings *)mergedSettings updatedEssentialServices:(NSArray<USDKService *> *)updatedEssentialServices __attribute__((swift_name("init(mergedServices:mergedSettings:updatedEssentialServices:)"))) __attribute__((objc_designated_initializer));
- (USDKMergedServicesSettings *)doCopyMergedServices:(NSArray<USDKService *> *)mergedServices mergedSettings:(USDKExtendedSettings *)mergedSettings updatedEssentialServices:(NSArray<USDKService *> *)updatedEssentialServices __attribute__((swift_name("doCopy(mergedServices:mergedSettings:updatedEssentialServices:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKService *> *mergedServices __attribute__((swift_name("mergedServices")));
@property (readonly) USDKExtendedSettings *mergedSettings __attribute__((swift_name("mergedSettings")));
@property (readonly) NSArray<USDKService *> *updatedEssentialServices __attribute__((swift_name("updatedEssentialServices")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Versions")))
@interface USDKVersions : USDKBase
- (instancetype)initWithApplication:(NSString *)application service:(NSString *)service settings:(NSString *)settings __attribute__((swift_name("init(application:service:settings:)"))) __attribute__((objc_designated_initializer));
- (USDKVersions *)doCopyApplication:(NSString *)application service:(NSString *)service settings:(NSString *)settings __attribute__((swift_name("doCopy(application:service:settings:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *application __attribute__((swift_name("application")));
@property (readonly) NSString *service __attribute__((swift_name("service")));
@property (readonly) NSString *settings __attribute__((swift_name("settings")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Versions.Companion")))
@interface USDKVersionsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CacheEntry")))
@interface USDKCacheEntry : USDKBase
- (instancetype)initWithContent:(NSString *)content timestampInMillis:(int64_t)timestampInMillis __attribute__((swift_name("init(content:timestampInMillis:)"))) __attribute__((objc_designated_initializer));
- (USDKCacheEntry *)doCopyContent:(NSString *)content timestampInMillis:(int64_t)timestampInMillis __attribute__((swift_name("doCopy(content:timestampInMillis:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isValidMaxDurationInMillis:(int64_t)maxDurationInMillis __attribute__((swift_name("isValid(maxDurationInMillis:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) int64_t timestampInMillis __attribute__((swift_name("timestampInMillis")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CacheEntry.Companion")))
@interface USDKCacheEntryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageCCPA")))
@interface USDKStorageCCPA : USDKBase
- (instancetype)initWithTimestampInMillis:(double)timestampInMillis __attribute__((swift_name("init(timestampInMillis:)"))) __attribute__((objc_designated_initializer));
- (USDKStorageCCPA *)doCopyTimestampInMillis:(double)timestampInMillis __attribute__((swift_name("doCopy(timestampInMillis:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) double timestampInMillis __attribute__((swift_name("timestampInMillis")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageCCPA.Companion")))
@interface USDKStorageCCPACompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageCache")))
@interface USDKStorageCache : USDKBase
- (instancetype)initWithMap:(NSDictionary<NSString *, USDKCacheEntry *> *)map __attribute__((swift_name("init(map:)"))) __attribute__((objc_designated_initializer));
- (USDKStorageCache *)doCopyMap:(NSDictionary<NSString *, USDKCacheEntry *> *)map __attribute__((swift_name("doCopy(map:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSDictionary<NSString *, USDKCacheEntry *> *map __attribute__((swift_name("map")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageCache.Companion")))
@interface USDKStorageCacheCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageKeys")))
@interface USDKStorageKeys : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKStorageKeys *settings __attribute__((swift_name("settings")));
@property (class, readonly) USDKStorageKeys *userInteraction __attribute__((swift_name("userInteraction")));
@property (class, readonly) USDKStorageKeys *consentsBuffer __attribute__((swift_name("consentsBuffer")));
@property (class, readonly) USDKStorageKeys *tcf __attribute__((swift_name("tcf")));
@property (class, readonly) USDKStorageKeys *ccpa __attribute__((swift_name("ccpa")));
@property (class, readonly) USDKStorageKeys *cmpId __attribute__((swift_name("cmpId")));
@property (class, readonly) USDKStorageKeys *cacheKey __attribute__((swift_name("cacheKey")));
- (int32_t)compareToOther:(USDKStorageKeys *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageService")))
@interface USDKStorageService : USDKBase
- (instancetype)initWithHistory:(NSArray<USDKConsentHistory *> *)history id:(NSString *)id processorId:(NSString *)processorId status:(BOOL)status __attribute__((swift_name("init(history:id:processorId:status:)"))) __attribute__((objc_designated_initializer));
- (USDKStorageService *)doCopyHistory:(NSArray<USDKConsentHistory *> *)history id:(NSString *)id processorId:(NSString *)processorId status:(BOOL)status __attribute__((swift_name("doCopy(history:id:processorId:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKConsentHistory *> *history __attribute__((swift_name("history")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageService.Companion")))
@interface USDKStorageServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageSettings")))
@interface USDKStorageSettings : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId id:(NSString *)id language:(NSString *)language services:(NSArray<USDKStorageService *> *)services version:(NSString *)version __attribute__((swift_name("init(controllerId:id:language:services:version:)"))) __attribute__((objc_designated_initializer));
- (USDKStorageSettings *)doCopyControllerId:(NSString *)controllerId id:(NSString *)id language:(NSString *)language services:(NSArray<USDKStorageService *> *)services version:(NSString *)version __attribute__((swift_name("doCopy(controllerId:id:language:services:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isObjectWithValues __attribute__((swift_name("isObjectWithValues()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *language __attribute__((swift_name("language")));
@property (readonly) NSArray<USDKStorageService *> *services __attribute__((swift_name("services")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageSettings.Companion")))
@interface USDKStorageSettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageTCF")))
@interface USDKStorageTCF : USDKBase
- (instancetype)initWithVendorsDisclosed:(NSArray<USDKInt *> *)vendorsDisclosed tcString:(NSString *)tcString __attribute__((swift_name("init(vendorsDisclosed:tcString:)"))) __attribute__((objc_designated_initializer));
- (USDKStorageTCF *)doCopyVendorsDisclosed:(NSArray<USDKInt *> *)vendorsDisclosed tcString:(NSString *)tcString __attribute__((swift_name("doCopy(vendorsDisclosed:tcString:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *tcString __attribute__((swift_name("tcString")));
@property (readonly) NSArray<USDKInt *> *vendorsDisclosed __attribute__((swift_name("vendorsDisclosed")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageTCF.Companion")))
@interface USDKStorageTCFCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants")))
@interface USDKConstants : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (readonly) NSString *CALIFORNIA_REGION_CODE __attribute__((swift_name("CALIFORNIA_REGION_CODE")));
@property (readonly) USDKKotlinArray *EU_COUNTRIES __attribute__((swift_name("EU_COUNTRIES")));
@property (readonly) NSString *US_COUNTRY_CODE __attribute__((swift_name("US_COUNTRY_CODE")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CountryData")))
@interface USDKCountryData : USDKBase
- (instancetype)initWithCountryCode:(NSString *)countryCode countryName:(NSString *)countryName regionCode:(NSString *)regionCode __attribute__((swift_name("init(countryCode:countryName:regionCode:)"))) __attribute__((objc_designated_initializer));
- (USDKCountryData *)doCopyCountryCode:(NSString *)countryCode countryName:(NSString *)countryName regionCode:(NSString *)regionCode __attribute__((swift_name("doCopy(countryCode:countryName:regionCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *countryCode __attribute__((swift_name("countryCode")));
@property (readonly) NSString *countryName __attribute__((swift_name("countryName")));
@property (readonly) NSString *regionCode __attribute__((swift_name("regionCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CountryData.Companion")))
@interface USDKCountryDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LocationData")))
@interface USDKLocationData : USDKBase
- (instancetype)initWithClientLocation:(USDKCountryData *)clientLocation __attribute__((swift_name("init(clientLocation:)"))) __attribute__((objc_designated_initializer));
- (USDKLocationData *)doCopyClientLocation:(USDKCountryData *)clientLocation __attribute__((swift_name("doCopy(clientLocation:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKCountryData *clientLocation __attribute__((swift_name("clientLocation")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LocationData.Companion")))
@interface USDKLocationDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserCountry")))
@interface USDKUserCountry : USDKBase
- (instancetype)initWithCode:(NSString *)code name:(NSString *)name regionCode:(NSString *)regionCode __attribute__((swift_name("init(code:name:regionCode:)"))) __attribute__((objc_designated_initializer));
- (USDKUserCountry *)doCopyCode:(NSString *)code name:(NSString *)name regionCode:(NSString *)regionCode __attribute__((swift_name("doCopy(code:name:regionCode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isObjectWithValues __attribute__((swift_name("isObjectWithValues()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *code __attribute__((swift_name("code")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *regionCode __attribute__((swift_name("regionCode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserCountry.Companion")))
@interface USDKUserCountryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BaseService")))
@interface USDKBaseService : USDKBase
- (instancetype)initWithDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description id:(NSString *)id language:(USDKLanguage *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version __attribute__((swift_name("init(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description baseServiceDescription:(NSString *)baseServiceDescription id:(NSString *)id language:(USDKLanguage *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version __attribute__((swift_name("init(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:baseServiceDescription:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:)"))) __attribute__((objc_designated_initializer));
- (USDKBaseService *)doCopyDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description baseServiceDescription:(NSString *)baseServiceDescription id:(NSString *)id language:(USDKLanguage *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version __attribute__((swift_name("doCopy(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:baseServiceDescription:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *baseServiceDescription __attribute__((swift_name("baseServiceDescription")));
@property NSArray<NSString *> *dataCollected __attribute__((swift_name("dataCollected")));
@property USDKDataDistribution *dataDistribution __attribute__((swift_name("dataDistribution")));
@property NSArray<NSString *> *dataPurposes __attribute__((swift_name("dataPurposes")));
@property NSArray<NSString *> *dataRecipients __attribute__((swift_name("dataRecipients")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property NSString *id __attribute__((swift_name("id")));
@property USDKLanguage *language __attribute__((swift_name("language")));
@property NSArray<NSString *> *legalBasis __attribute__((swift_name("legalBasis")));
@property NSString *name __attribute__((swift_name("name")));
@property USDKProcessingCompany *processingCompany __attribute__((swift_name("processingCompany")));
@property NSString *retentionPeriodDescription __attribute__((swift_name("retentionPeriodDescription")));
@property NSArray<NSString *> *technologiesUsed __attribute__((swift_name("technologiesUsed")));
@property USDKURLs *urls __attribute__((swift_name("urls")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BaseService.Companion")))
@interface USDKBaseServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Button")))
@interface USDKButton : USDKBase
- (instancetype)initWithIsEnabled:(BOOL)isEnabled label:(NSString *)label url:(NSString * _Nullable)url __attribute__((swift_name("init(isEnabled:label:url:)"))) __attribute__((objc_designated_initializer));
- (USDKButton *)doCopyIsEnabled:(BOOL)isEnabled label:(NSString *)label url:(NSString * _Nullable)url __attribute__((swift_name("doCopy(isEnabled:label:url:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isEnabled __attribute__((swift_name("isEnabled")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@property (readonly) NSString * _Nullable url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Button.Companion")))
@interface USDKButtonCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Category")))
@interface USDKCategory : USDKBase
- (instancetype)initWithDescription:(NSString *)description isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label services:(NSArray<USDKService *> *)services slug:(NSString *)slug __attribute__((swift_name("init(description:isEssential:isHidden:label:services:slug:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDescription:(NSString *)description categoryDescription:(NSString *)categoryDescription isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label services:(NSArray<USDKService *> *)services slug:(NSString *)slug __attribute__((swift_name("init(description:categoryDescription:isEssential:isHidden:label:services:slug:)"))) __attribute__((objc_designated_initializer));
- (USDKCategory *)doCopyDescription:(NSString *)description categoryDescription:(NSString *)categoryDescription isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label services:(NSArray<USDKService *> *)services slug:(NSString *)slug __attribute__((swift_name("doCopy(description:categoryDescription:isEssential:isHidden:label:services:slug:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *categoryDescription __attribute__((swift_name("categoryDescription")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property BOOL isEssential __attribute__((swift_name("isEssential")));
@property BOOL isHidden __attribute__((swift_name("isHidden")));
@property NSString *label __attribute__((swift_name("label")));
@property NSArray<USDKService *> *services __attribute__((swift_name("services")));
@property NSString *slug __attribute__((swift_name("slug")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Category.Companion")))
@interface USDKCategoryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Consent_")))
@interface USDKConsent_ : USDKBase
- (instancetype)initWithHistory:(NSArray<USDKConsentHistory *> *)history status:(BOOL)status __attribute__((swift_name("init(history:status:)"))) __attribute__((objc_designated_initializer));
- (USDKConsent_ *)doCopyHistory:(NSArray<USDKConsentHistory *> *)history status:(BOOL)status __attribute__((swift_name("doCopy(history:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKConsentHistory *> *history __attribute__((swift_name("history")));
@property BOOL status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Consent_.Companion")))
@interface USDKConsent_Companion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentStatus")))
@interface USDKConsentStatus : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKConsentStatus *false_ __attribute__((swift_name("false_")));
@property (class, readonly) USDKConsentStatus *true_ __attribute__((swift_name("true_")));
- (int32_t)compareToOther:(USDKConsentStatus *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants_")))
@interface USDKConstants_ : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (readonly) NSString *CATEGORY_NONE __attribute__((swift_name("CATEGORY_NONE")));
@property (readonly) NSString *FIRST_LAYER_SERVICE_ID __attribute__((swift_name("FIRST_LAYER_SERVICE_ID")));
@property (readonly) NSString *USERCENTRICS_URL __attribute__((swift_name("USERCENTRICS_URL")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Customization")))
@interface USDKCustomization : USDKBase
- (instancetype)initWithColor:(USDKCustomizationColor *)color font:(USDKCustomizationFont *)font theme:(int32_t)theme logoUrl:(NSString * _Nullable)logoUrl __attribute__((swift_name("init(color:font:theme:logoUrl:)"))) __attribute__((objc_designated_initializer));
- (USDKCustomization *)doCopyColor:(USDKCustomizationColor *)color font:(USDKCustomizationFont *)font theme:(int32_t)theme logoUrl:(NSString * _Nullable)logoUrl __attribute__((swift_name("doCopy(color:font:theme:logoUrl:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKCustomizationColor *color __attribute__((swift_name("color")));
@property (readonly) USDKCustomizationFont *font __attribute__((swift_name("font")));
@property (readonly) NSString * _Nullable logoUrl __attribute__((swift_name("logoUrl")));
@property (readonly) int32_t theme __attribute__((swift_name("theme")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Customization.Companion")))
@interface USDKCustomizationCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CustomizationColor")))
@interface USDKCustomizationColor : USDKBase
- (instancetype)initWithPrimary:(NSString *)primary __attribute__((swift_name("init(primary:)"))) __attribute__((objc_designated_initializer));
- (USDKCustomizationColor *)doCopyPrimary:(NSString *)primary __attribute__((swift_name("doCopy(primary:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *primary __attribute__((swift_name("primary")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CustomizationColor.Companion")))
@interface USDKCustomizationColorCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CustomizationFont")))
@interface USDKCustomizationFont : USDKBase
- (instancetype)initWithFamily:(NSString *)family size:(int32_t)size __attribute__((swift_name("init(family:size:)"))) __attribute__((objc_designated_initializer));
- (USDKCustomizationFont *)doCopyFamily:(NSString *)family size:(int32_t)size __attribute__((swift_name("doCopy(family:size:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *family __attribute__((swift_name("family")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CustomizationFont.Companion")))
@interface USDKCustomizationFontCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataDistribution")))
@interface USDKDataDistribution : USDKBase
- (instancetype)initWithProcessingLocation:(NSString *)processingLocation thirdPartyCountries:(NSString *)thirdPartyCountries __attribute__((swift_name("init(processingLocation:thirdPartyCountries:)"))) __attribute__((objc_designated_initializer));
- (USDKDataDistribution *)doCopyProcessingLocation:(NSString *)processingLocation thirdPartyCountries:(NSString *)thirdPartyCountries __attribute__((swift_name("doCopy(processingLocation:thirdPartyCountries:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *processingLocation __attribute__((swift_name("processingLocation")));
@property NSString *thirdPartyCountries __attribute__((swift_name("thirdPartyCountries")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataDistribution.Companion")))
@interface USDKDataDistributionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataDistributionTitle")))
@interface USDKDataDistributionTitle : USDKBase
- (instancetype)initWithProcessingLocationTitle:(NSString *)processingLocationTitle thirdPartyCountriesTitle:(NSString *)thirdPartyCountriesTitle __attribute__((swift_name("init(processingLocationTitle:thirdPartyCountriesTitle:)"))) __attribute__((objc_designated_initializer));
- (USDKDataDistributionTitle *)doCopyProcessingLocationTitle:(NSString *)processingLocationTitle thirdPartyCountriesTitle:(NSString *)thirdPartyCountriesTitle __attribute__((swift_name("doCopy(processingLocationTitle:thirdPartyCountriesTitle:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *processingLocationTitle __attribute__((swift_name("processingLocationTitle")));
@property (readonly) NSString *thirdPartyCountriesTitle __attribute__((swift_name("thirdPartyCountriesTitle")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataDistributionTitle.Companion")))
@interface USDKDataDistributionTitleCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataExchangeSetting")))
@interface USDKDataExchangeSetting : USDKBase
- (instancetype)initWithNames:(NSArray<NSString *> *)names type:(int32_t)type __attribute__((swift_name("init(names:type:)"))) __attribute__((objc_designated_initializer));
- (USDKDataExchangeSetting *)doCopyNames:(NSArray<NSString *> *)names type:(int32_t)type __attribute__((swift_name("doCopy(names:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<NSString *> *names __attribute__((swift_name("names")));
@property int32_t type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataExchangeType")))
@interface USDKDataExchangeType : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKDataExchangeType *dataLayer __attribute__((swift_name("dataLayer")));
@property (class, readonly) USDKDataExchangeType *windowEvent __attribute__((swift_name("windowEvent")));
- (int32_t)compareToOther:(USDKDataExchangeType *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DescriptionTitle")))
@interface USDKDescriptionTitle : USDKBase
- (instancetype)initWithDescription:(NSString *)description title:(NSString *)title __attribute__((swift_name("init(description:title:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDescription:(NSString *)description descriptionTitleDescription:(NSString *)descriptionTitleDescription title:(NSString *)title __attribute__((swift_name("init(description:descriptionTitleDescription:title:)"))) __attribute__((objc_designated_initializer));
- (USDKDescriptionTitle *)doCopyDescription:(NSString *)description descriptionTitleDescription:(NSString *)descriptionTitleDescription title:(NSString *)title __attribute__((swift_name("doCopy(description:descriptionTitleDescription:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *descriptionTitleDescription __attribute__((swift_name("descriptionTitleDescription")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DescriptionTitle.Companion")))
@interface USDKDescriptionTitleCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ExtendedSettings")))
@interface USDKExtendedSettings : USDKBase
- (instancetype)initWithAcceptAllImplicitlyOutsideEU:(BOOL)acceptAllImplicitlyOutsideEU categories:(NSArray<USDKCategory *> *)categories ccpa:(USDKCCPAOptions * _Nullable)ccpa ccpaui:(USDKCCPAUISettings * _Nullable)ccpaui controllerId:(NSString *)controllerId dataExchangeSettings:(NSArray<USDKDataExchangeSetting *> *)dataExchangeSettings id:(NSString *)id isTcfEnabled:(BOOL)isTcfEnabled showFirstLayerOnVersionChange:(NSArray<USDKInt *> *)showFirstLayerOnVersionChange tcf:(USDKTCFOptions * _Nullable)tcf tcfui:(USDKTCFUISettings * _Nullable)tcfui ui:(USDKUISettings * _Nullable)ui version:(NSString *)version __attribute__((swift_name("init(acceptAllImplicitlyOutsideEU:categories:ccpa:ccpaui:controllerId:dataExchangeSettings:id:isTcfEnabled:showFirstLayerOnVersionChange:tcf:tcfui:ui:version:)"))) __attribute__((objc_designated_initializer));
- (USDKExtendedSettings *)doCopyAcceptAllImplicitlyOutsideEU:(BOOL)acceptAllImplicitlyOutsideEU categories:(NSArray<USDKCategory *> *)categories ccpa:(USDKCCPAOptions * _Nullable)ccpa ccpaui:(USDKCCPAUISettings * _Nullable)ccpaui controllerId:(NSString *)controllerId dataExchangeSettings:(NSArray<USDKDataExchangeSetting *> *)dataExchangeSettings id:(NSString *)id isTcfEnabled:(BOOL)isTcfEnabled showFirstLayerOnVersionChange:(NSArray<USDKInt *> *)showFirstLayerOnVersionChange tcf:(USDKTCFOptions * _Nullable)tcf tcfui:(USDKTCFUISettings * _Nullable)tcfui ui:(USDKUISettings * _Nullable)ui version:(NSString *)version __attribute__((swift_name("doCopy(acceptAllImplicitlyOutsideEU:categories:ccpa:ccpaui:controllerId:dataExchangeSettings:id:isTcfEnabled:showFirstLayerOnVersionChange:tcf:tcfui:ui:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL acceptAllImplicitlyOutsideEU __attribute__((swift_name("acceptAllImplicitlyOutsideEU")));
@property NSArray<USDKCategory *> *categories __attribute__((swift_name("categories")));
@property USDKCCPAOptions * _Nullable ccpa __attribute__((swift_name("ccpa")));
@property USDKCCPAUISettings * _Nullable ccpaui __attribute__((swift_name("ccpaui")));
@property NSString *controllerId __attribute__((swift_name("controllerId")));
@property NSArray<USDKDataExchangeSetting *> *dataExchangeSettings __attribute__((swift_name("dataExchangeSettings")));
@property NSString *id __attribute__((swift_name("id")));
@property BOOL isTcfEnabled __attribute__((swift_name("isTcfEnabled")));
@property NSArray<USDKInt *> *showFirstLayerOnVersionChange __attribute__((swift_name("showFirstLayerOnVersionChange")));
@property USDKTCFOptions * _Nullable tcf __attribute__((swift_name("tcf")));
@property USDKTCFUISettings * _Nullable tcfui __attribute__((swift_name("tcfui")));
@property USDKUISettings * _Nullable ui __attribute__((swift_name("ui")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GeneralLabels")))
@interface USDKGeneralLabels : USDKBase
- (instancetype)initWithConsentGiven:(NSString *)consentGiven consentNotGiven:(NSString *)consentNotGiven consentType:(NSString *)consentType controllerId:(NSString *)controllerId copy:(NSString *)copy date:(NSString *)date decision:(NSString *)decision explicit:(NSString *)explicit_ implicit:(NSString *)implicit processorId:(NSString *)processorId showMore:(NSString *)showMore readMore:(NSString *)readMore __attribute__((swift_name("init(consentGiven:consentNotGiven:consentType:controllerId:copy:date:decision:explicit:implicit:processorId:showMore:readMore:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithConsentGiven:(NSString *)consentGiven consentNotGiven:(NSString *)consentNotGiven consentType:(NSString *)consentType controllerId:(NSString *)controllerId copy:(NSString *)copy date:(NSString *)date decision:(NSString *)decision explicit:(NSString *)explicit_ explicitLabel:(NSString *)explicitLabel implicit:(NSString *)implicit implicitLabel:(NSString *)implicitLabel processorId:(NSString *)processorId showMore:(NSString *)showMore readMore:(NSString *)readMore __attribute__((swift_name("init(consentGiven:consentNotGiven:consentType:controllerId:copy:date:decision:explicit:explicitLabel:implicit:implicitLabel:processorId:showMore:readMore:)"))) __attribute__((objc_designated_initializer));
- (USDKGeneralLabels *)doCopyConsentGiven:(NSString *)consentGiven consentNotGiven:(NSString *)consentNotGiven consentType:(NSString *)consentType controllerId:(NSString *)controllerId copy:(NSString *)copy date:(NSString *)date decision:(NSString *)decision explicit:(NSString *)explicit_ explicitLabel:(NSString *)explicitLabel implicit:(NSString *)implicit implicitLabel:(NSString *)implicitLabel processorId:(NSString *)processorId showMore:(NSString *)showMore readMore:(NSString *)readMore __attribute__((swift_name("doCopy(consentGiven:consentNotGiven:consentType:controllerId:copy:date:decision:explicit:explicitLabel:implicit:implicitLabel:processorId:showMore:readMore:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *consentGiven __attribute__((swift_name("consentGiven")));
@property (readonly) NSString *consentNotGiven __attribute__((swift_name("consentNotGiven")));
@property (readonly) NSString *consentType __attribute__((swift_name("consentType")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly, getter=doCopy) NSString *copy __attribute__((swift_name("copy")));
@property (readonly) NSString *date __attribute__((swift_name("date")));
@property (readonly) NSString *decision __attribute__((swift_name("decision")));
@property (readonly, getter=explicit) NSString *explicit_ __attribute__((swift_name("explicit_")));
@property (readonly) NSString *explicitLabel __attribute__((swift_name("explicitLabel")));
@property (readonly) NSString *implicit __attribute__((swift_name("implicit")));
@property (readonly) NSString *implicitLabel __attribute__((swift_name("implicitLabel")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) NSString *readMore __attribute__((swift_name("readMore")));
@property (readonly) NSString *showMore __attribute__((swift_name("showMore")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GeneralLabels.Companion")))
@interface USDKGeneralLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Label")))
@interface USDKLabel : USDKBase
- (instancetype)initWithLabel:(NSString *)label __attribute__((swift_name("init(label:)"))) __attribute__((objc_designated_initializer));
- (USDKLabel *)doCopyLabel:(NSString *)label __attribute__((swift_name("doCopy(label:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Label.Companion")))
@interface USDKLabelCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Language")))
@interface USDKLanguage : USDKBase
- (instancetype)initWithAvailable:(NSArray<NSString *> *)available isSelectorEnabled:(USDKBoolean * _Nullable)isSelectorEnabled selected:(NSString *)selected __attribute__((swift_name("init(available:isSelectorEnabled:selected:)"))) __attribute__((objc_designated_initializer));
- (USDKLanguage *)doCopyAvailable:(NSArray<NSString *> *)available isSelectorEnabled:(USDKBoolean * _Nullable)isSelectorEnabled selected:(NSString *)selected __attribute__((swift_name("doCopy(available:isSelectorEnabled:selected:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<NSString *> *available __attribute__((swift_name("available")));
@property USDKBoolean * _Nullable isSelectorEnabled __attribute__((swift_name("isSelectorEnabled")));
@property NSString *selected __attribute__((swift_name("selected")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Language.Companion")))
@interface USDKLanguageCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Link")))
@interface USDKLink : USDKBase
- (instancetype)initWithLabel:(NSString *)label url:(NSString * _Nullable)url __attribute__((swift_name("init(label:url:)"))) __attribute__((objc_designated_initializer));
- (USDKLink *)doCopyLabel:(NSString *)label url:(NSString * _Nullable)url __attribute__((swift_name("doCopy(label:url:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@property (readonly) NSString * _Nullable url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Link.Companion")))
@interface USDKLinkCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Links")))
@interface USDKLinks : USDKBase
- (instancetype)initWithCookiePolicy:(USDKLink *)cookiePolicy imprint:(USDKLink *)imprint privacyPolicy:(USDKLink *)privacyPolicy __attribute__((swift_name("init(cookiePolicy:imprint:privacyPolicy:)"))) __attribute__((objc_designated_initializer));
- (USDKLinks *)doCopyCookiePolicy:(USDKLink *)cookiePolicy imprint:(USDKLink *)imprint privacyPolicy:(USDKLink *)privacyPolicy __attribute__((swift_name("doCopy(cookiePolicy:imprint:privacyPolicy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKLink *cookiePolicy __attribute__((swift_name("cookiePolicy")));
@property (readonly) USDKLink *imprint __attribute__((swift_name("imprint")));
@property (readonly) USDKLink *privacyPolicy __attribute__((swift_name("privacyPolicy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Links.Companion")))
@interface USDKLinksCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PoweredBy")))
@interface USDKPoweredBy : USDKBase
- (instancetype)initWithIsEnabled:(BOOL)isEnabled label:(NSString *)label partnerUrl:(NSString * _Nullable)partnerUrl partnerUrlLabel:(NSString * _Nullable)partnerUrlLabel url:(NSString *)url urlLabel:(NSString *)urlLabel __attribute__((swift_name("init(isEnabled:label:partnerUrl:partnerUrlLabel:url:urlLabel:)"))) __attribute__((objc_designated_initializer));
- (USDKPoweredBy *)doCopyIsEnabled:(BOOL)isEnabled label:(NSString *)label partnerUrl:(NSString * _Nullable)partnerUrl partnerUrlLabel:(NSString * _Nullable)partnerUrlLabel url:(NSString *)url urlLabel:(NSString *)urlLabel __attribute__((swift_name("doCopy(isEnabled:label:partnerUrl:partnerUrlLabel:url:urlLabel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isEnabled __attribute__((swift_name("isEnabled")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@property (readonly) NSString * _Nullable partnerUrl __attribute__((swift_name("partnerUrl")));
@property (readonly) NSString * _Nullable partnerUrlLabel __attribute__((swift_name("partnerUrlLabel")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *urlLabel __attribute__((swift_name("urlLabel")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PoweredBy.Companion")))
@interface USDKPoweredByCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProcessingCompany")))
@interface USDKProcessingCompany : USDKBase
- (instancetype)initWithAddress:(NSString *)address dataProtectionOfficer:(NSString *)dataProtectionOfficer name:(NSString *)name __attribute__((swift_name("init(address:dataProtectionOfficer:name:)"))) __attribute__((objc_designated_initializer));
- (USDKProcessingCompany *)doCopyAddress:(NSString *)address dataProtectionOfficer:(NSString *)dataProtectionOfficer name:(NSString *)name __attribute__((swift_name("doCopy(address:dataProtectionOfficer:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *address __attribute__((swift_name("address")));
@property NSString *dataProtectionOfficer __attribute__((swift_name("dataProtectionOfficer")));
@property NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProcessingCompany.Companion")))
@interface USDKProcessingCompanyCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Service")))
@interface USDKService : USDKBase
- (instancetype)initWithDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description id:(NSString *)id language:(USDKLanguage *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version categorySlug:(NSString *)categorySlug consent:(USDKConsent_ *)consent isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden processorId:(NSString *)processorId subServices:(NSArray<USDKBaseService *> *)subServices __attribute__((swift_name("init(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:categorySlug:consent:isEssential:isHidden:processorId:subServices:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description serviceDescription:(NSString *)serviceDescription id:(NSString *)id language:(USDKLanguage *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version categorySlug:(NSString *)categorySlug consent:(USDKConsent_ *)consent isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden processorId:(NSString *)processorId subServices:(NSArray<USDKBaseService *> *)subServices __attribute__((swift_name("init(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:serviceDescription:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:categorySlug:consent:isEssential:isHidden:processorId:subServices:)"))) __attribute__((objc_designated_initializer));
- (USDKService *)doCopyDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description serviceDescription:(NSString *)serviceDescription id:(NSString *)id language:(USDKLanguage *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version categorySlug:(NSString *)categorySlug consent:(USDKConsent_ *)consent isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden processorId:(NSString *)processorId subServices:(NSArray<USDKBaseService *> *)subServices __attribute__((swift_name("doCopy(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:serviceDescription:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:categorySlug:consent:isEssential:isHidden:processorId:subServices:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *categorySlug __attribute__((swift_name("categorySlug")));
@property USDKConsent_ *consent __attribute__((swift_name("consent")));
@property NSArray<NSString *> *dataCollected __attribute__((swift_name("dataCollected")));
@property USDKDataDistribution *dataDistribution __attribute__((swift_name("dataDistribution")));
@property NSArray<NSString *> *dataPurposes __attribute__((swift_name("dataPurposes")));
@property NSArray<NSString *> *dataRecipients __attribute__((swift_name("dataRecipients")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property NSString *id __attribute__((swift_name("id")));
@property BOOL isEssential __attribute__((swift_name("isEssential")));
@property BOOL isHidden __attribute__((swift_name("isHidden")));
@property USDKLanguage *language __attribute__((swift_name("language")));
@property NSArray<NSString *> *legalBasis __attribute__((swift_name("legalBasis")));
@property NSString *name __attribute__((swift_name("name")));
@property USDKProcessingCompany *processingCompany __attribute__((swift_name("processingCompany")));
@property NSString *processorId __attribute__((swift_name("processorId")));
@property NSString *retentionPeriodDescription __attribute__((swift_name("retentionPeriodDescription")));
@property NSString *serviceDescription __attribute__((swift_name("serviceDescription")));
@property NSArray<USDKBaseService *> *subServices __attribute__((swift_name("subServices")));
@property NSArray<NSString *> *technologiesUsed __attribute__((swift_name("technologiesUsed")));
@property USDKURLs *urls __attribute__((swift_name("urls")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Service.Companion")))
@interface USDKServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceLabels")))
@interface USDKServiceLabels : USDKBase
- (instancetype)initWithDataCollected:(USDKDescriptionTitle *)dataCollected dataDistribution:(USDKDataDistributionTitle *)dataDistribution dataPurposes:(USDKDescriptionTitle *)dataPurposes dataRecipientsTitle:(NSString *)dataRecipientsTitle descriptionTitle:(NSString *)descriptionTitle history:(USDKDescriptionTitle *)history legalBasis:(USDKDescriptionTitle *)legalBasis processingCompanyTitle:(NSString *)processingCompanyTitle retentionPeriodTitle:(NSString *)retentionPeriodTitle technologiesUsed:(USDKDescriptionTitle *)technologiesUsed urls:(USDKURLsTitle *)urls __attribute__((swift_name("init(dataCollected:dataDistribution:dataPurposes:dataRecipientsTitle:descriptionTitle:history:legalBasis:processingCompanyTitle:retentionPeriodTitle:technologiesUsed:urls:)"))) __attribute__((objc_designated_initializer));
- (USDKServiceLabels *)doCopyDataCollected:(USDKDescriptionTitle *)dataCollected dataDistribution:(USDKDataDistributionTitle *)dataDistribution dataPurposes:(USDKDescriptionTitle *)dataPurposes dataRecipientsTitle:(NSString *)dataRecipientsTitle descriptionTitle:(NSString *)descriptionTitle history:(USDKDescriptionTitle *)history legalBasis:(USDKDescriptionTitle *)legalBasis processingCompanyTitle:(NSString *)processingCompanyTitle retentionPeriodTitle:(NSString *)retentionPeriodTitle technologiesUsed:(USDKDescriptionTitle *)technologiesUsed urls:(USDKURLsTitle *)urls __attribute__((swift_name("doCopy(dataCollected:dataDistribution:dataPurposes:dataRecipientsTitle:descriptionTitle:history:legalBasis:processingCompanyTitle:retentionPeriodTitle:technologiesUsed:urls:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKDescriptionTitle *dataCollected __attribute__((swift_name("dataCollected")));
@property (readonly) USDKDataDistributionTitle *dataDistribution __attribute__((swift_name("dataDistribution")));
@property (readonly) USDKDescriptionTitle *dataPurposes __attribute__((swift_name("dataPurposes")));
@property (readonly) NSString *dataRecipientsTitle __attribute__((swift_name("dataRecipientsTitle")));
@property (readonly) NSString *descriptionTitle __attribute__((swift_name("descriptionTitle")));
@property (readonly) USDKDescriptionTitle *history __attribute__((swift_name("history")));
@property (readonly) USDKDescriptionTitle *legalBasis __attribute__((swift_name("legalBasis")));
@property (readonly) NSString *processingCompanyTitle __attribute__((swift_name("processingCompanyTitle")));
@property (readonly) NSString *retentionPeriodTitle __attribute__((swift_name("retentionPeriodTitle")));
@property (readonly) USDKDescriptionTitle *technologiesUsed __attribute__((swift_name("technologiesUsed")));
@property (readonly) USDKURLsTitle *urls __attribute__((swift_name("urls")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceLabels.Companion")))
@interface USDKServiceLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Settings")))
@interface USDKSettings : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId id:(NSString *)id ui:(USDKUISettings * _Nullable)ui ccpaui:(USDKCCPAUISettings * _Nullable)ccpaui tcfui:(USDKTCFUISettings * _Nullable)tcfui version:(NSString *)version __attribute__((swift_name("init(controllerId:id:ui:ccpaui:tcfui:version:)"))) __attribute__((objc_designated_initializer));
- (USDKSettings *)doCopyControllerId:(NSString *)controllerId id:(NSString *)id ui:(USDKUISettings * _Nullable)ui ccpaui:(USDKCCPAUISettings * _Nullable)ccpaui tcfui:(USDKTCFUISettings * _Nullable)tcfui version:(NSString *)version __attribute__((swift_name("doCopy(controllerId:id:ui:ccpaui:tcfui:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKCCPAUISettings * _Nullable ccpaui __attribute__((swift_name("ccpaui")));
@property NSString *controllerId __attribute__((swift_name("controllerId")));
@property NSString *id __attribute__((swift_name("id")));
@property USDKTCFUISettings * _Nullable tcfui __attribute__((swift_name("tcfui")));
@property USDKUISettings * _Nullable ui __attribute__((swift_name("ui")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SettingsVersion")))
@interface USDKSettingsVersion : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKSettingsVersion *major __attribute__((swift_name("major")));
@property (class, readonly) USDKSettingsVersion *minor __attribute__((swift_name("minor")));
@property (class, readonly) USDKSettingsVersion *patch __attribute__((swift_name("patch")));
- (int32_t)compareToOther:(USDKSettingsVersion *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ThemeSettings")))
@interface USDKThemeSettings : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKThemeSettings *dark __attribute__((swift_name("dark")));
@property (class, readonly) USDKThemeSettings *light __attribute__((swift_name("light")));
- (int32_t)compareToOther:(USDKThemeSettings *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("UISettingsInterface")))
@protocol USDKUISettingsInterface
@required
@property (readonly) USDKCustomization *customization __attribute__((swift_name("customization")));
@property (readonly) BOOL isEmbeddingsEnabled __attribute__((swift_name("isEmbeddingsEnabled")));
@property (readonly) USDKLanguage *language __attribute__((swift_name("language")));
@property (readonly) USDKLinks *links __attribute__((swift_name("links")));
@property (readonly) USDKPoweredBy *poweredBy __attribute__((swift_name("poweredBy")));
@property (readonly) USDKButton *privacyButton __attribute__((swift_name("privacyButton")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("URLs")))
@interface USDKURLs : USDKBase
- (instancetype)initWithCookiePolicy:(NSString *)cookiePolicy dataProcessingAgreement:(NSString *)dataProcessingAgreement optOut:(NSString *)optOut privacyPolicy:(NSString *)privacyPolicy __attribute__((swift_name("init(cookiePolicy:dataProcessingAgreement:optOut:privacyPolicy:)"))) __attribute__((objc_designated_initializer));
- (USDKURLs *)doCopyCookiePolicy:(NSString *)cookiePolicy dataProcessingAgreement:(NSString *)dataProcessingAgreement optOut:(NSString *)optOut privacyPolicy:(NSString *)privacyPolicy __attribute__((swift_name("doCopy(cookiePolicy:dataProcessingAgreement:optOut:privacyPolicy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *cookiePolicy __attribute__((swift_name("cookiePolicy")));
@property NSString *dataProcessingAgreement __attribute__((swift_name("dataProcessingAgreement")));
@property NSString *optOut __attribute__((swift_name("optOut")));
@property NSString *privacyPolicy __attribute__((swift_name("privacyPolicy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("URLs.Companion")))
@interface USDKURLsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("URLsTitle")))
@interface USDKURLsTitle : USDKBase
- (instancetype)initWithCookiePolicyTitle:(NSString *)cookiePolicyTitle dataProcessingAgreementTitle:(NSString *)dataProcessingAgreementTitle optOutTitle:(NSString *)optOutTitle privacyPolicyTitle:(NSString *)privacyPolicyTitle __attribute__((swift_name("init(cookiePolicyTitle:dataProcessingAgreementTitle:optOutTitle:privacyPolicyTitle:)"))) __attribute__((objc_designated_initializer));
- (USDKURLsTitle *)doCopyCookiePolicyTitle:(NSString *)cookiePolicyTitle dataProcessingAgreementTitle:(NSString *)dataProcessingAgreementTitle optOutTitle:(NSString *)optOutTitle privacyPolicyTitle:(NSString *)privacyPolicyTitle __attribute__((swift_name("doCopy(cookiePolicyTitle:dataProcessingAgreementTitle:optOutTitle:privacyPolicyTitle:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *cookiePolicyTitle __attribute__((swift_name("cookiePolicyTitle")));
@property (readonly) NSString *dataProcessingAgreementTitle __attribute__((swift_name("dataProcessingAgreementTitle")));
@property (readonly) NSString *optOutTitle __attribute__((swift_name("optOutTitle")));
@property (readonly) NSString *privacyPolicyTitle __attribute__((swift_name("privacyPolicyTitle")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("URLsTitle.Companion")))
@interface USDKURLsTitleCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LabelOnOff")))
@interface USDKLabelOnOff : USDKBase
- (instancetype)initWithOnLabel:(NSString *)onLabel offLabel:(NSString *)offLabel __attribute__((swift_name("init(onLabel:offLabel:)"))) __attribute__((objc_designated_initializer));
- (USDKLabelOnOff *)doCopyOnLabel:(NSString *)onLabel offLabel:(NSString *)offLabel __attribute__((swift_name("doCopy(onLabel:offLabel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *offLabel __attribute__((swift_name("offLabel")));
@property (readonly) NSString *onLabel __attribute__((swift_name("onLabel")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LabelOnOff.Companion")))
@interface USDKLabelOnOffCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFButtons")))
@interface USDKTCFButtons : USDKBase
- (instancetype)initWithAcceptAll:(USDKLabel *)acceptAll denyAll:(USDKButton *)denyAll save:(USDKLabel *)save manageSettings:(USDKLabel *)manageSettings showVendorTab:(USDKLabel *)showVendorTab __attribute__((swift_name("init(acceptAll:denyAll:save:manageSettings:showVendorTab:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFButtons *)doCopyAcceptAll:(USDKLabel *)acceptAll denyAll:(USDKButton *)denyAll save:(USDKLabel *)save manageSettings:(USDKLabel *)manageSettings showVendorTab:(USDKLabel *)showVendorTab __attribute__((swift_name("doCopy(acceptAll:denyAll:save:manageSettings:showVendorTab:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKLabel *acceptAll __attribute__((swift_name("acceptAll")));
@property (readonly) USDKButton *denyAll __attribute__((swift_name("denyAll")));
@property (readonly) USDKLabel *manageSettings __attribute__((swift_name("manageSettings")));
@property (readonly) USDKLabel *save __attribute__((swift_name("save")));
@property (readonly) USDKLabel *showVendorTab __attribute__((swift_name("showVendorTab")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFButtons.Companion")))
@interface USDKTCFButtonsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFChangedPurposes")))
@interface USDKTCFChangedPurposes : USDKBase
- (instancetype)initWithPurposes:(NSArray<USDKInt *> *)purposes legitimateInterestPurposes:(NSArray<USDKInt *> *)legitimateInterestPurposes __attribute__((swift_name("init(purposes:legitimateInterestPurposes:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFChangedPurposes *)doCopyPurposes:(NSArray<USDKInt *> *)purposes legitimateInterestPurposes:(NSArray<USDKInt *> *)legitimateInterestPurposes __attribute__((swift_name("doCopy(purposes:legitimateInterestPurposes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKInt *> *legitimateInterestPurposes __attribute__((swift_name("legitimateInterestPurposes")));
@property NSArray<USDKInt *> *purposes __attribute__((swift_name("purposes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFFirstLayer")))
@interface USDKTCFFirstLayer : USDKBase
- (instancetype)initWithDescription:(USDKTCFFirstLayerDescription *)description hideToggles:(BOOL)hideToggles isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title appLayerNoteResurface:(NSString *)appLayerNoteResurface __attribute__((swift_name("init(description:hideToggles:isOverlayEnabled:title:appLayerNoteResurface:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDescription:(USDKTCFFirstLayerDescription *)description hideToggles:(BOOL)hideToggles isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title appLayerNoteResurface:(NSString *)appLayerNoteResurface tcfFirstLayerDescription:(USDKTCFFirstLayerDescription *)tcfFirstLayerDescription __attribute__((swift_name("init(description:hideToggles:isOverlayEnabled:title:appLayerNoteResurface:tcfFirstLayerDescription:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFFirstLayer *)doCopyDescription:(USDKTCFFirstLayerDescription *)description hideToggles:(BOOL)hideToggles isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title appLayerNoteResurface:(NSString *)appLayerNoteResurface tcfFirstLayerDescription:(USDKTCFFirstLayerDescription *)tcfFirstLayerDescription __attribute__((swift_name("doCopy(description:hideToggles:isOverlayEnabled:title:appLayerNoteResurface:tcfFirstLayerDescription:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *appLayerNoteResurface __attribute__((swift_name("appLayerNoteResurface")));
@property (readonly, getter=description_) USDKTCFFirstLayerDescription *description __attribute__((swift_name("description")));
@property (readonly) BOOL hideToggles __attribute__((swift_name("hideToggles")));
@property (readonly) BOOL isOverlayEnabled __attribute__((swift_name("isOverlayEnabled")));
@property (readonly) USDKTCFFirstLayerDescription *tcfFirstLayerDescription __attribute__((swift_name("tcfFirstLayerDescription")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFFirstLayer.Companion")))
@interface USDKTCFFirstLayerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFFirstLayerDescription")))
@interface USDKTCFFirstLayerDescription : USDKBase
- (instancetype)initWithAdditionalInfo:(NSString * _Nullable)additionalInfo default:(NSString *)default_ __attribute__((swift_name("init(additionalInfo:default:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithAdditionalInfo:(NSString * _Nullable)additionalInfo default:(NSString *)default_ defaultDescription:(NSString *)defaultDescription __attribute__((swift_name("init(additionalInfo:default:defaultDescription:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFFirstLayerDescription *)doCopyAdditionalInfo:(NSString * _Nullable)additionalInfo default:(NSString *)default_ defaultDescription:(NSString *)defaultDescription __attribute__((swift_name("doCopy(additionalInfo:default:defaultDescription:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable additionalInfo __attribute__((swift_name("additionalInfo")));
@property (readonly, getter=default) NSString *default_ __attribute__((swift_name("default_")));
@property (readonly) NSString *defaultDescription __attribute__((swift_name("defaultDescription")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFFirstLayerDescription.Companion")))
@interface USDKTCFFirstLayerDescriptionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFGeneralLabels")))
@interface USDKTCFGeneralLabels : USDKBase
- (instancetype)initWithDisclaimer:(NSString *)disclaimer features:(NSString *)features iabVendors:(NSString *)iabVendors nonIabPurposes:(NSString *)nonIabPurposes nonIabVendors:(NSString *)nonIabVendors purposes:(NSString *)purposes __attribute__((swift_name("init(disclaimer:features:iabVendors:nonIabPurposes:nonIabVendors:purposes:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFGeneralLabels *)doCopyDisclaimer:(NSString *)disclaimer features:(NSString *)features iabVendors:(NSString *)iabVendors nonIabPurposes:(NSString *)nonIabPurposes nonIabVendors:(NSString *)nonIabVendors purposes:(NSString *)purposes __attribute__((swift_name("doCopy(disclaimer:features:iabVendors:nonIabPurposes:nonIabVendors:purposes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *disclaimer __attribute__((swift_name("disclaimer")));
@property (readonly) NSString *features __attribute__((swift_name("features")));
@property (readonly) NSString *iabVendors __attribute__((swift_name("iabVendors")));
@property (readonly) NSString *nonIabPurposes __attribute__((swift_name("nonIabPurposes")));
@property (readonly) NSString *nonIabVendors __attribute__((swift_name("nonIabVendors")));
@property (readonly) NSString *purposes __attribute__((swift_name("purposes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFGeneralLabels.Companion")))
@interface USDKTCFGeneralLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFLabels")))
@interface USDKTCFLabels : USDKBase
- (instancetype)initWithGeneral:(USDKTCFGeneralLabels *)general nonTCFLabels:(USDKLabels *)nonTCFLabels vendor:(USDKVendor *)vendor __attribute__((swift_name("init(general:nonTCFLabels:vendor:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFLabels *)doCopyGeneral:(USDKTCFGeneralLabels *)general nonTCFLabels:(USDKLabels *)nonTCFLabels vendor:(USDKVendor *)vendor __attribute__((swift_name("doCopy(general:nonTCFLabels:vendor:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKTCFGeneralLabels *general __attribute__((swift_name("general")));
@property (readonly) USDKLabels *nonTCFLabels __attribute__((swift_name("nonTCFLabels")));
@property (readonly) USDKVendor *vendor __attribute__((swift_name("vendor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFLabels.Companion")))
@interface USDKTCFLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFOptions")))
@interface USDKTCFOptions : USDKBase
- (instancetype)initWithChangedPurposes:(USDKTCFChangedPurposes *)changedPurposes cmpId:(int32_t)cmpId cmpVersion:(int32_t)cmpVersion consensuScriptPath:(NSString *)consensuScriptPath consensuSubdomain:(NSString *)consensuSubdomain gdprApplies:(BOOL)gdprApplies isServiceSpecific:(BOOL)isServiceSpecific publisherCountryCode:(NSString *)publisherCountryCode purposeOneTreatment:(BOOL)purposeOneTreatment vendorIds:(NSArray<USDKInt *> *)vendorIds stackIds:(NSArray<USDKInt *> *)stackIds __attribute__((swift_name("init(changedPurposes:cmpId:cmpVersion:consensuScriptPath:consensuSubdomain:gdprApplies:isServiceSpecific:publisherCountryCode:purposeOneTreatment:vendorIds:stackIds:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFOptions *)doCopyChangedPurposes:(USDKTCFChangedPurposes *)changedPurposes cmpId:(int32_t)cmpId cmpVersion:(int32_t)cmpVersion consensuScriptPath:(NSString *)consensuScriptPath consensuSubdomain:(NSString *)consensuSubdomain gdprApplies:(BOOL)gdprApplies isServiceSpecific:(BOOL)isServiceSpecific publisherCountryCode:(NSString *)publisherCountryCode purposeOneTreatment:(BOOL)purposeOneTreatment vendorIds:(NSArray<USDKInt *> *)vendorIds stackIds:(NSArray<USDKInt *> *)stackIds __attribute__((swift_name("doCopy(changedPurposes:cmpId:cmpVersion:consensuScriptPath:consensuSubdomain:gdprApplies:isServiceSpecific:publisherCountryCode:purposeOneTreatment:vendorIds:stackIds:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKTCFChangedPurposes *changedPurposes __attribute__((swift_name("changedPurposes")));
@property int32_t cmpId __attribute__((swift_name("cmpId")));
@property int32_t cmpVersion __attribute__((swift_name("cmpVersion")));
@property NSString *consensuScriptPath __attribute__((swift_name("consensuScriptPath")));
@property NSString *consensuSubdomain __attribute__((swift_name("consensuSubdomain")));
@property BOOL gdprApplies __attribute__((swift_name("gdprApplies")));
@property BOOL isServiceSpecific __attribute__((swift_name("isServiceSpecific")));
@property NSString *publisherCountryCode __attribute__((swift_name("publisherCountryCode")));
@property BOOL purposeOneTreatment __attribute__((swift_name("purposeOneTreatment")));
@property NSArray<USDKInt *> *stackIds __attribute__((swift_name("stackIds")));
@property NSArray<USDKInt *> *vendorIds __attribute__((swift_name("vendorIds")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFScope")))
@interface USDKTCFScope : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKTCFScope *global __attribute__((swift_name("global")));
@property (class, readonly) USDKTCFScope *service __attribute__((swift_name("service")));
- (int32_t)compareToOther:(USDKTCFScope *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFSecondLayer")))
@interface USDKTCFSecondLayer : USDKBase
- (instancetype)initWithTitle:(NSString *)title description:(NSString *)description isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTCFTabs *)tabs __attribute__((swift_name("init(title:description:isOverlayEnabled:tabs:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDescription:(NSString *)description isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTCFTabs *)tabs title:(NSString *)title tcfSecondLayerDescription:(NSString *)tcfSecondLayerDescription __attribute__((swift_name("init(description:isOverlayEnabled:tabs:title:tcfSecondLayerDescription:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFSecondLayer *)doCopyDescription:(NSString *)description isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTCFTabs *)tabs title:(NSString *)title tcfSecondLayerDescription:(NSString *)tcfSecondLayerDescription __attribute__((swift_name("doCopy(description:isOverlayEnabled:tabs:title:tcfSecondLayerDescription:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) BOOL isOverlayEnabled __attribute__((swift_name("isOverlayEnabled")));
@property (readonly) USDKTCFTabs *tabs __attribute__((swift_name("tabs")));
@property (readonly) NSString *tcfSecondLayerDescription __attribute__((swift_name("tcfSecondLayerDescription")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFSecondLayer.Companion")))
@interface USDKTCFSecondLayerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFTabs")))
@interface USDKTCFTabs : USDKBase
- (instancetype)initWithPurposes:(USDKLabel *)purposes vendors:(USDKLabel *)vendors __attribute__((swift_name("init(purposes:vendors:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFTabs *)doCopyPurposes:(USDKLabel *)purposes vendors:(USDKLabel *)vendors __attribute__((swift_name("doCopy(purposes:vendors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKLabel *purposes __attribute__((swift_name("purposes")));
@property (readonly) USDKLabel *vendors __attribute__((swift_name("vendors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFTabs.Companion")))
@interface USDKTCFTabsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFUISettings")))
@interface USDKTCFUISettings : USDKBase <USDKUISettingsInterface>
- (instancetype)initWithCustomization:(USDKCustomization *)customization isEmbeddingsEnabled:(BOOL)isEmbeddingsEnabled language:(USDKLanguage *)language links:(USDKLinks *)links poweredBy:(USDKPoweredBy *)poweredBy privacyButton:(USDKButton *)privacyButton buttons:(USDKTCFButtons *)buttons firstLayer:(USDKTCFFirstLayer *)firstLayer labels:(USDKTCFLabels *)labels secondLayer:(USDKTCFSecondLayer *)secondLayer toggles:(USDKToggles *)toggles __attribute__((swift_name("init(customization:isEmbeddingsEnabled:language:links:poweredBy:privacyButton:buttons:firstLayer:labels:secondLayer:toggles:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFUISettings *)doCopyCustomization:(USDKCustomization *)customization isEmbeddingsEnabled:(BOOL)isEmbeddingsEnabled language:(USDKLanguage *)language links:(USDKLinks *)links poweredBy:(USDKPoweredBy *)poweredBy privacyButton:(USDKButton *)privacyButton buttons:(USDKTCFButtons *)buttons firstLayer:(USDKTCFFirstLayer *)firstLayer labels:(USDKTCFLabels *)labels secondLayer:(USDKTCFSecondLayer *)secondLayer toggles:(USDKToggles *)toggles __attribute__((swift_name("doCopy(customization:isEmbeddingsEnabled:language:links:poweredBy:privacyButton:buttons:firstLayer:labels:secondLayer:toggles:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKTCFButtons *buttons __attribute__((swift_name("buttons")));
@property (readonly) USDKCustomization *customization __attribute__((swift_name("customization")));
@property (readonly) USDKTCFFirstLayer *firstLayer __attribute__((swift_name("firstLayer")));
@property (readonly) BOOL isEmbeddingsEnabled __attribute__((swift_name("isEmbeddingsEnabled")));
@property (readonly) USDKTCFLabels *labels __attribute__((swift_name("labels")));
@property (readonly) USDKLanguage *language __attribute__((swift_name("language")));
@property (readonly) USDKLinks *links __attribute__((swift_name("links")));
@property (readonly) USDKPoweredBy *poweredBy __attribute__((swift_name("poweredBy")));
@property (readonly) USDKButton *privacyButton __attribute__((swift_name("privacyButton")));
@property (readonly) USDKTCFSecondLayer *secondLayer __attribute__((swift_name("secondLayer")));
@property (readonly) USDKToggles *toggles __attribute__((swift_name("toggles")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFUISettings.Companion")))
@interface USDKTCFUISettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Toggles")))
@interface USDKToggles : USDKBase
- (instancetype)initWithConsent:(USDKLabel *)consent legitimateInterest:(USDKLabel *)legitimateInterest specialFeaturesToggle:(USDKLabelOnOff *)specialFeaturesToggle __attribute__((swift_name("init(consent:legitimateInterest:specialFeaturesToggle:)"))) __attribute__((objc_designated_initializer));
- (USDKToggles *)doCopyConsent:(USDKLabel *)consent legitimateInterest:(USDKLabel *)legitimateInterest specialFeaturesToggle:(USDKLabelOnOff *)specialFeaturesToggle __attribute__((swift_name("doCopy(consent:legitimateInterest:specialFeaturesToggle:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKLabel *consent __attribute__((swift_name("consent")));
@property (readonly) USDKLabel *legitimateInterest __attribute__((swift_name("legitimateInterest")));
@property (readonly) USDKLabelOnOff *specialFeaturesToggle __attribute__((swift_name("specialFeaturesToggle")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Toggles.Companion")))
@interface USDKTogglesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Vendor")))
@interface USDKVendor : USDKBase
- (instancetype)initWithFeatures:(NSString *)features legitimateInterest:(NSString *)legitimateInterest privacyPolicy:(NSString *)privacyPolicy purposes:(NSString *)purposes specialFeatures:(NSString *)specialFeatures specialPurposes:(NSString *)specialPurposes __attribute__((swift_name("init(features:legitimateInterest:privacyPolicy:purposes:specialFeatures:specialPurposes:)"))) __attribute__((objc_designated_initializer));
- (USDKVendor *)doCopyFeatures:(NSString *)features legitimateInterest:(NSString *)legitimateInterest privacyPolicy:(NSString *)privacyPolicy purposes:(NSString *)purposes specialFeatures:(NSString *)specialFeatures specialPurposes:(NSString *)specialPurposes __attribute__((swift_name("doCopy(features:legitimateInterest:privacyPolicy:purposes:specialFeatures:specialPurposes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *features __attribute__((swift_name("features")));
@property (readonly) NSString *legitimateInterest __attribute__((swift_name("legitimateInterest")));
@property (readonly) NSString *privacyPolicy __attribute__((swift_name("privacyPolicy")));
@property (readonly) NSString *purposes __attribute__((swift_name("purposes")));
@property (readonly) NSString *specialFeatures __attribute__((swift_name("specialFeatures")));
@property (readonly) NSString *specialPurposes __attribute__((swift_name("specialPurposes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Vendor.Companion")))
@interface USDKVendorCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("ApiBaseServiceInterface")))
@protocol USDKApiBaseServiceInterface
@required
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property NSString *templateId __attribute__((swift_name("templateId")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiAggregatorService")))
@interface USDKApiAggregatorService : USDKBase <USDKApiBaseServiceInterface>
- (instancetype)initWithDescription:(NSString * _Nullable)description descriptionOfService:(NSString *)descriptionOfService templateId:(NSString *)templateId version:(NSString *)version addressOfProcessingCompany:(NSString *)addressOfProcessingCompany cookiePolicyURL:(NSString *)cookiePolicyURL dataCollectedList:(NSArray<NSString *> *)dataCollectedList dataProtectionOfficer:(NSString *)dataProtectionOfficer dataProcessor:(NSString * _Nullable)dataProcessor dataProcessors:(NSArray<NSString *> * _Nullable)dataProcessors dataPurposes:(NSArray<NSString *> *)dataPurposes dataPurposesList:(NSArray<NSString *> *)dataPurposesList dataRecipientsList:(NSArray<NSString *> *)dataRecipientsList language:(NSString *)language languagesAvailable:(NSArray<NSString *> *)languagesAvailable legalBasisList:(NSArray<NSString *> *)legalBasisList legalGround:(NSString *)legalGround linkToDpa:(NSString *)linkToDpa locationOfProcessing:(NSString *)locationOfProcessing nameOfProcessingCompany:(NSString *)nameOfProcessingCompany optOutUrl:(NSString *)optOutUrl policyOfProcessorUrl:(NSString *)policyOfProcessorUrl privacyPolicyURL:(NSString *)privacyPolicyURL processingCompany:(NSString * _Nullable)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription retentionPeriodList:(NSArray<NSString *> *)retentionPeriodList technologyUsed:(NSArray<NSString *> *)technologyUsed thirdCountryTransfer:(NSString *)thirdCountryTransfer __attribute__((swift_name("init(description:descriptionOfService:templateId:version:addressOfProcessingCompany:cookiePolicyURL:dataCollectedList:dataProtectionOfficer:dataProcessor:dataProcessors:dataPurposes:dataPurposesList:dataRecipientsList:language:languagesAvailable:legalBasisList:legalGround:linkToDpa:locationOfProcessing:nameOfProcessingCompany:optOutUrl:policyOfProcessorUrl:privacyPolicyURL:processingCompany:retentionPeriodDescription:retentionPeriodList:technologyUsed:thirdCountryTransfer:)"))) __attribute__((objc_designated_initializer));
- (USDKApiAggregatorService *)doCopyDescription:(NSString * _Nullable)description descriptionOfService:(NSString *)descriptionOfService templateId:(NSString *)templateId version:(NSString *)version addressOfProcessingCompany:(NSString *)addressOfProcessingCompany cookiePolicyURL:(NSString *)cookiePolicyURL dataCollectedList:(NSArray<NSString *> *)dataCollectedList dataProtectionOfficer:(NSString *)dataProtectionOfficer dataProcessor:(NSString * _Nullable)dataProcessor dataProcessors:(NSArray<NSString *> * _Nullable)dataProcessors dataPurposes:(NSArray<NSString *> *)dataPurposes dataPurposesList:(NSArray<NSString *> *)dataPurposesList dataRecipientsList:(NSArray<NSString *> *)dataRecipientsList language:(NSString *)language languagesAvailable:(NSArray<NSString *> *)languagesAvailable legalBasisList:(NSArray<NSString *> *)legalBasisList legalGround:(NSString *)legalGround linkToDpa:(NSString *)linkToDpa locationOfProcessing:(NSString *)locationOfProcessing nameOfProcessingCompany:(NSString *)nameOfProcessingCompany optOutUrl:(NSString *)optOutUrl policyOfProcessorUrl:(NSString *)policyOfProcessorUrl privacyPolicyURL:(NSString *)privacyPolicyURL processingCompany:(NSString * _Nullable)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription retentionPeriodList:(NSArray<NSString *> *)retentionPeriodList technologyUsed:(NSArray<NSString *> *)technologyUsed thirdCountryTransfer:(NSString *)thirdCountryTransfer __attribute__((swift_name("doCopy(description:descriptionOfService:templateId:version:addressOfProcessingCompany:cookiePolicyURL:dataCollectedList:dataProtectionOfficer:dataProcessor:dataProcessors:dataPurposes:dataPurposesList:dataRecipientsList:language:languagesAvailable:legalBasisList:legalGround:linkToDpa:locationOfProcessing:nameOfProcessingCompany:optOutUrl:policyOfProcessorUrl:privacyPolicyURL:processingCompany:retentionPeriodDescription:retentionPeriodList:technologyUsed:thirdCountryTransfer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *addressOfProcessingCompany __attribute__((swift_name("addressOfProcessingCompany")));
@property NSString *cookiePolicyURL __attribute__((swift_name("cookiePolicyURL")));
@property NSArray<NSString *> *dataCollectedList __attribute__((swift_name("dataCollectedList")));
@property NSString * _Nullable dataProcessor __attribute__((swift_name("dataProcessor")));
@property NSArray<NSString *> * _Nullable dataProcessors __attribute__((swift_name("dataProcessors")));
@property NSString *dataProtectionOfficer __attribute__((swift_name("dataProtectionOfficer")));
@property NSArray<NSString *> *dataPurposes __attribute__((swift_name("dataPurposes")));
@property NSArray<NSString *> *dataPurposesList __attribute__((swift_name("dataPurposesList")));
@property NSArray<NSString *> *dataRecipientsList __attribute__((swift_name("dataRecipientsList")));
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property NSString *descriptionOfService __attribute__((swift_name("descriptionOfService")));
@property NSString *language __attribute__((swift_name("language")));
@property NSArray<NSString *> *languagesAvailable __attribute__((swift_name("languagesAvailable")));
@property NSArray<NSString *> *legalBasisList __attribute__((swift_name("legalBasisList")));
@property NSString *legalGround __attribute__((swift_name("legalGround")));
@property NSString *linkToDpa __attribute__((swift_name("linkToDpa")));
@property NSString *locationOfProcessing __attribute__((swift_name("locationOfProcessing")));
@property NSString *nameOfProcessingCompany __attribute__((swift_name("nameOfProcessingCompany")));
@property NSString *optOutUrl __attribute__((swift_name("optOutUrl")));
@property NSString *policyOfProcessorUrl __attribute__((swift_name("policyOfProcessorUrl")));
@property NSString *privacyPolicyURL __attribute__((swift_name("privacyPolicyURL")));
@property NSString * _Nullable processingCompany __attribute__((swift_name("processingCompany")));
@property NSString *retentionPeriodDescription __attribute__((swift_name("retentionPeriodDescription")));
@property NSArray<NSString *> *retentionPeriodList __attribute__((swift_name("retentionPeriodList")));
@property NSArray<NSString *> *technologyUsed __attribute__((swift_name("technologyUsed")));
@property NSString *templateId __attribute__((swift_name("templateId")));
@property NSString *thirdCountryTransfer __attribute__((swift_name("thirdCountryTransfer")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiAggregatorService.Companion")))
@interface USDKApiAggregatorServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiAggregatorTemplates")))
@interface USDKApiAggregatorTemplates : USDKBase
- (instancetype)initWithTemplates:(NSArray<USDKApiAggregatorService *> *)templates __attribute__((swift_name("init(templates:)"))) __attribute__((objc_designated_initializer));
- (USDKApiAggregatorTemplates *)doCopyTemplates:(NSArray<USDKApiAggregatorService *> *)templates __attribute__((swift_name("doCopy(templates:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKApiAggregatorService *> *templates __attribute__((swift_name("templates")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiAggregatorTemplates.Companion")))
@interface USDKApiAggregatorTemplatesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiBackgroundOverlay")))
@interface USDKApiBackgroundOverlay : USDKBase
- (instancetype)initWithDarken:(int32_t)darken target:(NSArray<USDKInt *> *)target __attribute__((swift_name("init(darken:target:)"))) __attribute__((objc_designated_initializer));
- (USDKApiBackgroundOverlay *)doCopyDarken:(int32_t)darken target:(NSArray<USDKInt *> *)target __attribute__((swift_name("doCopy(darken:target:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property int32_t darken __attribute__((swift_name("darken")));
@property NSArray<USDKInt *> *target __attribute__((swift_name("target")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiBackgroundOverlay.Companion")))
@interface USDKApiBackgroundOverlayCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiBackgroundOverlayTarget")))
@interface USDKApiBackgroundOverlayTarget : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKApiBackgroundOverlayTarget *firstLayer __attribute__((swift_name("firstLayer")));
@property (class, readonly) USDKApiBackgroundOverlayTarget *secondLayer __attribute__((swift_name("secondLayer")));
- (int32_t)compareToOther:(USDKApiBackgroundOverlayTarget *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) int32_t i __attribute__((swift_name("i")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiBaseService")))
@interface USDKApiBaseService : USDKBase <USDKApiBaseServiceInterface>
- (instancetype)initWithDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version __attribute__((swift_name("init(description:templateId:version:)"))) __attribute__((objc_designated_initializer));
- (USDKApiBaseService *)doCopyDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version __attribute__((swift_name("doCopy(description:templateId:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property NSString *templateId __attribute__((swift_name("templateId")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiBaseService.Companion")))
@interface USDKApiBaseServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCCPA")))
@interface USDKApiCCPA : USDKBase
- (instancetype)initWithBtnMoreInfo:(NSString *)btnMoreInfo btnSave:(NSString *)btnSave iabAgreementExists:(BOOL)iabAgreementExists isActive:(BOOL)isActive firstLayerHideLanguageSwitch:(BOOL)firstLayerHideLanguageSwitch firstLayerDescription:(NSString *)firstLayerDescription firstLayerMobileDescription:(NSString *)firstLayerMobileDescription firstLayerMobileDescriptionIsActive:(BOOL)firstLayerMobileDescriptionIsActive firstLayerTitle:(NSString *)firstLayerTitle firstLayerVariant:(USDKFirstLayerVariant *)firstLayerVariant optOutNoticeLabel:(NSString *)optOutNoticeLabel region:(USDKCCPARegion *)region reshowAfterDays:(int32_t)reshowAfterDays reshowCMP:(BOOL)reshowCMP secondLayerDescription:(NSString *)secondLayerDescription secondLayerHideLanguageSwitch:(BOOL)secondLayerHideLanguageSwitch secondLayerTitle:(NSString *)secondLayerTitle secondLayerVariant:(USDKSecondLayerVariant *)secondLayerVariant showOnPageLoad:(BOOL)showOnPageLoad __attribute__((swift_name("init(btnMoreInfo:btnSave:iabAgreementExists:isActive:firstLayerHideLanguageSwitch:firstLayerDescription:firstLayerMobileDescription:firstLayerMobileDescriptionIsActive:firstLayerTitle:firstLayerVariant:optOutNoticeLabel:region:reshowAfterDays:reshowCMP:secondLayerDescription:secondLayerHideLanguageSwitch:secondLayerTitle:secondLayerVariant:showOnPageLoad:)"))) __attribute__((objc_designated_initializer));
- (USDKApiCCPA *)doCopyBtnMoreInfo:(NSString *)btnMoreInfo btnSave:(NSString *)btnSave iabAgreementExists:(BOOL)iabAgreementExists isActive:(BOOL)isActive firstLayerHideLanguageSwitch:(BOOL)firstLayerHideLanguageSwitch firstLayerDescription:(NSString *)firstLayerDescription firstLayerMobileDescription:(NSString *)firstLayerMobileDescription firstLayerMobileDescriptionIsActive:(BOOL)firstLayerMobileDescriptionIsActive firstLayerTitle:(NSString *)firstLayerTitle firstLayerVariant:(USDKFirstLayerVariant *)firstLayerVariant optOutNoticeLabel:(NSString *)optOutNoticeLabel region:(USDKCCPARegion *)region reshowAfterDays:(int32_t)reshowAfterDays reshowCMP:(BOOL)reshowCMP secondLayerDescription:(NSString *)secondLayerDescription secondLayerHideLanguageSwitch:(BOOL)secondLayerHideLanguageSwitch secondLayerTitle:(NSString *)secondLayerTitle secondLayerVariant:(USDKSecondLayerVariant *)secondLayerVariant showOnPageLoad:(BOOL)showOnPageLoad __attribute__((swift_name("doCopy(btnMoreInfo:btnSave:iabAgreementExists:isActive:firstLayerHideLanguageSwitch:firstLayerDescription:firstLayerMobileDescription:firstLayerMobileDescriptionIsActive:firstLayerTitle:firstLayerVariant:optOutNoticeLabel:region:reshowAfterDays:reshowCMP:secondLayerDescription:secondLayerHideLanguageSwitch:secondLayerTitle:secondLayerVariant:showOnPageLoad:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *btnMoreInfo __attribute__((swift_name("btnMoreInfo")));
@property NSString *btnSave __attribute__((swift_name("btnSave")));
@property NSString *firstLayerDescription __attribute__((swift_name("firstLayerDescription")));
@property BOOL firstLayerHideLanguageSwitch __attribute__((swift_name("firstLayerHideLanguageSwitch")));
@property NSString *firstLayerMobileDescription __attribute__((swift_name("firstLayerMobileDescription")));
@property BOOL firstLayerMobileDescriptionIsActive __attribute__((swift_name("firstLayerMobileDescriptionIsActive")));
@property NSString *firstLayerTitle __attribute__((swift_name("firstLayerTitle")));
@property USDKFirstLayerVariant *firstLayerVariant __attribute__((swift_name("firstLayerVariant")));
@property BOOL iabAgreementExists __attribute__((swift_name("iabAgreementExists")));
@property BOOL isActive __attribute__((swift_name("isActive")));
@property NSString *optOutNoticeLabel __attribute__((swift_name("optOutNoticeLabel")));
@property USDKCCPARegion *region __attribute__((swift_name("region")));
@property int32_t reshowAfterDays __attribute__((swift_name("reshowAfterDays")));
@property BOOL reshowCMP __attribute__((swift_name("reshowCMP")));
@property NSString *secondLayerDescription __attribute__((swift_name("secondLayerDescription")));
@property BOOL secondLayerHideLanguageSwitch __attribute__((swift_name("secondLayerHideLanguageSwitch")));
@property NSString *secondLayerTitle __attribute__((swift_name("secondLayerTitle")));
@property USDKSecondLayerVariant *secondLayerVariant __attribute__((swift_name("secondLayerVariant")));
@property BOOL showOnPageLoad __attribute__((swift_name("showOnPageLoad")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCCPA.Companion")))
@interface USDKApiCCPACompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCategory")))
@interface USDKApiCategory : USDKBase
- (instancetype)initWithCategorySlug:(NSString *)categorySlug description:(NSString *)description isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label __attribute__((swift_name("init(categorySlug:description:isEssential:isHidden:label:)"))) __attribute__((objc_designated_initializer));
- (USDKApiCategory *)doCopyCategorySlug:(NSString *)categorySlug description:(NSString *)description isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label __attribute__((swift_name("doCopy(categorySlug:description:isEssential:isHidden:label:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *categorySlug __attribute__((swift_name("categorySlug")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property BOOL isEssential __attribute__((swift_name("isEssential")));
@property BOOL isHidden __attribute__((swift_name("isHidden")));
@property NSString *label __attribute__((swift_name("label")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCategory.Companion")))
@interface USDKApiCategoryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCustomization")))
@interface USDKApiCustomization : USDKBase
- (instancetype)initWithColor:(USDKApiCustomizationColor * _Nullable)color font:(USDKApiCustomizationFont * _Nullable)font logoUrl:(NSString * _Nullable)logoUrl __attribute__((swift_name("init(color:font:logoUrl:)"))) __attribute__((objc_designated_initializer));
- (USDKApiCustomization *)doCopyColor:(USDKApiCustomizationColor * _Nullable)color font:(USDKApiCustomizationFont * _Nullable)font logoUrl:(NSString * _Nullable)logoUrl __attribute__((swift_name("doCopy(color:font:logoUrl:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKApiCustomizationColor * _Nullable color __attribute__((swift_name("color")));
@property USDKApiCustomizationFont * _Nullable font __attribute__((swift_name("font")));
@property NSString * _Nullable logoUrl __attribute__((swift_name("logoUrl")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCustomization.Companion")))
@interface USDKApiCustomizationCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCustomizationColor")))
@interface USDKApiCustomizationColor : USDKBase
- (instancetype)initWithPrimary:(NSString *)primary __attribute__((swift_name("init(primary:)"))) __attribute__((objc_designated_initializer));
- (USDKApiCustomizationColor *)doCopyPrimary:(NSString *)primary __attribute__((swift_name("doCopy(primary:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *primary __attribute__((swift_name("primary")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCustomizationColor.Companion")))
@interface USDKApiCustomizationColorCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCustomizationFont")))
@interface USDKApiCustomizationFont : USDKBase
- (instancetype)initWithFamily:(NSString *)family size:(int32_t)size __attribute__((swift_name("init(family:size:)"))) __attribute__((objc_designated_initializer));
- (USDKApiCustomizationFont *)doCopyFamily:(NSString *)family size:(int32_t)size __attribute__((swift_name("doCopy(family:size:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *family __attribute__((swift_name("family")));
@property int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiCustomizationFont.Companion")))
@interface USDKApiCustomizationFontCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiDataExchangeSetting")))
@interface USDKApiDataExchangeSetting : USDKBase
- (instancetype)initWithNames:(NSArray<NSString *> *)names type:(int32_t)type __attribute__((swift_name("init(names:type:)"))) __attribute__((objc_designated_initializer));
- (USDKApiDataExchangeSetting *)doCopyNames:(NSArray<NSString *> *)names type:(int32_t)type __attribute__((swift_name("doCopy(names:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<NSString *> *names __attribute__((swift_name("names")));
@property int32_t type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiDataExchangeSetting.Companion")))
@interface USDKApiDataExchangeSettingCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiDataExchangeType")))
@interface USDKApiDataExchangeType : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKApiDataExchangeType *dataLayer __attribute__((swift_name("dataLayer")));
@property (class, readonly) USDKApiDataExchangeType *windowEvent __attribute__((swift_name("windowEvent")));
@property (class, readonly) USDKApiDataExchangeType *unknown __attribute__((swift_name("unknown")));
- (int32_t)compareToOther:(USDKApiDataExchangeType *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) int32_t i __attribute__((swift_name("i")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiErrors")))
@interface USDKApiErrors : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)apiErrors __attribute__((swift_name("init()")));
@property (readonly) NSString *AVAILABLE_LANGUAGES_NOT_FOUND __attribute__((swift_name("AVAILABLE_LANGUAGES_NOT_FOUND")));
@property (readonly) NSString *FETCHING_SERVICES_FAILED __attribute__((swift_name("FETCHING_SERVICES_FAILED")));
@property (readonly) NSString *FETCH_AVAILABLE_LANGUAGES __attribute__((swift_name("FETCH_AVAILABLE_LANGUAGES")));
@property (readonly) NSString *FETCH_DATA_PROCESSING_SERVICES __attribute__((swift_name("FETCH_DATA_PROCESSING_SERVICES")));
@property (readonly) NSString *FETCH_SETTINGS __attribute__((swift_name("FETCH_SETTINGS")));
@property (readonly) NSString *FETCH_USER_CONSENTS __attribute__((swift_name("FETCH_USER_CONSENTS")));
@property (readonly) NSString *FETCH_USER_COUNTRY __attribute__((swift_name("FETCH_USER_COUNTRY")));
@property (readonly) NSString *GENERATE_DATA_PROCESSING_SERVICES __attribute__((swift_name("GENERATE_DATA_PROCESSING_SERVICES")));
@property (readonly) NSString *SAVE_CONSENTS __attribute__((swift_name("SAVE_CONSENTS")));
@property (readonly) NSString *SETTINGS_NOT_FOUND __attribute__((swift_name("SETTINGS_NOT_FOUND")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiLabels")))
@interface USDKApiLabels : USDKBase
- (instancetype)initWithAccepted:(NSString *)accepted btnAcceptAll:(NSString *)btnAcceptAll btnBannerReadMore:(NSString * _Nullable)btnBannerReadMore btnDeny:(NSString *)btnDeny btnMore:(NSString *)btnMore btnSave:(NSString *)btnSave categories:(NSString *)categories consentType:(NSString *)consentType cookiePolicyInfo:(NSString *)cookiePolicyInfo cookiePolicyLinkText:(NSString *)cookiePolicyLinkText copy:(NSString *)copy dataCollectedInfo:(NSString *)dataCollectedInfo dataCollectedList:(NSString *)dataCollectedList dataRecipientsList:(NSString *)dataRecipientsList dataRecipientsListInfo:(NSString *)dataRecipientsListInfo dataPurposes:(NSString *)dataPurposes dataPurposesInfo:(NSString *)dataPurposesInfo date:(NSString *)date decision:(NSString *)decision denied:(NSString *)denied descriptionOfService:(NSString *)descriptionOfService explicit:(NSString *)explicit_ firstLayerTitle:(NSString * _Nullable)firstLayerTitle furtherInformationOptOut:(NSString *)furtherInformationOptOut headerCenterSecondary:(NSString *)headerCenterSecondary headerCorner:(NSString *)headerCorner history:(NSString *)history historyDescription:(NSString *)historyDescription implicit:(NSString *)implicit imprintLinkText:(NSString *)imprintLinkText legalBasisList:(NSString *)legalBasisList legalBasisInfo:(NSString *)legalBasisInfo linkToDpaInfo:(NSString *)linkToDpaInfo locationOfProcessing:(NSString *)locationOfProcessing optOut:(NSString *)optOut partnerPoweredByLinkText:(NSString *)partnerPoweredByLinkText policyOf:(NSString *)policyOf poweredBy:(NSString *)poweredBy privacyPolicyLinkText:(NSString *)privacyPolicyLinkText processingCompanyTitle:(NSString *)processingCompanyTitle retentionPeriod:(NSString *)retentionPeriod showMore:(NSString *)showMore technologiesUsed:(NSString *)technologiesUsed technologiesUsedInfo:(NSString *)technologiesUsedInfo titleCenterSecondary:(NSString *)titleCenterSecondary titleCorner:(NSString *)titleCorner transferToThirdCountries:(NSString *)transferToThirdCountries __attribute__((swift_name("init(accepted:btnAcceptAll:btnBannerReadMore:btnDeny:btnMore:btnSave:categories:consentType:cookiePolicyInfo:cookiePolicyLinkText:copy:dataCollectedInfo:dataCollectedList:dataRecipientsList:dataRecipientsListInfo:dataPurposes:dataPurposesInfo:date:decision:denied:descriptionOfService:explicit:firstLayerTitle:furtherInformationOptOut:headerCenterSecondary:headerCorner:history:historyDescription:implicit:imprintLinkText:legalBasisList:legalBasisInfo:linkToDpaInfo:locationOfProcessing:optOut:partnerPoweredByLinkText:policyOf:poweredBy:privacyPolicyLinkText:processingCompanyTitle:retentionPeriod:showMore:technologiesUsed:technologiesUsedInfo:titleCenterSecondary:titleCorner:transferToThirdCountries:)"))) __attribute__((objc_designated_initializer));
- (USDKApiLabels *)doCopyAccepted:(NSString *)accepted btnAcceptAll:(NSString *)btnAcceptAll btnBannerReadMore:(NSString * _Nullable)btnBannerReadMore btnDeny:(NSString *)btnDeny btnMore:(NSString *)btnMore btnSave:(NSString *)btnSave categories:(NSString *)categories consentType:(NSString *)consentType cookiePolicyInfo:(NSString *)cookiePolicyInfo cookiePolicyLinkText:(NSString *)cookiePolicyLinkText copy:(NSString *)copy dataCollectedInfo:(NSString *)dataCollectedInfo dataCollectedList:(NSString *)dataCollectedList dataRecipientsList:(NSString *)dataRecipientsList dataRecipientsListInfo:(NSString *)dataRecipientsListInfo dataPurposes:(NSString *)dataPurposes dataPurposesInfo:(NSString *)dataPurposesInfo date:(NSString *)date decision:(NSString *)decision denied:(NSString *)denied descriptionOfService:(NSString *)descriptionOfService explicit:(NSString *)explicit_ firstLayerTitle:(NSString * _Nullable)firstLayerTitle furtherInformationOptOut:(NSString *)furtherInformationOptOut headerCenterSecondary:(NSString *)headerCenterSecondary headerCorner:(NSString *)headerCorner history:(NSString *)history historyDescription:(NSString *)historyDescription implicit:(NSString *)implicit imprintLinkText:(NSString *)imprintLinkText legalBasisList:(NSString *)legalBasisList legalBasisInfo:(NSString *)legalBasisInfo linkToDpaInfo:(NSString *)linkToDpaInfo locationOfProcessing:(NSString *)locationOfProcessing optOut:(NSString *)optOut partnerPoweredByLinkText:(NSString *)partnerPoweredByLinkText policyOf:(NSString *)policyOf poweredBy:(NSString *)poweredBy privacyPolicyLinkText:(NSString *)privacyPolicyLinkText processingCompanyTitle:(NSString *)processingCompanyTitle retentionPeriod:(NSString *)retentionPeriod showMore:(NSString *)showMore technologiesUsed:(NSString *)technologiesUsed technologiesUsedInfo:(NSString *)technologiesUsedInfo titleCenterSecondary:(NSString *)titleCenterSecondary titleCorner:(NSString *)titleCorner transferToThirdCountries:(NSString *)transferToThirdCountries __attribute__((swift_name("doCopy(accepted:btnAcceptAll:btnBannerReadMore:btnDeny:btnMore:btnSave:categories:consentType:cookiePolicyInfo:cookiePolicyLinkText:copy:dataCollectedInfo:dataCollectedList:dataRecipientsList:dataRecipientsListInfo:dataPurposes:dataPurposesInfo:date:decision:denied:descriptionOfService:explicit:firstLayerTitle:furtherInformationOptOut:headerCenterSecondary:headerCorner:history:historyDescription:implicit:imprintLinkText:legalBasisList:legalBasisInfo:linkToDpaInfo:locationOfProcessing:optOut:partnerPoweredByLinkText:policyOf:poweredBy:privacyPolicyLinkText:processingCompanyTitle:retentionPeriod:showMore:technologiesUsed:technologiesUsedInfo:titleCenterSecondary:titleCorner:transferToThirdCountries:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *accepted __attribute__((swift_name("accepted")));
@property NSString *btnAcceptAll __attribute__((swift_name("btnAcceptAll")));
@property NSString * _Nullable btnBannerReadMore __attribute__((swift_name("btnBannerReadMore")));
@property NSString *btnDeny __attribute__((swift_name("btnDeny")));
@property NSString *btnMore __attribute__((swift_name("btnMore")));
@property NSString *btnSave __attribute__((swift_name("btnSave")));
@property NSString *categories __attribute__((swift_name("categories")));
@property NSString *consentType __attribute__((swift_name("consentType")));
@property NSString *cookiePolicyInfo __attribute__((swift_name("cookiePolicyInfo")));
@property NSString *cookiePolicyLinkText __attribute__((swift_name("cookiePolicyLinkText")));
@property (getter=doCopy) NSString *copy __attribute__((swift_name("copy")));
@property NSString *dataCollectedInfo __attribute__((swift_name("dataCollectedInfo")));
@property NSString *dataCollectedList __attribute__((swift_name("dataCollectedList")));
@property NSString *dataPurposes __attribute__((swift_name("dataPurposes")));
@property NSString *dataPurposesInfo __attribute__((swift_name("dataPurposesInfo")));
@property NSString *dataRecipientsList __attribute__((swift_name("dataRecipientsList")));
@property NSString *dataRecipientsListInfo __attribute__((swift_name("dataRecipientsListInfo")));
@property NSString *date __attribute__((swift_name("date")));
@property NSString *decision __attribute__((swift_name("decision")));
@property NSString *denied __attribute__((swift_name("denied")));
@property NSString *descriptionOfService __attribute__((swift_name("descriptionOfService")));
@property (getter=explicit, setter=setExplicit:) NSString *explicit_ __attribute__((swift_name("explicit_")));
@property NSString * _Nullable firstLayerTitle __attribute__((swift_name("firstLayerTitle")));
@property NSString *furtherInformationOptOut __attribute__((swift_name("furtherInformationOptOut")));
@property NSString *headerCenterSecondary __attribute__((swift_name("headerCenterSecondary")));
@property NSString *headerCorner __attribute__((swift_name("headerCorner")));
@property NSString *history __attribute__((swift_name("history")));
@property NSString *historyDescription __attribute__((swift_name("historyDescription")));
@property NSString *implicit __attribute__((swift_name("implicit")));
@property NSString *imprintLinkText __attribute__((swift_name("imprintLinkText")));
@property NSString *legalBasisInfo __attribute__((swift_name("legalBasisInfo")));
@property NSString *legalBasisList __attribute__((swift_name("legalBasisList")));
@property NSString *linkToDpaInfo __attribute__((swift_name("linkToDpaInfo")));
@property NSString *locationOfProcessing __attribute__((swift_name("locationOfProcessing")));
@property NSString *optOut __attribute__((swift_name("optOut")));
@property NSString *partnerPoweredByLinkText __attribute__((swift_name("partnerPoweredByLinkText")));
@property NSString *policyOf __attribute__((swift_name("policyOf")));
@property NSString *poweredBy __attribute__((swift_name("poweredBy")));
@property NSString *privacyPolicyLinkText __attribute__((swift_name("privacyPolicyLinkText")));
@property NSString *processingCompanyTitle __attribute__((swift_name("processingCompanyTitle")));
@property NSString *retentionPeriod __attribute__((swift_name("retentionPeriod")));
@property NSString *showMore __attribute__((swift_name("showMore")));
@property NSString *technologiesUsed __attribute__((swift_name("technologiesUsed")));
@property NSString *technologiesUsedInfo __attribute__((swift_name("technologiesUsedInfo")));
@property NSString *titleCenterSecondary __attribute__((swift_name("titleCenterSecondary")));
@property NSString *titleCorner __attribute__((swift_name("titleCorner")));
@property NSString *transferToThirdCountries __attribute__((swift_name("transferToThirdCountries")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiLabels.Companion")))
@interface USDKApiLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiService")))
@interface USDKApiService : USDKBase <USDKApiBaseServiceInterface>
- (instancetype)initWithDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version categorySlug:(NSString *)categorySlug defaultConsentStatus:(BOOL)defaultConsentStatus isDeactivated:(BOOL)isDeactivated isHidden:(BOOL)isHidden subConsents:(NSArray<USDKApiBaseService *> *)subConsents __attribute__((swift_name("init(description:templateId:version:categorySlug:defaultConsentStatus:isDeactivated:isHidden:subConsents:)"))) __attribute__((objc_designated_initializer));
- (USDKApiService *)doCopyDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version categorySlug:(NSString *)categorySlug defaultConsentStatus:(BOOL)defaultConsentStatus isDeactivated:(BOOL)isDeactivated isHidden:(BOOL)isHidden subConsents:(NSArray<USDKApiBaseService *> *)subConsents __attribute__((swift_name("doCopy(description:templateId:version:categorySlug:defaultConsentStatus:isDeactivated:isHidden:subConsents:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *categorySlug __attribute__((swift_name("categorySlug")));
@property BOOL defaultConsentStatus __attribute__((swift_name("defaultConsentStatus")));
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property BOOL isDeactivated __attribute__((swift_name("isDeactivated")));
@property BOOL isHidden __attribute__((swift_name("isHidden")));
@property NSArray<USDKApiBaseService *> *subConsents __attribute__((swift_name("subConsents")));
@property NSString *templateId __attribute__((swift_name("templateId")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiService.Companion")))
@interface USDKApiServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiSettings")))
@interface USDKApiSettings : USDKBase
- (instancetype)initWithBackgroundOverlay:(NSArray<USDKApiBackgroundOverlay *> *)backgroundOverlay bannerMessage:(NSString *)bannerMessage bannerMobileDescription:(NSString * _Nullable)bannerMobileDescription bannerMobileDescriptionIsActive:(BOOL)bannerMobileDescriptionIsActive btnDenyIsVisible:(BOOL)btnDenyIsVisible btnMoreInfoActionSelection:(int32_t)btnMoreInfoActionSelection btnMoreInfoIsVisible:(BOOL)btnMoreInfoIsVisible categories:(NSArray<USDKApiCategory *> *)categories ccpa:(USDKApiCCPA * _Nullable)ccpa consentTemplates:(NSArray<USDKApiService *> *)consentTemplates dataExchangeOnPage:(NSArray<USDKApiDataExchangeSetting *> *)dataExchangeOnPage displayOnlyForEU:(BOOL)displayOnlyForEU enablePoweredBy:(BOOL)enablePoweredBy imprintUrl:(NSString * _Nullable)imprintUrl labels:(USDKApiLabels *)labels language:(NSString *)language languagesAvailable:(NSArray<NSString *> *)languagesAvailable moreInfoButtonUrl:(NSString * _Nullable)moreInfoButtonUrl partnerPoweredByUrl:(NSString * _Nullable)partnerPoweredByUrl privacyButtonIsVisible:(BOOL)privacyButtonIsVisible privacyPolicyUrl:(NSString * _Nullable)privacyPolicyUrl settingsId:(NSString *)settingsId showInitialViewForVersionChange:(NSArray<NSString *> *)showInitialViewForVersionChange showLanguageDropdown:(BOOL)showLanguageDropdown cookiePolicyURL:(NSString * _Nullable)cookiePolicyURL customization:(USDKApiCustomization *)customization tcf2:(USDKApiTCF2 *)tcf2 tcf2Enabled:(BOOL)tcf2Enabled version:(NSString *)version __attribute__((swift_name("init(backgroundOverlay:bannerMessage:bannerMobileDescription:bannerMobileDescriptionIsActive:btnDenyIsVisible:btnMoreInfoActionSelection:btnMoreInfoIsVisible:categories:ccpa:consentTemplates:dataExchangeOnPage:displayOnlyForEU:enablePoweredBy:imprintUrl:labels:language:languagesAvailable:moreInfoButtonUrl:partnerPoweredByUrl:privacyButtonIsVisible:privacyPolicyUrl:settingsId:showInitialViewForVersionChange:showLanguageDropdown:cookiePolicyURL:customization:tcf2:tcf2Enabled:version:)"))) __attribute__((objc_designated_initializer));
- (USDKApiSettings *)doCopyBackgroundOverlay:(NSArray<USDKApiBackgroundOverlay *> *)backgroundOverlay bannerMessage:(NSString *)bannerMessage bannerMobileDescription:(NSString * _Nullable)bannerMobileDescription bannerMobileDescriptionIsActive:(BOOL)bannerMobileDescriptionIsActive btnDenyIsVisible:(BOOL)btnDenyIsVisible btnMoreInfoActionSelection:(int32_t)btnMoreInfoActionSelection btnMoreInfoIsVisible:(BOOL)btnMoreInfoIsVisible categories:(NSArray<USDKApiCategory *> *)categories ccpa:(USDKApiCCPA * _Nullable)ccpa consentTemplates:(NSArray<USDKApiService *> *)consentTemplates dataExchangeOnPage:(NSArray<USDKApiDataExchangeSetting *> *)dataExchangeOnPage displayOnlyForEU:(BOOL)displayOnlyForEU enablePoweredBy:(BOOL)enablePoweredBy imprintUrl:(NSString * _Nullable)imprintUrl labels:(USDKApiLabels *)labels language:(NSString *)language languagesAvailable:(NSArray<NSString *> *)languagesAvailable moreInfoButtonUrl:(NSString * _Nullable)moreInfoButtonUrl partnerPoweredByUrl:(NSString * _Nullable)partnerPoweredByUrl privacyButtonIsVisible:(BOOL)privacyButtonIsVisible privacyPolicyUrl:(NSString * _Nullable)privacyPolicyUrl settingsId:(NSString *)settingsId showInitialViewForVersionChange:(NSArray<NSString *> *)showInitialViewForVersionChange showLanguageDropdown:(BOOL)showLanguageDropdown cookiePolicyURL:(NSString * _Nullable)cookiePolicyURL customization:(USDKApiCustomization *)customization tcf2:(USDKApiTCF2 *)tcf2 tcf2Enabled:(BOOL)tcf2Enabled version:(NSString *)version __attribute__((swift_name("doCopy(backgroundOverlay:bannerMessage:bannerMobileDescription:bannerMobileDescriptionIsActive:btnDenyIsVisible:btnMoreInfoActionSelection:btnMoreInfoIsVisible:categories:ccpa:consentTemplates:dataExchangeOnPage:displayOnlyForEU:enablePoweredBy:imprintUrl:labels:language:languagesAvailable:moreInfoButtonUrl:partnerPoweredByUrl:privacyButtonIsVisible:privacyPolicyUrl:settingsId:showInitialViewForVersionChange:showLanguageDropdown:cookiePolicyURL:customization:tcf2:tcf2Enabled:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKApiBackgroundOverlay *> *backgroundOverlay __attribute__((swift_name("backgroundOverlay")));
@property NSString *bannerMessage __attribute__((swift_name("bannerMessage")));
@property NSString * _Nullable bannerMobileDescription __attribute__((swift_name("bannerMobileDescription")));
@property BOOL bannerMobileDescriptionIsActive __attribute__((swift_name("bannerMobileDescriptionIsActive")));
@property BOOL btnDenyIsVisible __attribute__((swift_name("btnDenyIsVisible")));
@property int32_t btnMoreInfoActionSelection __attribute__((swift_name("btnMoreInfoActionSelection")));
@property BOOL btnMoreInfoIsVisible __attribute__((swift_name("btnMoreInfoIsVisible")));
@property NSArray<USDKApiCategory *> *categories __attribute__((swift_name("categories")));
@property USDKApiCCPA * _Nullable ccpa __attribute__((swift_name("ccpa")));
@property NSArray<USDKApiService *> *consentTemplates __attribute__((swift_name("consentTemplates")));
@property NSString * _Nullable cookiePolicyURL __attribute__((swift_name("cookiePolicyURL")));
@property USDKApiCustomization *customization __attribute__((swift_name("customization")));
@property NSArray<USDKApiDataExchangeSetting *> *dataExchangeOnPage __attribute__((swift_name("dataExchangeOnPage")));
@property BOOL displayOnlyForEU __attribute__((swift_name("displayOnlyForEU")));
@property BOOL enablePoweredBy __attribute__((swift_name("enablePoweredBy")));
@property NSString * _Nullable imprintUrl __attribute__((swift_name("imprintUrl")));
@property USDKApiLabels *labels __attribute__((swift_name("labels")));
@property NSString *language __attribute__((swift_name("language")));
@property NSArray<NSString *> *languagesAvailable __attribute__((swift_name("languagesAvailable")));
@property NSString * _Nullable moreInfoButtonUrl __attribute__((swift_name("moreInfoButtonUrl")));
@property NSString * _Nullable partnerPoweredByUrl __attribute__((swift_name("partnerPoweredByUrl")));
@property BOOL privacyButtonIsVisible __attribute__((swift_name("privacyButtonIsVisible")));
@property NSString * _Nullable privacyPolicyUrl __attribute__((swift_name("privacyPolicyUrl")));
@property NSString *settingsId __attribute__((swift_name("settingsId")));
@property NSArray<NSString *> *showInitialViewForVersionChange __attribute__((swift_name("showInitialViewForVersionChange")));
@property BOOL showLanguageDropdown __attribute__((swift_name("showLanguageDropdown")));
@property USDKApiTCF2 *tcf2 __attribute__((swift_name("tcf2")));
@property BOOL tcf2Enabled __attribute__((swift_name("tcf2Enabled")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiSettings.Companion")))
@interface USDKApiSettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiSettingsVersion")))
@interface USDKApiSettingsVersion : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKApiSettingsVersion *major __attribute__((swift_name("major")));
@property (class, readonly) USDKApiSettingsVersion *minor __attribute__((swift_name("minor")));
@property (class, readonly) USDKApiSettingsVersion *patch __attribute__((swift_name("patch")));
- (int32_t)compareToOther:(USDKApiSettingsVersion *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *s __attribute__((swift_name("s")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiStringToListProperties")))
@interface USDKApiStringToListProperties : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKApiStringToListProperties *dataCollectedList __attribute__((swift_name("dataCollectedList")));
@property (class, readonly) USDKApiStringToListProperties *dataPurposesList __attribute__((swift_name("dataPurposesList")));
@property (class, readonly) USDKApiStringToListProperties *dataRecipientsList __attribute__((swift_name("dataRecipientsList")));
@property (class, readonly) USDKApiStringToListProperties *technologyUsed __attribute__((swift_name("technologyUsed")));
- (int32_t)compareToOther:(USDKApiStringToListProperties *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *s __attribute__((swift_name("s")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiTCF2")))
@interface USDKApiTCF2 : USDKBase
- (instancetype)initWithAppLayerNoteResurface:(NSString * _Nullable)appLayerNoteResurface buttonsAcceptAllLabel:(NSString *)buttonsAcceptAllLabel buttonsDenyAllLabel:(NSString *)buttonsDenyAllLabel buttonsDenyAllIsEnabled:(BOOL)buttonsDenyAllIsEnabled buttonsSaveLabel:(NSString *)buttonsSaveLabel cmpId:(USDKInt * _Nullable)cmpId cmpVersion:(USDKInt * _Nullable)cmpVersion changedPurposes:(USDKApiTCF2ChangedPurposes * _Nullable)changedPurposes consensuDomain:(NSString * _Nullable)consensuDomain consensuScriptPath:(NSString * _Nullable)consensuScriptPath firstLayerAdditionalInfo:(NSString * _Nullable)firstLayerAdditionalInfo firstLayerDescription:(NSString *)firstLayerDescription firstLayerHideToggles:(BOOL)firstLayerHideToggles firstLayerTitle:(NSString *)firstLayerTitle gdprApplies:(BOOL)gdprApplies labelsDisclaimer:(NSString *)labelsDisclaimer labelsFeatures:(NSString *)labelsFeatures labelsIabVendors:(NSString *)labelsIabVendors labelsNonIabPurposes:(NSString *)labelsNonIabPurposes labelsNonIabVendors:(NSString *)labelsNonIabVendors labelsPurposes:(NSString *)labelsPurposes linksManageSettingsLabel:(NSString *)linksManageSettingsLabel linksVendorListLinkLabel:(NSString *)linksVendorListLinkLabel publisherCountryCode:(NSString *)publisherCountryCode purposeOneTreatment:(BOOL)purposeOneTreatment secondLayerDescription:(NSString *)secondLayerDescription secondLayerTitle:(NSString *)secondLayerTitle selectedVendorIds:(NSArray<USDKInt *> *)selectedVendorIds selectedStacks:(NSArray<USDKInt *> *)selectedStacks scope:(USDKTCFScope *)scope tabsPurposeLabel:(NSString *)tabsPurposeLabel tabsVendorsLabel:(NSString *)tabsVendorsLabel togglesConsentToggleLabel:(NSString *)togglesConsentToggleLabel togglesLegIntToggleLabel:(NSString *)togglesLegIntToggleLabel togglesSpecialFeaturesToggleOff:(NSString *)togglesSpecialFeaturesToggleOff togglesSpecialFeaturesToggleOn:(NSString *)togglesSpecialFeaturesToggleOn __attribute__((swift_name("init(appLayerNoteResurface:buttonsAcceptAllLabel:buttonsDenyAllLabel:buttonsDenyAllIsEnabled:buttonsSaveLabel:cmpId:cmpVersion:changedPurposes:consensuDomain:consensuScriptPath:firstLayerAdditionalInfo:firstLayerDescription:firstLayerHideToggles:firstLayerTitle:gdprApplies:labelsDisclaimer:labelsFeatures:labelsIabVendors:labelsNonIabPurposes:labelsNonIabVendors:labelsPurposes:linksManageSettingsLabel:linksVendorListLinkLabel:publisherCountryCode:purposeOneTreatment:secondLayerDescription:secondLayerTitle:selectedVendorIds:selectedStacks:scope:tabsPurposeLabel:tabsVendorsLabel:togglesConsentToggleLabel:togglesLegIntToggleLabel:togglesSpecialFeaturesToggleOff:togglesSpecialFeaturesToggleOn:)"))) __attribute__((objc_designated_initializer));
- (USDKApiTCF2 *)doCopyAppLayerNoteResurface:(NSString * _Nullable)appLayerNoteResurface buttonsAcceptAllLabel:(NSString *)buttonsAcceptAllLabel buttonsDenyAllLabel:(NSString *)buttonsDenyAllLabel buttonsDenyAllIsEnabled:(BOOL)buttonsDenyAllIsEnabled buttonsSaveLabel:(NSString *)buttonsSaveLabel cmpId:(USDKInt * _Nullable)cmpId cmpVersion:(USDKInt * _Nullable)cmpVersion changedPurposes:(USDKApiTCF2ChangedPurposes * _Nullable)changedPurposes consensuDomain:(NSString * _Nullable)consensuDomain consensuScriptPath:(NSString * _Nullable)consensuScriptPath firstLayerAdditionalInfo:(NSString * _Nullable)firstLayerAdditionalInfo firstLayerDescription:(NSString *)firstLayerDescription firstLayerHideToggles:(BOOL)firstLayerHideToggles firstLayerTitle:(NSString *)firstLayerTitle gdprApplies:(BOOL)gdprApplies labelsDisclaimer:(NSString *)labelsDisclaimer labelsFeatures:(NSString *)labelsFeatures labelsIabVendors:(NSString *)labelsIabVendors labelsNonIabPurposes:(NSString *)labelsNonIabPurposes labelsNonIabVendors:(NSString *)labelsNonIabVendors labelsPurposes:(NSString *)labelsPurposes linksManageSettingsLabel:(NSString *)linksManageSettingsLabel linksVendorListLinkLabel:(NSString *)linksVendorListLinkLabel publisherCountryCode:(NSString *)publisherCountryCode purposeOneTreatment:(BOOL)purposeOneTreatment secondLayerDescription:(NSString *)secondLayerDescription secondLayerTitle:(NSString *)secondLayerTitle selectedVendorIds:(NSArray<USDKInt *> *)selectedVendorIds selectedStacks:(NSArray<USDKInt *> *)selectedStacks scope:(USDKTCFScope *)scope tabsPurposeLabel:(NSString *)tabsPurposeLabel tabsVendorsLabel:(NSString *)tabsVendorsLabel togglesConsentToggleLabel:(NSString *)togglesConsentToggleLabel togglesLegIntToggleLabel:(NSString *)togglesLegIntToggleLabel togglesSpecialFeaturesToggleOff:(NSString *)togglesSpecialFeaturesToggleOff togglesSpecialFeaturesToggleOn:(NSString *)togglesSpecialFeaturesToggleOn __attribute__((swift_name("doCopy(appLayerNoteResurface:buttonsAcceptAllLabel:buttonsDenyAllLabel:buttonsDenyAllIsEnabled:buttonsSaveLabel:cmpId:cmpVersion:changedPurposes:consensuDomain:consensuScriptPath:firstLayerAdditionalInfo:firstLayerDescription:firstLayerHideToggles:firstLayerTitle:gdprApplies:labelsDisclaimer:labelsFeatures:labelsIabVendors:labelsNonIabPurposes:labelsNonIabVendors:labelsPurposes:linksManageSettingsLabel:linksVendorListLinkLabel:publisherCountryCode:purposeOneTreatment:secondLayerDescription:secondLayerTitle:selectedVendorIds:selectedStacks:scope:tabsPurposeLabel:tabsVendorsLabel:togglesConsentToggleLabel:togglesLegIntToggleLabel:togglesSpecialFeaturesToggleOff:togglesSpecialFeaturesToggleOn:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString * _Nullable appLayerNoteResurface __attribute__((swift_name("appLayerNoteResurface")));
@property NSString *buttonsAcceptAllLabel __attribute__((swift_name("buttonsAcceptAllLabel")));
@property BOOL buttonsDenyAllIsEnabled __attribute__((swift_name("buttonsDenyAllIsEnabled")));
@property NSString *buttonsDenyAllLabel __attribute__((swift_name("buttonsDenyAllLabel")));
@property NSString *buttonsSaveLabel __attribute__((swift_name("buttonsSaveLabel")));
@property USDKApiTCF2ChangedPurposes * _Nullable changedPurposes __attribute__((swift_name("changedPurposes")));
@property USDKInt * _Nullable cmpId __attribute__((swift_name("cmpId")));
@property USDKInt * _Nullable cmpVersion __attribute__((swift_name("cmpVersion")));
@property NSString * _Nullable consensuDomain __attribute__((swift_name("consensuDomain")));
@property NSString * _Nullable consensuScriptPath __attribute__((swift_name("consensuScriptPath")));
@property NSString * _Nullable firstLayerAdditionalInfo __attribute__((swift_name("firstLayerAdditionalInfo")));
@property NSString *firstLayerDescription __attribute__((swift_name("firstLayerDescription")));
@property BOOL firstLayerHideToggles __attribute__((swift_name("firstLayerHideToggles")));
@property NSString *firstLayerTitle __attribute__((swift_name("firstLayerTitle")));
@property BOOL gdprApplies __attribute__((swift_name("gdprApplies")));
@property NSString *labelsDisclaimer __attribute__((swift_name("labelsDisclaimer")));
@property NSString *labelsFeatures __attribute__((swift_name("labelsFeatures")));
@property NSString *labelsIabVendors __attribute__((swift_name("labelsIabVendors")));
@property NSString *labelsNonIabPurposes __attribute__((swift_name("labelsNonIabPurposes")));
@property NSString *labelsNonIabVendors __attribute__((swift_name("labelsNonIabVendors")));
@property NSString *labelsPurposes __attribute__((swift_name("labelsPurposes")));
@property NSString *linksManageSettingsLabel __attribute__((swift_name("linksManageSettingsLabel")));
@property NSString *linksVendorListLinkLabel __attribute__((swift_name("linksVendorListLinkLabel")));
@property NSString *publisherCountryCode __attribute__((swift_name("publisherCountryCode")));
@property BOOL purposeOneTreatment __attribute__((swift_name("purposeOneTreatment")));
@property USDKTCFScope *scope __attribute__((swift_name("scope")));
@property NSString *secondLayerDescription __attribute__((swift_name("secondLayerDescription")));
@property NSString *secondLayerTitle __attribute__((swift_name("secondLayerTitle")));
@property NSArray<USDKInt *> *selectedStacks __attribute__((swift_name("selectedStacks")));
@property NSArray<USDKInt *> *selectedVendorIds __attribute__((swift_name("selectedVendorIds")));
@property NSString *tabsPurposeLabel __attribute__((swift_name("tabsPurposeLabel")));
@property NSString *tabsVendorsLabel __attribute__((swift_name("tabsVendorsLabel")));
@property NSString *togglesConsentToggleLabel __attribute__((swift_name("togglesConsentToggleLabel")));
@property NSString *togglesLegIntToggleLabel __attribute__((swift_name("togglesLegIntToggleLabel")));
@property NSString *togglesSpecialFeaturesToggleOff __attribute__((swift_name("togglesSpecialFeaturesToggleOff")));
@property NSString *togglesSpecialFeaturesToggleOn __attribute__((swift_name("togglesSpecialFeaturesToggleOn")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiTCF2.Companion")))
@interface USDKApiTCF2Companion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiTCF2ChangedPurposes")))
@interface USDKApiTCF2ChangedPurposes : USDKBase
- (instancetype)initWithPurposes:(NSArray<USDKInt *> *)purposes legIntPurposes:(NSArray<USDKInt *> *)legIntPurposes __attribute__((swift_name("init(purposes:legIntPurposes:)"))) __attribute__((objc_designated_initializer));
- (USDKApiTCF2ChangedPurposes *)doCopyPurposes:(NSArray<USDKInt *> *)purposes legIntPurposes:(NSArray<USDKInt *> *)legIntPurposes __attribute__((swift_name("doCopy(purposes:legIntPurposes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKInt *> *legIntPurposes __attribute__((swift_name("legIntPurposes")));
@property NSArray<USDKInt *> *purposes __attribute__((swift_name("purposes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiTCF2ChangedPurposes.Companion")))
@interface USDKApiTCF2ChangedPurposesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentData")))
@interface USDKConsentData : USDKBase
- (instancetype)initWithConsentId:(NSString *)consentId __attribute__((swift_name("init(consentId:)"))) __attribute__((objc_designated_initializer));
- (USDKConsentData *)doCopyConsentId:(NSString *)consentId __attribute__((swift_name("doCopy(consentId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *consentId __attribute__((swift_name("consentId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentData.Companion")))
@interface USDKConsentDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentsList")))
@interface USDKConsentsList : USDKBase
- (instancetype)initWithData:(NSArray<USDKConsentData *> *)data __attribute__((swift_name("init(data:)"))) __attribute__((objc_designated_initializer));
- (USDKConsentsList *)doCopyData:(NSArray<USDKConsentData *> *)data __attribute__((swift_name("doCopy(data:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKConsentData *> *data __attribute__((swift_name("data")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentsList.Companion")))
@interface USDKConsentsListCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants__")))
@interface USDKConstants__ : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (readonly) NSString *AGGREGATOR_CDN __attribute__((swift_name("AGGREGATOR_CDN")));
@property (readonly) NSString *CDN __attribute__((swift_name("CDN")));
@property (readonly) NSString *CONSENTS __attribute__((swift_name("CONSENTS")));
@property (readonly) int64_t DEFAULT_TIMEOUT_MILLIS __attribute__((swift_name("DEFAULT_TIMEOUT_MILLIS")));
@property (readonly) NSString *FALLBACK_VERSION __attribute__((swift_name("FALLBACK_VERSION")));
@property (readonly) NSString *GRAPHQL __attribute__((swift_name("GRAPHQL")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetConsentsVariables")))
@interface USDKGetConsentsVariables : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId history:(BOOL)history __attribute__((swift_name("init(controllerId:history:)"))) __attribute__((objc_designated_initializer));
- (USDKGetConsentsVariables *)doCopyControllerId:(NSString *)controllerId history:(BOOL)history __attribute__((swift_name("doCopy(controllerId:history:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly) BOOL history __attribute__((swift_name("history")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetConsentsVariables.Companion")))
@interface USDKGetConsentsVariablesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLConsent")))
@interface USDKGraphQLConsent : USDKBase
- (instancetype)initWithAction:(NSString *)action appVersion:(NSString *)appVersion controllerId:(NSString *)controllerId consentStatus:(NSString *)consentStatus consentTemplateId:(NSString *)consentTemplateId consentTemplateVersion:(NSString *)consentTemplateVersion language:(NSString *)language processorId:(NSString *)processorId referrerControllerId:(NSString *)referrerControllerId settingsId:(NSString *)settingsId settingsVersion:(NSString *)settingsVersion updatedBy:(NSString *)updatedBy __attribute__((swift_name("init(action:appVersion:controllerId:consentStatus:consentTemplateId:consentTemplateVersion:language:processorId:referrerControllerId:settingsId:settingsVersion:updatedBy:)"))) __attribute__((objc_designated_initializer));
- (USDKGraphQLConsent *)doCopyAction:(NSString *)action appVersion:(NSString *)appVersion controllerId:(NSString *)controllerId consentStatus:(NSString *)consentStatus consentTemplateId:(NSString *)consentTemplateId consentTemplateVersion:(NSString *)consentTemplateVersion language:(NSString *)language processorId:(NSString *)processorId referrerControllerId:(NSString *)referrerControllerId settingsId:(NSString *)settingsId settingsVersion:(NSString *)settingsVersion updatedBy:(NSString *)updatedBy __attribute__((swift_name("doCopy(action:appVersion:controllerId:consentStatus:consentTemplateId:consentTemplateVersion:language:processorId:referrerControllerId:settingsId:settingsVersion:updatedBy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *action __attribute__((swift_name("action")));
@property (readonly) NSString *appVersion __attribute__((swift_name("appVersion")));
@property (readonly) NSString *consentStatus __attribute__((swift_name("consentStatus")));
@property (readonly) NSString *consentTemplateId __attribute__((swift_name("consentTemplateId")));
@property (readonly) NSString *consentTemplateVersion __attribute__((swift_name("consentTemplateVersion")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly) NSString *language __attribute__((swift_name("language")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) NSString *referrerControllerId __attribute__((swift_name("referrerControllerId")));
@property (readonly) NSString *settingsId __attribute__((swift_name("settingsId")));
@property (readonly) NSString *settingsVersion __attribute__((swift_name("settingsVersion")));
@property (readonly) NSString *updatedBy __attribute__((swift_name("updatedBy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLConsent.Companion")))
@interface USDKGraphQLConsentCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLConsentString")))
@interface USDKGraphQLConsentString : USDKBase
- (instancetype)initWithCCPA:(NSString * _Nullable)CCPA TCF2:(NSString * _Nullable)TCF2 __attribute__((swift_name("init(CCPA:TCF2:)"))) __attribute__((objc_designated_initializer));
- (USDKGraphQLConsentString *)doCopyCCPA:(NSString * _Nullable)CCPA TCF2:(NSString * _Nullable)TCF2 __attribute__((swift_name("doCopy(CCPA:TCF2:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable CCPA __attribute__((swift_name("CCPA")));
@property (readonly) NSString * _Nullable TCF2 __attribute__((swift_name("TCF2")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLConsentString.Companion")))
@interface USDKGraphQLConsentStringCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLQuery")))
@interface USDKGraphQLQuery : USDKBase
- (instancetype)initWithOperationName:(NSString *)operationName query:(NSString *)query variables:(USDKGetConsentsVariables * _Nullable)variables __attribute__((swift_name("init(operationName:query:variables:)"))) __attribute__((objc_designated_initializer));
- (USDKGraphQLQuery *)doCopyOperationName:(NSString *)operationName query:(NSString *)query variables:(USDKGetConsentsVariables * _Nullable)variables __attribute__((swift_name("doCopy(operationName:query:variables:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *operationName __attribute__((swift_name("operationName")));
@property (readonly) NSString *query __attribute__((swift_name("query")));
@property (readonly) USDKGetConsentsVariables * _Nullable variables __attribute__((swift_name("variables")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLQuery.Companion")))
@interface USDKGraphQLQueryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLQueryMutation")))
@interface USDKGraphQLQueryMutation : USDKBase
- (instancetype)initWithOperationName:(NSString *)operationName query:(NSString *)query variables:(USDKSaveConsentsVariables * _Nullable)variables __attribute__((swift_name("init(operationName:query:variables:)"))) __attribute__((objc_designated_initializer));
- (USDKGraphQLQueryMutation *)doCopyOperationName:(NSString *)operationName query:(NSString *)query variables:(USDKSaveConsentsVariables * _Nullable)variables __attribute__((swift_name("doCopy(operationName:query:variables:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *operationName __attribute__((swift_name("operationName")));
@property (readonly) NSString *query __attribute__((swift_name("query")));
@property (readonly) USDKSaveConsentsVariables * _Nullable variables __attribute__((swift_name("variables")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLQueryMutation.Companion")))
@interface USDKGraphQLQueryMutationCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HttpConstants")))
@interface USDKHttpConstants : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)httpConstants __attribute__((swift_name("init()")));
@property (readonly) NSString *CONNECTION_OFFLINE __attribute__((swift_name("CONNECTION_OFFLINE")));
@property (readonly) NSString *RESOURCE_NOT_FOUND __attribute__((swift_name("RESOURCE_NOT_FOUND")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LanguageData")))
@interface USDKLanguageData : USDKBase
- (instancetype)initWithLanguagesAvailable:(NSArray<NSString *> *)languagesAvailable editableLanguages:(NSArray<NSString *> *)editableLanguages __attribute__((swift_name("init(languagesAvailable:editableLanguages:)"))) __attribute__((objc_designated_initializer));
- (USDKLanguageData *)doCopyLanguagesAvailable:(NSArray<NSString *> *)languagesAvailable editableLanguages:(NSArray<NSString *> *)editableLanguages __attribute__((swift_name("doCopy(languagesAvailable:editableLanguages:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> *editableLanguages __attribute__((swift_name("editableLanguages")));
@property (readonly) NSArray<NSString *> *languagesAvailable __attribute__((swift_name("languagesAvailable")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LanguageData.Companion")))
@interface USDKLanguageDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OptionalOptions")))
@interface USDKOptionalOptions : USDKBase
- (instancetype)initWithCredentials:(id)credentials headers:(NSDictionary<NSString *, NSString *> *)headers mode:(id)mode __attribute__((swift_name("init(credentials:headers:mode:)"))) __attribute__((objc_designated_initializer));
- (USDKOptionalOptions *)doCopyCredentials:(id)credentials headers:(NSDictionary<NSString *, NSString *> *)headers mode:(id)mode __attribute__((swift_name("doCopy(credentials:headers:mode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id credentials __attribute__((swift_name("credentials")));
@property (readonly) NSDictionary<NSString *, NSString *> *headers __attribute__((swift_name("headers")));
@property (readonly) id mode __attribute__((swift_name("mode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OptionalOptions.Companion")))
@interface USDKOptionalOptionsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponseContainer")))
@interface USDKResponseContainer : USDKBase
- (instancetype)initWithData:(id _Nullable)data __attribute__((swift_name("init(data:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id _Nullable data __attribute__((swift_name("data")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponseContainer.Companion")))
@interface USDKResponseContainerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializerTypeParamsSerializers:(USDKKotlinArray *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializerTypeSerial0:(id<USDKKotlinx_serialization_runtimeKSerializer>)typeSerial0 __attribute__((swift_name("serializer(typeSerial0:)")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsentsData")))
@interface USDKSaveConsentsData : USDKBase
- (instancetype)initWithDataTransferObject:(USDKDataTransferObject *)dataTransferObject consentString:(USDKGraphQLConsentString * _Nullable)consentString __attribute__((swift_name("init(dataTransferObject:consentString:)"))) __attribute__((objc_designated_initializer));
- (USDKSaveConsentsData *)doCopyDataTransferObject:(USDKDataTransferObject *)dataTransferObject consentString:(USDKGraphQLConsentString * _Nullable)consentString __attribute__((swift_name("doCopy(dataTransferObject:consentString:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKGraphQLConsentString * _Nullable consentString __attribute__((swift_name("consentString")));
@property (readonly) USDKDataTransferObject *dataTransferObject __attribute__((swift_name("dataTransferObject")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsentsResponse")))
@interface USDKSaveConsentsResponse : USDKBase
- (instancetype)initWithSaveConsents:(USDKConsentsList *)saveConsents __attribute__((swift_name("init(saveConsents:)"))) __attribute__((objc_designated_initializer));
- (USDKSaveConsentsResponse *)doCopySaveConsents:(USDKConsentsList *)saveConsents __attribute__((swift_name("doCopy(saveConsents:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKConsentsList *saveConsents __attribute__((swift_name("saveConsents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsentsResponse.Companion")))
@interface USDKSaveConsentsResponseCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsentsVariables")))
@interface USDKSaveConsentsVariables : USDKBase
- (instancetype)initWithConsents:(NSArray<USDKGraphQLConsent *> *)consents consentString:(USDKGraphQLConsentString * _Nullable)consentString __attribute__((swift_name("init(consents:consentString:)"))) __attribute__((objc_designated_initializer));
- (USDKSaveConsentsVariables *)doCopyConsents:(NSArray<USDKGraphQLConsent *> *)consents consentString:(USDKGraphQLConsentString * _Nullable)consentString __attribute__((swift_name("doCopy(consents:consentString:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKGraphQLConsentString * _Nullable consentString __attribute__((swift_name("consentString")));
@property (readonly) NSArray<USDKGraphQLConsent *> *consents __attribute__((swift_name("consents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsentsVariables.Companion")))
@interface USDKSaveConsentsVariablesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceGenerateData")))
@interface USDKServiceGenerateData : USDKBase
- (instancetype)initWithFileName:(NSString *)fileName __attribute__((swift_name("init(fileName:)"))) __attribute__((objc_designated_initializer));
- (USDKServiceGenerateData *)doCopyFileName:(NSString *)fileName __attribute__((swift_name("doCopy(fileName:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *fileName __attribute__((swift_name("fileName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceGenerateData.Companion")))
@interface USDKServiceGenerateDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerializationStrategy")))
@protocol USDKKotlinx_serialization_runtimeSerializationStrategy
@required
- (void)serializeEncoder:(id<USDKKotlinx_serialization_runtimeEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeDeserializationStrategy")))
@protocol USDKKotlinx_serialization_runtimeDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (id _Nullable)patchDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder old:(id _Nullable)old __attribute__((swift_name("patch(decoder:old:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeKSerializer")))
@protocol USDKKotlinx_serialization_runtimeKSerializer <USDKKotlinx_serialization_runtimeSerializationStrategy, USDKKotlinx_serialization_runtimeDeserializationStrategy>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SettingsVersionSerializer")))
@interface USDKSettingsVersionSerializer : USDKBase <USDKKotlinx_serialization_runtimeKSerializer>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)settingsVersionSerializer __attribute__((swift_name("init()")));
- (USDKApiSettingsVersion *)deserializeDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (USDKApiSettingsVersion *)patchDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder old:(USDKApiSettingsVersion *)old __attribute__((swift_name("patch(decoder:old:)")));
- (void)serializeEncoder:(id<USDKKotlinx_serialization_runtimeEncoder>)encoder value:(USDKApiSettingsVersion *)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeJsonTransformingSerializer")))
@interface USDKKotlinx_serialization_runtimeJsonTransformingSerializer : USDKBase <USDKKotlinx_serialization_runtimeKSerializer>
- (instancetype)initWithTSerializer:(id<USDKKotlinx_serialization_runtimeKSerializer>)tSerializer transformationName:(NSString *)transformationName __attribute__((swift_name("init(tSerializer:transformationName:)"))) __attribute__((objc_designated_initializer));
- (id)deserializeDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (id)patchDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder old:(id)old __attribute__((swift_name("patch(decoder:old:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)readTransformElement:(USDKKotlinx_serialization_runtimeJsonElement *)element __attribute__((swift_name("readTransform(element:)")));
- (void)serializeEncoder:(id<USDKKotlinx_serialization_runtimeEncoder>)encoder value:(id)value __attribute__((swift_name("serialize(encoder:value:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)writeTransformElement:(USDKKotlinx_serialization_runtimeJsonElement *)element __attribute__((swift_name("writeTransform(element:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StringOrListSerializer")))
@interface USDKStringOrListSerializer : USDKKotlinx_serialization_runtimeJsonTransformingSerializer
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithTSerializer:(id<USDKKotlinx_serialization_runtimeKSerializer>)tSerializer transformationName:(NSString *)transformationName __attribute__((swift_name("init(tSerializer:transformationName:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)stringOrListSerializer __attribute__((swift_name("init()")));
- (NSArray<NSString *> *)deserializeDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (NSArray<NSString *> *)patchDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder old:(NSArray<NSString *> *)old __attribute__((swift_name("patch(decoder:old:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)readTransformElement:(USDKKotlinx_serialization_runtimeJsonElement *)element __attribute__((swift_name("readTransform(element:)")));
- (void)serializeEncoder:(id<USDKKotlinx_serialization_runtimeEncoder>)encoder value:(NSArray<NSString *> *)value __attribute__((swift_name("serialize(encoder:value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserConsentResponse")))
@interface USDKUserConsentResponse : USDKBase
- (instancetype)initWithConsentId:(NSString *)consentId status:(BOOL)status templateId:(NSString *)templateId timestampInSeconds:(NSString *)timestampInSeconds action:(NSString *)action updatedBy:(NSString *)updatedBy settingsVersion:(NSString *)settingsVersion __attribute__((swift_name("init(consentId:status:templateId:timestampInSeconds:action:updatedBy:settingsVersion:)"))) __attribute__((objc_designated_initializer));
- (USDKUserConsentResponse *)doCopyConsentId:(NSString *)consentId status:(BOOL)status templateId:(NSString *)templateId timestampInSeconds:(NSString *)timestampInSeconds action:(NSString *)action updatedBy:(NSString *)updatedBy settingsVersion:(NSString *)settingsVersion __attribute__((swift_name("doCopy(consentId:status:templateId:timestampInSeconds:action:updatedBy:settingsVersion:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *action __attribute__((swift_name("action")));
@property (readonly) NSString *consentId __attribute__((swift_name("consentId")));
@property (readonly) NSString *settingsVersion __attribute__((swift_name("settingsVersion")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@property (readonly) NSString *templateId __attribute__((swift_name("templateId")));
@property (readonly) NSString *timestampInSeconds __attribute__((swift_name("timestampInSeconds")));
@property (readonly) NSString *updatedBy __attribute__((swift_name("updatedBy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserConsentResponse.Companion")))
@interface USDKUserConsentResponseCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CustomAssets")))
@interface USDKCustomAssets : USDKBase
- (instancetype)initWithCustomFontRegular:(UIFont * _Nullable)customFontRegular customFontBold:(UIFont * _Nullable)customFontBold __attribute__((swift_name("init(customFontRegular:customFontBold:)"))) __attribute__((objc_designated_initializer));
- (USDKCustomAssets *)doCopyCustomFontRegular:(UIFont * _Nullable)customFontRegular customFontBold:(UIFont * _Nullable)customFontBold __attribute__((swift_name("doCopy(customFontRegular:customFontBold:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) UIFont * _Nullable customFontBold __attribute__((swift_name("customFontBold")));
@property (readonly) UIFont * _Nullable customFontRegular __attribute__((swift_name("customFontRegular")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitialUIValues")))
@interface USDKInitialUIValues : USDKBase
- (instancetype)initWithInitialLayer:(USDKInitialView *)initialLayer variant:(USDKUIVariant *)variant __attribute__((swift_name("init(initialLayer:variant:)"))) __attribute__((objc_designated_initializer));
- (USDKInitialUIValues *)doCopyInitialLayer:(USDKInitialView *)initialLayer variant:(USDKUIVariant *)variant __attribute__((swift_name("doCopy(initialLayer:variant:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKInitialView *initialLayer __attribute__((swift_name("initialLayer")));
@property USDKUIVariant *variant __attribute__((swift_name("variant")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitialView")))
@interface USDKInitialView : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKInitialView *firstLayer __attribute__((swift_name("firstLayer")));
@property (class, readonly) USDKInitialView *none __attribute__((swift_name("none")));
- (int32_t)compareToOther:(USDKInitialView *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("ResultCallback")))
@protocol USDKResultCallback
@required
- (void)onErrorError:(USDKUCError * _Nullable)error __attribute__((swift_name("onError(error:)")));
- (void)onSuccessResult:(id _Nullable)result __attribute__((swift_name("onSuccess(result:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UCError")))
@interface USDKUCError : USDKBase
- (instancetype)initWithMessage:(NSString *)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (USDKUCError *)doCopyMessage:(NSString *)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("doCopy(message:cause:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UIVariant")))
@interface USDKUIVariant : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKUIVariant *default_ __attribute__((swift_name("default_")));
@property (class, readonly) USDKUIVariant *ccpa __attribute__((swift_name("ccpa")));
@property (class, readonly) USDKUIVariant *tcf __attribute__((swift_name("tcf")));
- (int32_t)compareToOther:(USDKUIVariant *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserAgentInfo")))
@interface USDKUserAgentInfo : USDKBase
- (instancetype)initWithPlatform:(NSString *)platform osVersion:(NSString *)osVersion sdkVersion:(NSString *)sdkVersion appID:(NSString *)appID __attribute__((swift_name("init(platform:osVersion:sdkVersion:appID:)"))) __attribute__((objc_designated_initializer));
- (USDKUserAgentInfo *)doCopyPlatform:(NSString *)platform osVersion:(NSString *)osVersion sdkVersion:(NSString *)sdkVersion appID:(NSString *)appID __attribute__((swift_name("doCopy(platform:osVersion:sdkVersion:appID:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)toHeader __attribute__((swift_name("toHeader()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *appID __attribute__((swift_name("appID")));
@property NSString *osVersion __attribute__((swift_name("osVersion")));
@property NSString *platform __attribute__((swift_name("platform")));
@property NSString *sdkVersion __attribute__((swift_name("sdkVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserDecision")))
@interface USDKUserDecision : USDKBase
- (instancetype)initWithServiceId:(NSString *)serviceId status:(BOOL)status __attribute__((swift_name("init(serviceId:status:)"))) __attribute__((objc_designated_initializer));
- (USDKUserDecision *)doCopyServiceId:(NSString *)serviceId status:(BOOL)status __attribute__((swift_name("doCopy(serviceId:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *serviceId __attribute__((swift_name("serviceId")));
@property BOOL status __attribute__((swift_name("status")));
@end;

__attribute__((swift_name("ParcelizeParcelable")))
@protocol USDKParcelizeParcelable
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserOptions")))
@interface USDKUserOptions : USDKBase <USDKParcelizeParcelable>
- (instancetype)initWithControllerId:(NSString * _Nullable)controllerId defaultLanguage:(NSString * _Nullable)defaultLanguage version:(NSString * _Nullable)version debugMode:(USDKBoolean * _Nullable)debugMode predefinedUI:(USDKBoolean * _Nullable)predefinedUI timeoutMillis:(USDKLong * _Nullable)timeoutMillis noCache:(USDKBoolean * _Nullable)noCache __attribute__((swift_name("init(controllerId:defaultLanguage:version:debugMode:predefinedUI:timeoutMillis:noCache:)"))) __attribute__((objc_designated_initializer));
- (USDKUserOptions *)doCopyControllerId:(NSString * _Nullable)controllerId defaultLanguage:(NSString * _Nullable)defaultLanguage version:(NSString * _Nullable)version debugMode:(USDKBoolean * _Nullable)debugMode predefinedUI:(USDKBoolean * _Nullable)predefinedUI timeoutMillis:(USDKLong * _Nullable)timeoutMillis noCache:(USDKBoolean * _Nullable)noCache __attribute__((swift_name("doCopy(controllerId:defaultLanguage:version:debugMode:predefinedUI:timeoutMillis:noCache:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable controllerId __attribute__((swift_name("controllerId")));
@property (readonly) USDKBoolean * _Nullable debugMode __attribute__((swift_name("debugMode")));
@property (readonly) NSString * _Nullable defaultLanguage __attribute__((swift_name("defaultLanguage")));
@property (readonly) USDKBoolean * _Nullable noCache __attribute__((swift_name("noCache")));
@property (readonly) USDKBoolean * _Nullable predefinedUI __attribute__((swift_name("predefinedUI")));
@property (readonly) USDKLong * _Nullable timeoutMillis __attribute__((swift_name("timeoutMillis")));
@property (readonly) NSString * _Nullable version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAButtons")))
@interface USDKCCPAButtons : USDKBase
- (instancetype)initWithSave:(USDKLabel *)save showSecondLayer:(USDKButton *)showSecondLayer optOutNotice:(USDKLabel *)optOutNotice __attribute__((swift_name("init(save:showSecondLayer:optOutNotice:)"))) __attribute__((objc_designated_initializer));
- (USDKCCPAButtons *)doCopySave:(USDKLabel *)save showSecondLayer:(USDKButton *)showSecondLayer optOutNotice:(USDKLabel *)optOutNotice __attribute__((swift_name("doCopy(save:showSecondLayer:optOutNotice:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKLabel *optOutNotice __attribute__((swift_name("optOutNotice")));
@property (readonly) USDKLabel *save __attribute__((swift_name("save")));
@property (readonly) USDKButton *showSecondLayer __attribute__((swift_name("showSecondLayer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAButtons.Companion")))
@interface USDKCCPAButtonsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAErrors")))
@interface USDKCCPAErrors : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)cCPAErrors __attribute__((swift_name("init()")));
@property (readonly) NSString *SETTINGS_UNDEFINED __attribute__((swift_name("SETTINGS_UNDEFINED")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAFirstLayer")))
@interface USDKCCPAFirstLayer : USDKBase
- (instancetype)initWithDescription:(USDKFirstLayerDescription *)description isLanguageSelectorEnabled:(BOOL)isLanguageSelectorEnabled isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title variant:(USDKFirstLayerVariant *)variant __attribute__((swift_name("init(description:isLanguageSelectorEnabled:isOverlayEnabled:title:variant:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDescription:(USDKFirstLayerDescription *)description firstLayerDescription:(USDKFirstLayerDescription *)firstLayerDescription isLanguageSelectorEnabled:(BOOL)isLanguageSelectorEnabled isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title variant:(USDKFirstLayerVariant *)variant __attribute__((swift_name("init(description:firstLayerDescription:isLanguageSelectorEnabled:isOverlayEnabled:title:variant:)"))) __attribute__((objc_designated_initializer));
- (USDKCCPAFirstLayer *)doCopyDescription:(USDKFirstLayerDescription *)description firstLayerDescription:(USDKFirstLayerDescription *)firstLayerDescription isLanguageSelectorEnabled:(BOOL)isLanguageSelectorEnabled isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title variant:(USDKFirstLayerVariant *)variant __attribute__((swift_name("doCopy(description:firstLayerDescription:isLanguageSelectorEnabled:isOverlayEnabled:title:variant:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) USDKFirstLayerDescription *description __attribute__((swift_name("description")));
@property (readonly) USDKFirstLayerDescription *firstLayerDescription __attribute__((swift_name("firstLayerDescription")));
@property (readonly) BOOL isLanguageSelectorEnabled __attribute__((swift_name("isLanguageSelectorEnabled")));
@property (readonly) BOOL isOverlayEnabled __attribute__((swift_name("isOverlayEnabled")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@property (readonly) USDKFirstLayerVariant *variant __attribute__((swift_name("variant")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAFirstLayer.Companion")))
@interface USDKCCPAFirstLayerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAOptions")))
@interface USDKCCPAOptions : USDKBase
- (instancetype)initWithIabAgreementExists:(BOOL)iabAgreementExists isActive:(BOOL)isActive region:(USDKCCPARegion *)region reshowAfterDays:(int32_t)reshowAfterDays showOnPageLoad:(BOOL)showOnPageLoad __attribute__((swift_name("init(iabAgreementExists:isActive:region:reshowAfterDays:showOnPageLoad:)"))) __attribute__((objc_designated_initializer));
- (USDKCCPAOptions *)doCopyIabAgreementExists:(BOOL)iabAgreementExists isActive:(BOOL)isActive region:(USDKCCPARegion *)region reshowAfterDays:(int32_t)reshowAfterDays showOnPageLoad:(BOOL)showOnPageLoad __attribute__((swift_name("doCopy(iabAgreementExists:isActive:region:reshowAfterDays:showOnPageLoad:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL iabAgreementExists __attribute__((swift_name("iabAgreementExists")));
@property BOOL isActive __attribute__((swift_name("isActive")));
@property USDKCCPARegion *region __attribute__((swift_name("region")));
@property int32_t reshowAfterDays __attribute__((swift_name("reshowAfterDays")));
@property BOOL showOnPageLoad __attribute__((swift_name("showOnPageLoad")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPARegion")))
@interface USDKCCPARegion : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKCCPARegion *usCaOnly __attribute__((swift_name("usCaOnly")));
@property (class, readonly) USDKCCPARegion *us __attribute__((swift_name("us")));
- (int32_t)compareToOther:(USDKCCPARegion *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPASecondLayer")))
@interface USDKCCPASecondLayer : USDKBase
- (instancetype)initWithDescription:(NSString *)description isLanguageSelectorEnabled:(BOOL)isLanguageSelectorEnabled isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTabs *)tabs title:(NSString *)title variant:(USDKSecondLayerVariant *)variant __attribute__((swift_name("init(description:isLanguageSelectorEnabled:isOverlayEnabled:tabs:title:variant:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDescription:(NSString *)description secondLayerDescription:(NSString *)secondLayerDescription isLanguageSelectorEnabled:(BOOL)isLanguageSelectorEnabled isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTabs *)tabs title:(NSString *)title variant:(USDKSecondLayerVariant *)variant __attribute__((swift_name("init(description:secondLayerDescription:isLanguageSelectorEnabled:isOverlayEnabled:tabs:title:variant:)"))) __attribute__((objc_designated_initializer));
- (USDKCCPASecondLayer *)doCopyDescription:(NSString *)description secondLayerDescription:(NSString *)secondLayerDescription isLanguageSelectorEnabled:(BOOL)isLanguageSelectorEnabled isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTabs *)tabs title:(NSString *)title variant:(USDKSecondLayerVariant *)variant __attribute__((swift_name("doCopy(description:secondLayerDescription:isLanguageSelectorEnabled:isOverlayEnabled:tabs:title:variant:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) BOOL isLanguageSelectorEnabled __attribute__((swift_name("isLanguageSelectorEnabled")));
@property (readonly) BOOL isOverlayEnabled __attribute__((swift_name("isOverlayEnabled")));
@property (readonly) NSString *secondLayerDescription __attribute__((swift_name("secondLayerDescription")));
@property (readonly) USDKTabs *tabs __attribute__((swift_name("tabs")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@property (readonly) USDKSecondLayerVariant *variant __attribute__((swift_name("variant")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPASecondLayer.Companion")))
@interface USDKCCPASecondLayerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAUISettings")))
@interface USDKCCPAUISettings : USDKBase <USDKUISettingsInterface>
- (instancetype)initWithCustomization:(USDKCustomization *)customization isEmbeddingsEnabled:(BOOL)isEmbeddingsEnabled language:(USDKLanguage *)language links:(USDKLinks *)links poweredBy:(USDKPoweredBy *)poweredBy privacyButton:(USDKButton *)privacyButton buttons:(USDKCCPAButtons *)buttons firstLayer:(USDKCCPAFirstLayer *)firstLayer labels:(USDKLabels *)labels secondLayer:(USDKCCPASecondLayer *)secondLayer __attribute__((swift_name("init(customization:isEmbeddingsEnabled:language:links:poweredBy:privacyButton:buttons:firstLayer:labels:secondLayer:)"))) __attribute__((objc_designated_initializer));
- (USDKCCPAUISettings *)doCopyCustomization:(USDKCustomization *)customization isEmbeddingsEnabled:(BOOL)isEmbeddingsEnabled language:(USDKLanguage *)language links:(USDKLinks *)links poweredBy:(USDKPoweredBy *)poweredBy privacyButton:(USDKButton *)privacyButton buttons:(USDKCCPAButtons *)buttons firstLayer:(USDKCCPAFirstLayer *)firstLayer labels:(USDKLabels *)labels secondLayer:(USDKCCPASecondLayer *)secondLayer __attribute__((swift_name("doCopy(customization:isEmbeddingsEnabled:language:links:poweredBy:privacyButton:buttons:firstLayer:labels:secondLayer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKCCPAButtons *buttons __attribute__((swift_name("buttons")));
@property (readonly) USDKCustomization *customization __attribute__((swift_name("customization")));
@property (readonly) USDKCCPAFirstLayer *firstLayer __attribute__((swift_name("firstLayer")));
@property (readonly) BOOL isEmbeddingsEnabled __attribute__((swift_name("isEmbeddingsEnabled")));
@property (readonly) USDKLabels *labels __attribute__((swift_name("labels")));
@property (readonly) USDKLanguage *language __attribute__((swift_name("language")));
@property (readonly) USDKLinks *links __attribute__((swift_name("links")));
@property (readonly) USDKPoweredBy *poweredBy __attribute__((swift_name("poweredBy")));
@property (readonly) USDKButton *privacyButton __attribute__((swift_name("privacyButton")));
@property (readonly) USDKCCPASecondLayer *secondLayer __attribute__((swift_name("secondLayer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAUISettings.Companion")))
@interface USDKCCPAUISettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayerVariant")))
@interface USDKFirstLayerVariant : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKFirstLayerVariant *banner __attribute__((swift_name("banner")));
@property (class, readonly) USDKFirstLayerVariant *wall __attribute__((swift_name("wall")));
- (int32_t)compareToOther:(USDKFirstLayerVariant *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SecondLayerVariant")))
@interface USDKSecondLayerVariant : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKSecondLayerVariant *center __attribute__((swift_name("center")));
@property (class, readonly) USDKSecondLayerVariant *side __attribute__((swift_name("side")));
- (int32_t)compareToOther:(USDKSecondLayerVariant *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WarningMessages")))
@interface USDKWarningMessages : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKWarningMessages *defaultDeviceLanguage __attribute__((swift_name("defaultDeviceLanguage")));
@property (class, readonly) USDKWarningMessages *defaultSdkLanguage __attribute__((swift_name("defaultSdkLanguage")));
- (int32_t)compareToOther:(USDKWarningMessages *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Buttons")))
@interface USDKButtons : USDKBase
- (instancetype)initWithAcceptAll:(USDKLabel *)acceptAll denyAll:(USDKButton *)denyAll save:(USDKLabel *)save showSecondLayer:(USDKButton *)showSecondLayer __attribute__((swift_name("init(acceptAll:denyAll:save:showSecondLayer:)"))) __attribute__((objc_designated_initializer));
- (USDKButtons *)doCopyAcceptAll:(USDKLabel *)acceptAll denyAll:(USDKButton *)denyAll save:(USDKLabel *)save showSecondLayer:(USDKButton *)showSecondLayer __attribute__((swift_name("doCopy(acceptAll:denyAll:save:showSecondLayer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKLabel *acceptAll __attribute__((swift_name("acceptAll")));
@property (readonly) USDKButton *denyAll __attribute__((swift_name("denyAll")));
@property (readonly) USDKLabel *save __attribute__((swift_name("save")));
@property (readonly) USDKButton *showSecondLayer __attribute__((swift_name("showSecondLayer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Buttons.Companion")))
@interface USDKButtonsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayer")))
@interface USDKFirstLayer : USDKBase
- (instancetype)initWithDescription:(USDKFirstLayerDescription *)description isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title __attribute__((swift_name("init(description:isOverlayEnabled:title:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDescription:(USDKFirstLayerDescription *)description firstLayerDescription:(USDKFirstLayerDescription *)firstLayerDescription isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title __attribute__((swift_name("init(description:firstLayerDescription:isOverlayEnabled:title:)"))) __attribute__((objc_designated_initializer));
- (USDKFirstLayer *)doCopyDescription:(USDKFirstLayerDescription *)description firstLayerDescription:(USDKFirstLayerDescription *)firstLayerDescription isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title __attribute__((swift_name("doCopy(description:firstLayerDescription:isOverlayEnabled:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) USDKFirstLayerDescription *description __attribute__((swift_name("description")));
@property (readonly) USDKFirstLayerDescription *firstLayerDescription __attribute__((swift_name("firstLayerDescription")));
@property (readonly) BOOL isOverlayEnabled __attribute__((swift_name("isOverlayEnabled")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayer.Companion")))
@interface USDKFirstLayerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayerDescription")))
@interface USDKFirstLayerDescription : USDKBase
- (instancetype)initWithDefault:(NSString *)default_ short:(NSString * _Nullable)short_ isShortEnabled:(BOOL)isShortEnabled __attribute__((swift_name("init(default:short:isShortEnabled:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDefault:(NSString *)default_ short:(NSString * _Nullable)short_ isShortEnabled:(BOOL)isShortEnabled defaultDescription:(NSString *)defaultDescription shortDescription:(NSString * _Nullable)shortDescription __attribute__((swift_name("init(default:short:isShortEnabled:defaultDescription:shortDescription:)"))) __attribute__((objc_designated_initializer));
- (USDKFirstLayerDescription *)doCopyDefault:(NSString *)default_ short:(NSString * _Nullable)short_ isShortEnabled:(BOOL)isShortEnabled defaultDescription:(NSString *)defaultDescription shortDescription:(NSString * _Nullable)shortDescription __attribute__((swift_name("doCopy(default:short:isShortEnabled:defaultDescription:shortDescription:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=default) NSString *default_ __attribute__((swift_name("default_")));
@property (readonly) NSString *defaultDescription __attribute__((swift_name("defaultDescription")));
@property (readonly) BOOL isShortEnabled __attribute__((swift_name("isShortEnabled")));
@property (readonly, getter=short) NSString * _Nullable short_ __attribute__((swift_name("short_")));
@property (readonly) NSString * _Nullable shortDescription __attribute__((swift_name("shortDescription")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayerDescription.Companion")))
@interface USDKFirstLayerDescriptionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Labels")))
@interface USDKLabels : USDKBase
- (instancetype)initWithGeneral:(USDKGeneralLabels *)general service:(USDKServiceLabels *)service __attribute__((swift_name("init(general:service:)"))) __attribute__((objc_designated_initializer));
- (USDKLabels *)doCopyGeneral:(USDKGeneralLabels *)general service:(USDKServiceLabels *)service __attribute__((swift_name("doCopy(general:service:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKGeneralLabels *general __attribute__((swift_name("general")));
@property (readonly) USDKServiceLabels *service __attribute__((swift_name("service")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Labels.Companion")))
@interface USDKLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SecondLayer")))
@interface USDKSecondLayer : USDKBase
- (instancetype)initWithTitle:(NSString *)title description:(NSString *)description isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTabs *)tabs __attribute__((swift_name("init(title:description:isOverlayEnabled:tabs:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDescription:(NSString *)description isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTabs *)tabs title:(NSString *)title secondLayerDescription:(NSString *)secondLayerDescription __attribute__((swift_name("init(description:isOverlayEnabled:tabs:title:secondLayerDescription:)"))) __attribute__((objc_designated_initializer));
- (USDKSecondLayer *)doCopyDescription:(NSString *)description isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTabs *)tabs title:(NSString *)title secondLayerDescription:(NSString *)secondLayerDescription __attribute__((swift_name("doCopy(description:isOverlayEnabled:tabs:title:secondLayerDescription:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) BOOL isOverlayEnabled __attribute__((swift_name("isOverlayEnabled")));
@property (readonly) NSString *secondLayerDescription __attribute__((swift_name("secondLayerDescription")));
@property (readonly) USDKTabs *tabs __attribute__((swift_name("tabs")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SecondLayer.Companion")))
@interface USDKSecondLayerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Tabs")))
@interface USDKTabs : USDKBase
- (instancetype)initWithCategories:(USDKButton *)categories services:(USDKButton *)services __attribute__((swift_name("init(categories:services:)"))) __attribute__((objc_designated_initializer));
- (USDKTabs *)doCopyCategories:(USDKButton *)categories services:(USDKButton *)services __attribute__((swift_name("doCopy(categories:services:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKButton *categories __attribute__((swift_name("categories")));
@property (readonly) USDKButton *services __attribute__((swift_name("services")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Tabs.Companion")))
@interface USDKTabsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UISettings")))
@interface USDKUISettings : USDKBase <USDKUISettingsInterface>
- (instancetype)initWithCustomization:(USDKCustomization *)customization isEmbeddingsEnabled:(BOOL)isEmbeddingsEnabled language:(USDKLanguage *)language links:(USDKLinks *)links poweredBy:(USDKPoweredBy *)poweredBy privacyButton:(USDKButton *)privacyButton buttons:(USDKButtons *)buttons firstLayer:(USDKFirstLayer *)firstLayer labels:(USDKLabels *)labels secondLayer:(USDKSecondLayer *)secondLayer __attribute__((swift_name("init(customization:isEmbeddingsEnabled:language:links:poweredBy:privacyButton:buttons:firstLayer:labels:secondLayer:)"))) __attribute__((objc_designated_initializer));
- (USDKUISettings *)doCopyCustomization:(USDKCustomization *)customization isEmbeddingsEnabled:(BOOL)isEmbeddingsEnabled language:(USDKLanguage *)language links:(USDKLinks *)links poweredBy:(USDKPoweredBy *)poweredBy privacyButton:(USDKButton *)privacyButton buttons:(USDKButtons *)buttons firstLayer:(USDKFirstLayer *)firstLayer labels:(USDKLabels *)labels secondLayer:(USDKSecondLayer *)secondLayer __attribute__((swift_name("doCopy(customization:isEmbeddingsEnabled:language:links:poweredBy:privacyButton:buttons:firstLayer:labels:secondLayer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKButtons *buttons __attribute__((swift_name("buttons")));
@property (readonly) USDKCustomization *customization __attribute__((swift_name("customization")));
@property (readonly) USDKFirstLayer *firstLayer __attribute__((swift_name("firstLayer")));
@property (readonly) BOOL isEmbeddingsEnabled __attribute__((swift_name("isEmbeddingsEnabled")));
@property (readonly) USDKLabels *labels __attribute__((swift_name("labels")));
@property (readonly) USDKLanguage *language __attribute__((swift_name("language")));
@property (readonly) USDKLinks *links __attribute__((swift_name("links")));
@property (readonly) USDKPoweredBy *poweredBy __attribute__((swift_name("poweredBy")));
@property (readonly) USDKButton *privacyButton __attribute__((swift_name("privacyButton")));
@property (readonly) USDKSecondLayer *secondLayer __attribute__((swift_name("secondLayer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UISettings.Companion")))
@interface USDKUISettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("Card")))
@interface USDKCard : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("CustomButton")))
@interface USDKCustomButton : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("CustomIcon")))
@interface USDKCustomIcon : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("CustomScrollView")))
@interface USDKCustomScrollView : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("CustomSwitch")))
@interface USDKCustomSwitch : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("HistoryCellContainer")))
@interface USDKHistoryCellContainer : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("HistoryTable")))
@interface USDKHistoryTable : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("LinkGestureRecognizer")))
@interface USDKLinkGestureRecognizer : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("Picker")))
@interface USDKPicker : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("Select")))
@interface USDKSelect : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("ServiceSectionContainer")))
@interface USDKServiceSectionContainer : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("TagsView")))
@interface USDKTagsView : NSObject
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("ToggleHistoryTable")))
@interface USDKToggleHistoryTable : NSObject
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Language_")))
@interface USDKLanguage_ : USDKBase
- (instancetype)initWithLanguageApi:(USDKLanguageApi *)languageApi storage:(USDKDeviceStorage *)storage noCacheFlag:(BOOL)noCacheFlag logger:(USDKUsercentricsLogger *)logger __attribute__((swift_name("init(languageApi:storage:noCacheFlag:logger:)"))) __attribute__((objc_designated_initializer));
- (void)resolveLanguageSettingsId:(NSString *)settingsId jsonFileVersion:(NSString *)jsonFileVersion jsonFileLanguage:(NSString *)jsonFileLanguage callback:(void (^)(NSString *))callback onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("resolveLanguage(settingsId:jsonFileVersion:jsonFileLanguage:callback:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Language_.Companion")))
@interface USDKLanguage_Companion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) int64_t CACHE_MAX_DURATION_MILLIS __attribute__((swift_name("CACHE_MAX_DURATION_MILLIS")));
@property (readonly) NSString *LIST_LANGUAGES_CACHE __attribute__((swift_name("LIST_LANGUAGES_CACHE")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("APIArgs")))
@interface USDKAPIArgs : USDKBase
- (instancetype)initWithCommand:(NSString *)command version:(int32_t)version callback:(void (^)(NSArray<id> *))callback params:(NSArray<id> *)params __attribute__((swift_name("init(command:version:callback:params:)"))) __attribute__((objc_designated_initializer));
- (USDKAPIArgs *)doCopyCommand:(NSString *)command version:(int32_t)version callback:(void (^)(NSArray<id> *))callback params:(NSArray<id> *)params __attribute__((swift_name("doCopy(command:version:callback:params:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property void (^callback)(NSArray<id> *) __attribute__((swift_name("callback")));
@property NSString *command __attribute__((swift_name("command")));
@property NSArray<id> *params __attribute__((swift_name("params")));
@property int32_t version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CallResponder")))
@interface USDKCallResponder : USDKBase
- (instancetype)initWithCustomCommands:(USDKCustomCommands * _Nullable)customCommands storage:(USDKDeviceStorage *)storage jsonHttpClient:(id<USDKJsonHttpClient>)jsonHttpClient __attribute__((swift_name("init(customCommands:storage:jsonHttpClient:)"))) __attribute__((objc_designated_initializer));
- (void)apiCallCommandName:(NSString *)commandName version:(int32_t)version callback:(void (^)(NSArray<id> *))callback params:(NSArray<id> *)params __attribute__((swift_name("apiCall(commandName:version:callback:params:)")));
- (void)purgeQueuedCalls __attribute__((swift_name("purgeQueuedCalls()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CmpApi")))
@interface USDKCmpApi : USDKBase
- (instancetype)initWithCmpId:(int32_t)cmpId cmpVersion:(int32_t)cmpVersion isServiceSpecific:(BOOL)isServiceSpecific customCommands:(USDKCustomCommands * _Nullable)customCommands storage:(USDKDeviceStorage *)storage jsonHttpClient:(id<USDKJsonHttpClient>)jsonHttpClient __attribute__((swift_name("init(cmpId:cmpVersion:isServiceSpecific:customCommands:storage:jsonHttpClient:)"))) __attribute__((objc_designated_initializer));
- (void)disable __attribute__((swift_name("disable()")));
- (void)updateEncodedTCString:(NSString * _Nullable)encodedTCString uiVisible:(BOOL)uiVisible __attribute__((swift_name("update(encodedTCString:uiVisible:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CmpApiModel")))
@interface USDKCmpApiModel : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CmpApiModel.Companion")))
@interface USDKCmpApiModelCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (void)reset __attribute__((swift_name("reset()")));
@property (readonly) NSString *apiVersion __attribute__((swift_name("apiVersion")));
@property USDKInt * _Nullable cmpId __attribute__((swift_name("cmpId")));
@property USDKCmpStatus *cmpStatus __attribute__((swift_name("cmpStatus")));
@property USDKInt * _Nullable cmpVersion __attribute__((swift_name("cmpVersion")));
@property BOOL disabled __attribute__((swift_name("disabled")));
@property USDKDisplayStatus *displayStatus __attribute__((swift_name("displayStatus")));
@property (readonly) USDKEventListenerQueue *eventQueue __attribute__((swift_name("eventQueue")));
@property USDKEventStatus * _Nullable eventStatus __attribute__((swift_name("eventStatus")));
@property USDKBoolean * _Nullable gdprApplies __attribute__((swift_name("gdprApplies")));
@property USDKTCModel * _Nullable tcModel __attribute__((swift_name("tcModel")));
@property NSString * _Nullable tcString __attribute__((swift_name("tcString")));
@property (readonly) int32_t tcfPolicyVersion __attribute__((swift_name("tcfPolicyVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CmpStatus")))
@interface USDKCmpStatus : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKCmpStatus *stub __attribute__((swift_name("stub")));
@property (class, readonly) USDKCmpStatus *loading __attribute__((swift_name("loading")));
@property (class, readonly) USDKCmpStatus *loaded __attribute__((swift_name("loaded")));
@property (class, readonly) USDKCmpStatus *error __attribute__((swift_name("error")));
- (int32_t)compareToOther:(USDKCmpStatus *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CustomCommands")))
@interface USDKCustomCommands : USDKBase
- (instancetype)initWithCommands:(NSDictionary<NSString *, USDKKotlinUnit *(^)(NSArray<id> *)> *)commands __attribute__((swift_name("init(commands:)"))) __attribute__((objc_designated_initializer));
- (USDKCustomCommands *)doCopyCommands:(NSDictionary<NSString *, USDKKotlinUnit *(^)(NSArray<id> *)> *)commands __attribute__((swift_name("doCopy(commands:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKKotlinUnit *(^)(NSArray<id> *)> *commands __attribute__((swift_name("commands")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DisplayStatus")))
@interface USDKDisplayStatus : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKDisplayStatus *visible __attribute__((swift_name("visible")));
@property (class, readonly) USDKDisplayStatus *hidden __attribute__((swift_name("hidden")));
@property (class, readonly) USDKDisplayStatus *disabled __attribute__((swift_name("disabled")));
- (int32_t)compareToOther:(USDKDisplayStatus *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventItem")))
@interface USDKEventItem : USDKBase
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("init(callback:param:next:)"))) __attribute__((objc_designated_initializer));
- (USDKEventItem *)doCopyCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("doCopy(callback:param:next:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property void (^callback)(NSArray<id> *) __attribute__((swift_name("callback")));
@property void (^ _Nullable next)(NSArray<id> *) __attribute__((swift_name("next")));
@property id _Nullable param __attribute__((swift_name("param")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventListenerQueue")))
@interface USDKEventListenerQueue : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (int32_t)addEventItems:(USDKEventItem *)eventItems __attribute__((swift_name("add(eventItems:)")));
- (void)clear __attribute__((swift_name("clear()")));
- (void)exec __attribute__((swift_name("exec()")));
- (int32_t)getSize __attribute__((swift_name("getSize()")));
- (BOOL)removeListenerId:(int32_t)listenerId __attribute__((swift_name("remove(listenerId:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventStatus")))
@interface USDKEventStatus : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKEventStatus *tcLoaded __attribute__((swift_name("tcLoaded")));
@property (class, readonly) USDKEventStatus *cmpUiShown __attribute__((swift_name("cmpUiShown")));
@property (class, readonly) USDKEventStatus *userActionComplete __attribute__((swift_name("userActionComplete")));
- (int32_t)compareToOther:(USDKEventStatus *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SupportedVersions")))
@interface USDKSupportedVersions : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SupportedVersions.Companion")))
@interface USDKSupportedVersionsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (BOOL)hasValue:(USDKValidType *)value __attribute__((swift_name("has(value:)")));
@end;

__attribute__((swift_name("ValidType")))
@interface USDKValidType : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ValidType.Int")))
@interface USDKValidTypeInt : USDKValidType
- (instancetype)initWithValue:(int32_t)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ValidType.String")))
@interface USDKValidTypeString : USDKValidType
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("Command")))
@interface USDKCommand : USDKBase
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("init(callback:param:listenerId:next:)"))) __attribute__((objc_designated_initializer));
- (void)getResponseOnFinish:(void (^)(USDKCommandRespType * _Nullable))onFinish __attribute__((swift_name("getResponse(onFinish:)")));
@property void (^callback)(NSArray<id> *) __attribute__((swift_name("callback")));
@property USDKInt * _Nullable listenerId __attribute__((swift_name("listenerId")));
@property void (^ _Nullable next)(NSArray<id> *) __attribute__((swift_name("next")));
@property id _Nullable param __attribute__((swift_name("param")));
@property BOOL success __attribute__((swift_name("success")));
@end;

__attribute__((swift_name("GetTCDataCommand")))
@interface USDKGetTCDataCommand : USDKCommand
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("init(callback:param:listenerId:next:)"))) __attribute__((objc_designated_initializer));
- (void)getResponseOnFinish:(void (^)(USDKCommandRespType * _Nullable))onFinish __attribute__((swift_name("getResponse(onFinish:)")));
- (void)throwIfParamInvalid __attribute__((swift_name("throwIfParamInvalid()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddEventListenerCommand")))
@interface USDKAddEventListenerCommand : USDKGetTCDataCommand
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("init(callback:param:listenerId:next:)"))) __attribute__((objc_designated_initializer));
- (void)getResponseOnFinish:(void (^)(USDKCommandRespType * _Nullable))onFinish __attribute__((swift_name("getResponse(onFinish:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CommandMap")))
@interface USDKCommandMap : USDKBase
- (instancetype)initWithJsonHttpClient:(id<USDKJsonHttpClient>)jsonHttpClient __attribute__((swift_name("init(jsonHttpClient:)"))) __attribute__((objc_designated_initializer));
- (USDKCommand * _Nullable)getCommandCommandName:(NSString *)commandName callback:(void (^ _Nullable)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("getCommand(commandName:callback:param:listenerId:next:)")));
@property USDKAddEventListenerCommand * _Nullable addEventListener __attribute__((swift_name("addEventListener")));
@property USDKGetInAppTCDataCommand * _Nullable inAppTCData __attribute__((swift_name("inAppTCData")));
@property USDKPingCommand * _Nullable ping __attribute__((swift_name("ping")));
@property USDKRemoveEventListenerCommand * _Nullable removeEventListener __attribute__((swift_name("removeEventListener")));
@property USDKGetTCDataCommand * _Nullable tcData __attribute__((swift_name("tcData")));
@property USDKGetVendorListCommand * _Nullable vendorList __attribute__((swift_name("vendorList")));
@end;

__attribute__((swift_name("CommandRespType")))
@interface USDKCommandRespType : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CommandRespType.Boolean")))
@interface USDKCommandRespTypeBoolean : USDKCommandRespType
- (instancetype)initWithValue:(USDKBoolean * _Nullable)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) USDKBoolean * _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CommandRespType.Response")))
@interface USDKCommandRespTypeResponse : USDKCommandRespType
- (instancetype)initWithValue:(USDKResponse * _Nullable)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) USDKResponse * _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CommandRespType.VendorList")))
@interface USDKCommandRespTypeVendorList : USDKCommandRespType
- (instancetype)initWithValue:(USDKVendorList * _Nullable)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) USDKVendorList * _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVLData")))
@interface USDKGVLData : USDKBase
- (instancetype)initWithVersionOrVendorList:(id _Nullable)versionOrVendorList lastUpdated:(NSString * _Nullable)lastUpdated gvlSpecificationVersion:(int32_t)gvlSpecificationVersion vendorListVersion:(int32_t)vendorListVersion tcfPolicyVersion:(int32_t)tcfPolicyVersion vendors:(NSDictionary<NSString *, USDKVendor_ *> *)vendors features:(NSDictionary<NSString *, USDKFeature *> *)features purposes:(NSDictionary<NSString *, USDKPurpose *> *)purposes specialFeatures:(NSDictionary<NSString *, USDKFeature *> *)specialFeatures specialPurposes:(NSDictionary<NSString *, USDKPurpose *> *)specialPurposes stacks:(NSDictionary<NSString *, USDKStack *> *)stacks __attribute__((swift_name("init(versionOrVendorList:lastUpdated:gvlSpecificationVersion:vendorListVersion:tcfPolicyVersion:vendors:features:purposes:specialFeatures:specialPurposes:stacks:)"))) __attribute__((objc_designated_initializer));
- (USDKGVLData *)doCopyVersionOrVendorList:(id _Nullable)versionOrVendorList lastUpdated:(NSString * _Nullable)lastUpdated gvlSpecificationVersion:(int32_t)gvlSpecificationVersion vendorListVersion:(int32_t)vendorListVersion tcfPolicyVersion:(int32_t)tcfPolicyVersion vendors:(NSDictionary<NSString *, USDKVendor_ *> *)vendors features:(NSDictionary<NSString *, USDKFeature *> *)features purposes:(NSDictionary<NSString *, USDKPurpose *> *)purposes specialFeatures:(NSDictionary<NSString *, USDKFeature *> *)specialFeatures specialPurposes:(NSDictionary<NSString *, USDKPurpose *> *)specialPurposes stacks:(NSDictionary<NSString *, USDKStack *> *)stacks __attribute__((swift_name("doCopy(versionOrVendorList:lastUpdated:gvlSpecificationVersion:vendorListVersion:tcfPolicyVersion:vendors:features:purposes:specialFeatures:specialPurposes:stacks:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKFeature *> *features __attribute__((swift_name("features")));
@property int32_t gvlSpecificationVersion __attribute__((swift_name("gvlSpecificationVersion")));
@property NSString * _Nullable lastUpdated __attribute__((swift_name("lastUpdated")));
@property NSDictionary<NSString *, USDKPurpose *> *purposes __attribute__((swift_name("purposes")));
@property NSDictionary<NSString *, USDKFeature *> *specialFeatures __attribute__((swift_name("specialFeatures")));
@property NSDictionary<NSString *, USDKPurpose *> *specialPurposes __attribute__((swift_name("specialPurposes")));
@property NSDictionary<NSString *, USDKStack *> *stacks __attribute__((swift_name("stacks")));
@property int32_t tcfPolicyVersion __attribute__((swift_name("tcfPolicyVersion")));
@property int32_t vendorListVersion __attribute__((swift_name("vendorListVersion")));
@property NSDictionary<NSString *, USDKVendor_ *> *vendors __attribute__((swift_name("vendors")));
@property id _Nullable versionOrVendorList __attribute__((swift_name("versionOrVendorList")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetInAppTCDataCommand")))
@interface USDKGetInAppTCDataCommand : USDKGetTCDataCommand
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("init(callback:param:listenerId:next:)"))) __attribute__((objc_designated_initializer));
- (void)getResponseOnFinish:(void (^)(USDKCommandRespType * _Nullable))onFinish __attribute__((swift_name("getResponse(onFinish:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetVendorListCommand")))
@interface USDKGetVendorListCommand : USDKCommand
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next jsonHttpClient:(id<USDKJsonHttpClient>)jsonHttpClient __attribute__((swift_name("init(callback:param:listenerId:next:jsonHttpClient:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("init(callback:param:listenerId:next:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)getResponseOnFinish:(void (^)(USDKCommandRespType * _Nullable))onFinish __attribute__((swift_name("getResponse(onFinish:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PingCommand")))
@interface USDKPingCommand : USDKCommand
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("init(callback:param:listenerId:next:)"))) __attribute__((objc_designated_initializer));
- (void)getResponseOnFinish:(void (^)(USDKCommandRespType * _Nullable))onFinish __attribute__((swift_name("getResponse(onFinish:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RemoveEventListenerCommand")))
@interface USDKRemoveEventListenerCommand : USDKCommand
- (instancetype)initWithCallback:(void (^)(NSArray<id> *))callback param:(id _Nullable)param listenerId:(USDKInt * _Nullable)listenerId next:(void (^ _Nullable)(NSArray<id> *))next __attribute__((swift_name("init(callback:param:listenerId:next:)"))) __attribute__((objc_designated_initializer));
- (void)getResponseOnFinish:(void (^)(USDKCommandRespType * _Nullable))onFinish __attribute__((swift_name("getResponse(onFinish:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFCommand")))
@interface USDKTCFCommand : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKTCFCommand *ping __attribute__((swift_name("ping")));
@property (class, readonly) USDKTCFCommand *getTcData __attribute__((swift_name("getTcData")));
@property (class, readonly) USDKTCFCommand *getInAppTcData __attribute__((swift_name("getInAppTcData")));
@property (class, readonly) USDKTCFCommand *getVendorList __attribute__((swift_name("getVendorList")));
@property (class, readonly) USDKTCFCommand *addEventListener __attribute__((swift_name("addEventListener")));
@property (class, readonly) USDKTCFCommand *removeEventListener __attribute__((swift_name("removeEventListener")));
- (int32_t)compareToOther:(USDKTCFCommand *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@end;

__attribute__((swift_name("BooleanVectorOrString")))
@interface USDKBooleanVectorOrString : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BooleanVectorOrString.MapStringBool")))
@interface USDKBooleanVectorOrStringMapStringBool : USDKBooleanVectorOrString
- (instancetype)initWithValue:(NSDictionary<NSString *, USDKBoolean *> *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSDictionary<NSString *, USDKBoolean *> *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BooleanVectorOrString.String")))
@interface USDKBooleanVectorOrStringString : USDKBooleanVectorOrString
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentData_")))
@interface USDKConsentData_ : USDKBase
- (instancetype)initWithConsents:(USDKBooleanVectorOrString *)consents legitimateInterests:(USDKBooleanVectorOrString *)legitimateInterests __attribute__((swift_name("init(consents:legitimateInterests:)"))) __attribute__((objc_designated_initializer));
- (USDKConsentData_ *)doCopyConsents:(USDKBooleanVectorOrString *)consents legitimateInterests:(USDKBooleanVectorOrString *)legitimateInterests __attribute__((swift_name("doCopy(consents:legitimateInterests:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKBooleanVectorOrString *consents __attribute__((swift_name("consents")));
@property USDKBooleanVectorOrString *legitimateInterests __attribute__((swift_name("legitimateInterests")));
@end;

__attribute__((swift_name("Response")))
@interface USDKResponse : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (readonly) USDKInt * _Nullable cmpId __attribute__((swift_name("cmpId")));
@property (readonly) USDKInt * _Nullable cmpVersion __attribute__((swift_name("cmpVersion")));
@property (readonly) USDKBoolean * _Nullable gdprApplies __attribute__((swift_name("gdprApplies")));
@property (readonly) int32_t tcfPolicyVersion __attribute__((swift_name("tcfPolicyVersion")));
@end;

__attribute__((swift_name("TCData")))
@interface USDKTCData : USDKResponse
- (instancetype)initWithVendorIds:(NSArray<USDKInt *> * _Nullable)vendorIds listenerId:(USDKInt * _Nullable)listenerId __attribute__((swift_name("init(vendorIds:listenerId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property USDKCmpStatus *cmpStatus __attribute__((swift_name("cmpStatus")));
@property USDKEventStatus * _Nullable eventStatus __attribute__((swift_name("eventStatus")));
@property BOOL isServiceSpecific __attribute__((swift_name("isServiceSpecific")));
@property USDKInt * _Nullable listenerId __attribute__((swift_name("listenerId")));
@property USDKOutOfBand * _Nullable outOfBand __attribute__((swift_name("outOfBand")));
@property USDKPublisherData *publisher __attribute__((swift_name("publisher")));
@property NSString *publisherCC __attribute__((swift_name("publisherCC")));
@property USDKConsentData_ *purpose __attribute__((swift_name("purpose")));
@property BOOL purposeOneTreatment __attribute__((swift_name("purposeOneTreatment")));
@property USDKBooleanVectorOrString *specialFeatureOptins __attribute__((swift_name("specialFeatureOptins")));
@property NSString *tcString __attribute__((swift_name("tcString")));
@property BOOL useNonStandardStacks __attribute__((swift_name("useNonStandardStacks")));
@property USDKConsentData_ *vendor __attribute__((swift_name("vendor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InAppTCData")))
@interface USDKInAppTCData : USDKTCData
- (instancetype)initWithVendorIds:(NSArray<USDKInt *> * _Nullable)vendorIds __attribute__((swift_name("init(vendorIds:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithVendorIds:(NSArray<USDKInt *> * _Nullable)vendorIds listenerId:(USDKInt * _Nullable)listenerId __attribute__((swift_name("init(vendorIds:listenerId:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OutOfBand")))
@interface USDKOutOfBand : USDKBase
- (instancetype)initWithAllowedVendors:(USDKBooleanVectorOrString *)allowedVendors disclosedVendors:(USDKBooleanVectorOrString *)disclosedVendors __attribute__((swift_name("init(allowedVendors:disclosedVendors:)"))) __attribute__((objc_designated_initializer));
- (USDKOutOfBand *)doCopyAllowedVendors:(USDKBooleanVectorOrString *)allowedVendors disclosedVendors:(USDKBooleanVectorOrString *)disclosedVendors __attribute__((swift_name("doCopy(allowedVendors:disclosedVendors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKBooleanVectorOrString *allowedVendors __attribute__((swift_name("allowedVendors")));
@property USDKBooleanVectorOrString *disclosedVendors __attribute__((swift_name("disclosedVendors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ping")))
@interface USDKPing : USDKResponse
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property NSString *apiVersion __attribute__((swift_name("apiVersion")));
@property BOOL cmpLoaded __attribute__((swift_name("cmpLoaded")));
@property USDKCmpStatus *cmpStatus __attribute__((swift_name("cmpStatus")));
@property USDKDisplayStatus *displayStatus __attribute__((swift_name("displayStatus")));
@property int32_t gvlVersion __attribute__((swift_name("gvlVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PublisherData")))
@interface USDKPublisherData : USDKBase
- (instancetype)initWithConsents:(USDKBooleanVectorOrString *)consents legitimateInterests:(USDKBooleanVectorOrString *)legitimateInterests customPurpose:(USDKConsentData_ *)customPurpose restrictions:(USDKRestrictions *)restrictions __attribute__((swift_name("init(consents:legitimateInterests:customPurpose:restrictions:)"))) __attribute__((objc_designated_initializer));
- (USDKPublisherData *)doCopyConsents:(USDKBooleanVectorOrString *)consents legitimateInterests:(USDKBooleanVectorOrString *)legitimateInterests customPurpose:(USDKConsentData_ *)customPurpose restrictions:(USDKRestrictions *)restrictions __attribute__((swift_name("doCopy(consents:legitimateInterests:customPurpose:restrictions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKBooleanVectorOrString *consents __attribute__((swift_name("consents")));
@property USDKConsentData_ *customPurpose __attribute__((swift_name("customPurpose")));
@property USDKBooleanVectorOrString *legitimateInterests __attribute__((swift_name("legitimateInterests")));
@property USDKRestrictions *restrictions __attribute__((swift_name("restrictions")));
@end;

__attribute__((swift_name("Restrictions")))
@interface USDKRestrictions : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Restrictions.MapOfMap")))
@interface USDKRestrictionsMapOfMap : USDKRestrictions
- (instancetype)initWithValue:(NSDictionary<NSString *, NSDictionary<NSString *, USDKRestrictionType *> *> *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSDictionary<NSString *, NSDictionary<NSString *, USDKRestrictionType *> *> *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Restrictions.MapOfString")))
@interface USDKRestrictionsMapOfString : USDKRestrictions
- (instancetype)initWithValue:(NSDictionary<NSString *, NSString *> *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSDictionary<NSString *, NSString *> *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Json")))
@interface USDKJson : USDKBase
- (instancetype)initWithRestClient:(id<USDKJsonHttpClient>)restClient __attribute__((swift_name("init(restClient:)"))) __attribute__((objc_designated_initializer));
- (void)fetchUrl:(NSString *)url sendCookies:(BOOL)sendCookies timeout:(int64_t)timeout onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("fetch(url:sendCookies:timeout:onSuccess:onError:)")));
- (void)postUrl:(NSString *)url body:(NSString *)body sendCookies:(BOOL)sendCookies timeout:(int64_t)timeout onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("post(url:body:sendCookies:timeout:onSuccess:onError:)")));
@end;

__attribute__((swift_name("JsonHttpClient")))
@protocol USDKJsonHttpClient
@required
- (void)getUrl:(NSString *)url optionalOptions:(USDKOptionalOptions * _Nullable)optionalOptions onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("get(url:optionalOptions:onSuccess:onError:)")));
- (void)postUrl:(NSString *)url bodyData:(NSString *)bodyData optionalOptions:(USDKOptionalOptions * _Nullable)optionalOptions onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("post(url:bodyData:optionalOptions:onSuccess:onError:)")));
@end;

__attribute__((swift_name("StringOrNumber")))
@interface USDKStringOrNumber : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StringOrNumber.Int")))
@interface USDKStringOrNumberInt : USDKStringOrNumber
- (instancetype)initWithValue:(int32_t)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StringOrNumber.String")))
@interface USDKStringOrNumberString : USDKStringOrNumber
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModel")))
@interface USDKTCModel : USDKBase
- (instancetype)initWithGvl_:(USDKGVL * _Nullable)gvl_ __attribute__((swift_name("init(gvl_:)"))) __attribute__((objc_designated_initializer));
- (USDKTCModel *)clone __attribute__((swift_name("clone()")));
- (USDKTCModel *)doCopyGvl_:(USDKGVL * _Nullable)gvl_ __attribute__((swift_name("doCopy(gvl_:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (USDKStringOrNumber *)getCmpVersion __attribute__((swift_name("getCmpVersion()")));
- (NSString *)getConsentLanguage __attribute__((swift_name("getConsentLanguage()")));
- (USDKStringOrNumber *)getConsentScreen __attribute__((swift_name("getConsentScreen()")));
- (USDKTCModelPropType *)getFieldByNameName:(NSString *)name __attribute__((swift_name("getFieldByName(name:)")));
- (USDKGVL * _Nullable)getGvl __attribute__((swift_name("getGvl()")));
- (BOOL)getIsServiceSpecific __attribute__((swift_name("getIsServiceSpecific()")));
- (USDKStringOrNumber *)getNumCustomPurposes __attribute__((swift_name("getNumCustomPurposes()")));
- (USDKStringOrNumber *)getPolicyVersion __attribute__((swift_name("getPolicyVersion()")));
- (NSString *)getPublisherCountryCode __attribute__((swift_name("getPublisherCountryCode()")));
- (BOOL)getPurposeOneTreatment __attribute__((swift_name("getPurposeOneTreatment()")));
- (BOOL)getSupportOOB __attribute__((swift_name("getSupportOOB()")));
- (BOOL)getUserNonStandardStacks __attribute__((swift_name("getUserNonStandardStacks()")));
- (USDKStringOrNumber *)getVendorListVersion __attribute__((swift_name("getVendorListVersion()")));
- (int32_t)getVersion __attribute__((swift_name("getVersion()")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (void)readablePrint __attribute__((swift_name("readablePrint()")));
- (void)setAll __attribute__((swift_name("setAll()")));
- (void)setAllPurposeConsents __attribute__((swift_name("setAllPurposeConsents()")));
- (void)setAllPurposeLegitimateInterests __attribute__((swift_name("setAllPurposeLegitimateInterests()")));
- (void)setAllSpecialFeatureOptins __attribute__((swift_name("setAllSpecialFeatureOptins()")));
- (void)setAllVendorConsents __attribute__((swift_name("setAllVendorConsents()")));
- (void)setAllVendorLegitimateInterests __attribute__((swift_name("setAllVendorLegitimateInterests()")));
- (void)setAllVendorsAllowed __attribute__((swift_name("setAllVendorsAllowed()")));
- (void)setAllVendorsDisclosed __attribute__((swift_name("setAllVendorsDisclosed()")));
- (void)setCmpIdInteger:(USDKStringOrNumber *)integer __attribute__((swift_name("setCmpId(integer:)")));
- (void)setCmpVersionInteger:(USDKStringOrNumber *)integer __attribute__((swift_name("setCmpVersion(integer:)")));
- (void)setConsentLanguageLang:(NSString *)lang __attribute__((swift_name("setConsentLanguage(lang:)")));
- (void)setConsentScreenInteger:(USDKStringOrNumber *)integer __attribute__((swift_name("setConsentScreen(integer:)")));
- (void)setGvlGvl:(USDKGVL *)gvl __attribute__((swift_name("setGvl(gvl:)")));
- (void)setIsServiceSpecificBool:(BOOL)bool_ __attribute__((swift_name("setIsServiceSpecific(bool:)")));
- (void)setNumCustomPurposesNum:(USDKStringOrNumber *)num __attribute__((swift_name("setNumCustomPurposes(num:)")));
- (void)setPolicyVersionNum:(USDKStringOrNumber *)num __attribute__((swift_name("setPolicyVersion(num:)")));
- (void)setPublisherCountryCodeCountryCode:(NSString *)countryCode __attribute__((swift_name("setPublisherCountryCode(countryCode:)")));
- (void)setPurposeOneTreatmentBool:(BOOL)bool_ __attribute__((swift_name("setPurposeOneTreatment(bool:)")));
- (void)setSupportOOBBool:(BOOL)bool_ __attribute__((swift_name("setSupportOOB(bool:)")));
- (void)setUseNonStandardStacksBool:(BOOL)bool_ __attribute__((swift_name("setUseNonStandardStacks(bool:)")));
- (void)setVendorListVersionInteger:(USDKStringOrNumber *)integer __attribute__((swift_name("setVendorListVersion(integer:)")));
- (void)setVersionNum:(USDKStringOrNumber *)num __attribute__((swift_name("setVersion(num:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (void)unsetAll __attribute__((swift_name("unsetAll()")));
- (void)unsetAllPurposeConsents __attribute__((swift_name("unsetAllPurposeConsents()")));
- (void)unsetAllPurposeLegitimateInterests __attribute__((swift_name("unsetAllPurposeLegitimateInterests()")));
- (void)unsetAllSpecialFeatureOptins __attribute__((swift_name("unsetAllSpecialFeatureOptins()")));
- (void)unsetAllVendorConsents __attribute__((swift_name("unsetAllVendorConsents()")));
- (void)unsetAllVendorLegitimateInterests __attribute__((swift_name("unsetAllVendorLegitimateInterests()")));
- (void)unsetAllVendorsAllowed __attribute__((swift_name("unsetAllVendorsAllowed()")));
- (void)unsetAllVendorsDisclosed __attribute__((swift_name("unsetAllVendorsDisclosed()")));
- (void)updated __attribute__((swift_name("updated()")));
@property (readonly) USDKConsentLanguages *consentLanguages __attribute__((swift_name("consentLanguages")));
@property id _Nullable created __attribute__((swift_name("created")));
@property NSDictionary<NSString *, USDKPurpose *> *customPurposes __attribute__((swift_name("customPurposes")));
@property USDKGVL * _Nullable gvl_ __attribute__((swift_name("gvl_")));
@property id _Nullable lastUpdated __attribute__((swift_name("lastUpdated")));
@property USDKVector *publisherConsents __attribute__((swift_name("publisherConsents")));
@property USDKVector *publisherCustomConsents __attribute__((swift_name("publisherCustomConsents")));
@property USDKVector *publisherCustomLegitimateInterests __attribute__((swift_name("publisherCustomLegitimateInterests")));
@property USDKVector *publisherLegitimateInterests __attribute__((swift_name("publisherLegitimateInterests")));
@property USDKPurposeRestrictionVector *publisherRestrictions __attribute__((swift_name("publisherRestrictions")));
@property USDKVector *purposeConsents __attribute__((swift_name("purposeConsents")));
@property USDKVector *purposeLegitimateInterests __attribute__((swift_name("purposeLegitimateInterests")));
@property USDKVector *specialFeatureOptins __attribute__((swift_name("specialFeatureOptins")));
@property USDKVector *vendorConsents __attribute__((swift_name("vendorConsents")));
@property USDKVector *vendorLegitimateInterests __attribute__((swift_name("vendorLegitimateInterests")));
@property USDKVector *vendorsAllowed __attribute__((swift_name("vendorsAllowed")));
@property USDKVector *vendorsDisclosed __attribute__((swift_name("vendorsDisclosed")));
@end;

__attribute__((swift_name("TCModelPropType")))
@interface USDKTCModelPropType : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelPropType.Boolean")))
@interface USDKTCModelPropTypeBoolean : USDKTCModelPropType
- (instancetype)initWithValue:(BOOL)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) BOOL value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelPropType.Date")))
@interface USDKTCModelPropTypeDate : USDKTCModelPropType
- (instancetype)initWithValue:(id _Nullable)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelPropType.Int")))
@interface USDKTCModelPropTypeInt : USDKTCModelPropType
- (instancetype)initWithValue:(int32_t)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelPropType.PurposeRestrictionVector")))
@interface USDKTCModelPropTypePurposeRestrictionVector : USDKTCModelPropType
- (instancetype)initWithValue:(USDKPurposeRestrictionVector *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) USDKPurposeRestrictionVector *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelPropType.String")))
@interface USDKTCModelPropTypeString : USDKTCModelPropType
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelPropType.StringOrNumber")))
@interface USDKTCModelPropTypeStringOrNumber : USDKTCModelPropType
- (instancetype)initWithValue:(USDKStringOrNumber *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) USDKStringOrNumber *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelPropType.Vector")))
@interface USDKTCModelPropTypeVector : USDKTCModelPropType
- (instancetype)initWithValue:(USDKVector *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) USDKVector *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Fields")))
@interface USDKFields : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKFields *cmpid __attribute__((swift_name("cmpid")));
@property (class, readonly) USDKFields *cmpversion __attribute__((swift_name("cmpversion")));
@property (class, readonly) USDKFields *consentlanguage __attribute__((swift_name("consentlanguage")));
@property (class, readonly) USDKFields *consentscreen __attribute__((swift_name("consentscreen")));
@property (class, readonly) USDKFields *created __attribute__((swift_name("created")));
@property (class, readonly) USDKFields *supportoob __attribute__((swift_name("supportoob")));
@property (class, readonly) USDKFields *isservicespecific __attribute__((swift_name("isservicespecific")));
@property (class, readonly) USDKFields *lastupdated __attribute__((swift_name("lastupdated")));
@property (class, readonly) USDKFields *numcustompurposes __attribute__((swift_name("numcustompurposes")));
@property (class, readonly) USDKFields *policyversion __attribute__((swift_name("policyversion")));
@property (class, readonly) USDKFields *publishercountrycode __attribute__((swift_name("publishercountrycode")));
@property (class, readonly) USDKFields *publishercustomconsents __attribute__((swift_name("publishercustomconsents")));
@property (class, readonly) USDKFields *publishercustomlegitimateinterests __attribute__((swift_name("publishercustomlegitimateinterests")));
@property (class, readonly) USDKFields *publisherlegitimateinterests __attribute__((swift_name("publisherlegitimateinterests")));
@property (class, readonly) USDKFields *publisherconsents __attribute__((swift_name("publisherconsents")));
@property (class, readonly) USDKFields *publisherrestrictions __attribute__((swift_name("publisherrestrictions")));
@property (class, readonly) USDKFields *purposeconsents __attribute__((swift_name("purposeconsents")));
@property (class, readonly) USDKFields *purposelegitimateinterests __attribute__((swift_name("purposelegitimateinterests")));
@property (class, readonly) USDKFields *purposeonetreatment __attribute__((swift_name("purposeonetreatment")));
@property (class, readonly) USDKFields *specialfeatureoptins __attribute__((swift_name("specialfeatureoptins")));
@property (class, readonly) USDKFields *usenonstandardstacks __attribute__((swift_name("usenonstandardstacks")));
@property (class, readonly) USDKFields *vendorconsents __attribute__((swift_name("vendorconsents")));
@property (class, readonly) USDKFields *vendorlegitimateinterests __attribute__((swift_name("vendorlegitimateinterests")));
@property (class, readonly) USDKFields *vendorlistversion __attribute__((swift_name("vendorlistversion")));
@property (class, readonly) USDKFields *vendorsallowed __attribute__((swift_name("vendorsallowed")));
@property (class, readonly) USDKFields *vendorsdisclosed __attribute__((swift_name("vendorsdisclosed")));
@property (class, readonly) USDKFields *version_ __attribute__((swift_name("version_")));
- (int32_t)compareToOther:(USDKFields *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface USDKKotlinThrowable : USDKBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (USDKKotlinArray *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVLError")))
@interface USDKGVLError : USDKKotlinThrowable
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelError")))
@interface USDKTCModelError : USDKKotlinThrowable
- (instancetype)initWithFieldName:(NSString *)fieldName passedValue:(id)passedValue msg:(NSString *)msg __attribute__((swift_name("init(fieldName:passedValue:msg:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Location")))
@interface USDKLocation : USDKBase
- (USDKUserCountry *)getUserCountry __attribute__((swift_name("getUserCountry()")));
- (BOOL)isUserInCalifornia __attribute__((swift_name("isUserInCalifornia()")));
- (BOOL)isUserInEU __attribute__((swift_name("isUserInEU()")));
- (BOOL)isUserInUS __attribute__((swift_name("isUserInUS()")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Location.Companion")))
@interface USDKLocationCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKLocation *)createAndResolveCountryLocationApi:(USDKUserCountryApi *)locationApi storage:(USDKDeviceStorage *)storage noCacheFlag:(BOOL)noCacheFlag logger:(USDKUsercentricsLogger *)logger __attribute__((swift_name("createAndResolveCountry(locationApi:storage:noCacheFlag:logger:)")));
@property (readonly) int64_t CACHE_MAX_DURATION_MILLIS __attribute__((swift_name("CACHE_MAX_DURATION_MILLIS")));
@property (readonly) NSString *USER_COUNTRY_CACHE __attribute__((swift_name("USER_COUNTRY_CACHE")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SettingsService")))
@interface USDKSettingsService : USDKBase
- (instancetype)initWithSettingsApi:(USDKSettingsApi *)settingsApi servicesApi:(USDKServicesApi *)servicesApi locationInstance:(USDKLocation *)locationInstance storageInstance:(USDKDeviceStorage *)storageInstance logger:(USDKUsercentricsLogger *)logger __attribute__((swift_name("init(settingsApi:servicesApi:locationInstance:storageInstance:logger:)"))) __attribute__((objc_designated_initializer));
- (USDKSettings *)getBaseSettings __attribute__((swift_name("getBaseSettings()")));
- (NSArray<USDKCategory *> *)getCategories __attribute__((swift_name("getCategories()")));
- (USDKCategory * _Nullable)getCategoryBySlugCategorySlug:(NSString *)categorySlug __attribute__((swift_name("getCategoryBySlug(categorySlug:)")));
- (NSString *)getControllerId __attribute__((swift_name("getControllerId()")));
- (NSArray<USDKDataExchangeSetting *> * _Nullable)getDataExchangeSettings __attribute__((swift_name("getDataExchangeSettings()")));
- (NSArray<USDKCategory *> *)getEssentialCategories __attribute__((swift_name("getEssentialCategories()")));
- (void)getGdprAppliesOnSuccess:(void (^)(USDKBoolean *))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("getGdprApplies(onSuccess:onError:)")));
- (NSArray<USDKCategory *> *)getNonEssentialCategories __attribute__((swift_name("getNonEssentialCategories()")));
- (NSArray<USDKService *> *)getServices __attribute__((swift_name("getServices()")));
- (NSArray<USDKService *> *)getServicesByIdsIds:(NSArray<NSString *> *)ids __attribute__((swift_name("getServicesByIds(ids:)")));
- (NSArray<USDKService *> *)getServicesFromCategoriesCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("getServicesFromCategories(categories:)")));
- (NSArray<USDKService *> *)getServicesWithConsent __attribute__((swift_name("getServicesWithConsent()")));
- (USDKExtendedSettings *)getSettings __attribute__((swift_name("getSettings()")));
- (USDKTCFChangedPurposes *)getTCFChangedPurposes __attribute__((swift_name("getTCFChangedPurposes()")));
- (BOOL)getTCFIsServiceSpecific __attribute__((swift_name("getTCFIsServiceSpecific()")));
- (NSString *)getTCFPublisherCountryCode __attribute__((swift_name("getTCFPublisherCountryCode()")));
- (BOOL)getTCFPurposeOneTreatment __attribute__((swift_name("getTCFPurposeOneTreatment()")));
- (NSArray<USDKInt *> *)getTCFStacksIds __attribute__((swift_name("getTCFStacksIds()")));
- (NSArray<USDKInt *> *)getTCFVendorIds __attribute__((swift_name("getTCFVendorIds()")));
- (void)doInitSettingsSettingsId:(NSString *)settingsId jsonFileVersion:(NSString *)jsonFileVersion jsonFileLanguage:(NSString *)jsonFileLanguage controllerId:(NSString * _Nullable)controllerId callback:(void (^)(void))callback onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("doInitSettings(settingsId:jsonFileVersion:jsonFileLanguage:controllerId:callback:onError:)")));
- (BOOL)isCCPAEnabled __attribute__((swift_name("isCCPAEnabled()")));
- (BOOL)isTCFEnabled __attribute__((swift_name("isTCFEnabled()")));
- (NSArray<USDKCategory *> *)mergeServicesIntoExistingCategoriesUpdatedServices:(NSArray<USDKService *> *)updatedServices __attribute__((swift_name("mergeServicesIntoExistingCategories(updatedServices:)")));
- (USDKExtendedSettings *)removeEmptyCategoriesSettings:(USDKExtendedSettings *)settings __attribute__((swift_name("removeEmptyCategories(settings:)")));
- (NSArray<USDKCategory *> *)removeNoneCategoryCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("removeNoneCategory(categories:)")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
- (void)setCategoriesCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("setCategories(categories:)")));
- (void)setControllerIdControllerId:(NSString *)controllerId __attribute__((swift_name("setControllerId(controllerId:)")));
- (void)setSettingsSettings:(USDKExtendedSettings *)settings __attribute__((swift_name("setSettings(settings:)")));
- (void)shouldAcceptAllImplicitlyOnInitCallback:(void (^)(USDKBoolean *))callback onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("shouldAcceptAllImplicitlyOnInit(callback:onError:)")));
- (BOOL)shouldShowFirstLayerOnVersionChange __attribute__((swift_name("shouldShowFirstLayerOnVersionChange()")));
- (NSArray<USDKService *> *)updateServicesWithConsentServices:(NSArray<USDKService *> *)services status:(USDKConsentStatus *)status __attribute__((swift_name("updateServicesWithConsent(services:status:)")));
- (NSArray<USDKService *> *)updateServicesWithConsentsServices:(NSArray<USDKService *> *)services userDecisions:(NSArray<USDKUserDecision *> *)userDecisions __attribute__((swift_name("updateServicesWithConsents(services:userDecisions:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeviceStorage")))
@interface USDKDeviceStorage : USDKBase
- (instancetype)initWithStorage:(USDKStorage *)storage logger:(USDKUsercentricsLogger *)logger __attribute__((swift_name("init(storage:logger:)"))) __attribute__((objc_designated_initializer));
- (void)clear __attribute__((swift_name("clear()")));
- (void)clearCCPATimestamp __attribute__((swift_name("clearCCPATimestamp()")));
- (void)deleteKeyKey:(NSString *)key __attribute__((swift_name("deleteKey(key:)")));
- (USDKStorageSettings *)fetchSettings __attribute__((swift_name("fetchSettings()")));
- (NSString *)fetchSettingsVersion __attribute__((swift_name("fetchSettingsVersion()")));
- (USDKStorageTCF *)fetchTCFData __attribute__((swift_name("fetchTCFData()")));
- (NSString *)fetchTCString __attribute__((swift_name("fetchTCString()")));
- (BOOL)fetchUserActionPerformed __attribute__((swift_name("fetchUserActionPerformed()")));
- (USDKStorageCCPA * _Nullable)getCCPATimeStamp __attribute__((swift_name("getCCPATimeStamp()")));
- (USDKCacheEntry * _Nullable)getCacheKey:(NSString *)key __attribute__((swift_name("getCache(key:)")));
- (NSString *)getConsentBuffer __attribute__((swift_name("getConsentBuffer()")));
- (NSString *)getControllerId __attribute__((swift_name("getControllerId()")));
- (NSString *)getSettingsId __attribute__((swift_name("getSettingsId()")));
- (NSString *)getSettingsLanguage __attribute__((swift_name("getSettingsLanguage()")));
- (NSString * _Nullable)getValueKey:(NSString *)key defaultValue:(NSString * _Nullable)defaultValue __attribute__((swift_name("getValue(key:defaultValue:)")));
- (USDKStorageSettings *)mapStorageSettingsSettings:(USDKExtendedSettings *)settings services:(NSArray<USDKService *> *)services __attribute__((swift_name("mapStorageSettings(settings:services:)")));
- (void)saveCacheKey:(NSString *)key value:(NSString *)value __attribute__((swift_name("saveCache(key:value:)")));
- (void)saveIABTCFKeysKeys:(USDKIABTCF_KEYS *)keys __attribute__((swift_name("saveIABTCFKeys(keys:)")));
- (void)saveSettingsSettings:(USDKStorageSettings *)settings __attribute__((swift_name("saveSettings(settings:)")));
- (void)saveTCFDataTcfData:(USDKStorageTCF *)tcfData __attribute__((swift_name("saveTCFData(tcfData:)")));
- (void)saveTCStringTCString:(NSString *)TCString __attribute__((swift_name("saveTCString(TCString:)")));
- (void)saveVendorsDisclosedVendorsDisclosed:(NSArray<USDKInt *> *)vendorsDisclosed __attribute__((swift_name("saveVendorsDisclosed(vendorsDisclosed:)")));
- (void)setCCPATimestampCcpaTimestamp:(USDKStorageCCPA *)ccpaTimestamp __attribute__((swift_name("setCCPATimestamp(ccpaTimestamp:)")));
- (void)setCMPIDValue:(NSString *)value __attribute__((swift_name("setCMPID(value:)")));
- (void)setConsentBufferConsents:(NSDictionary<USDKDouble *, USDKDataTransferObject *> *)consents __attribute__((swift_name("setConsentBuffer(consents:)")));
- (void)setUserActionPerformedValue:(BOOL)value __attribute__((swift_name("setUserActionPerformed(value:)")));
- (void)setValueKey:(NSString *)key value:(NSString *)value __attribute__((swift_name("setValue(key:value:)")));
- (BOOL)settingsExist __attribute__((swift_name("settingsExist()")));
@property (readonly) USDKUsercentricsLogger *logger __attribute__((swift_name("logger")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentStatusObject")))
@interface USDKConsentStatusObject : USDKBase
- (instancetype)initWithName:(NSString *)name status:(USDKBoolean * _Nullable)status __attribute__((swift_name("init(name:status:)"))) __attribute__((objc_designated_initializer));
- (USDKConsentStatusObject *)doCopyName:(NSString *)name status:(USDKBoolean * _Nullable)status __attribute__((swift_name("doCopy(name:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) USDKBoolean * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Event")))
@interface USDKEvent : USDKBase
- (instancetype)initWithName:(NSString *)name type:(USDKConsentType * _Nullable)type services:(NSArray<USDKConsentStatusObject *> *)services __attribute__((swift_name("init(name:type:services:)"))) __attribute__((objc_designated_initializer));
- (USDKEvent *)doCopyName:(NSString *)name type:(USDKConsentType * _Nullable)type services:(NSArray<USDKConsentStatusObject *> *)services __attribute__((swift_name("doCopy(name:type:services:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSArray<USDKConsentStatusObject *> *services __attribute__((swift_name("services")));
@property (readonly) USDKConsentType * _Nullable type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventManager")))
@interface USDKEventManager : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)notifyEvent:(USDKEvent *)event __attribute__((swift_name("notify(event:)")));
- (void)registerEventName:(NSString *)eventName action:(void (^)(USDKEvent *))action __attribute__((swift_name("register(eventName:action:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventDispatcher")))
@interface USDKEventDispatcher : USDKBase
- (instancetype)initWithEventManager:(USDKEventManager *)eventManager __attribute__((swift_name("init(eventManager:)"))) __attribute__((objc_designated_initializer));
- (void)dispatchDataTransferObject:(USDKDataTransferObject *)dataTransferObject __attribute__((swift_name("dispatch(dataTransferObject:)")));
- (void)doInitDataExchangeSettings:(NSArray<USDKDataExchangeSetting *> *)dataExchangeSettings __attribute__((swift_name("doInit(dataExchangeSettings:)")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants___")))
@interface USDKConstants___ : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (readonly) NSString *FALLBACK_LANGUAGE __attribute__((swift_name("FALLBACK_LANGUAGE")));
@property (readonly) NSString *GVL_BASE_URL __attribute__((swift_name("GVL_BASE_URL")));
@property (readonly) int32_t PURPOSE_ONE_ID __attribute__((swift_name("PURPOSE_ONE_ID")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RestClientWrapper")))
@interface USDKRestClientWrapper : USDKBase <USDKJsonHttpClient>
- (instancetype)initWithRestClient:(USDKHttpRequests *)restClient __attribute__((swift_name("init(restClient:)"))) __attribute__((objc_designated_initializer));
- (void)getUrl:(NSString *)url optionalOptions:(USDKOptionalOptions * _Nullable)optionalOptions onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("get(url:optionalOptions:onSuccess:onError:)")));
- (void)postUrl:(NSString *)url bodyData:(NSString *)bodyData optionalOptions:(USDKOptionalOptions * _Nullable)optionalOptions onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("post(url:bodyData:optionalOptions:onSuccess:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCF")))
@interface USDKTCF : USDKBase
- (instancetype)initWithSettingsInstance:(USDKSettingsService *)settingsInstance storageInstance:(USDKDeviceStorage *)storageInstance consentsApi:(USDKConsentsApi *)consentsApi restClient:(USDKHttpRequests *)restClient __attribute__((swift_name("init(settingsInstance:storageInstance:consentsApi:restClient:)"))) __attribute__((objc_designated_initializer));
- (void)acceptAllDisclosedFromLayer:(USDKTCF_DECISION_UI_LAYER *)fromLayer onSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("acceptAllDisclosed(fromLayer:onSuccess:onError:)")));
- (void)changeLanguageLanguage:(NSString *)language onSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("changeLanguage(language:onSuccess:onError:)")));
- (void)denyAllDisclosedFromLayer:(USDKTCF_DECISION_UI_LAYER *)fromLayer onSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("denyAllDisclosed(fromLayer:onSuccess:onError:)")));
- (NSArray<USDKInt *> *)getSelectedVendorIds __attribute__((swift_name("getSelectedVendorIds()")));
- (NSArray<USDKTCFStack *> *)getStacks __attribute__((swift_name("getStacks()")));
- (USDKTCFData *)getTCFData __attribute__((swift_name("getTCFData()")));
- (NSArray<USDKTCFVendor *> *)getVendors __attribute__((swift_name("getVendors()")));
- (void)initializeCallback:(void (^)(void))callback onFailure:(void (^)(USDKUCError *))onFailure __attribute__((swift_name("initialize(callback:onFailure:)")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
- (void)setUIAsClosedOnSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("setUIAsClosed(onSuccess:onError:)")));
- (void)setUIAsOpenOnSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("setUIAsOpen(onSuccess:onError:)")));
- (void)updateChoicesDecisions:(USDKTCFUserDecisions *)decisions fromLayer:(USDKTCF_DECISION_UI_LAYER *)fromLayer onSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("updateChoices(decisions:fromLayer:onSuccess:onError:)")));
@property BOOL selectedVendorsIncludeNonDisclosed __attribute__((swift_name("selectedVendorsIncludeNonDisclosed")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCF.Companion")))
@interface USDKTCFCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (NSString *)resolveLanguageLanguage:(NSString *)language __attribute__((swift_name("resolveLanguage(language:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCF_DECISION_UI_LAYER")))
@interface USDKTCF_DECISION_UI_LAYER : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKTCF_DECISION_UI_LAYER *firstLayer __attribute__((swift_name("firstLayer")));
@property (class, readonly) USDKTCF_DECISION_UI_LAYER *secondLayer __attribute__((swift_name("secondLayer")));
- (int32_t)compareToOther:(USDKTCF_DECISION_UI_LAYER *)other __attribute__((swift_name("compareTo(other:)")));
@property int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCF_DECISION_UI_LAYER.Companion")))
@interface USDKTCF_DECISION_UI_LAYERCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKTCF_DECISION_UI_LAYER *)getDecisionLayerLayer:(NSString *)layer __attribute__((swift_name("getDecisionLayer(layer:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCF_VENDOR_PURPOSE_TYPE")))
@interface USDKTCF_VENDOR_PURPOSE_TYPE : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKTCF_VENDOR_PURPOSE_TYPE *legitimateInterest __attribute__((swift_name("legitimateInterest")));
@property (class, readonly) USDKTCF_VENDOR_PURPOSE_TYPE *purposes __attribute__((swift_name("purposes")));
- (int32_t)compareToOther:(USDKTCF_VENDOR_PURPOSE_TYPE *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCF_WARN_MESSAGES")))
@interface USDKTCF_WARN_MESSAGES : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKTCF_WARN_MESSAGES *cookieBridgeNotAvailable __attribute__((swift_name("cookieBridgeNotAvailable")));
@property (class, readonly) USDKTCF_WARN_MESSAGES *cookieBridgeOptionsNotSet __attribute__((swift_name("cookieBridgeOptionsNotSet")));
@property (class, readonly) USDKTCF_WARN_MESSAGES *getGlobalTcStringFailure __attribute__((swift_name("getGlobalTcStringFailure")));
@property (class, readonly) USDKTCF_WARN_MESSAGES *theInitTcfError __attribute__((swift_name("theInitTcfError")));
@property (class, readonly) USDKTCF_WARN_MESSAGES *setGlobalTcStringFailure __attribute__((swift_name("setGlobalTcStringFailure")));
@property (class, readonly) USDKTCF_WARN_MESSAGES *resetGvlFailure __attribute__((swift_name("resetGvlFailure")));
@property (class, readonly) USDKTCF_WARN_MESSAGES *vendorRemoved __attribute__((swift_name("vendorRemoved")));
- (int32_t)compareToOther:(USDKTCF_WARN_MESSAGES *)other __attribute__((swift_name("compareTo(other:)")));
@property NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((swift_name("BasePurpose")))
@interface USDKBasePurpose : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *descriptionLegal __attribute__((swift_name("descriptionLegal")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BasePurpose.Companion")))
@interface USDKBasePurposeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("BaseTCFUserDecision")))
@interface USDKBaseTCFUserDecision : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property BOOL consent __attribute__((swift_name("consent")));
@property int32_t id __attribute__((swift_name("id")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IdAndName")))
@interface USDKIdAndName : USDKBase
- (instancetype)initWithId:(int32_t)id name:(NSString *)name __attribute__((swift_name("init(id:name:)"))) __attribute__((objc_designated_initializer));
- (USDKIdAndName *)doCopyId:(int32_t)id name:(NSString *)name __attribute__((swift_name("doCopy(id:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property int32_t id __attribute__((swift_name("id")));
@property NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IdAndName.Companion")))
@interface USDKIdAndNameCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFData")))
@interface USDKTCFData : USDKBase
- (instancetype)initWithFeatures:(NSArray<USDKTCFFeature *> *)features purposes:(NSArray<USDKTCFPurpose *> *)purposes specialFeatures:(NSArray<USDKTCFSpecialFeature *> *)specialFeatures specialPurposes:(NSArray<USDKTCFSpecialPurpose *> *)specialPurposes stacks:(NSArray<USDKTCFStack *> *)stacks vendors:(NSArray<USDKTCFVendor *> *)vendors __attribute__((swift_name("init(features:purposes:specialFeatures:specialPurposes:stacks:vendors:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFData *)doCopyFeatures:(NSArray<USDKTCFFeature *> *)features purposes:(NSArray<USDKTCFPurpose *> *)purposes specialFeatures:(NSArray<USDKTCFSpecialFeature *> *)specialFeatures specialPurposes:(NSArray<USDKTCFSpecialPurpose *> *)specialPurposes stacks:(NSArray<USDKTCFStack *> *)stacks vendors:(NSArray<USDKTCFVendor *> *)vendors __attribute__((swift_name("doCopy(features:purposes:specialFeatures:specialPurposes:stacks:vendors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKTCFFeature *> *features __attribute__((swift_name("features")));
@property NSArray<USDKTCFPurpose *> *purposes __attribute__((swift_name("purposes")));
@property NSArray<USDKTCFSpecialFeature *> *specialFeatures __attribute__((swift_name("specialFeatures")));
@property NSArray<USDKTCFSpecialPurpose *> *specialPurposes __attribute__((swift_name("specialPurposes")));
@property NSArray<USDKTCFStack *> *stacks __attribute__((swift_name("stacks")));
@property NSArray<USDKTCFVendor *> *vendors __attribute__((swift_name("vendors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFData.Companion")))
@interface USDKTCFDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFFeature")))
@interface USDKTCFFeature : USDKBasePurpose
- (instancetype)initWithDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name __attribute__((swift_name("init(description:descriptionLegal:id:name:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (USDKTCFFeature *)doCopyDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name __attribute__((swift_name("doCopy(description:descriptionLegal:id:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *descriptionLegal __attribute__((swift_name("descriptionLegal")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFFeature.Companion")))
@interface USDKTCFFeatureCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFPurpose")))
@interface USDKTCFPurpose : USDKBasePurpose
- (instancetype)initWithDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name consent:(BOOL)consent isPartOfASelectedStack:(BOOL)isPartOfASelectedStack legitimateInterestConsent:(BOOL)legitimateInterestConsent showLegitimateInterestToggle:(BOOL)showLegitimateInterestToggle stackId:(USDKInt * _Nullable)stackId __attribute__((swift_name("init(description:descriptionLegal:id:name:consent:isPartOfASelectedStack:legitimateInterestConsent:showLegitimateInterestToggle:stackId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (USDKTCFPurpose *)doCopyDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name consent:(BOOL)consent isPartOfASelectedStack:(BOOL)isPartOfASelectedStack legitimateInterestConsent:(BOOL)legitimateInterestConsent showLegitimateInterestToggle:(BOOL)showLegitimateInterestToggle stackId:(USDKInt * _Nullable)stackId __attribute__((swift_name("doCopy(description:descriptionLegal:id:name:consent:isPartOfASelectedStack:legitimateInterestConsent:showLegitimateInterestToggle:stackId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL consent __attribute__((swift_name("consent")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *descriptionLegal __attribute__((swift_name("descriptionLegal")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property BOOL isPartOfASelectedStack __attribute__((swift_name("isPartOfASelectedStack")));
@property BOOL legitimateInterestConsent __attribute__((swift_name("legitimateInterestConsent")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property BOOL showLegitimateInterestToggle __attribute__((swift_name("showLegitimateInterestToggle")));
@property USDKInt * _Nullable stackId __attribute__((swift_name("stackId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFPurpose.Companion")))
@interface USDKTCFPurposeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFSpecialFeature")))
@interface USDKTCFSpecialFeature : USDKBasePurpose
- (instancetype)initWithDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name consent:(BOOL)consent isPartOfASelectedStack:(BOOL)isPartOfASelectedStack stackId:(USDKInt * _Nullable)stackId __attribute__((swift_name("init(description:descriptionLegal:id:name:consent:isPartOfASelectedStack:stackId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (USDKTCFSpecialFeature *)doCopyDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name consent:(BOOL)consent isPartOfASelectedStack:(BOOL)isPartOfASelectedStack stackId:(USDKInt * _Nullable)stackId __attribute__((swift_name("doCopy(description:descriptionLegal:id:name:consent:isPartOfASelectedStack:stackId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL consent __attribute__((swift_name("consent")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *descriptionLegal __attribute__((swift_name("descriptionLegal")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property BOOL isPartOfASelectedStack __attribute__((swift_name("isPartOfASelectedStack")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property USDKInt * _Nullable stackId __attribute__((swift_name("stackId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFSpecialFeature.Companion")))
@interface USDKTCFSpecialFeatureCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFSpecialPurpose")))
@interface USDKTCFSpecialPurpose : USDKBasePurpose
- (instancetype)initWithDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name __attribute__((swift_name("init(description:descriptionLegal:id:name:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (USDKTCFSpecialPurpose *)doCopyDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name __attribute__((swift_name("doCopy(description:descriptionLegal:id:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *descriptionLegal __attribute__((swift_name("descriptionLegal")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFSpecialPurpose.Companion")))
@interface USDKTCFSpecialPurposeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFStack")))
@interface USDKTCFStack : USDKBase
- (instancetype)initWithDescription:(NSString *)description id:(int32_t)id name:(NSString *)name purposeIds:(NSArray<USDKInt *> *)purposeIds specialFeatureIds:(NSArray<USDKInt *> *)specialFeatureIds __attribute__((swift_name("init(description:id:name:purposeIds:specialFeatureIds:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFStack *)doCopyDescription:(NSString *)description id:(int32_t)id name:(NSString *)name purposeIds:(NSArray<USDKInt *> *)purposeIds specialFeatureIds:(NSArray<USDKInt *> *)specialFeatureIds __attribute__((swift_name("doCopy(description:id:name:purposeIds:specialFeatureIds:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property NSArray<USDKInt *> *purposeIds __attribute__((swift_name("purposeIds")));
@property NSArray<USDKInt *> *specialFeatureIds __attribute__((swift_name("specialFeatureIds")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFStack.Companion")))
@interface USDKTCFStackCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFUserDecisionOnPurpose")))
@interface USDKTCFUserDecisionOnPurpose : USDKBaseTCFUserDecision
- (instancetype)initWithId:(int32_t)id consent:(BOOL)consent legitimateInterestConsent:(BOOL)legitimateInterestConsent __attribute__((swift_name("init(id:consent:legitimateInterestConsent:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (USDKTCFUserDecisionOnPurpose *)doCopyId:(int32_t)id consent:(BOOL)consent legitimateInterestConsent:(BOOL)legitimateInterestConsent __attribute__((swift_name("doCopy(id:consent:legitimateInterestConsent:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL consent __attribute__((swift_name("consent")));
@property int32_t id __attribute__((swift_name("id")));
@property BOOL legitimateInterestConsent __attribute__((swift_name("legitimateInterestConsent")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFUserDecisionOnSpecialFeature")))
@interface USDKTCFUserDecisionOnSpecialFeature : USDKBaseTCFUserDecision
- (instancetype)initWithId:(int32_t)id consent:(BOOL)consent __attribute__((swift_name("init(id:consent:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (USDKTCFUserDecisionOnSpecialFeature *)doCopyId:(int32_t)id consent:(BOOL)consent __attribute__((swift_name("doCopy(id:consent:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL consent __attribute__((swift_name("consent")));
@property int32_t id __attribute__((swift_name("id")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFUserDecisionOnVendor")))
@interface USDKTCFUserDecisionOnVendor : USDKBaseTCFUserDecision
- (instancetype)initWithId:(int32_t)id consent:(BOOL)consent legitimateInterestConsent:(BOOL)legitimateInterestConsent __attribute__((swift_name("init(id:consent:legitimateInterestConsent:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (USDKTCFUserDecisionOnVendor *)doCopyId:(int32_t)id consent:(BOOL)consent legitimateInterestConsent:(BOOL)legitimateInterestConsent __attribute__((swift_name("doCopy(id:consent:legitimateInterestConsent:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL consent __attribute__((swift_name("consent")));
@property int32_t id __attribute__((swift_name("id")));
@property BOOL legitimateInterestConsent __attribute__((swift_name("legitimateInterestConsent")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFUserDecisions")))
@interface USDKTCFUserDecisions : USDKBase
- (instancetype)initWithPurposes:(NSArray<USDKTCFUserDecisionOnPurpose *> * _Nullable)purposes specialFeatures:(NSArray<USDKTCFUserDecisionOnSpecialFeature *> * _Nullable)specialFeatures vendors:(NSArray<USDKTCFUserDecisionOnVendor *> * _Nullable)vendors __attribute__((swift_name("init(purposes:specialFeatures:vendors:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFUserDecisions *)doCopyPurposes:(NSArray<USDKTCFUserDecisionOnPurpose *> * _Nullable)purposes specialFeatures:(NSArray<USDKTCFUserDecisionOnSpecialFeature *> * _Nullable)specialFeatures vendors:(NSArray<USDKTCFUserDecisionOnVendor *> * _Nullable)vendors __attribute__((swift_name("doCopy(purposes:specialFeatures:vendors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKTCFUserDecisionOnPurpose *> * _Nullable purposes __attribute__((swift_name("purposes")));
@property NSArray<USDKTCFUserDecisionOnSpecialFeature *> * _Nullable specialFeatures __attribute__((swift_name("specialFeatures")));
@property NSArray<USDKTCFUserDecisionOnVendor *> * _Nullable vendors __attribute__((swift_name("vendors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFVendor")))
@interface USDKTCFVendor : USDKBase
- (instancetype)initWithConsent:(BOOL)consent features:(NSArray<USDKIdAndName *> *)features flexiblePurposes:(NSArray<USDKIdAndName *> *)flexiblePurposes id:(int32_t)id legitimateInterestConsent:(BOOL)legitimateInterestConsent legitimateInterestPurposes:(NSArray<USDKIdAndName *> *)legitimateInterestPurposes name:(NSString *)name policyUrl:(NSString *)policyUrl purposes:(NSArray<USDKIdAndName *> *)purposes specialFeatures:(NSArray<USDKIdAndName *> *)specialFeatures specialPurposes:(NSArray<USDKIdAndName *> *)specialPurposes showConsentToggle:(BOOL)showConsentToggle showLegitimateInterestConsentToggle:(BOOL)showLegitimateInterestConsentToggle __attribute__((swift_name("init(consent:features:flexiblePurposes:id:legitimateInterestConsent:legitimateInterestPurposes:name:policyUrl:purposes:specialFeatures:specialPurposes:showConsentToggle:showLegitimateInterestConsentToggle:)"))) __attribute__((objc_designated_initializer));
- (USDKTCFVendor *)doCopyConsent:(BOOL)consent features:(NSArray<USDKIdAndName *> *)features flexiblePurposes:(NSArray<USDKIdAndName *> *)flexiblePurposes id:(int32_t)id legitimateInterestConsent:(BOOL)legitimateInterestConsent legitimateInterestPurposes:(NSArray<USDKIdAndName *> *)legitimateInterestPurposes name:(NSString *)name policyUrl:(NSString *)policyUrl purposes:(NSArray<USDKIdAndName *> *)purposes specialFeatures:(NSArray<USDKIdAndName *> *)specialFeatures specialPurposes:(NSArray<USDKIdAndName *> *)specialPurposes showConsentToggle:(BOOL)showConsentToggle showLegitimateInterestConsentToggle:(BOOL)showLegitimateInterestConsentToggle __attribute__((swift_name("doCopy(consent:features:flexiblePurposes:id:legitimateInterestConsent:legitimateInterestPurposes:name:policyUrl:purposes:specialFeatures:specialPurposes:showConsentToggle:showLegitimateInterestConsentToggle:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property BOOL consent __attribute__((swift_name("consent")));
@property NSArray<USDKIdAndName *> *features __attribute__((swift_name("features")));
@property NSArray<USDKIdAndName *> *flexiblePurposes __attribute__((swift_name("flexiblePurposes")));
@property int32_t id __attribute__((swift_name("id")));
@property BOOL legitimateInterestConsent __attribute__((swift_name("legitimateInterestConsent")));
@property NSArray<USDKIdAndName *> *legitimateInterestPurposes __attribute__((swift_name("legitimateInterestPurposes")));
@property NSString *name __attribute__((swift_name("name")));
@property NSString *policyUrl __attribute__((swift_name("policyUrl")));
@property NSArray<USDKIdAndName *> *purposes __attribute__((swift_name("purposes")));
@property BOOL showConsentToggle __attribute__((swift_name("showConsentToggle")));
@property BOOL showLegitimateInterestConsentToggle __attribute__((swift_name("showLegitimateInterestConsentToggle")));
@property NSArray<USDKIdAndName *> *specialFeatures __attribute__((swift_name("specialFeatures")));
@property NSArray<USDKIdAndName *> *specialPurposes __attribute__((swift_name("specialPurposes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFVendor.Companion")))
@interface USDKTCFVendorCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCModelInitOptions")))
@interface USDKTCModelInitOptions : USDKBase
- (instancetype)initWithCmpId:(int32_t)cmpId cmpVersion:(int32_t)cmpVersion tcString:(NSString * _Nullable)tcString __attribute__((swift_name("init(cmpId:cmpVersion:tcString:)"))) __attribute__((objc_designated_initializer));
- (USDKTCModelInitOptions *)doCopyCmpId:(int32_t)cmpId cmpVersion:(int32_t)cmpVersion tcString:(NSString * _Nullable)tcString __attribute__((swift_name("doCopy(cmpId:cmpVersion:tcString:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property int32_t cmpId __attribute__((swift_name("cmpId")));
@property int32_t cmpVersion __attribute__((swift_name("cmpVersion")));
@property NSString * _Nullable tcString __attribute__((swift_name("tcString")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPA")))
@interface USDKCCPA : USDKBase
- (void)clearCCPAData __attribute__((swift_name("clearCCPAData()")));
- (USDKCCPAData *)getCCPAData __attribute__((swift_name("getCCPAData()")));
- (NSString *)getCCPADataAsString __attribute__((swift_name("getCCPADataAsString()")));
- (void)initializeCcpaOptions:(USDKCCPAOptions *)ccpaOptions __attribute__((swift_name("initialize(ccpaOptions:)")));
- (void)setCcpaStorageIsOptedOut:(BOOL)isOptedOut isNoticeGiven:(USDKBoolean * _Nullable)isNoticeGiven __attribute__((swift_name("setCcpaStorage(isOptedOut:isNoticeGiven:)")));
- (BOOL)shouldShowFirstLayer __attribute__((swift_name("shouldShowFirstLayer()")));
@property (readonly) int32_t ccpaVersion __attribute__((swift_name("ccpaVersion")));
@property USDKBoolean * _Nullable isOptedOut __attribute__((swift_name("isOptedOut")));
@property (readonly) USDKUsercentricsLogger *logger __attribute__((swift_name("logger")));
@property (readonly) USDKDeviceStorage *storage __attribute__((swift_name("storage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPA.Companion")))
@interface USDKCCPACompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKCCPA *)createOrReuseStorage:(USDKDeviceStorage *)storage logger:(USDKUsercentricsLogger *)logger __attribute__((swift_name("createOrReuse(storage:logger:)")));
@property USDKCCPA * _Nullable instance __attribute__((swift_name("instance")));
@end;

__attribute__((swift_name("CCPAStorage")))
@protocol USDKCCPAStorage
@required
- (void)deleteKeyKey:(NSString *)key __attribute__((swift_name("deleteKey(key:)")));
- (NSString * _Nullable)getValueKey:(NSString *)key defaultValue:(NSString * _Nullable)defaultValue __attribute__((swift_name("getValue(key:defaultValue:)")));
- (void)putValueKey:(NSString *)key value:(NSString *)value __attribute__((swift_name("putValue(key:value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAStorageProxy")))
@interface USDKCCPAStorageProxy : USDKBase <USDKCCPAStorage>
- (instancetype)initWithStorage:(USDKDeviceStorage *)storage __attribute__((swift_name("init(storage:)"))) __attribute__((objc_designated_initializer));
- (void)deleteKeyKey:(NSString *)key __attribute__((swift_name("deleteKey(key:)")));
- (NSString * _Nullable)getValueKey:(NSString *)key defaultValue:(NSString * _Nullable)defaultValue __attribute__((swift_name("getValue(key:defaultValue:)")));
- (void)putValueKey:(NSString *)key value:(NSString *)value __attribute__((swift_name("putValue(key:value:)")));
@property (readonly) USDKDeviceStorage *storage __attribute__((swift_name("storage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentsApi")))
@interface USDKConsentsApi : USDKBase
- (instancetype)initWithLogger:(USDKUsercentricsLogger *)logger restClient:(USDKHttpRequests *)restClient json:(USDKKotlinx_serialization_runtimeJson *)json storageInstance:(USDKDeviceStorage *)storageInstance retryTimer:(USDKTimeTimer *)retryTimer __attribute__((swift_name("init(logger:restClient:json:storageInstance:retryTimer:)"))) __attribute__((objc_designated_initializer));
- (void)fetchUserConsentsControllerId:(NSString *)controllerId onSuccess:(void (^)(NSArray<USDKUserConsentResponse *> *))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("fetchUserConsents(controllerId:onSuccess:onError:)")));
- (void)saveConsentsConsentsData:(USDKSaveConsentsData *)consentsData onSuccess:(void (^ _Nullable)(void))onSuccess __attribute__((swift_name("saveConsents(consentsData:onSuccess:)")));
- (void)saveTCFConsentsTcString:(NSString *)tcString settings:(USDKExtendedSettings *)settings consentAction:(USDKConsentAction *)consentAction consentType:(USDKConsentType *)consentType onSuccess:(void (^)(void))onSuccess __attribute__((swift_name("saveTCFConsents(tcString:settings:consentAction:consentType:onSuccess:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HttpRequests")))
@interface USDKHttpRequests : USDKBase
- (instancetype)initWithClient:(USDKKtor_client_coreHttpClient *)client __attribute__((swift_name("init(client:)"))) __attribute__((objc_designated_initializer));
- (void)getUrl:(NSString *)url optionalOptions:(USDKOptionalOptions * _Nullable)optionalOptions onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("get(url:optionalOptions:onSuccess:onError:)")));
- (void)postUrl:(NSString *)url bodyData:(NSString *)bodyData optionalOptions:(USDKOptionalOptions * _Nullable)optionalOptions onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("post(url:bodyData:optionalOptions:onSuccess:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LanguageApi")))
@interface USDKLanguageApi : USDKBase
- (instancetype)initWithLogger:(USDKUsercentricsLogger *)logger restClient:(USDKHttpRequests *)restClient json:(USDKKotlinx_serialization_runtimeJson *)json __attribute__((swift_name("init(logger:restClient:json:)"))) __attribute__((objc_designated_initializer));
- (void)fetchAvailableLanguagesSettingsId:(NSString *)settingsId jsonFileVersion:(NSString *)jsonFileVersion onSuccess:(void (^)(NSArray<NSString *> *))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("fetchAvailableLanguages(settingsId:jsonFileVersion:onSuccess:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServicesApi")))
@interface USDKServicesApi : USDKBase
- (instancetype)initWithLogger:(USDKUsercentricsLogger *)logger restClient:(USDKHttpRequests *)restClient json:(USDKKotlinx_serialization_runtimeJson *)json __attribute__((swift_name("init(logger:restClient:json:)"))) __attribute__((objc_designated_initializer));
- (void)fetchServicesJsonLanguage:(NSString *)language services:(NSArray<id<USDKApiBaseServiceInterface>> *)services onSuccess:(void (^)(NSArray<USDKApiAggregatorService *> *))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("fetchServicesJson(language:services:onSuccess:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SettingsApi")))
@interface USDKSettingsApi : USDKBase
- (instancetype)initWithLogger:(USDKUsercentricsLogger *)logger restClient:(USDKHttpRequests *)restClient json:(USDKKotlinx_serialization_runtimeJson *)json __attribute__((swift_name("init(logger:restClient:json:)"))) __attribute__((objc_designated_initializer));
- (void)fetchSettingsJsonSettingsId:(NSString *)settingsId jsonFileVersion:(NSString *)jsonFileVersion jsonFileLanguage:(NSString *)jsonFileLanguage onSuccess:(void (^)(USDKApiSettings *))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("fetchSettingsJson(settingsId:jsonFileVersion:jsonFileLanguage:onSuccess:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserCountryApi")))
@interface USDKUserCountryApi : USDKBase
- (instancetype)initWithLogger:(USDKUsercentricsLogger *)logger restClient:(USDKHttpRequests *)restClient json:(USDKKotlinx_serialization_runtimeJson *)json __attribute__((swift_name("init(logger:restClient:json:)"))) __attribute__((objc_designated_initializer));
- (void)fetchUserCountryOnSuccess:(void (^)(USDKLocationData *))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("fetchUserCountry(onSuccess:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataFacade")))
@interface USDKDataFacade : USDKBase
- (instancetype)initWithConsentsApi:(USDKConsentsApi *)consentsApi settingsInstance:(USDKSettingsService *)settingsInstance storageInstance:(USDKDeviceStorage *)storageInstance eventDispatcherInstance:(USDKEventDispatcher *)eventDispatcherInstance json:(USDKKotlinx_serialization_runtimeJson *)json logger:(USDKUsercentricsLogger *)logger __attribute__((swift_name("init(consentsApi:settingsInstance:storageInstance:eventDispatcherInstance:json:logger:)"))) __attribute__((objc_designated_initializer));
- (void)executeControllerId:(NSString *)controllerId services:(NSArray<USDKService *> *)services consentAction:(USDKConsentAction *)consentAction consentType:(USDKConsentType *)consentType consentString:(USDKGraphQLConsentString * _Nullable)consentString onSuccess:(void (^)(void))onSuccess __attribute__((swift_name("execute(controllerId:services:consentAction:consentType:consentString:onSuccess:)")));
- (USDKMergedServicesSettings *)getMergedServicesAndSettingsFromStorage __attribute__((swift_name("getMergedServicesAndSettingsFromStorage()")));
- (void)mergeSettingsFromStorageControllerId:(NSString *)controllerId callback:(void (^)(void))callback __attribute__((swift_name("mergeSettingsFromStorage(controllerId:callback:)")));
- (void)restoreUserSessionControllerId:(NSString *)controllerId onSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKUCError *))onError __attribute__((swift_name("restoreUserSession(controllerId:onSuccess:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAApi")))
@interface USDKCCPAApi : USDKBase
- (instancetype)initWithStorage:(id<USDKCCPAStorage>)storage debug:(void (^)(NSString *))debug __attribute__((swift_name("init(storage:debug:)"))) __attribute__((objc_designated_initializer));
- (void)clearPrivacyData __attribute__((swift_name("clearPrivacyData()")));
- (USDKCCPAData *)getPrivacyDataApiVersion:(int32_t)apiVersion __attribute__((swift_name("getPrivacyData(apiVersion:)")));
- (NSString *)getPrivacyDataAsStringApiVersion:(int32_t)apiVersion __attribute__((swift_name("getPrivacyDataAsString(apiVersion:)")));
- (void)setPrivacyDataApiVersion:(int32_t)apiVersion ccpaData:(USDKCCPAData *)ccpaData __attribute__((swift_name("setPrivacyData(apiVersion:ccpaData:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAApi.Companion")))
@interface USDKCCPAApiCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *privacyStringStorageKey __attribute__((swift_name("privacyStringStorageKey")));
@property (readonly) int32_t supportedApiVersion __attribute__((swift_name("supportedApiVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAData")))
@interface USDKCCPAData : USDKBase
- (instancetype)initWithVersion:(int32_t)version noticeGiven:(USDKBoolean * _Nullable)noticeGiven optedOut:(USDKBoolean * _Nullable)optedOut lspact:(USDKBoolean * _Nullable)lspact __attribute__((swift_name("init(version:noticeGiven:optedOut:lspact:)"))) __attribute__((objc_designated_initializer));
- (USDKCCPAData *)doCopyVersion:(int32_t)version noticeGiven:(USDKBoolean * _Nullable)noticeGiven optedOut:(USDKBoolean * _Nullable)optedOut lspact:(USDKBoolean * _Nullable)lspact __attribute__((swift_name("doCopy(version:noticeGiven:optedOut:lspact:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)toCCPAString __attribute__((swift_name("toCCPAString()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKBoolean * _Nullable lspact __attribute__((swift_name("lspact")));
@property (readonly) USDKBoolean * _Nullable noticeGiven __attribute__((swift_name("noticeGiven")));
@property USDKBoolean * _Nullable optedOut __attribute__((swift_name("optedOut")));
@property (readonly) int32_t version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAData.Companion")))
@interface USDKCCPADataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKCCPAData *)fromCCPAStringCcpaString:(NSString *)ccpaString __attribute__((swift_name("fromCCPAString(ccpaString:)")));
@end;

__attribute__((swift_name("KotlinException")))
@interface USDKKotlinException : USDKKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAException")))
@interface USDKCCPAException : USDKKotlinException
- (instancetype)initWithMessage:(NSString *)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAException.Companion")))
@interface USDKCCPAExceptionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKCCPAException *)invalidStringCcpaString:(NSString *)ccpaString __attribute__((swift_name("invalidString(ccpaString:)")));
- (USDKCCPAException *)invalidVersionSupportedApiVersion:(int32_t)supportedApiVersion incomingVersion:(int32_t)incomingVersion __attribute__((swift_name("invalidVersion(supportedApiVersion:incomingVersion:)")));
- (USDKCCPAException *)parseStringCcpaString:(NSString *)ccpaString cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("parseString(ccpaString:cause:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAStringValidator")))
@interface USDKCCPAStringValidator : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)cCPAStringValidator __attribute__((swift_name("init()")));
- (BOOL)isValidStringCcpaString:(NSString *)ccpaString __attribute__((swift_name("isValidString(ccpaString:)")));
@property (readonly) NSString *initialValue __attribute__((swift_name("initialValue")));
@end;

__attribute__((swift_name("Declarations")))
@interface USDKDeclarations : USDKBase
- (instancetype)initWithPurposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)purposes specialPurposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)specialPurposes features:(NSDictionary<NSString *, USDKFeature *> * _Nullable)features specialFeatures:(NSDictionary<NSString *, USDKFeature *> * _Nullable)specialFeatures stacks:(NSDictionary<NSString *, USDKStack *> * _Nullable)stacks __attribute__((swift_name("init(purposes:specialPurposes:features:specialFeatures:stacks:)"))) __attribute__((objc_designated_initializer));
@property NSDictionary<NSString *, USDKFeature *> * _Nullable features __attribute__((swift_name("features")));
@property NSDictionary<NSString *, USDKPurpose *> * _Nullable purposes __attribute__((swift_name("purposes")));
@property NSDictionary<NSString *, USDKFeature *> * _Nullable specialFeatures __attribute__((swift_name("specialFeatures")));
@property NSDictionary<NSString *, USDKPurpose *> * _Nullable specialPurposes __attribute__((swift_name("specialPurposes")));
@property NSDictionary<NSString *, USDKStack *> * _Nullable stacks __attribute__((swift_name("stacks")));
@end;

__attribute__((swift_name("VendorList")))
@interface USDKVendorList : USDKDeclarations
- (instancetype)initWithLastUpdated:(NSString * _Nullable)lastUpdated gvlSpecificationVersion:(USDKInt * _Nullable)gvlSpecificationVersion vendorListVersion:(USDKInt * _Nullable)vendorListVersion tcfPolicyVersion:(USDKInt * _Nullable)tcfPolicyVersion vendors:(NSDictionary<NSString *, USDKVendor_ *> * _Nullable)vendors purposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)purposes features:(NSDictionary<NSString *, USDKFeature *> * _Nullable)features specialFeatures:(NSDictionary<NSString *, USDKFeature *> * _Nullable)specialFeatures specialPurposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)specialPurposes stacks:(NSDictionary<NSString *, USDKStack *> * _Nullable)stacks __attribute__((swift_name("init(lastUpdated:gvlSpecificationVersion:vendorListVersion:tcfPolicyVersion:vendors:purposes:features:specialFeatures:specialPurposes:stacks:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPurposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)purposes specialPurposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)specialPurposes features:(NSDictionary<NSString *, USDKFeature *> * _Nullable)features specialFeatures:(NSDictionary<NSString *, USDKFeature *> * _Nullable)specialFeatures stacks:(NSDictionary<NSString *, USDKStack *> * _Nullable)stacks __attribute__((swift_name("init(purposes:specialPurposes:features:specialFeatures:stacks:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property NSDictionary<NSString *, USDKFeature *> * _Nullable features __attribute__((swift_name("features")));
@property USDKInt * _Nullable gvlSpecificationVersion __attribute__((swift_name("gvlSpecificationVersion")));
@property NSString * _Nullable lastUpdated __attribute__((swift_name("lastUpdated")));
@property NSDictionary<NSString *, USDKPurpose *> * _Nullable purposes __attribute__((swift_name("purposes")));
@property NSDictionary<NSString *, USDKFeature *> * _Nullable specialFeatures __attribute__((swift_name("specialFeatures")));
@property NSDictionary<NSString *, USDKPurpose *> * _Nullable specialPurposes __attribute__((swift_name("specialPurposes")));
@property NSDictionary<NSString *, USDKStack *> * _Nullable stacks __attribute__((swift_name("stacks")));
@property USDKInt * _Nullable tcfPolicyVersion __attribute__((swift_name("tcfPolicyVersion")));
@property USDKInt * _Nullable vendorListVersion __attribute__((swift_name("vendorListVersion")));
@property NSDictionary<NSString *, USDKVendor_ *> * _Nullable vendors __attribute__((swift_name("vendors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL")))
@interface USDKGVL : USDKVendorList
- (instancetype)initWithJsonHttpClient:(id<USDKJsonHttpClient>)jsonHttpClient versionOrVendorList:(id _Nullable)versionOrVendorList lastUpdated:(NSString * _Nullable)lastUpdated gvlSpecificationVersion:(USDKInt * _Nullable)gvlSpecificationVersion vendorListVersion:(USDKInt * _Nullable)vendorListVersion tcfPolicyVersion:(USDKInt * _Nullable)tcfPolicyVersion vendors:(NSDictionary<NSString *, USDKVendor_ *> * _Nullable)vendors features:(NSDictionary<NSString *, USDKFeature *> * _Nullable)features purposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)purposes specialFeatures:(NSDictionary<NSString *, USDKFeature *> * _Nullable)specialFeatures specialPurposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)specialPurposes stacks:(NSDictionary<NSString *, USDKStack *> * _Nullable)stacks __attribute__((swift_name("init(jsonHttpClient:versionOrVendorList:lastUpdated:gvlSpecificationVersion:vendorListVersion:tcfPolicyVersion:vendors:features:purposes:specialFeatures:specialPurposes:stacks:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithLastUpdated:(NSString * _Nullable)lastUpdated gvlSpecificationVersion:(USDKInt * _Nullable)gvlSpecificationVersion vendorListVersion:(USDKInt * _Nullable)vendorListVersion tcfPolicyVersion:(USDKInt * _Nullable)tcfPolicyVersion vendors:(NSDictionary<NSString *, USDKVendor_ *> * _Nullable)vendors purposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)purposes features:(NSDictionary<NSString *, USDKFeature *> * _Nullable)features specialFeatures:(NSDictionary<NSString *, USDKFeature *> * _Nullable)specialFeatures specialPurposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)specialPurposes stacks:(NSDictionary<NSString *, USDKStack *> * _Nullable)stacks __attribute__((swift_name("init(lastUpdated:gvlSpecificationVersion:vendorListVersion:tcfPolicyVersion:vendors:purposes:features:specialFeatures:specialPurposes:stacks:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)changeLanguageLang:(NSString *)lang onSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKGVLError *))onError __attribute__((swift_name("changeLanguage(lang:onSuccess:onError:)")));
- (USDKGVL *)doCopyJsonHttpClient:(id<USDKJsonHttpClient>)jsonHttpClient versionOrVendorList:(id _Nullable)versionOrVendorList lastUpdated:(NSString * _Nullable)lastUpdated gvlSpecificationVersion:(USDKInt * _Nullable)gvlSpecificationVersion vendorListVersion:(USDKInt * _Nullable)vendorListVersion tcfPolicyVersion:(USDKInt * _Nullable)tcfPolicyVersion vendors:(NSDictionary<NSString *, USDKVendor_ *> * _Nullable)vendors features:(NSDictionary<NSString *, USDKFeature *> * _Nullable)features purposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)purposes specialFeatures:(NSDictionary<NSString *, USDKFeature *> * _Nullable)specialFeatures specialPurposes:(NSDictionary<NSString *, USDKPurpose *> * _Nullable)specialPurposes stacks:(NSDictionary<NSString *, USDKStack *> * _Nullable)stacks __attribute__((swift_name("doCopy(jsonHttpClient:versionOrVendorList:lastUpdated:gvlSpecificationVersion:vendorListVersion:tcfPolicyVersion:vendors:features:purposes:specialFeatures:specialPurposes:stacks:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (BOOL)getIsReady __attribute__((swift_name("getIsReady()")));
- (USDKVendorList *)getJson __attribute__((swift_name("getJson()")));
- (NSString *)getLanguage __attribute__((swift_name("getLanguage()")));
- (void)getReadyPromiseCallback:(void (^)(void))callback __attribute__((swift_name("getReadyPromise(callback:)")));
- (NSDictionary<NSString *, USDKVendor_ *> *)getVendorsWithConsentPurposePurposeId:(int32_t)purposeId __attribute__((swift_name("getVendorsWithConsentPurpose(purposeId:)")));
- (NSDictionary<NSString *, USDKVendor_ *> *)getVendorsWithFeatureFeatureId:(int32_t)featureId __attribute__((swift_name("getVendorsWithFeature(featureId:)")));
- (NSDictionary<NSString *, USDKVendor_ *> *)getVendorsWithFlexiblePurposePurposeId:(int32_t)purposeId __attribute__((swift_name("getVendorsWithFlexiblePurpose(purposeId:)")));
- (NSDictionary<NSString *, USDKVendor_ *> *)getVendorsWithLegIntPurposePurposeId:(int32_t)purposeId __attribute__((swift_name("getVendorsWithLegIntPurpose(purposeId:)")));
- (NSDictionary<NSString *, USDKVendor_ *> *)getVendorsWithSpecialFeatureSpecialFeatureId:(int32_t)specialFeatureId __attribute__((swift_name("getVendorsWithSpecialFeature(specialFeatureId:)")));
- (NSDictionary<NSString *, USDKVendor_ *> *)getVendorsWithSpecialPurposeSpecialPurposeId:(int32_t)specialPurposeId __attribute__((swift_name("getVendorsWithSpecialPurpose(specialPurposeId:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (void)narrowVendorsToVendorIds:(NSArray<USDKInt *> *)vendorIds __attribute__((swift_name("narrowVendorsTo(vendorIds:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKFeature *> * _Nullable features __attribute__((swift_name("features")));
@property USDKInt * _Nullable gvlSpecificationVersion __attribute__((swift_name("gvlSpecificationVersion")));
@property NSString * _Nullable lastUpdated __attribute__((swift_name("lastUpdated")));
@property NSDictionary<NSString *, USDKPurpose *> * _Nullable purposes __attribute__((swift_name("purposes")));
@property void (^readyPromise)(USDKKotlinUnit *(^)(void)) __attribute__((swift_name("readyPromise")));
@property NSDictionary<NSString *, USDKFeature *> * _Nullable specialFeatures __attribute__((swift_name("specialFeatures")));
@property NSDictionary<NSString *, USDKPurpose *> * _Nullable specialPurposes __attribute__((swift_name("specialPurposes")));
@property NSDictionary<NSString *, USDKStack *> * _Nullable stacks __attribute__((swift_name("stacks")));
@property USDKInt * _Nullable tcfPolicyVersion __attribute__((swift_name("tcfPolicyVersion")));
@property NSSet<USDKInt *> *vendorIds __attribute__((swift_name("vendorIds")));
@property USDKInt * _Nullable vendorListVersion __attribute__((swift_name("vendorListVersion")));
@property NSDictionary<NSString *, USDKVendor_ *> * _Nullable vendors __attribute__((swift_name("vendors")));
@property id _Nullable versionOrVendorList __attribute__((swift_name("versionOrVendorList")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.Companion")))
@interface USDKGVLCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (BOOL)emptyCacheVendorListVersion:(USDKInt * _Nullable)vendorListVersion __attribute__((swift_name("emptyCache(vendorListVersion:)")));
- (BOOL)emptyLanguageCacheLang:(NSString * _Nullable)lang __attribute__((swift_name("emptyLanguageCache(lang:)")));
- (NSString *)getBaseUrl __attribute__((swift_name("getBaseUrl()")));
- (NSString *)getLanguageFileName __attribute__((swift_name("getLanguageFileName()")));
- (void)setBaseUrlUrl:(NSString *)url __attribute__((swift_name("setBaseUrl(url:)")));
- (void)setLangFilenameFileName:(NSString *)fileName __attribute__((swift_name("setLangFilename(fileName:)")));
- (void)setLateFilenameFileName:(NSString *)fileName __attribute__((swift_name("setLateFilename(fileName:)")));
@property (readonly) NSString *DEFAULT_LANGUAGE __attribute__((swift_name("DEFAULT_LANGUAGE")));
@property (readonly) USDKConsentLanguages *consentLanguages __attribute__((swift_name("consentLanguages")));
@property NSString *languageFilename __attribute__((swift_name("languageFilename")));
@property NSString *latestFilename __attribute__((swift_name("latestFilename")));
@property (readonly) NSString *versionedFilename __attribute__((swift_name("versionedFilename")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedBaseParams")))
@interface USDKGVLParsedBaseParams : USDKBase
- (instancetype)initWithLastUpdated:(NSString * _Nullable)lastUpdated gvlSpecificationVersion:(USDKInt * _Nullable)gvlSpecificationVersion vendorListVersion:(USDKInt * _Nullable)vendorListVersion tcfPolicyVersion:(USDKInt * _Nullable)tcfPolicyVersion __attribute__((swift_name("init(lastUpdated:gvlSpecificationVersion:vendorListVersion:tcfPolicyVersion:)"))) __attribute__((objc_designated_initializer));
- (USDKGVLParsedBaseParams *)doCopyLastUpdated:(NSString * _Nullable)lastUpdated gvlSpecificationVersion:(USDKInt * _Nullable)gvlSpecificationVersion vendorListVersion:(USDKInt * _Nullable)vendorListVersion tcfPolicyVersion:(USDKInt * _Nullable)tcfPolicyVersion __attribute__((swift_name("doCopy(lastUpdated:gvlSpecificationVersion:vendorListVersion:tcfPolicyVersion:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKInt * _Nullable gvlSpecificationVersion __attribute__((swift_name("gvlSpecificationVersion")));
@property NSString * _Nullable lastUpdated __attribute__((swift_name("lastUpdated")));
@property USDKInt * _Nullable tcfPolicyVersion __attribute__((swift_name("tcfPolicyVersion")));
@property USDKInt * _Nullable vendorListVersion __attribute__((swift_name("vendorListVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedBaseParamsCompanion")))
@interface USDKGVLParsedBaseParamsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedFeatures")))
@interface USDKGVLParsedFeatures : USDKBase
- (instancetype)initWithFeatures:(NSDictionary<NSString *, USDKFeature *> *)features __attribute__((swift_name("init(features:)"))) __attribute__((objc_designated_initializer));
- (USDKGVLParsedFeatures *)doCopyFeatures:(NSDictionary<NSString *, USDKFeature *> *)features __attribute__((swift_name("doCopy(features:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKFeature *> *features __attribute__((swift_name("features")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedFeaturesCompanion")))
@interface USDKGVLParsedFeaturesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedPurposes")))
@interface USDKGVLParsedPurposes : USDKBase
- (instancetype)initWithPurposes:(NSDictionary<NSString *, USDKPurpose *> *)purposes __attribute__((swift_name("init(purposes:)"))) __attribute__((objc_designated_initializer));
- (USDKGVLParsedPurposes *)doCopyPurposes:(NSDictionary<NSString *, USDKPurpose *> *)purposes __attribute__((swift_name("doCopy(purposes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKPurpose *> *purposes __attribute__((swift_name("purposes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedPurposesCompanion")))
@interface USDKGVLParsedPurposesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedSpecialFeatures")))
@interface USDKGVLParsedSpecialFeatures : USDKBase
- (instancetype)initWithSpecialFeatures:(NSDictionary<NSString *, USDKFeature *> *)specialFeatures __attribute__((swift_name("init(specialFeatures:)"))) __attribute__((objc_designated_initializer));
- (USDKGVLParsedSpecialFeatures *)doCopySpecialFeatures:(NSDictionary<NSString *, USDKFeature *> *)specialFeatures __attribute__((swift_name("doCopy(specialFeatures:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKFeature *> *specialFeatures __attribute__((swift_name("specialFeatures")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedSpecialFeaturesCompanion")))
@interface USDKGVLParsedSpecialFeaturesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedSpecialPurposes")))
@interface USDKGVLParsedSpecialPurposes : USDKBase
- (instancetype)initWithSpecialPurposes:(NSDictionary<NSString *, USDKPurpose *> *)specialPurposes __attribute__((swift_name("init(specialPurposes:)"))) __attribute__((objc_designated_initializer));
- (USDKGVLParsedSpecialPurposes *)doCopySpecialPurposes:(NSDictionary<NSString *, USDKPurpose *> *)specialPurposes __attribute__((swift_name("doCopy(specialPurposes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKPurpose *> *specialPurposes __attribute__((swift_name("specialPurposes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedSpecialPurposesCompanion")))
@interface USDKGVLParsedSpecialPurposesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedStacks")))
@interface USDKGVLParsedStacks : USDKBase
- (instancetype)initWithStacks:(NSDictionary<NSString *, USDKStack *> *)stacks __attribute__((swift_name("init(stacks:)"))) __attribute__((objc_designated_initializer));
- (USDKGVLParsedStacks *)doCopyStacks:(NSDictionary<NSString *, USDKStack *> *)stacks __attribute__((swift_name("doCopy(stacks:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKStack *> *stacks __attribute__((swift_name("stacks")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedStacksCompanion")))
@interface USDKGVLParsedStacksCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedVendors")))
@interface USDKGVLParsedVendors : USDKBase
- (instancetype)initWithVendors:(NSDictionary<NSString *, USDKVendor_ *> *)vendors __attribute__((swift_name("init(vendors:)"))) __attribute__((objc_designated_initializer));
- (USDKGVLParsedVendors *)doCopyVendors:(NSDictionary<NSString *, USDKVendor_ *> *)vendors __attribute__((swift_name("doCopy(vendors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSDictionary<NSString *, USDKVendor_ *> *vendors __attribute__((swift_name("vendors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GVL.ParsedVendorsCompanion")))
@interface USDKGVLParsedVendorsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IABTCF_KEYS")))
@interface USDKIABTCF_KEYS : USDKBase
- (instancetype)initWithIABTCF_CmpSdkID:(USDKInt * _Nullable)IABTCF_CmpSdkID IABTCF_CmpSdkVersion:(USDKInt * _Nullable)IABTCF_CmpSdkVersion IABTCF_PolicyVersion:(USDKInt * _Nullable)IABTCF_PolicyVersion IABTCF_gdprApplies:(USDKInt * _Nullable)IABTCF_gdprApplies IABTCF_PublisherCC:(NSString * _Nullable)IABTCF_PublisherCC IABTCF_PurposeOneTreatment:(USDKInt * _Nullable)IABTCF_PurposeOneTreatment IABTCF_UseNonStandardStacks:(USDKInt * _Nullable)IABTCF_UseNonStandardStacks IABTCF_TCString:(NSString * _Nullable)IABTCF_TCString IABTCF_VendorConsents:(NSString * _Nullable)IABTCF_VendorConsents IABTCF_VendorLegitimateInterests:(NSString * _Nullable)IABTCF_VendorLegitimateInterests IABTCF_PurposeConsents:(NSString * _Nullable)IABTCF_PurposeConsents IABTCF_PurposeLegitimateInterests:(NSString * _Nullable)IABTCF_PurposeLegitimateInterests IABTCF_SpecialFeaturesOptIns:(NSString * _Nullable)IABTCF_SpecialFeaturesOptIns IABTCF_PublisherRestrictions:(NSString * _Nullable)IABTCF_PublisherRestrictions IABTCF_PublisherConsent:(NSString * _Nullable)IABTCF_PublisherConsent IABTCF_PublisherLegitimateInterests:(NSString * _Nullable)IABTCF_PublisherLegitimateInterests IABTCF_PublisherCustomPurposesConsents:(NSString * _Nullable)IABTCF_PublisherCustomPurposesConsents IABTCF_PublisherCustomPurposesLegitimateInterests:(NSString * _Nullable)IABTCF_PublisherCustomPurposesLegitimateInterests __attribute__((swift_name("init(IABTCF_CmpSdkID:IABTCF_CmpSdkVersion:IABTCF_PolicyVersion:IABTCF_gdprApplies:IABTCF_PublisherCC:IABTCF_PurposeOneTreatment:IABTCF_UseNonStandardStacks:IABTCF_TCString:IABTCF_VendorConsents:IABTCF_VendorLegitimateInterests:IABTCF_PurposeConsents:IABTCF_PurposeLegitimateInterests:IABTCF_SpecialFeaturesOptIns:IABTCF_PublisherRestrictions:IABTCF_PublisherConsent:IABTCF_PublisherLegitimateInterests:IABTCF_PublisherCustomPurposesConsents:IABTCF_PublisherCustomPurposesLegitimateInterests:)"))) __attribute__((objc_designated_initializer));
- (USDKIABTCF_KEYS *)doCopyIABTCF_CmpSdkID:(USDKInt * _Nullable)IABTCF_CmpSdkID IABTCF_CmpSdkVersion:(USDKInt * _Nullable)IABTCF_CmpSdkVersion IABTCF_PolicyVersion:(USDKInt * _Nullable)IABTCF_PolicyVersion IABTCF_gdprApplies:(USDKInt * _Nullable)IABTCF_gdprApplies IABTCF_PublisherCC:(NSString * _Nullable)IABTCF_PublisherCC IABTCF_PurposeOneTreatment:(USDKInt * _Nullable)IABTCF_PurposeOneTreatment IABTCF_UseNonStandardStacks:(USDKInt * _Nullable)IABTCF_UseNonStandardStacks IABTCF_TCString:(NSString * _Nullable)IABTCF_TCString IABTCF_VendorConsents:(NSString * _Nullable)IABTCF_VendorConsents IABTCF_VendorLegitimateInterests:(NSString * _Nullable)IABTCF_VendorLegitimateInterests IABTCF_PurposeConsents:(NSString * _Nullable)IABTCF_PurposeConsents IABTCF_PurposeLegitimateInterests:(NSString * _Nullable)IABTCF_PurposeLegitimateInterests IABTCF_SpecialFeaturesOptIns:(NSString * _Nullable)IABTCF_SpecialFeaturesOptIns IABTCF_PublisherRestrictions:(NSString * _Nullable)IABTCF_PublisherRestrictions IABTCF_PublisherConsent:(NSString * _Nullable)IABTCF_PublisherConsent IABTCF_PublisherLegitimateInterests:(NSString * _Nullable)IABTCF_PublisherLegitimateInterests IABTCF_PublisherCustomPurposesConsents:(NSString * _Nullable)IABTCF_PublisherCustomPurposesConsents IABTCF_PublisherCustomPurposesLegitimateInterests:(NSString * _Nullable)IABTCF_PublisherCustomPurposesLegitimateInterests __attribute__((swift_name("doCopy(IABTCF_CmpSdkID:IABTCF_CmpSdkVersion:IABTCF_PolicyVersion:IABTCF_gdprApplies:IABTCF_PublisherCC:IABTCF_PurposeOneTreatment:IABTCF_UseNonStandardStacks:IABTCF_TCString:IABTCF_VendorConsents:IABTCF_VendorLegitimateInterests:IABTCF_PurposeConsents:IABTCF_PurposeLegitimateInterests:IABTCF_SpecialFeaturesOptIns:IABTCF_PublisherRestrictions:IABTCF_PublisherConsent:IABTCF_PublisherLegitimateInterests:IABTCF_PublisherCustomPurposesConsents:IABTCF_PublisherCustomPurposesLegitimateInterests:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKInt * _Nullable IABTCF_CmpSdkID __attribute__((swift_name("IABTCF_CmpSdkID")));
@property USDKInt * _Nullable IABTCF_CmpSdkVersion __attribute__((swift_name("IABTCF_CmpSdkVersion")));
@property USDKInt * _Nullable IABTCF_PolicyVersion __attribute__((swift_name("IABTCF_PolicyVersion")));
@property NSString * _Nullable IABTCF_PublisherCC __attribute__((swift_name("IABTCF_PublisherCC")));
@property NSString * _Nullable IABTCF_PublisherConsent __attribute__((swift_name("IABTCF_PublisherConsent")));
@property NSString * _Nullable IABTCF_PublisherCustomPurposesConsents __attribute__((swift_name("IABTCF_PublisherCustomPurposesConsents")));
@property NSString * _Nullable IABTCF_PublisherCustomPurposesLegitimateInterests __attribute__((swift_name("IABTCF_PublisherCustomPurposesLegitimateInterests")));
@property NSString * _Nullable IABTCF_PublisherLegitimateInterests __attribute__((swift_name("IABTCF_PublisherLegitimateInterests")));
@property NSString * _Nullable IABTCF_PublisherRestrictions __attribute__((swift_name("IABTCF_PublisherRestrictions")));
@property NSString * _Nullable IABTCF_PurposeConsents __attribute__((swift_name("IABTCF_PurposeConsents")));
@property NSString * _Nullable IABTCF_PurposeLegitimateInterests __attribute__((swift_name("IABTCF_PurposeLegitimateInterests")));
@property USDKInt * _Nullable IABTCF_PurposeOneTreatment __attribute__((swift_name("IABTCF_PurposeOneTreatment")));
@property NSString * _Nullable IABTCF_SpecialFeaturesOptIns __attribute__((swift_name("IABTCF_SpecialFeaturesOptIns")));
@property NSString * _Nullable IABTCF_TCString __attribute__((swift_name("IABTCF_TCString")));
@property USDKInt * _Nullable IABTCF_UseNonStandardStacks __attribute__((swift_name("IABTCF_UseNonStandardStacks")));
@property NSString * _Nullable IABTCF_VendorConsents __attribute__((swift_name("IABTCF_VendorConsents")));
@property NSString * _Nullable IABTCF_VendorLegitimateInterests __attribute__((swift_name("IABTCF_VendorLegitimateInterests")));
@property USDKInt * _Nullable IABTCF_gdprApplies __attribute__((swift_name("IABTCF_gdprApplies")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCString")))
@interface USDKTCString : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCString.Companion")))
@interface USDKTCStringCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKTCModel *)decodeEncodedTCString:(NSString *)encodedTCString tcModel:(USDKTCModel * _Nullable)tcModel __attribute__((swift_name("decode(encodedTCString:tcModel:)")));
- (NSString *)encodeTcModel:(USDKTCModel *)tcModel options:(USDKEncodingOptions * _Nullable)options __attribute__((swift_name("encode(tcModel:options:)")));
- (USDKIABTCF_KEYS *)encodeIabTCFKeysTcModel:(USDKTCModel *)tcModel options:(USDKEncodingOptions * _Nullable)options __attribute__((swift_name("encodeIabTCFKeys(tcModel:options:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Base64Url")))
@interface USDKBase64Url : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Base64Url.Companion")))
@interface USDKBase64UrlCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (NSString *)decodeStr:(NSString *)str __attribute__((swift_name("decode(str:)")));
- (NSString *)encodeStr:(NSString *)str __attribute__((swift_name("encode(str:)")));
@property int32_t LCM __attribute__((swift_name("LCM")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BitLength")))
@interface USDKBitLength : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKBitLength *cmpid __attribute__((swift_name("cmpid")));
@property (class, readonly) USDKBitLength *cmpversion __attribute__((swift_name("cmpversion")));
@property (class, readonly) USDKBitLength *consentlanguage __attribute__((swift_name("consentlanguage")));
@property (class, readonly) USDKBitLength *consentscreen __attribute__((swift_name("consentscreen")));
@property (class, readonly) USDKBitLength *created __attribute__((swift_name("created")));
@property (class, readonly) USDKBitLength *isservicespecific __attribute__((swift_name("isservicespecific")));
@property (class, readonly) USDKBitLength *lastupdated __attribute__((swift_name("lastupdated")));
@property (class, readonly) USDKBitLength *policyversion __attribute__((swift_name("policyversion")));
@property (class, readonly) USDKBitLength *publishercountrycode __attribute__((swift_name("publishercountrycode")));
@property (class, readonly) USDKBitLength *publisherlegitimateinterests __attribute__((swift_name("publisherlegitimateinterests")));
@property (class, readonly) USDKBitLength *publisherconsents __attribute__((swift_name("publisherconsents")));
@property (class, readonly) USDKBitLength *purposeconsents __attribute__((swift_name("purposeconsents")));
@property (class, readonly) USDKBitLength *purposelegitimateinterests __attribute__((swift_name("purposelegitimateinterests")));
@property (class, readonly) USDKBitLength *purposeonetreatment __attribute__((swift_name("purposeonetreatment")));
@property (class, readonly) USDKBitLength *specialfeatureoptins __attribute__((swift_name("specialfeatureoptins")));
@property (class, readonly) USDKBitLength *usenonstandardstacks __attribute__((swift_name("usenonstandardstacks")));
@property (class, readonly) USDKBitLength *vendorlistversion __attribute__((swift_name("vendorlistversion")));
@property (class, readonly) USDKBitLength *version_ __attribute__((swift_name("version_")));
@property (class, readonly) USDKBitLength *anyboolean __attribute__((swift_name("anyboolean")));
@property (class, readonly) USDKBitLength *encodingtype __attribute__((swift_name("encodingtype")));
@property (class, readonly) USDKBitLength *maxid __attribute__((swift_name("maxid")));
@property (class, readonly) USDKBitLength *numcustompurposes __attribute__((swift_name("numcustompurposes")));
@property (class, readonly) USDKBitLength *numentries __attribute__((swift_name("numentries")));
@property (class, readonly) USDKBitLength *numrestrictions __attribute__((swift_name("numrestrictions")));
@property (class, readonly) USDKBitLength *purposeid __attribute__((swift_name("purposeid")));
@property (class, readonly) USDKBitLength *restrictiontype __attribute__((swift_name("restrictiontype")));
@property (class, readonly) USDKBitLength *segmenttype __attribute__((swift_name("segmenttype")));
@property (class, readonly) USDKBitLength *singleorrange __attribute__((swift_name("singleorrange")));
@property (class, readonly) USDKBitLength *vendorid __attribute__((swift_name("vendorid")));
- (int32_t)compareToOther:(USDKBitLength *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) int32_t integer __attribute__((swift_name("integer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BitLength.Companion")))
@interface USDKBitLengthCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKBitLength * _Nullable)getByNameName:(NSString *)name __attribute__((swift_name("getByName(name:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EncodedTCFKeys")))
@interface USDKEncodedTCFKeys : USDKBase
- (instancetype)initWithVersion:(USDKInt * _Nullable)version numCustomPurposes:(NSString *)numCustomPurposes cmpId:(USDKInt * _Nullable)cmpId cmpVersion:(USDKInt * _Nullable)cmpVersion consentScreen:(NSString *)consentScreen vendorListVersion:(NSString *)vendorListVersion policyVersion:(USDKInt * _Nullable)policyVersion segmentType:(NSString *)segmentType created:(NSString *)created lastUpdated:(NSString *)lastUpdated consentLanguage:(NSString *)consentLanguage publisherCountryCode:(NSString *)publisherCountryCode isServiceSpecific:(NSString *)isServiceSpecific useNonStandardStacks:(NSString *)useNonStandardStacks purposeOneTreatment:(USDKInt * _Nullable)purposeOneTreatment specialFeatureOptins:(NSString *)specialFeatureOptins purposeConsents:(NSString *)purposeConsents purposeLegitimateInterests:(NSString *)purposeLegitimateInterests publisherConsents:(NSString *)publisherConsents publisherLegitimateInterests:(NSString *)publisherLegitimateInterests publisherCustomConsents:(NSString *)publisherCustomConsents publisherCustomLegitimateInterests:(NSString *)publisherCustomLegitimateInterests vendorConsents:(NSString *)vendorConsents vendorLegitimateInterests:(NSString *)vendorLegitimateInterests vendorsDisclosed:(NSString *)vendorsDisclosed vendorsAllowed:(NSString *)vendorsAllowed publisherRestrictions:(NSString *)publisherRestrictions __attribute__((swift_name("init(version:numCustomPurposes:cmpId:cmpVersion:consentScreen:vendorListVersion:policyVersion:segmentType:created:lastUpdated:consentLanguage:publisherCountryCode:isServiceSpecific:useNonStandardStacks:purposeOneTreatment:specialFeatureOptins:purposeConsents:purposeLegitimateInterests:publisherConsents:publisherLegitimateInterests:publisherCustomConsents:publisherCustomLegitimateInterests:vendorConsents:vendorLegitimateInterests:vendorsDisclosed:vendorsAllowed:publisherRestrictions:)"))) __attribute__((objc_designated_initializer));
- (USDKEncodedTCFKeys *)doCopyVersion:(USDKInt * _Nullable)version numCustomPurposes:(NSString *)numCustomPurposes cmpId:(USDKInt * _Nullable)cmpId cmpVersion:(USDKInt * _Nullable)cmpVersion consentScreen:(NSString *)consentScreen vendorListVersion:(NSString *)vendorListVersion policyVersion:(USDKInt * _Nullable)policyVersion segmentType:(NSString *)segmentType created:(NSString *)created lastUpdated:(NSString *)lastUpdated consentLanguage:(NSString *)consentLanguage publisherCountryCode:(NSString *)publisherCountryCode isServiceSpecific:(NSString *)isServiceSpecific useNonStandardStacks:(NSString *)useNonStandardStacks purposeOneTreatment:(USDKInt * _Nullable)purposeOneTreatment specialFeatureOptins:(NSString *)specialFeatureOptins purposeConsents:(NSString *)purposeConsents purposeLegitimateInterests:(NSString *)purposeLegitimateInterests publisherConsents:(NSString *)publisherConsents publisherLegitimateInterests:(NSString *)publisherLegitimateInterests publisherCustomConsents:(NSString *)publisherCustomConsents publisherCustomLegitimateInterests:(NSString *)publisherCustomLegitimateInterests vendorConsents:(NSString *)vendorConsents vendorLegitimateInterests:(NSString *)vendorLegitimateInterests vendorsDisclosed:(NSString *)vendorsDisclosed vendorsAllowed:(NSString *)vendorsAllowed publisherRestrictions:(NSString *)publisherRestrictions __attribute__((swift_name("doCopy(version:numCustomPurposes:cmpId:cmpVersion:consentScreen:vendorListVersion:policyVersion:segmentType:created:lastUpdated:consentLanguage:publisherCountryCode:isServiceSpecific:useNonStandardStacks:purposeOneTreatment:specialFeatureOptins:purposeConsents:purposeLegitimateInterests:publisherConsents:publisherLegitimateInterests:publisherCustomConsents:publisherCustomLegitimateInterests:vendorConsents:vendorLegitimateInterests:vendorsDisclosed:vendorsAllowed:publisherRestrictions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKInt * _Nullable cmpId __attribute__((swift_name("cmpId")));
@property USDKInt * _Nullable cmpVersion __attribute__((swift_name("cmpVersion")));
@property NSString *consentLanguage __attribute__((swift_name("consentLanguage")));
@property NSString *consentScreen __attribute__((swift_name("consentScreen")));
@property NSString *created __attribute__((swift_name("created")));
@property NSString *isServiceSpecific __attribute__((swift_name("isServiceSpecific")));
@property NSString *lastUpdated __attribute__((swift_name("lastUpdated")));
@property NSString *numCustomPurposes __attribute__((swift_name("numCustomPurposes")));
@property USDKInt * _Nullable policyVersion __attribute__((swift_name("policyVersion")));
@property NSString *publisherConsents __attribute__((swift_name("publisherConsents")));
@property NSString *publisherCountryCode __attribute__((swift_name("publisherCountryCode")));
@property NSString *publisherCustomConsents __attribute__((swift_name("publisherCustomConsents")));
@property NSString *publisherCustomLegitimateInterests __attribute__((swift_name("publisherCustomLegitimateInterests")));
@property NSString *publisherLegitimateInterests __attribute__((swift_name("publisherLegitimateInterests")));
@property NSString *publisherRestrictions __attribute__((swift_name("publisherRestrictions")));
@property NSString *purposeConsents __attribute__((swift_name("purposeConsents")));
@property NSString *purposeLegitimateInterests __attribute__((swift_name("purposeLegitimateInterests")));
@property USDKInt * _Nullable purposeOneTreatment __attribute__((swift_name("purposeOneTreatment")));
@property NSString *segmentType __attribute__((swift_name("segmentType")));
@property NSString *specialFeatureOptins __attribute__((swift_name("specialFeatureOptins")));
@property NSString *useNonStandardStacks __attribute__((swift_name("useNonStandardStacks")));
@property NSString *vendorConsents __attribute__((swift_name("vendorConsents")));
@property NSString *vendorLegitimateInterests __attribute__((swift_name("vendorLegitimateInterests")));
@property NSString *vendorListVersion __attribute__((swift_name("vendorListVersion")));
@property NSString *vendorsAllowed __attribute__((swift_name("vendorsAllowed")));
@property NSString *vendorsDisclosed __attribute__((swift_name("vendorsDisclosed")));
@property USDKInt * _Nullable version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EncodingOptions")))
@interface USDKEncodingOptions : USDKBase
- (instancetype)initWithIsForVendors:(USDKBoolean * _Nullable)isForVendors version:(USDKEncodingOptionsVersion * _Nullable)version segments:(NSArray<USDKSegment *> *)segments __attribute__((swift_name("init(isForVendors:version:segments:)"))) __attribute__((objc_designated_initializer));
- (USDKEncodingOptions *)doCopyIsForVendors:(USDKBoolean * _Nullable)isForVendors version:(USDKEncodingOptionsVersion * _Nullable)version segments:(NSArray<USDKSegment *> *)segments __attribute__((swift_name("doCopy(isForVendors:version:segments:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKBoolean * _Nullable isForVendors __attribute__((swift_name("isForVendors")));
@property NSArray<USDKSegment *> *segments __attribute__((swift_name("segments")));
@property USDKEncodingOptionsVersion * _Nullable version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EncodingOptionsVersion")))
@interface USDKEncodingOptionsVersion : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKEncodingOptionsVersion *one __attribute__((swift_name("one")));
@property (class, readonly) USDKEncodingOptionsVersion *two __attribute__((swift_name("two")));
- (int32_t)compareToOther:(USDKEncodingOptionsVersion *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EncodingOptionsVersion.Companion")))
@interface USDKEncodingOptionsVersionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKEncodingOptionsVersion *)getEncodingOptionVersionByValueValue:(int32_t)value __attribute__((swift_name("getEncodingOptionVersionByValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SegmentEncoder")))
@interface USDKSegmentEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SegmentEncoder.Companion")))
@interface USDKSegmentEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKTCModel *)decodeEncodedString:(NSString *)encodedString tcModel:(USDKTCModel *)tcModel segment:(NSString *)segment __attribute__((swift_name("decode(encodedString:tcModel:segment:)")));
- (NSString *)encodeTcModel:(USDKTCModel *)tcModel segment:(USDKSegment *)segment __attribute__((swift_name("encode(tcModel:segment:)")));
- (USDKEncodedTCFKeys *)encodeIabTCFKeysTcModel:(USDKTCModel *)tcModel segment:(USDKSegment *)segment __attribute__((swift_name("encodeIabTCFKeys(tcModel:segment:)")));
@property USDKFieldSequence *fieldSequence __attribute__((swift_name("fieldSequence")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SemanticPreEncoder")))
@interface USDKSemanticPreEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SemanticPreEncoder.Companion")))
@interface USDKSemanticPreEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKTCModel *)processTcModel:(USDKTCModel *)tcModel options:(USDKEncodingOptions * _Nullable)options __attribute__((swift_name("process(tcModel:options:)")));
@end;

__attribute__((swift_name("SequenceVersionMap")))
@interface USDKSequenceVersionMap : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property USDKSequenceVersionMapType *one __attribute__((swift_name("one")));
@property USDKSequenceVersionMapType *two __attribute__((swift_name("two")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FieldSequence")))
@interface USDKFieldSequence : USDKSequenceVersionMap
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property USDKSequenceVersionMapType *one __attribute__((swift_name("one")));
@property USDKSequenceVersionMapType *two __attribute__((swift_name("two")));
@end;

__attribute__((swift_name("SequenceVersionMapType")))
@interface USDKSequenceVersionMapType : USDKBase
@end;

__attribute__((swift_name("SequenceVersionMapType.SVMItem")))
@interface USDKSequenceVersionMapTypeSVMItem : USDKSequenceVersionMapType
- (instancetype)initWithLabel:(NSString *)label value:(NSArray<NSString *> *)value __attribute__((swift_name("init(label:value:)"))) __attribute__((objc_designated_initializer));
@property NSString *label __attribute__((swift_name("label")));
@property NSArray<NSString *> *value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("SVMItem")))
@interface USDKSVMItem : USDKSequenceVersionMapTypeSVMItem
- (instancetype)initWithLabel:(NSString *)label value:(NSArray<NSString *> *)value __attribute__((swift_name("init(label:value:)"))) __attribute__((objc_designated_initializer));
@property NSString *label __attribute__((swift_name("label")));
@property NSArray<NSString *> *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SegmentSequence")))
@interface USDKSegmentSequence : USDKSequenceVersionMap
- (instancetype)initWithTcModel:(USDKTCModel *)tcModel options:(USDKEncodingOptions * _Nullable)options __attribute__((swift_name("init(tcModel:options:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property USDKSequenceVersionMapType *one __attribute__((swift_name("one")));
@property USDKSequenceVersionMapType *two __attribute__((swift_name("two")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SequenceVersionMapType.List")))
@interface USDKSequenceVersionMapTypeList : USDKSequenceVersionMapType
- (instancetype)initWithValue:(NSArray<USDKSegment *> *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property NSArray<USDKSegment *> *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SequenceVersionMapType.SVMItemMap")))
@interface USDKSequenceVersionMapTypeSVMItemMap : USDKSequenceVersionMapType
- (instancetype)initWithMap:(NSDictionary<USDKSegment *, NSArray<NSString *> *> *)map __attribute__((swift_name("init(map:)"))) __attribute__((objc_designated_initializer));
@property NSDictionary<USDKSegment *, NSArray<NSString *> *> *map __attribute__((swift_name("map")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BooleanEncoder")))
@interface USDKBooleanEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BooleanEncoder.Companion")))
@interface USDKBooleanEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (BOOL)decodeValue:(NSString *)value __attribute__((swift_name("decode(value:)")));
- (NSString *)encodeValue:(BOOL)value __attribute__((swift_name("encode(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DateEncoder")))
@interface USDKDateEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DateEncoder.Companion")))
@interface USDKDateEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (double)decodeValue:(NSString *)value numBits:(int32_t)numBits __attribute__((swift_name("decode(value:numBits:)")));
- (NSString *)encodeValue:(double)value numBits:(int32_t)numBits __attribute__((swift_name("encode(value:numBits:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FieldEncoderMap")))
@interface USDKFieldEncoderMap : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FieldEncoderMap.Companion")))
@interface USDKFieldEncoderMapCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKIntEncoderCompanion *)getCmpIdEncoder __attribute__((swift_name("getCmpIdEncoder()")));
- (USDKIntEncoderCompanion *)getCmpVersionEncoder __attribute__((swift_name("getCmpVersionEncoder()")));
- (USDKLangEncoderCompanion *)getConsentLanguageEncoder __attribute__((swift_name("getConsentLanguageEncoder()")));
- (USDKIntEncoderCompanion *)getConsentScreenEncoder __attribute__((swift_name("getConsentScreenEncoder()")));
- (USDKDateEncoderCompanion *)getCreatedEncoder __attribute__((swift_name("getCreatedEncoder()")));
- (USDKBooleanEncoderCompanion *)getIsServiceSpecificEncoder __attribute__((swift_name("getIsServiceSpecificEncoder()")));
- (USDKDateEncoderCompanion *)getLastUpdatedEncoder __attribute__((swift_name("getLastUpdatedEncoder()")));
- (USDKIntEncoderCompanion *)getNumCustomPurposesEncoder __attribute__((swift_name("getNumCustomPurposesEncoder()")));
- (USDKIntEncoderCompanion *)getPolicyVersionEncoder __attribute__((swift_name("getPolicyVersionEncoder()")));
- (USDKFixedVectorEncoderCompanion *)getPublisherConsentsEncoder __attribute__((swift_name("getPublisherConsentsEncoder()")));
- (USDKFixedVectorEncoderCompanion *)getPublisherCustomConsentsEncoder __attribute__((swift_name("getPublisherCustomConsentsEncoder()")));
- (USDKFixedVectorEncoderCompanion *)getPublisherCustomLegitimateInterestsEncoder __attribute__((swift_name("getPublisherCustomLegitimateInterestsEncoder()")));
- (USDKFixedVectorEncoderCompanion *)getPublisherLegitimateInterestsEncoder __attribute__((swift_name("getPublisherLegitimateInterestsEncoder()")));
- (USDKPurposeRestrictionVectorEncoderCompanion *)getPublisherRestrictionsEncoder __attribute__((swift_name("getPublisherRestrictionsEncoder()")));
- (USDKFixedVectorEncoderCompanion *)getPurposeConsentsEncoder __attribute__((swift_name("getPurposeConsentsEncoder()")));
- (USDKFixedVectorEncoderCompanion *)getPurposeLegitimateInterestsEncoder __attribute__((swift_name("getPurposeLegitimateInterestsEncoder()")));
- (USDKBooleanEncoderCompanion *)getPurposeOneTreatmentEncoder __attribute__((swift_name("getPurposeOneTreatmentEncoder()")));
- (USDKIntEncoderCompanion *)getSegmentTypeEncoder __attribute__((swift_name("getSegmentTypeEncoder()")));
- (USDKFixedVectorEncoderCompanion *)getSpecialFeatureOptinsEncoder __attribute__((swift_name("getSpecialFeatureOptinsEncoder()")));
- (USDKBooleanEncoderCompanion *)getUseNonStandardStacksEncoder __attribute__((swift_name("getUseNonStandardStacksEncoder()")));
- (USDKVendorVectorEncoderCompanion *)getVendorConsentsEncoder __attribute__((swift_name("getVendorConsentsEncoder()")));
- (USDKVendorVectorEncoderCompanion *)getVendorLegitimateInterestsEncoder __attribute__((swift_name("getVendorLegitimateInterestsEncoder()")));
- (USDKIntEncoderCompanion *)getVendorListVersionEncoder __attribute__((swift_name("getVendorListVersionEncoder()")));
- (USDKVendorVectorEncoderCompanion *)getVendorsAllowedEncoder __attribute__((swift_name("getVendorsAllowedEncoder()")));
- (USDKVendorVectorEncoderCompanion *)getVendorsDisclosedEncoder __attribute__((swift_name("getVendorsDisclosedEncoder()")));
- (USDKIntEncoderCompanion *)getVersionEncoder __attribute__((swift_name("getVersionEncoder()")));
@property USDKIntEncoderCompanion *cmpId __attribute__((swift_name("cmpId")));
@property USDKIntEncoderCompanion *cmpVersion __attribute__((swift_name("cmpVersion")));
@property USDKLangEncoderCompanion *consentLanguage __attribute__((swift_name("consentLanguage")));
@property USDKIntEncoderCompanion *consentScreen __attribute__((swift_name("consentScreen")));
@property USDKDateEncoderCompanion *created __attribute__((swift_name("created")));
@property USDKBooleanEncoderCompanion *isServiceSpecific __attribute__((swift_name("isServiceSpecific")));
@property USDKDateEncoderCompanion *lastUpdated __attribute__((swift_name("lastUpdated")));
@property USDKIntEncoderCompanion *numCustomPurposes __attribute__((swift_name("numCustomPurposes")));
@property USDKIntEncoderCompanion *policyVersion __attribute__((swift_name("policyVersion")));
@property USDKFixedVectorEncoderCompanion *publisherConsents __attribute__((swift_name("publisherConsents")));
@property USDKLangEncoderCompanion *publisherCountryCode __attribute__((swift_name("publisherCountryCode")));
@property USDKFixedVectorEncoderCompanion *publisherCustomConsents __attribute__((swift_name("publisherCustomConsents")));
@property USDKFixedVectorEncoderCompanion *publisherCustomLegitimateInterests __attribute__((swift_name("publisherCustomLegitimateInterests")));
@property USDKFixedVectorEncoderCompanion *publisherLegitimateInterests __attribute__((swift_name("publisherLegitimateInterests")));
@property USDKPurposeRestrictionVectorEncoderCompanion *publisherRestrictions __attribute__((swift_name("publisherRestrictions")));
@property USDKFixedVectorEncoderCompanion *purposeConsents __attribute__((swift_name("purposeConsents")));
@property USDKFixedVectorEncoderCompanion *purposeLegitimateInterests __attribute__((swift_name("purposeLegitimateInterests")));
@property USDKBooleanEncoderCompanion *purposeOneTreatment __attribute__((swift_name("purposeOneTreatment")));
@property USDKIntEncoderCompanion *segmentType __attribute__((swift_name("segmentType")));
@property USDKFixedVectorEncoderCompanion *specialFeatureOptins __attribute__((swift_name("specialFeatureOptins")));
@property USDKBooleanEncoderCompanion *useNonStandardStacks __attribute__((swift_name("useNonStandardStacks")));
@property USDKVendorVectorEncoderCompanion *vendorConsents __attribute__((swift_name("vendorConsents")));
@property USDKVendorVectorEncoderCompanion *vendorLegitimateInterests __attribute__((swift_name("vendorLegitimateInterests")));
@property USDKIntEncoderCompanion *vendorListVersion __attribute__((swift_name("vendorListVersion")));
@property USDKVendorVectorEncoderCompanion *vendorsAllowed __attribute__((swift_name("vendorsAllowed")));
@property USDKVendorVectorEncoderCompanion *vendorsDisclosed __attribute__((swift_name("vendorsDisclosed")));
@property USDKIntEncoderCompanion *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FixedVectorEncoder")))
@interface USDKFixedVectorEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FixedVectorEncoder.Companion")))
@interface USDKFixedVectorEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKVector *)decodeValue:(NSString *)value numBits:(int32_t)numBits __attribute__((swift_name("decode(value:numBits:)")));
- (NSString *)encodeValue:(USDKVector *)value numBits:(int32_t)numBits __attribute__((swift_name("encode(value:numBits:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IntEncoder")))
@interface USDKIntEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IntEncoder.Companion")))
@interface USDKIntEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (int64_t)decodeValue:(NSString *)value numBits:(int32_t)numBits __attribute__((swift_name("decode(value:numBits:)")));
- (NSString *)encodeValue:(USDKStringOrNumber *)value numBits:(int32_t)numBits __attribute__((swift_name("encode(value:numBits:)")));
- (NSString *)encodeLongValue:(int64_t)value numBits:(int32_t)numBits __attribute__((swift_name("encodeLong(value:numBits:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LangEncoder")))
@interface USDKLangEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LangEncoder.Companion")))
@interface USDKLangEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (NSString *)decodeValue:(NSString *)value numBits:(int32_t)numBits __attribute__((swift_name("decode(value:numBits:)")));
- (NSString *)encodeValue:(NSString *)value numBits:(int32_t)numBits __attribute__((swift_name("encode(value:numBits:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PurposeRestrictionVectorEncoder")))
@interface USDKPurposeRestrictionVectorEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PurposeRestrictionVectorEncoder.Companion")))
@interface USDKPurposeRestrictionVectorEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKPurposeRestrictionVector *)decodeEncodedString:(NSString *)encodedString __attribute__((swift_name("decode(encodedString:)")));
- (NSString *)encodePrVector:(USDKPurposeRestrictionVector *)prVector __attribute__((swift_name("encode(prVector:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VectorEncodingType")))
@interface USDKVectorEncodingType : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKVectorEncodingType *field __attribute__((swift_name("field")));
@property (class, readonly) USDKVectorEncodingType *range __attribute__((swift_name("range")));
- (int32_t)compareToOther:(USDKVectorEncodingType *)other __attribute__((swift_name("compareTo(other:)")));
@property int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VectorEncodingType.Companion")))
@interface USDKVectorEncodingTypeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKVectorEncodingType *)getVectorEncodingTypeByValueValue:(int32_t)value __attribute__((swift_name("getVectorEncodingTypeByValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VendorVectorEncoder")))
@interface USDKVendorVectorEncoder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VendorVectorEncoder.Companion")))
@interface USDKVendorVectorEncoderCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKVector *)decodeValue:(NSString *)value __attribute__((swift_name("decode(value:)")));
- (NSString *)encodeValue:(USDKVector *)value __attribute__((swift_name("encode(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BinarySearchTree")))
@interface USDKBinarySearchTree : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addValue:(int32_t)value __attribute__((swift_name("add(value:)")));
- (BOOL)containsValue:(int32_t)value __attribute__((swift_name("contains(value:)")));
- (NSArray<USDKInt *> *)get __attribute__((swift_name("get()")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (USDKInt * _Nullable)max_current:(USDKTreeNode * _Nullable)_current __attribute__((swift_name("max(_current:)")));
- (USDKInt * _Nullable)min_current:(USDKTreeNode * _Nullable)_current __attribute__((swift_name("min(_current:)")));
- (void)removeValue:(int32_t)value _current:(USDKTreeNode * _Nullable)_current __attribute__((swift_name("remove(value:_current:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BinarySearchTree.Companion")))
@interface USDKBinarySearchTreeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentLanguages")))
@interface USDKConsentLanguages : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)consentLanguages __attribute__((swift_name("init()")));
- (void)forEachCallback:(void (^)(NSString *))callback __attribute__((swift_name("forEach(callback:)")));
- (id)getSize __attribute__((swift_name("getSize()")));
- (BOOL)hasKey:(NSString *)key __attribute__((swift_name("has(key:)")));
@property NSSet<NSString *> *langSet __attribute__((swift_name("langSet")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PurposeRestriction")))
@interface USDKPurposeRestriction : USDKBase
- (instancetype)initWithPurposeId:(USDKInt * _Nullable)purposeId restrictionType:(USDKRestrictionType * _Nullable)restrictionType __attribute__((swift_name("init(purposeId:restrictionType:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getHash __attribute__((swift_name("getHash()")));
- (USDKInt * _Nullable)getPurposeId __attribute__((swift_name("getPurposeId()")));
- (BOOL)isSameAsOtherPR:(USDKPurposeRestriction *)otherPR __attribute__((swift_name("isSameAs(otherPR:)")));
- (BOOL)isValid __attribute__((swift_name("isValid()")));
- (void)setPurposeIdIdNum:(int32_t)idNum __attribute__((swift_name("setPurposeId(idNum:)")));
@property USDKRestrictionType *restrictionType __attribute__((swift_name("restrictionType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PurposeRestriction.Companion")))
@interface USDKPurposeRestrictionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKPurposeRestriction *)unHashHash:(NSString *)hash __attribute__((swift_name("unHash(hash:)")));
@property NSString *hashSeparator __attribute__((swift_name("hashSeparator")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PurposeRestrictionVector")))
@interface USDKPurposeRestrictionVector : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addVendorId:(int32_t)vendorId purposeRestriction:(USDKPurposeRestriction *)purposeRestriction __attribute__((swift_name("add(vendorId:purposeRestriction:)")));
- (USDKPurposeRestrictionVector *)clone __attribute__((swift_name("clone()")));
- (USDKGVL * _Nullable)getGVL __attribute__((swift_name("getGVL()")));
- (int32_t)getMaxVendorId __attribute__((swift_name("getMaxVendorId()")));
- (int32_t)getNumRestrictions __attribute__((swift_name("getNumRestrictions()")));
- (NSArray<USDKInt *> *)getPurposes __attribute__((swift_name("getPurposes()")));
- (USDKRestrictionType * _Nullable)getRestrictionTypeVendorId:(int32_t)vendorId purposeId:(int32_t)purposeId __attribute__((swift_name("getRestrictionType(vendorId:purposeId:)")));
- (NSArray<USDKPurposeRestriction *> *)getRestrictionsVendorId:(USDKInt * _Nullable)vendorId __attribute__((swift_name("getRestrictions(vendorId:)")));
- (NSArray<USDKInt *> *)getVendorsPurposeRestriction:(USDKPurposeRestriction * _Nullable)purposeRestriction __attribute__((swift_name("getVendors(purposeRestriction:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (void)removeVendorId:(int32_t)vendorId purposeRestriction:(USDKPurposeRestriction *)purposeRestriction __attribute__((swift_name("remove(vendorId:purposeRestriction:)")));
- (void)setGvlValue:(USDKGVL *)value __attribute__((swift_name("setGvl(value:)")));
- (BOOL)vendorHasRestrictionVendorId:(int32_t)vendorId purposeRestriction:(USDKPurposeRestriction *)purposeRestriction __attribute__((swift_name("vendorHasRestriction(vendorId:purposeRestriction:)")));
@property int32_t bitLength __attribute__((swift_name("bitLength")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RestrictionType")))
@interface USDKRestrictionType : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKRestrictionType *notAllowed __attribute__((swift_name("notAllowed")));
@property (class, readonly) USDKRestrictionType *requireConsent __attribute__((swift_name("requireConsent")));
@property (class, readonly) USDKRestrictionType *requireLi __attribute__((swift_name("requireLi")));
- (int32_t)compareToOther:(USDKRestrictionType *)other __attribute__((swift_name("compareTo(other:)")));
@property int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("RestrictionType.Companion")))
@interface USDKRestrictionTypeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKRestrictionType *)getRestrictionTypeByValueValue:(int32_t)value __attribute__((swift_name("getRestrictionTypeByValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Segment")))
@interface USDKSegment : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKSegment *core __attribute__((swift_name("core")));
@property (class, readonly) USDKSegment *vendorsDisclosed __attribute__((swift_name("vendorsDisclosed")));
@property (class, readonly) USDKSegment *vendorsAllowed __attribute__((swift_name("vendorsAllowed")));
@property (class, readonly) USDKSegment *publisherTc __attribute__((swift_name("publisherTc")));
- (int32_t)compareToOther:(USDKSegment *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Segment.Companion")))
@interface USDKSegmentCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKSegment *)getSegmentByTypeType:(NSString *)type __attribute__((swift_name("getSegmentByType(type:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SegmentIDs")))
@interface USDKSegmentIDs : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SegmentIDs.Companion")))
@interface USDKSegmentIDsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property NSArray<USDKSegment *> *ID_TO_KEY __attribute__((swift_name("ID_TO_KEY")));
@property NSDictionary<USDKSegment *, USDKInt *> *KEY_TO_ID __attribute__((swift_name("KEY_TO_ID")));
@end;

__attribute__((swift_name("SingleIDOrCollection")))
@interface USDKSingleIDOrCollection : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SingleIDOrCollection.Int")))
@interface USDKSingleIDOrCollectionInt : USDKSingleIDOrCollection
- (instancetype)initWithValue:(int32_t)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SingleIDOrCollection.List")))
@interface USDKSingleIDOrCollectionList : USDKSingleIDOrCollection
- (instancetype)initWithValue:(NSArray<id> *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSArray<id> *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SingleIDOrCollection.Map")))
@interface USDKSingleIDOrCollectionMap : USDKSingleIDOrCollection
- (instancetype)initWithValue:(NSDictionary<id, id> *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSDictionary<id, id> *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SingleIDOrCollection.Set")))
@interface USDKSingleIDOrCollectionSet : USDKSingleIDOrCollection
- (instancetype)initWithValue:(NSSet<id> *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSSet<id> *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SingleIDOrCollection.String")))
@interface USDKSingleIDOrCollectionString : USDKSingleIDOrCollection
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TreeNode")))
@interface USDKTreeNode : USDKBase
- (instancetype)initWithValue:(int32_t)value right:(USDKTreeNode * _Nullable)right left:(USDKTreeNode * _Nullable)left __attribute__((swift_name("init(value:right:left:)"))) __attribute__((objc_designated_initializer));
@property USDKTreeNode * _Nullable left __attribute__((swift_name("left")));
@property USDKTreeNode * _Nullable right __attribute__((swift_name("right")));
@property int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TreeNode.Companion")))
@interface USDKTreeNodeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("KotlinIterable")))
@protocol USDKKotlinIterable
@required
- (id<USDKKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Vector")))
@interface USDKVector : USDKBase <USDKKotlinIterable>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)empty __attribute__((swift_name("empty()")));
- (void)forEachCallback:(void (^)(USDKBoolean *, USDKInt *))callback __attribute__((swift_name("forEach(callback:)")));
- (int32_t)getMaxId __attribute__((swift_name("getMaxId()")));
- (int32_t)getSize __attribute__((swift_name("getSize()")));
- (BOOL)hasId:(int32_t)id __attribute__((swift_name("has(id:)")));
- (id<USDKKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setItem:(id)item __attribute__((swift_name("set(item:)")));
- (void)setAllIntMap:(NSDictionary<NSString *, id> *)intMap __attribute__((swift_name("setAll(intMap:)")));
- (void)unsetId:(id)id __attribute__((swift_name("unset(id:)")));
@property int32_t bitLength __attribute__((swift_name("bitLength")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ByPurposeVendorMap")))
@interface USDKByPurposeVendorMap : USDKBase
- (instancetype)initWithLegInt:(USDKMutableSet<USDKInt *> *)legInt consent:(USDKMutableSet<USDKInt *> *)consent flexible:(USDKMutableSet<USDKInt *> *)flexible __attribute__((swift_name("init(legInt:consent:flexible:)"))) __attribute__((objc_designated_initializer));
- (USDKByPurposeVendorMap *)doCopyLegInt:(USDKMutableSet<USDKInt *> *)legInt consent:(USDKMutableSet<USDKInt *> *)consent flexible:(USDKMutableSet<USDKInt *> *)flexible __attribute__((swift_name("doCopy(legInt:consent:flexible:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKMutableSet<USDKInt *> *consent __attribute__((swift_name("consent")));
@property USDKMutableSet<USDKInt *> *flexible __attribute__((swift_name("flexible")));
@property USDKMutableSet<USDKInt *> *legInt __attribute__((swift_name("legInt")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ByPurposeVendorMap.Companion")))
@interface USDKByPurposeVendorMapCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Declarations.Companion")))
@interface USDKDeclarationsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("GVLMapItem")))
@protocol USDKGVLMapItem
@required
@property int32_t id __attribute__((swift_name("id")));
@property NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Feature")))
@interface USDKFeature : USDKBase <USDKGVLMapItem>
- (instancetype)initWithDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name __attribute__((swift_name("init(description:descriptionLegal:id:name:)"))) __attribute__((objc_designated_initializer));
- (USDKFeature *)doCopyDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name __attribute__((swift_name("doCopy(description:descriptionLegal:id:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property NSString *descriptionLegal __attribute__((swift_name("descriptionLegal")));
@property int32_t id __attribute__((swift_name("id")));
@property NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Feature.Companion")))
@interface USDKFeatureCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Overflow")))
@interface USDKOverflow : USDKBase
- (instancetype)initWithHttpGetLimit:(int32_t)httpGetLimit __attribute__((swift_name("init(httpGetLimit:)"))) __attribute__((objc_designated_initializer));
- (USDKOverflow *)doCopyHttpGetLimit:(int32_t)httpGetLimit __attribute__((swift_name("doCopy(httpGetLimit:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property int32_t httpGetLimit __attribute__((swift_name("httpGetLimit")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Overflow.Companion")))
@interface USDKOverflowCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Purpose")))
@interface USDKPurpose : USDKBase <USDKGVLMapItem>
- (instancetype)initWithDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name __attribute__((swift_name("init(description:descriptionLegal:id:name:)"))) __attribute__((objc_designated_initializer));
- (USDKPurpose *)doCopyDescription:(NSString *)description descriptionLegal:(NSString *)descriptionLegal id:(int32_t)id name:(NSString *)name __attribute__((swift_name("doCopy(description:descriptionLegal:id:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property NSString *descriptionLegal __attribute__((swift_name("descriptionLegal")));
@property int32_t id __attribute__((swift_name("id")));
@property NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Purpose.Companion")))
@interface USDKPurposeCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Stack")))
@interface USDKStack : USDKBase <USDKGVLMapItem>
- (instancetype)initWithPurposes:(NSArray<USDKInt *> *)purposes specialFeatures:(NSArray<USDKInt *> *)specialFeatures description:(NSString *)description id:(int32_t)id name:(NSString *)name __attribute__((swift_name("init(purposes:specialFeatures:description:id:name:)"))) __attribute__((objc_designated_initializer));
- (USDKStack *)doCopyPurposes:(NSArray<USDKInt *> *)purposes specialFeatures:(NSArray<USDKInt *> *)specialFeatures description:(NSString *)description id:(int32_t)id name:(NSString *)name __attribute__((swift_name("doCopy(purposes:specialFeatures:description:id:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property int32_t id __attribute__((swift_name("id")));
@property NSString *name __attribute__((swift_name("name")));
@property NSArray<USDKInt *> *purposes __attribute__((swift_name("purposes")));
@property NSArray<USDKInt *> *specialFeatures __attribute__((swift_name("specialFeatures")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Stack.Companion")))
@interface USDKStackCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Vendor_")))
@interface USDKVendor_ : USDKBase <USDKGVLMapItem>
- (instancetype)initWithPurposes:(NSArray<USDKInt *> *)purposes legIntPurposes:(NSArray<USDKInt *> *)legIntPurposes flexiblePurposes:(NSArray<USDKInt *> *)flexiblePurposes specialPurposes:(NSArray<USDKInt *> *)specialPurposes features:(NSArray<USDKInt *> *)features specialFeatures:(NSArray<USDKInt *> *)specialFeatures policyUrl:(NSString *)policyUrl deletedDate:(NSString * _Nullable)deletedDate overflow:(USDKOverflow * _Nullable)overflow id:(int32_t)id name:(NSString *)name __attribute__((swift_name("init(purposes:legIntPurposes:flexiblePurposes:specialPurposes:features:specialFeatures:policyUrl:deletedDate:overflow:id:name:)"))) __attribute__((objc_designated_initializer));
- (USDKVendor_ *)doCopyPurposes:(NSArray<USDKInt *> *)purposes legIntPurposes:(NSArray<USDKInt *> *)legIntPurposes flexiblePurposes:(NSArray<USDKInt *> *)flexiblePurposes specialPurposes:(NSArray<USDKInt *> *)specialPurposes features:(NSArray<USDKInt *> *)features specialFeatures:(NSArray<USDKInt *> *)specialFeatures policyUrl:(NSString *)policyUrl deletedDate:(NSString * _Nullable)deletedDate overflow:(USDKOverflow * _Nullable)overflow id:(int32_t)id name:(NSString *)name __attribute__((swift_name("doCopy(purposes:legIntPurposes:flexiblePurposes:specialPurposes:features:specialFeatures:policyUrl:deletedDate:overflow:id:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString * _Nullable deletedDate __attribute__((swift_name("deletedDate")));
@property NSArray<USDKInt *> *features __attribute__((swift_name("features")));
@property NSArray<USDKInt *> *flexiblePurposes __attribute__((swift_name("flexiblePurposes")));
@property int32_t id __attribute__((swift_name("id")));
@property NSArray<USDKInt *> *legIntPurposes __attribute__((swift_name("legIntPurposes")));
@property NSString *name __attribute__((swift_name("name")));
@property USDKOverflow * _Nullable overflow __attribute__((swift_name("overflow")));
@property NSString *policyUrl __attribute__((swift_name("policyUrl")));
@property NSArray<USDKInt *> *purposes __attribute__((swift_name("purposes")));
@property NSArray<USDKInt *> *specialFeatures __attribute__((swift_name("specialFeatures")));
@property NSArray<USDKInt *> *specialPurposes __attribute__((swift_name("specialPurposes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Vendor_.Companion")))
@interface USDKVendor_Companion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VendorList.Companion")))
@interface USDKVendorListCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DecodingError")))
@interface USDKDecodingError : USDKKotlinThrowable
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EncodingError")))
@interface USDKEncodingError : USDKKotlinThrowable
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialFormat")))
@protocol USDKKotlinx_serialization_runtimeSerialFormat
@required
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeStringFormat")))
@protocol USDKKotlinx_serialization_runtimeStringFormat <USDKKotlinx_serialization_runtimeSerialFormat>
@required
- (id _Nullable)parseDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("parse(deserializer:string:)")));
- (NSString *)stringifySerializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("stringify(serializer:value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtimeJson")))
@interface USDKKotlinx_serialization_runtimeJson : USDKBase <USDKKotlinx_serialization_runtimeStringFormat>
- (instancetype)initWithBlock:(void (^)(USDKKotlinx_serialization_runtimeJsonBuilder *))block __attribute__((swift_name("init(block:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable("Default constructor is deprecated, please specify the desired configuration explicitly or use Json(JsonConfiguration.Default)")));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithConfiguration:(USDKKotlinx_serialization_runtimeJsonConfiguration *)configuration context:(id<USDKKotlinx_serialization_runtimeSerialModule>)context __attribute__((swift_name("init(configuration:context:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)fromJsonDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer json:(USDKKotlinx_serialization_runtimeJsonElement *)json __attribute__((swift_name("fromJson(deserializer:json:)")));
- (id)fromJsonTree:(USDKKotlinx_serialization_runtimeJsonElement *)tree __attribute__((swift_name("fromJson(tree:)")));
- (id _Nullable)parseDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("parse(deserializer:string:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)parseJsonString:(NSString *)string __attribute__((swift_name("parseJson(string:)")));
- (NSString *)stringifySerializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("stringify(serializer:value:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)toJsonValue:(id)value __attribute__((swift_name("toJson(value:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)toJsonSerializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("toJson(serializer:value:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@end;

@interface USDKKotlinx_serialization_runtimeJson (Extensions)
- (id _Nullable)tryToParseDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer string:(NSString *)string logger:(USDKUsercentricsLogger * _Nullable)logger __attribute__((swift_name("tryToParse(deserializer:string:logger:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApplicationDispatcherKt")))
@interface USDKApplicationDispatcherKt : USDKBase
@property (class, readonly) USDKKotlinx_coroutines_coreCoroutineDispatcher *ApplicationDispatcher __attribute__((swift_name("ApplicationDispatcher")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActualKt")))
@interface USDKActualKt : USDKBase
+ (UIViewController * _Nullable)generateUsercentricsViewContext:(id _Nullable)context data:(USDKViewData *)data consentHandlers:(USDKConsentHandlers *)consentHandlers viewHandlers:(USDKViewHandlers *)viewHandlers customFont:(NSDictionary<NSString *, UIFont *> * _Nullable)customFont __attribute__((swift_name("generateUsercentricsView(context:data:consentHandlers:viewHandlers:customFont:)")));
+ (NSString *)platformLanguage __attribute__((swift_name("platformLanguage()")));
+ (USDKUserAgentInfo *)platformUserAgent __attribute__((swift_name("platformUserAgent()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CommonKt")))
@interface USDKCommonKt : USDKBase
+ (USDKResourcesFileResource *)getGeneratedFilesResource:(NSString *)resource __attribute__((swift_name("getGeneratedFiles(resource:)")));
+ (USDKResourcesImageResource *)getGeneratedImagesResource:(NSString *)resource __attribute__((swift_name("getGeneratedImages(resource:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HeaderKt")))
@interface USDKHeaderKt : USDKBase
@property (class, readonly) double LOGO_HEIGHT __attribute__((swift_name("LOGO_HEIGHT")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAContentKt")))
@interface USDKCCPAContentKt : USDKBase
@property (class, readonly) double CARD_BOTTOM_MARGIN __attribute__((swift_name("CARD_BOTTOM_MARGIN")));
@property (class, readonly) double CLIPBOARD_CONTAINER_HEIGHT __attribute__((swift_name("CLIPBOARD_CONTAINER_HEIGHT")));
@property (class, readonly) double CONTROLLER_ID_ICON_HEIGHT __attribute__((swift_name("CONTROLLER_ID_ICON_HEIGHT")));
@property (class, readonly) double CONTROLLER_ID_ICON_TOP_MARGIN __attribute__((swift_name("CONTROLLER_ID_ICON_TOP_MARGIN")));
@property (class, readonly) double CONTROLLER_ID_ICON_WIDTH __attribute__((swift_name("CONTROLLER_ID_ICON_WIDTH")));
@property (class, readonly) double CONTROLLER_ID_LABEL_HEIGHT __attribute__((swift_name("CONTROLLER_ID_LABEL_HEIGHT")));
@property (class, readonly) double CONTROLLER_ID_LABEL_TITLE_MIN_HEIGHT __attribute__((swift_name("CONTROLLER_ID_LABEL_TITLE_MIN_HEIGHT")));
@property (class, readonly) double CONTROLLER_ID_LABEL_TOP_MARGIN __attribute__((swift_name("CONTROLLER_ID_LABEL_TOP_MARGIN")));
@property (class, readonly) double CONTROLLER_ID_WIDTH_MULTIPLIER __attribute__((swift_name("CONTROLLER_ID_WIDTH_MULTIPLIER")));
@property (class, readonly) double HEADER_SEPARATOR_TOP_MARGIN __attribute__((swift_name("HEADER_SEPARATOR_TOP_MARGIN")));
@property (class, readonly) double HISTORY_TABLE_COLLAPSED_HEIGHT __attribute__((swift_name("HISTORY_TABLE_COLLAPSED_HEIGHT")));
@property (class, readonly) double HISTORY_TABLE_ENTRY_HEIGHT __attribute__((swift_name("HISTORY_TABLE_ENTRY_HEIGHT")));
@property (class, readonly) double HISTORY_TABLE_HEADER_HEIGHT __attribute__((swift_name("HISTORY_TABLE_HEADER_HEIGHT")));
@property (class, readonly) double INFO_CLICK_Y_OFFSET __attribute__((swift_name("INFO_CLICK_Y_OFFSET")));
@property (class, readonly) double SEGMENTED_CONTROL_HEIGHT __attribute__((swift_name("SEGMENTED_CONTROL_HEIGHT")));
@property (class, readonly) double SEGMENTED_CONTROL_TOP_MARGIN __attribute__((swift_name("SEGMENTED_CONTROL_TOP_MARGIN")));
@property (class, readonly) double SEGMENTED_UNDERLINE_TOP_MARGIN __attribute__((swift_name("SEGMENTED_UNDERLINE_TOP_MARGIN")));
@property (class, readonly) BOOL SHOULD_HIDE_TOGGLES __attribute__((swift_name("SHOULD_HIDE_TOGGLES")));
@property (class, readonly) double STACK_VIEW_BOTTOM_INSET __attribute__((swift_name("STACK_VIEW_BOTTOM_INSET")));
@property (class, readonly) double STACK_VIEW_LEFT_INSET __attribute__((swift_name("STACK_VIEW_LEFT_INSET")));
@property (class, readonly) double STACK_VIEW_RIGHT_INSET __attribute__((swift_name("STACK_VIEW_RIGHT_INSET")));
@property (class, readonly) double STACK_VIEW_SPACING __attribute__((swift_name("STACK_VIEW_SPACING")));
@property (class, readonly) double STACK_VIEW_TOP_INSET __attribute__((swift_name("STACK_VIEW_TOP_INSET")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GenerateCardKt")))
@interface USDKGenerateCardKt : USDKBase
+ (UIView *)generateCard __attribute__((swift_name("generateCard()")));
+ (UIView *)generateCard_ __attribute__((swift_name("generateCard_()")));
@property (class, readonly) double CORNER_RADIUS __attribute__((swift_name("CORNER_RADIUS")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateServicesListTogglesKt")))
@interface USDKUpdateServicesListTogglesKt : USDKBase
+ (void)updateServicesListTogglesSender:(UISwitch *)sender stackView:(UIStackView *)stackView __attribute__((swift_name("updateServicesListToggles(sender:stackView:)")));
@property (class, readonly) int32_t CUSTOM_SWITCH_INDEX_INSIDE_CARD_CONTAINER __attribute__((swift_name("CUSTOM_SWITCH_INDEX_INSIDE_CARD_CONTAINER")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateCategoryStateKt")))
@interface USDKUpdateCategoryStateKt : USDKBase
+ (void)updateCategoryStateSender:(UISwitch *)sender categoriesStackView:(UIStackView *)categoriesStackView __attribute__((swift_name("updateCategoryState(sender:categoriesStackView:)")));
@property (class, readonly) int32_t CUSTOM_SWITCH_INDEX_INSIDE_CATEGORY_CARD_CONTAINER __attribute__((swift_name("CUSTOM_SWITCH_INDEX_INSIDE_CATEGORY_CARD_CONTAINER")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateServiceToggleKt")))
@interface USDKUpdateServiceToggleKt : USDKBase
+ (void)updateServiceChildToggleServicesStackView:(UIStackView *)servicesStackView sender:(UISwitch *)sender __attribute__((swift_name("updateServiceChildToggle(servicesStackView:sender:)")));
+ (void)updateServiceParentToggleCategoriesStackView:(UIStackView *)categoriesStackView sender:(UISwitch *)sender __attribute__((swift_name("updateServiceParentToggle(categoriesStackView:sender:)")));
+ (void)updateServiceToggleSender:(UISwitch *)sender servicesStackView:(UIStackView *)servicesStackView categoriesStackView:(UIStackView *)categoriesStackView __attribute__((swift_name("updateServiceToggle(sender:servicesStackView:categoriesStackView:)")));
@property (class, readonly) int32_t CUSTOM_SWITCH_INDEX_INSIDE_SERVICE_CARD_CONTAINER __attribute__((swift_name("CUSTOM_SWITCH_INDEX_INSIDE_SERVICE_CARD_CONTAINER")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateServicesListKt")))
@interface USDKUpdateServicesListKt : USDKBase
+ (void)updateServicesListData:(USDKViewData *)data servicesInList:(NSArray<UIView *> *)servicesInList expandedServicesInfo:(NSMutableArray<NSMutableArray<USDKServiceSection *> *> *)expandedServicesInfo __attribute__((swift_name("updateServicesList(data:servicesInList:expandedServicesInfo:)")));
@property (class, readonly) int32_t MAX_NUM_ENTRIES_WITHOUT_FOOTER __attribute__((swift_name("MAX_NUM_ENTRIES_WITHOUT_FOOTER")));
@property (class, readonly) int32_t MIN_NUM_ENTRIES_WITH_FOOTER __attribute__((swift_name("MIN_NUM_ENTRIES_WITH_FOOTER")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateServiceHistoryTableKt")))
@interface USDKUpdateServiceHistoryTableKt : USDKBase
+ (void)updateServiceHistoryTableHasHistoryTableFooter:(BOOL)hasHistoryTableFooter hiddenStackContent:(NSArray<id> *)hiddenStackContent index:(int32_t)index data:(USDKViewData *)data labels:(USDKHistoryLabels *)labels __attribute__((swift_name("updateServiceHistoryTable(hasHistoryTableFooter:hiddenStackContent:index:data:labels:)")));
@property (class, readonly) int32_t NUM_ITEMS_BEFORE_HISTORY_TABLE_WITHOUT_HTABLE_FOOTER __attribute__((swift_name("NUM_ITEMS_BEFORE_HISTORY_TABLE_WITHOUT_HTABLE_FOOTER")));
@property (class, readonly) int32_t NUM_ITEMS_BEFORE_HISTORY_TABLE_WITH_HTABLE_FOOTER __attribute__((swift_name("NUM_ITEMS_BEFORE_HISTORY_TABLE_WITH_HTABLE_FOOTER")));
@property (class, readonly) int32_t NUM_ITEMS_BEFORE_TITLE_CONTAINER_WITHOUT_HTABLE_FOOTER __attribute__((swift_name("NUM_ITEMS_BEFORE_TITLE_CONTAINER_WITHOUT_HTABLE_FOOTER")));
@property (class, readonly) int32_t NUM_ITEMS_BEFORE_TITLE_CONTAINER_WITH_HTABLE_FOOTER __attribute__((swift_name("NUM_ITEMS_BEFORE_TITLE_CONTAINER_WITH_HTABLE_FOOTER")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateServiceDetailsKt")))
@interface USDKUpdateServiceDetailsKt : USDKBase
+ (void)updateServiceDetailsHiddenStackContent:(NSArray<id> *)hiddenStackContent numberOfHistoryItems:(int32_t)numberOfHistoryItems updatedServicesInfo:(NSMutableArray<USDKServiceSection *> *)updatedServicesInfo __attribute__((swift_name("updateServiceDetails(hiddenStackContent:numberOfHistoryItems:updatedServicesInfo:)")));
@property (class, readonly) int32_t URL_INDEX_IF_IS_UILABEL __attribute__((swift_name("URL_INDEX_IF_IS_UILABEL")));
@property (class, readonly) int32_t URL_INDEX_IF_NOT_UILABEL __attribute__((swift_name("URL_INDEX_IF_NOT_UILABEL")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateCategoriesListKt")))
@interface USDKUpdateCategoriesListKt : USDKBase
+ (void)updateCategoriesListData:(USDKViewData *)data categoriesInList:(NSArray<UIView *> *)categoriesInList __attribute__((swift_name("updateCategoriesList(data:categoriesInList:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateCategoriesServicesTogglesKt")))
@interface USDKUpdateCategoriesServicesTogglesKt : USDKBase
+ (void)updateCategoriesServicesTogglesSender:(UISwitch *)sender data:(USDKViewData *)data servicesUpdated:(NSMutableArray<USDKUserDecision *> *)servicesUpdated __attribute__((swift_name("updateCategoriesServicesToggles(sender:data:servicesUpdated:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateSegmentedControlTitlesKt")))
@interface USDKUpdateSegmentedControlTitlesKt : USDKBase
+ (void)updateSegmentedControlTitlesSegmentedControl:(UISegmentedControl *)segmentedControl categoriesLabel:(NSString *)categoriesLabel servicesLabel:(NSString *)servicesLabel __attribute__((swift_name("updateSegmentedControlTitles(segmentedControl:categoriesLabel:servicesLabel:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddToCategoriesListKt")))
@interface USDKAddToCategoriesListKt : USDKBase
+ (void)addToCategoriesListStackView:(UIStackView *)stackView categories:(NSArray<USDKCategory *> *)categories self:(UIViewController *)self hideToggles:(BOOL)hideToggles __attribute__((swift_name("addToCategoriesList(stackView:categories:self:hideToggles:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddToServicesListKt")))
@interface USDKAddToServicesListKt : USDKBase
+ (void)addToServicesListStackView:(UIStackView *)stackView categories:(NSArray<USDKCategory *> *)categories self:(UIViewController *)self viewController:(UIViewController *)viewController uiVariant:(USDKUIVariant *)uiVariant __attribute__((swift_name("addToServicesList(stackView:categories:self:viewController:uiVariant:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConstantsKt")))
@interface USDKConstantsKt : USDKBase
@property (class, readonly) NSString *MUST_BE_TCF_USER_DECISIONS __attribute__((swift_name("MUST_BE_TCF_USER_DECISIONS")));
@property (class, readonly) NSString *MUST_BE_USER_DECISION __attribute__((swift_name("MUST_BE_USER_DECISION")));
@property (class, readonly) NSString *NOT_INITIALIZED_ERROR __attribute__((swift_name("NOT_INITIALIZED_ERROR")));
@property (class, readonly) NSString *NOT_PREDEFINED_UI_ERROR __attribute__((swift_name("NOT_PREDEFINED_UI_ERROR")));
@property (class, readonly) NSString *NULL_APP_CONTEXT_ANDROID_ERROR __attribute__((swift_name("NULL_APP_CONTEXT_ANDROID_ERROR")));
@property (class, readonly) NSString *NULL_VIEW_CONTEXT_ANDROID_ERROR __attribute__((swift_name("NULL_VIEW_CONTEXT_ANDROID_ERROR")));
@property (class, readonly) NSString *REQUIRES_FROM_LAYER __attribute__((swift_name("REQUIRES_FROM_LAYER")));
@property (class, readonly) NSString *SET_CMP_ID_ERROR __attribute__((swift_name("SET_CMP_ID_ERROR")));
@property (class, readonly) NSString *CONSENT_STATUS_EVENT __attribute__((swift_name("CONSENT_STATUS_EVENT")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CreateLoadingIndicatorKt")))
@interface USDKCreateLoadingIndicatorKt : USDKBase
+ (UIView *)createLoadingIndicator __attribute__((swift_name("createLoadingIndicator()")));
@property (class, readonly) double BACKGROUND_COLOR_ALPHA __attribute__((swift_name("BACKGROUND_COLOR_ALPHA")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UtilsKt")))
@interface USDKUtilsKt : USDKBase
+ (NSString *)getApplicationVersion __attribute__((swift_name("getApplicationVersion()")));
+ (NSString *)getFormattedLocalDateTimestampInMillis:(double)timestampInMillis __attribute__((swift_name("getFormattedLocalDate(timestampInMillis:)")));
+ (int32_t)getTcfCmpVersion __attribute__((swift_name("getTcfCmpVersion()")));
+ (NSArray<id> *)listDeepCopyOriginal:(NSArray<id> *)original __attribute__((swift_name("listDeepCopy(original:)")));
+ (USDKDataTransferObject *)mapDataTransferObjectSettings:(USDKExtendedSettings *)settings services:(NSArray<USDKService *> *)services consentAction:(USDKConsentAction *)consentAction consentType:(USDKConsentType *)consentType referrerControllerId:(NSString * _Nullable)referrerControllerId timestampInMillis:(USDKDouble * _Nullable)timestampInMillis __attribute__((swift_name("mapDataTransferObject(settings:services:consentAction:consentType:referrerControllerId:timestampInMillis:)")));
+ (double)millisToSeconds:(double)receiver __attribute__((swift_name("millisToSeconds(_:)")));
+ (double)secondsToMillis:(double)receiver __attribute__((swift_name("secondsToMillis(_:)")));
+ (USDKApiAggregatorService *)findServicesFromAggregatorArrayApiService:(id<USDKApiBaseServiceInterface>)apiService apiServices:(NSArray<USDKApiAggregatorService *> *)apiServices __attribute__((swift_name("findServicesFromAggregatorArray(apiService:apiServices:)")));
+ (NSString *)generateConsentIdSettingsId:(NSString *)settingsId controllerId:(NSString *)controllerId processorId:(NSString *)processorId __attribute__((swift_name("generateConsentId(settingsId:controllerId:processorId:)")));
+ (NSString *)generateIdentityId __attribute__((swift_name("generateIdentityId()")));
+ (NSString *)hashFunctionInput:(NSString *)input __attribute__((swift_name("hashFunction(input:)")));
+ (BOOL)isFirstLayerOverlayEnabledApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("isFirstLayerOverlayEnabled(apiSettings:)")));
+ (BOOL)isSecondLayerOverlayEnabledApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("isSecondLayerOverlayEnabled(apiSettings:)")));
+ (USDKApiSettings *)removeDeactivatedServicesApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("removeDeactivatedServices(apiSettings:)")));
+ (NSString *)resolveDataProcessorApiAggregatorService:(USDKApiAggregatorService *)apiAggregatorService __attribute__((swift_name("resolveDataProcessor(apiAggregatorService:)")));
+ (NSArray<NSString *> *)resolveDataPurposesListApiService:(USDKApiAggregatorService *)apiService __attribute__((swift_name("resolveDataPurposesList(apiService:)")));
+ (NSString *)resolveDescriptionApiService:(id<USDKApiBaseServiceInterface>)apiService apiAggregatorService:(USDKApiAggregatorService *)apiAggregatorService __attribute__((swift_name("resolveDescription(apiService:apiAggregatorService:)")));
+ (BOOL)resolveIsHiddenApiService:(USDKApiService *)apiService apiCategory:(USDKApiCategory *)apiCategory __attribute__((swift_name("resolveIsHidden(apiService:apiCategory:)")));
+ (NSArray<NSString *> *)resolveLegalBasisListApiAggregatorService:(USDKApiAggregatorService *)apiAggregatorService __attribute__((swift_name("resolveLegalBasisList(apiAggregatorService:)")));
+ (NSString *)resolvePrivacyPolicyUrlApiAggregatorService:(USDKApiAggregatorService *)apiAggregatorService __attribute__((swift_name("resolvePrivacyPolicyUrl(apiAggregatorService:)")));
+ (NSString *)resolveProcessingCompanyNameApiAggregatorService:(USDKApiAggregatorService *)apiAggregatorService __attribute__((swift_name("resolveProcessingCompanyName(apiAggregatorService:)")));
+ (NSString *)resolveRetentionPeriodDescriptionApiAggregatorService:(USDKApiAggregatorService *)apiAggregatorService __attribute__((swift_name("resolveRetentionPeriodDescription(apiAggregatorService:)")));
+ (BOOL)resolveStatusApiService:(USDKApiService *)apiService apiCategory:(USDKApiCategory *)apiCategory __attribute__((swift_name("resolveStatus(apiService:apiCategory:)")));
+ (NSArray<NSString *> *)resolveStringToListApiService:(USDKApiAggregatorService *)apiService property:(USDKApiStringToListProperties *)property __attribute__((swift_name("resolveStringToList(apiService:property:)")));
@property (class, readonly) int32_t TIME_SECONDS_LENGTH __attribute__((swift_name("TIME_SECONDS_LENGTH")));
@property (class, readonly) NSString *applicationVersion __attribute__((swift_name("applicationVersion")));
@property (class, readonly) int32_t tcfCmpVersion __attribute__((swift_name("tcfCmpVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConstantsKt_")))
@interface USDKConstantsKt_ : USDKBase
@property (class) NSString *baseBackgroundColor __attribute__((swift_name("baseBackgroundColor")));
@property (class) NSString *baseBrandColor __attribute__((swift_name("baseBrandColor")));
@property (class) NSString *baseFontColor __attribute__((swift_name("baseFontColor")));
@property (class) double defaultLeadingMargin __attribute__((swift_name("defaultLeadingMargin")));
@property (class) double defaultTrailingMargin __attribute__((swift_name("defaultTrailingMargin")));
@property (class) NSLayoutConstraint *descriptionLabelBottomConstraint __attribute__((swift_name("descriptionLabelBottomConstraint")));
@property (class) USDKFontTheme *fontTheme __attribute__((swift_name("fontTheme")));
@property (class) NSLayoutConstraint *headerContainerTopConstraint __attribute__((swift_name("headerContainerTopConstraint")));
@property (class) double headerHeightConstraintConstant __attribute__((swift_name("headerHeightConstraintConstant")));
@property (class) double headerOriginalHeight __attribute__((swift_name("headerOriginalHeight")));
@property (class) id<USDKUISettingsInterface> localizedSettings __attribute__((swift_name("localizedSettings")));
@property (class, getter=doNewHeaderHeightConstant) double newHeaderHeightConstant __attribute__((swift_name("newHeaderHeightConstant")));
@property (class) double quaternaryTintFactor __attribute__((swift_name("quaternaryTintFactor")));
@property (class) double secondaryTintFactor __attribute__((swift_name("secondaryTintFactor")));
@property (class) NSLayoutConstraint *showMoreDescriptionBottomConstraint __attribute__((swift_name("showMoreDescriptionBottomConstraint")));
@property (class) NSLayoutConstraint *showMoreDescriptionTopConstraint __attribute__((swift_name("showMoreDescriptionTopConstraint")));
@property (class) double tertiaryTintFactor __attribute__((swift_name("tertiaryTintFactor")));
@property (class) USDKTheme *theme __attribute__((swift_name("theme")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AspectFitImageKt")))
@interface USDKAspectFitImageKt : USDKBase
+ (UIImage * _Nullable)aspectFitImageRect:(USDKKotlinCValue *)rect image:(UIImage *)image __attribute__((swift_name("aspectFitImage(rect:image:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GenerateToggledServicesDataKt")))
@interface USDKGenerateToggledServicesDataKt : USDKBase
+ (void)generateToggledServicesDataData:(USDKViewData *)data toggledServices:(NSMutableArray<USDKBoolean *> *)toggledServices expandedServicesInfo:(NSMutableArray<NSMutableArray<USDKServiceSection *> *> *)expandedServicesInfo __attribute__((swift_name("generateToggledServicesData(data:toggledServices:expandedServicesInfo:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetStackViewItemCheckedStatusKt")))
@interface USDKGetStackViewItemCheckedStatusKt : USDKBase
+ (BOOL)getStackViewItemCheckedStatusElement:(id)element __attribute__((swift_name("getStackViewItemCheckedStatus(element:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetStackViewItemDescriptionKt")))
@interface USDKGetStackViewItemDescriptionKt : USDKBase
+ (NSString *)getStackViewItemDescriptionElement:(id)element __attribute__((swift_name("getStackViewItemDescription(element:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetStackViewItemDescriptionLegalKt")))
@interface USDKGetStackViewItemDescriptionLegalKt : USDKBase
+ (NSString *)getStackViewItemDescriptionLegalElement:(id)element __attribute__((swift_name("getStackViewItemDescriptionLegal(element:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetStackViewItemIdKt")))
@interface USDKGetStackViewItemIdKt : USDKBase
+ (NSString *)getStackViewItemIdElement:(id)element __attribute__((swift_name("getStackViewItemId(element:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetStackViewItemSwitchEnabledKt")))
@interface USDKGetStackViewItemSwitchEnabledKt : USDKBase
+ (BOOL)getStackViewItemSwitchEnabledElement:(id)element __attribute__((swift_name("getStackViewItemSwitchEnabled(element:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetStackViewItemTitleKt")))
@interface USDKGetStackViewItemTitleKt : USDKBase
+ (NSString *)getStackViewItemTitleElement:(id)element __attribute__((swift_name("getStackViewItemTitle(element:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetStackViewItemTypeKt")))
@interface USDKGetStackViewItemTypeKt : USDKBase
+ (NSString *)getStackViewItemTypeElement:(id)element __attribute__((swift_name("getStackViewItemType(element:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SetLogoWidthAnchorKt")))
@interface USDKSetLogoWidthAnchorKt : USDKBase
+ (void)setLogoWidthAnchorImageView:(UIImageView *)imageView originalImageHeight:(double)originalImageHeight originalImageWidth:(double)originalImageWidth __attribute__((swift_name("setLogoWidthAnchor(imageView:originalImageHeight:originalImageWidth:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SetUIImageViewFromUrlKt")))
@interface USDKSetUIImageViewFromUrlKt : USDKBase
+ (void)setUIImageViewFromUrlUrl:(NSURL *)url imageView:(UIImageView *)imageView __attribute__((swift_name("setUIImageViewFromUrl(url:imageView:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateToggledServicesDataKt")))
@interface USDKUpdateToggledServicesDataKt : USDKBase
+ (void)updatedToggledServicesDataData:(USDKViewData *)data expandedServicesInfo:(NSMutableArray<NSMutableArray<USDKServiceSection *> *> *)expandedServicesInfo __attribute__((swift_name("updatedToggledServicesData(data:expandedServicesInfo:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UIView_addDropShadowKt")))
@interface USDKUIView_addDropShadowKt : USDKBase
+ (void)addDropShadow:(UIView *)receiver radius:(double)radius offsetX:(double)offsetX offsetY:(double)offsetY color:(UIColor *)color __attribute__((swift_name("addDropShadow(_:radius:offsetX:offsetY:color:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UIView_roundCornersKt")))
@interface USDKUIView_roundCornersKt : USDKBase
+ (void)roundCorners:(UIView *)receiver radius:(double)radius shouldRoundTop:(BOOL)shouldRoundTop shouldRoundRight:(BOOL)shouldRoundRight shouldRoundBottom:(BOOL)shouldRoundBottom shouldRoundLeft:(BOOL)shouldRoundLeft shouldRoundTopLeft:(BOOL)shouldRoundTopLeft shouldRoundTopRight:(BOOL)shouldRoundTopRight shouldRoundBottomLeft:(BOOL)shouldRoundBottomLeft shouldRoundBottomRight:(BOOL)shouldRoundBottomRight __attribute__((swift_name("roundCorners(_:radius:shouldRoundTop:shouldRoundRight:shouldRoundBottom:shouldRoundLeft:shouldRoundTopLeft:shouldRoundTopRight:shouldRoundBottomLeft:shouldRoundBottomRight:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CallResponderKt")))
@interface USDKCallResponderKt : USDKBase
@property (class, readonly) NSString *API_KEY __attribute__((swift_name("API_KEY")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAMapperKt")))
@interface USDKCCPAMapperKt : USDKBase
+ (USDKCCPAUISettings *)mapCCCPAUISettingsApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapCCCPAUISettings(apiSettings:)")));
+ (USDKCCPAOptions *)mapCCPAOptionsSettings:(USDKApiSettings *)settings __attribute__((swift_name("mapCCPAOptions(settings:)")));
@property (class, readonly) int32_t DEFAULT_REFRESH_DAYS __attribute__((swift_name("DEFAULT_REFRESH_DAYS")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SettingsMapperKt")))
@interface USDKSettingsMapperKt : USDKBase
+ (USDKBaseService *)mapBaseServiceApiService:(id<USDKApiBaseServiceInterface>)apiService apiServices:(NSArray<USDKApiAggregatorService *> *)apiServices __attribute__((swift_name("mapBaseService(apiService:apiServices:)")));
+ (NSArray<USDKCategory *> *)mapCategoriesApiSettings:(USDKApiSettings *)apiSettings apiServices:(NSArray<USDKApiAggregatorService *> *)apiServices __attribute__((swift_name("mapCategories(apiSettings:apiServices:)")));
+ (USDKCustomization *)mapCustomizationApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapCustomization(apiSettings:)")));
+ (NSArray<USDKDataExchangeSetting *> *)mapDataExchangeSettingsApiDataExchangeSettings:(NSArray<USDKApiDataExchangeSetting *> *)apiDataExchangeSettings __attribute__((swift_name("mapDataExchangeSettings(apiDataExchangeSettings:)")));
+ (USDKLanguage *)mapLanguageApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapLanguage(apiSettings:)")));
+ (USDKLinks *)mapLinksApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapLinks(apiSettings:)")));
+ (USDKPoweredBy *)mapPoweredByApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapPoweredBy(apiSettings:)")));
+ (USDKService *)mapServiceApiService:(USDKApiService *)apiService apiServices:(NSArray<USDKApiAggregatorService *> *)apiServices apiCategory:(USDKApiCategory *)apiCategory __attribute__((swift_name("mapService(apiService:apiServices:apiCategory:)")));
+ (NSArray<USDKService *> *)mapServicesApiSettings:(USDKApiSettings *)apiSettings apiServices:(NSArray<USDKApiAggregatorService *> *)apiServices apiCategory:(USDKApiCategory *)apiCategory __attribute__((swift_name("mapServices(apiSettings:apiServices:apiCategory:)")));
+ (NSArray<USDKInt *> *)mapShowFirstLayerOnVersionChangeShowInitialViewForVersionChange:(NSArray<NSString *> *)showInitialViewForVersionChange __attribute__((swift_name("mapShowFirstLayerOnVersionChange(showInitialViewForVersionChange:)")));
+ (NSArray<USDKBaseService *> *)mapSubServicesApiSubServices:(NSArray<USDKApiBaseService *> *)apiSubServices apiServices:(NSArray<USDKApiAggregatorService *> *)apiServices __attribute__((swift_name("mapSubServices(apiSubServices:apiServices:)")));
+ (USDKLabels *)mapUILabelsApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapUILabels(apiSettings:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GDPRMapperKt")))
@interface USDKGDPRMapperKt : USDKBase
+ (USDKExtendedSettings *)mapSettingsApiSettings:(USDKApiSettings *)apiSettings apiServices:(NSArray<USDKApiAggregatorService *> *)apiServices controllerId:(NSString * _Nullable)controllerId __attribute__((swift_name("mapSettings(apiSettings:apiServices:controllerId:)")));
+ (USDKUISettings * _Nullable)mapUISettingsApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapUISettings(apiSettings:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TCFMapperKt")))
@interface USDKTCFMapperKt : USDKBase
+ (USDKTCFOptions *)mapTCFOptionsApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapTCFOptions(apiSettings:)")));
+ (USDKTCFUISettings *)mapTCFUISettingsApiSettings:(USDKApiSettings *)apiSettings __attribute__((swift_name("mapTCFUISettings(apiSettings:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPAKt")))
@interface USDKCCPAKt : USDKBase
@property (class, readonly) int32_t DAY __attribute__((swift_name("DAY")));
@property (class, readonly) int32_t MILLISECOND __attribute__((swift_name("MILLISECOND")));
@property (class, readonly) int32_t MINUTE_OR_HOUR __attribute__((swift_name("MINUTE_OR_HOUR")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CCPADataKt")))
@interface USDKCCPADataKt : USDKBase
+ (unichar)toYesOrNo:(BOOL)receiver __attribute__((swift_name("toYesOrNo(_:)")));
+ (USDKBoolean * _Nullable)yesOrNoToBoolean:(unichar)receiver __attribute__((swift_name("yesOrNoToBoolean(_:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResourcesFileResource")))
@interface USDKResourcesFileResource : USDKBase
- (instancetype)initWithFileName:(NSString *)fileName extension:(NSString *)extension bundle:(NSBundle *)bundle __attribute__((swift_name("init(fileName:extension:bundle:)"))) __attribute__((objc_designated_initializer));
- (NSString *)readText __attribute__((swift_name("readText()")));
@property (readonly) NSBundle *bundle __attribute__((swift_name("bundle")));
@property (readonly) NSString *extension __attribute__((swift_name("extension")));
@property (readonly) NSString *fileName __attribute__((swift_name("fileName")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@property (readonly) NSURL *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResourcesImageResource")))
@interface USDKResourcesImageResource : USDKBase
- (instancetype)initWithAssetImageName:(NSString *)assetImageName bundle:(NSBundle *)bundle __attribute__((swift_name("init(assetImageName:bundle:)"))) __attribute__((objc_designated_initializer));
- (UIImage * _Nullable)toUIImage __attribute__((swift_name("toUIImage()")));
@property (readonly) NSString *assetImageName __attribute__((swift_name("assetImageName")));
@property (readonly) NSBundle *bundle __attribute__((swift_name("bundle")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface USDKKotlinArray : USDKBase
+ (instancetype)arrayWithSize:(int32_t)size init:(id _Nullable (^)(USDKInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (id _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<USDKKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(id _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeEncoder")))
@protocol USDKKotlinx_serialization_runtimeEncoder
@required
- (id<USDKKotlinx_serialization_runtimeCompositeEncoder>)beginCollectionDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize typeSerializers:(USDKKotlinArray *)typeSerializers __attribute__((swift_name("beginCollection(descriptor:collectionSize:typeSerializers:)")));
- (id<USDKKotlinx_serialization_runtimeCompositeEncoder>)beginStructureDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor typeSerializers:(USDKKotlinArray *)typeSerializers __attribute__((swift_name("beginStructure(descriptor:typeSerializers:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
- (void)encodeUnit __attribute__((swift_name("encodeUnit()")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialDescriptor")))
@protocol USDKKotlinx_serialization_runtimeSerialDescriptor
@required
- (NSArray<id<USDKKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<USDKKotlinx_serialization_runtimeSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (NSArray<id<USDKKotlinAnnotation>> *)getEntityAnnotations __attribute__((swift_name("getEntityAnnotations()"))) __attribute__((deprecated("Deprecated in the favour of 'annotations' property")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<USDKKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) USDKKotlinx_serialization_runtimeSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *name __attribute__((swift_name("name"))) __attribute__((unavailable("name property deprecated in the favour of serialName")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeDecoder")))
@protocol USDKKotlinx_serialization_runtimeDecoder
@required
- (id<USDKKotlinx_serialization_runtimeCompositeDecoder>)beginStructureDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor typeParams:(USDKKotlinArray *)typeParams __attribute__((swift_name("beginStructure(descriptor:typeParams:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (USDKKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
- (void)decodeUnit __attribute__((swift_name("decodeUnit()")));
- (id _Nullable)updateNullableSerializableValueDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateNullableSerializableValue(deserializer:old:)")));
- (id _Nullable)updateSerializableValueDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateSerializableValue(deserializer:old:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@property (readonly) USDKKotlinx_serialization_runtimeUpdateMode *updateMode __attribute__((swift_name("updateMode")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeJsonElement")))
@interface USDKKotlinx_serialization_runtimeJsonElement : USDKBase
- (BOOL)containsKey:(NSString *)key __attribute__((swift_name("contains(key:)")));
@property (readonly) BOOL isNull __attribute__((swift_name("isNull")));
@property (readonly) NSArray<USDKKotlinx_serialization_runtimeJsonElement *> *jsonArray __attribute__((swift_name("jsonArray")));
@property (readonly) USDKKotlinx_serialization_runtimeJsonNull *jsonNull __attribute__((swift_name("jsonNull")));
@property (readonly) NSDictionary<NSString *, USDKKotlinx_serialization_runtimeJsonElement *> *jsonObject __attribute__((swift_name("jsonObject")));
@property (readonly) USDKKotlinx_serialization_runtimeJsonPrimitive *primitive __attribute__((swift_name("primitive")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinUnit")))
@interface USDKKotlinUnit : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)unit __attribute__((swift_name("init()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimeTimer")))
@interface USDKTimeTimer : USDKBase
- (instancetype)initWithIntervalMilliSeconds:(int64_t)intervalMilliSeconds block:(USDKBoolean *(^)(void))block __attribute__((swift_name("init(intervalMilliSeconds:block:)"))) __attribute__((objc_designated_initializer));
- (void)start __attribute__((swift_name("start()")));
- (void)stop __attribute__((swift_name("stop()")));
@property (readonly) int64_t intervalMilliSeconds __attribute__((swift_name("intervalMilliSeconds")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol USDKKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<USDKKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end;

__attribute__((swift_name("Ktor_ioCloseable")))
@protocol USDKKtor_ioCloseable
@required
- (void)close __attribute__((swift_name("close()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClient")))
@interface USDKKtor_client_coreHttpClient : USDKBase <USDKKotlinx_coroutines_coreCoroutineScope, USDKKtor_ioCloseable>
- (instancetype)initWithEngine:(id<USDKKtor_client_coreHttpClientEngine>)engine userConfig:(USDKKtor_client_coreHttpClientConfig *)userConfig __attribute__((swift_name("init(engine:userConfig:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (USDKKtor_client_coreHttpClient *)configBlock:(void (^)(USDKKtor_client_coreHttpClientConfig *))block __attribute__((swift_name("config(block:)")));
- (BOOL)isSupportedCapability:(id<USDKKtor_client_coreHttpClientEngineCapability>)capability __attribute__((swift_name("isSupported(capability:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<USDKKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) id<USDKKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property (readonly) USDKKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher"))) __attribute__((unavailable("[dispatcher] is deprecated. Use coroutineContext instead.")));
@property (readonly) id<USDKKtor_client_coreHttpClientEngine> engine __attribute__((swift_name("engine")));
@property (readonly) USDKKtor_client_coreHttpClientEngineConfig *engineConfig __attribute__((swift_name("engineConfig")));
@property (readonly) USDKKtor_client_coreHttpReceivePipeline *receivePipeline __attribute__((swift_name("receivePipeline")));
@property (readonly) USDKKtor_client_coreHttpRequestPipeline *requestPipeline __attribute__((swift_name("requestPipeline")));
@property (readonly) USDKKtor_client_coreHttpResponsePipeline *responsePipeline __attribute__((swift_name("responsePipeline")));
@property (readonly) USDKKtor_client_coreHttpSendPipeline *sendPipeline __attribute__((swift_name("sendPipeline")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol USDKKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next_ __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialModule")))
@protocol USDKKotlinx_serialization_runtimeSerialModule
@required
- (void)dumpToCollector:(id<USDKKotlinx_serialization_runtimeSerialModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<USDKKotlinx_serialization_runtimeKSerializer> _Nullable)getContextualKclass:(id<USDKKotlinKClass>)kclass __attribute__((swift_name("getContextual(kclass:)")));
- (id<USDKKotlinx_serialization_runtimeKSerializer> _Nullable)getPolymorphicBaseClass:(id<USDKKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<USDKKotlinx_serialization_runtimeKSerializer> _Nullable)getPolymorphicBaseClass:(id<USDKKotlinKClass>)baseClass serializedClassName:(NSString *)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtimeJsonBuilder")))
@interface USDKKotlinx_serialization_runtimeJsonBuilder : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (USDKKotlinx_serialization_runtimeJsonConfiguration *)buildConfiguration __attribute__((swift_name("buildConfiguration()")));
- (id<USDKKotlinx_serialization_runtimeSerialModule>)buildModule __attribute__((swift_name("buildModule()")));
@property BOOL allowStructuredMapKeys __attribute__((swift_name("allowStructuredMapKeys")));
@property NSString *classDiscriminator __attribute__((swift_name("classDiscriminator")));
@property BOOL encodeDefaults __attribute__((swift_name("encodeDefaults")));
@property BOOL ignoreUnknownKeys __attribute__((swift_name("ignoreUnknownKeys")));
@property NSString *indent __attribute__((swift_name("indent")));
@property BOOL isLenient __attribute__((swift_name("isLenient")));
@property BOOL prettyPrint __attribute__((swift_name("prettyPrint")));
@property id<USDKKotlinx_serialization_runtimeSerialModule> serialModule __attribute__((swift_name("serialModule")));
@property BOOL serializeSpecialFloatingPointValues __attribute__((swift_name("serializeSpecialFloatingPointValues")));
@property BOOL strictMode __attribute__((swift_name("strictMode"))) __attribute__((unavailable("'strictMode = true' is replaced with 3 new configuration parameters: 'ignoreUnknownKeys = false' to fail if an unknown key is encountered, 'serializeSpecialFloatingPointValues = false' to fail on 'NaN' and 'Infinity' values, 'isLenient = false' to prohibit parsing of any non-compliant or malformed JSON")));
@property BOOL unquoted __attribute__((swift_name("unquoted"))) __attribute__((unavailable("'unquoted' is deprecated in the favour of 'unquotedPrint'")));
@property BOOL unquotedPrint __attribute__((swift_name("unquotedPrint")));
@property BOOL useArrayPolymorphism __attribute__((swift_name("useArrayPolymorphism")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtimeJsonConfiguration")))
@interface USDKKotlinx_serialization_runtimeJsonConfiguration : USDKBase
- (instancetype)initWithEncodeDefaults:(BOOL)encodeDefaults ignoreUnknownKeys:(BOOL)ignoreUnknownKeys isLenient:(BOOL)isLenient serializeSpecialFloatingPointValues:(BOOL)serializeSpecialFloatingPointValues allowStructuredMapKeys:(BOOL)allowStructuredMapKeys prettyPrint:(BOOL)prettyPrint unquotedPrint:(BOOL)unquotedPrint indent:(NSString *)indent useArrayPolymorphism:(BOOL)useArrayPolymorphism classDiscriminator:(NSString *)classDiscriminator updateMode:(USDKKotlinx_serialization_runtimeUpdateMode *)updateMode __attribute__((swift_name("init(encodeDefaults:ignoreUnknownKeys:isLenient:serializeSpecialFloatingPointValues:allowStructuredMapKeys:prettyPrint:unquotedPrint:indent:useArrayPolymorphism:classDiscriminator:updateMode:)"))) __attribute__((objc_designated_initializer));
- (USDKKotlinx_serialization_runtimeJsonConfiguration *)doCopyEncodeDefaults:(BOOL)encodeDefaults ignoreUnknownKeys:(BOOL)ignoreUnknownKeys isLenient:(BOOL)isLenient serializeSpecialFloatingPointValues:(BOOL)serializeSpecialFloatingPointValues allowStructuredMapKeys:(BOOL)allowStructuredMapKeys prettyPrint:(BOOL)prettyPrint unquotedPrint:(BOOL)unquotedPrint indent:(NSString *)indent useArrayPolymorphism:(BOOL)useArrayPolymorphism classDiscriminator:(NSString *)classDiscriminator updateMode:(USDKKotlinx_serialization_runtimeUpdateMode *)updateMode __attribute__((swift_name("doCopy(encodeDefaults:ignoreUnknownKeys:isLenient:serializeSpecialFloatingPointValues:allowStructuredMapKeys:prettyPrint:unquotedPrint:indent:useArrayPolymorphism:classDiscriminator:updateMode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("KotlinCoroutineContext")))
@protocol USDKKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<USDKKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<USDKKotlinCoroutineContextElement> _Nullable)getKey:(id<USDKKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<USDKKotlinCoroutineContext>)minusKeyKey:(id<USDKKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<USDKKotlinCoroutineContext>)plusContext:(id<USDKKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol USDKKotlinCoroutineContextElement <USDKKotlinCoroutineContext>
@required
@property (readonly) id<USDKKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextElement")))
@interface USDKKotlinAbstractCoroutineContextElement : USDKBase <USDKKotlinCoroutineContextElement>
- (instancetype)initWithKey:(id<USDKKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<USDKKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinContinuationInterceptor")))
@protocol USDKKotlinContinuationInterceptor <USDKKotlinCoroutineContextElement>
@required
- (id<USDKKotlinContinuation>)interceptContinuationContinuation:(id<USDKKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (void)releaseInterceptedContinuationContinuation:(id<USDKKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher")))
@interface USDKKotlinx_coroutines_coreCoroutineDispatcher : USDKKotlinAbstractCoroutineContextElement <USDKKotlinContinuationInterceptor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithKey:(id<USDKKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)dispatchContext:(id<USDKKotlinCoroutineContext>)context block:(id<USDKKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatch(context:block:)")));
- (void)dispatchYieldContext:(id<USDKKotlinCoroutineContext>)context block:(id<USDKKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatchYield(context:block:)")));
- (id<USDKKotlinContinuation>)interceptContinuationContinuation:(id<USDKKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (BOOL)isDispatchNeededContext:(id<USDKKotlinCoroutineContext>)context __attribute__((swift_name("isDispatchNeeded(context:)")));
- (USDKKotlinx_coroutines_coreCoroutineDispatcher *)plusOther:(USDKKotlinx_coroutines_coreCoroutineDispatcher *)other __attribute__((swift_name("plus(other:)"))) __attribute__((unavailable("Operator '+' on two CoroutineDispatcher objects is meaningless. CoroutineDispatcher is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The dispatcher to the right of `+` just replaces the dispatcher to the left.")));
- (void)releaseInterceptedContinuationContinuation:(id<USDKKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("KotlinCValuesRef")))
@interface USDKKotlinCValuesRef : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void *)getPointerScope:(USDKKotlinAutofreeScope *)scope __attribute__((swift_name("getPointer(scope:)")));
@end;

__attribute__((swift_name("KotlinCValues")))
@interface USDKKotlinCValues : USDKKotlinCValuesRef
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (void *)getPointerScope:(USDKKotlinAutofreeScope *)scope __attribute__((swift_name("getPointer(scope:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (void *)placePlacement:(void *)placement __attribute__((swift_name("place(placement:)")));
@property (readonly) int32_t align __attribute__((swift_name("align")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("KotlinCValue")))
@interface USDKKotlinCValue : USDKKotlinCValues
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeCompositeEncoder")))
@protocol USDKKotlinx_serialization_runtimeCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (void)encodeIntElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNonSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(id)value __attribute__((swift_name("encodeNonSerializableElement(descriptor:index:value:)"))) __attribute__((unavailable("This method is deprecated for removal. Please remove it from your implementation and delegate to default method instead")));
- (void)encodeNullableSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)encodeUnitElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeUnitElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol USDKKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialKind")))
@interface USDKKotlinx_serialization_runtimeSerialKind : USDKBase
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeCompositeDecoder")))
@protocol USDKKotlinx_serialization_runtimeCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:)")));
- (int16_t)decodeShortElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)decodeUnitElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeUnitElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (id _Nullable)updateNullableSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateNullableSerializableElement(descriptor:index:deserializer:old:)")));
- (id _Nullable)updateSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateSerializableElement(descriptor:index:deserializer:old:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@property (readonly) USDKKotlinx_serialization_runtimeUpdateMode *updateMode __attribute__((swift_name("updateMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface USDKKotlinNothing : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtimeUpdateMode")))
@interface USDKKotlinx_serialization_runtimeUpdateMode : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKKotlinx_serialization_runtimeUpdateMode *banned __attribute__((swift_name("banned")));
@property (class, readonly) USDKKotlinx_serialization_runtimeUpdateMode *overwrite __attribute__((swift_name("overwrite")));
@property (class, readonly) USDKKotlinx_serialization_runtimeUpdateMode *update __attribute__((swift_name("update")));
- (int32_t)compareToOther:(USDKKotlinx_serialization_runtimeUpdateMode *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeJsonPrimitive")))
@interface USDKKotlinx_serialization_runtimeJsonPrimitive : USDKKotlinx_serialization_runtimeJsonElement
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL boolean __attribute__((swift_name("boolean")));
@property (readonly) USDKBoolean * _Nullable booleanOrNull __attribute__((swift_name("booleanOrNull")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) NSString * _Nullable contentOrNull __attribute__((swift_name("contentOrNull")));
@property (readonly, getter=double) double double_ __attribute__((swift_name("double_")));
@property (readonly) USDKDouble * _Nullable doubleOrNull __attribute__((swift_name("doubleOrNull")));
@property (readonly, getter=float) float float_ __attribute__((swift_name("float_")));
@property (readonly) USDKFloat * _Nullable floatOrNull __attribute__((swift_name("floatOrNull")));
@property (readonly, getter=int) int32_t int_ __attribute__((swift_name("int_")));
@property (readonly) USDKInt * _Nullable intOrNull __attribute__((swift_name("intOrNull")));
@property (readonly, getter=long) int64_t long_ __attribute__((swift_name("long_")));
@property (readonly) USDKLong * _Nullable longOrNull __attribute__((swift_name("longOrNull")));
@property (readonly) USDKKotlinx_serialization_runtimeJsonPrimitive *primitive __attribute__((swift_name("primitive")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtimeJsonNull")))
@interface USDKKotlinx_serialization_runtimeJsonNull : USDKKotlinx_serialization_runtimeJsonPrimitive
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)jsonNull __attribute__((swift_name("init()")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) NSString * _Nullable contentOrNull __attribute__((swift_name("contentOrNull")));
@property (readonly) USDKKotlinx_serialization_runtimeJsonNull *jsonNull __attribute__((swift_name("jsonNull")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngine")))
@protocol USDKKtor_client_coreHttpClientEngine <USDKKotlinx_coroutines_coreCoroutineScope, USDKKtor_ioCloseable>
@required
- (void)installClient:(USDKKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
@property (readonly) USDKKtor_client_coreHttpClientEngineConfig *config __attribute__((swift_name("config")));
@property (readonly) USDKKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher")));
@property (readonly) NSSet<id<USDKKtor_client_coreHttpClientEngineCapability>> *supportedCapabilities __attribute__((swift_name("supportedCapabilities")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientConfig")))
@interface USDKKtor_client_coreHttpClientConfig : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (USDKKtor_client_coreHttpClientConfig *)clone __attribute__((swift_name("clone()")));
- (void)engineBlock:(void (^)(USDKKtor_client_coreHttpClientEngineConfig *))block __attribute__((swift_name("engine(block:)")));
- (void)installClient:(USDKKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
- (void)installFeature:(id<USDKKtor_client_coreHttpClientFeature>)feature configure:(void (^)(id))configure __attribute__((swift_name("install(feature:configure:)")));
- (void)installKey:(NSString *)key block:(void (^)(USDKKtor_client_coreHttpClient *))block __attribute__((swift_name("install(key:block:)")));
- (void)plusAssignOther:(USDKKtor_client_coreHttpClientConfig *)other __attribute__((swift_name("plusAssign(other:)")));
@property BOOL expectSuccess __attribute__((swift_name("expectSuccess")));
@property BOOL followRedirects __attribute__((swift_name("followRedirects")));
@property BOOL useDefaultTransformers __attribute__((swift_name("useDefaultTransformers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineCapability")))
@protocol USDKKtor_client_coreHttpClientEngineCapability
@required
@end;

__attribute__((swift_name("Ktor_utilsAttributes")))
@protocol USDKKtor_utilsAttributes
@required
- (id)computeIfAbsentKey:(USDKKtor_utilsAttributeKey *)key block:(id (^)(void))block __attribute__((swift_name("computeIfAbsent(key:block:)")));
- (BOOL)containsKey_:(USDKKtor_utilsAttributeKey *)key __attribute__((swift_name("contains(key_:)")));
- (id)getKey_:(USDKKtor_utilsAttributeKey *)key __attribute__((swift_name("get(key_:)")));
- (id _Nullable)getOrNullKey:(USDKKtor_utilsAttributeKey *)key __attribute__((swift_name("getOrNull(key:)")));
- (void)putKey:(USDKKtor_utilsAttributeKey *)key value:(id)value __attribute__((swift_name("put(key:value:)")));
- (void)removeKey:(USDKKtor_utilsAttributeKey *)key __attribute__((swift_name("remove(key:)")));
- (id)takeKey:(USDKKtor_utilsAttributeKey *)key __attribute__((swift_name("take(key:)")));
- (id _Nullable)takeOrNullKey:(USDKKtor_utilsAttributeKey *)key __attribute__((swift_name("takeOrNull(key:)")));
@property (readonly) NSArray<USDKKtor_utilsAttributeKey *> *allKeys __attribute__((swift_name("allKeys")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineConfig")))
@interface USDKKtor_client_coreHttpClientEngineConfig : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property BOOL pipelining __attribute__((swift_name("pipelining")));
@property USDKKtor_client_coreProxyConfig * _Nullable proxy __attribute__((swift_name("proxy")));
@property (readonly) USDKKotlinNothing *response __attribute__((swift_name("response"))) __attribute__((unavailable("Response config is deprecated. See [HttpPlainText] feature for charset configuration")));
@property int32_t threadsCount __attribute__((swift_name("threadsCount")));
@end;

__attribute__((swift_name("Ktor_utilsPipeline")))
@interface USDKKtor_utilsPipeline : USDKBase
- (instancetype)initWithPhase:(USDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<USDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhases:(USDKKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer));
- (void)addPhasePhase:(USDKKtor_utilsPipelinePhase *)phase __attribute__((swift_name("addPhase(phase:)")));
- (void)afterIntercepted __attribute__((swift_name("afterIntercepted()")));
- (void)insertPhaseAfterReference:(USDKKtor_utilsPipelinePhase *)reference phase:(USDKKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseAfter(reference:phase:)")));
- (void)insertPhaseBeforeReference:(USDKKtor_utilsPipelinePhase *)reference phase:(USDKKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseBefore(reference:phase:)")));
- (void)interceptPhase:(USDKKtor_utilsPipelinePhase *)phase block:(id<USDKKotlinSuspendFunction2>)block __attribute__((swift_name("intercept(phase:block:)")));
- (void)mergeFrom:(USDKKtor_utilsPipeline *)from __attribute__((swift_name("merge(from:)")));
@property (readonly) id<USDKKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) BOOL isEmpty __attribute__((swift_name("isEmpty")));
@property (readonly) NSArray<USDKKtor_utilsPipelinePhase *> *items __attribute__((swift_name("items")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline")))
@interface USDKKtor_client_coreHttpReceivePipeline : USDKKtor_utilsPipeline
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithPhase:(USDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<USDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(USDKKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline")))
@interface USDKKtor_client_coreHttpRequestPipeline : USDKKtor_utilsPipeline
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithPhase:(USDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<USDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(USDKKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline")))
@interface USDKKtor_client_coreHttpResponsePipeline : USDKKtor_utilsPipeline
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithPhase:(USDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<USDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(USDKKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline")))
@interface USDKKtor_client_coreHttpSendPipeline : USDKKtor_utilsPipeline
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithPhase:(USDKKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<USDKKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(USDKKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialModuleCollector")))
@protocol USDKKotlinx_serialization_runtimeSerialModuleCollector
@required
- (void)contextualKClass:(id<USDKKotlinKClass>)kClass serializer:(id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<USDKKotlinKClass>)baseClass actualClass:(id<USDKKotlinKClass>)actualClass actualSerializer:(id<USDKKotlinx_serialization_runtimeKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol USDKKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol USDKKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol USDKKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol USDKKotlinKClass <USDKKotlinKDeclarationContainer, USDKKotlinKAnnotatedElement, USDKKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol USDKKotlinCoroutineContextKey
@required
@end;

__attribute__((swift_name("KotlinContinuation")))
@protocol USDKKotlinContinuation
@required
- (void)resumeWithResult:(id _Nullable)result __attribute__((swift_name("resumeWith(result:)")));
@property (readonly) id<USDKKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreRunnable")))
@protocol USDKKotlinx_coroutines_coreRunnable
@required
- (void)run __attribute__((swift_name("run()")));
@end;

__attribute__((swift_name("KotlinDeferScope")))
@interface USDKKotlinDeferScope : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)deferBlock:(void (^)(void))block __attribute__((swift_name("defer(block:)")));
@end;

__attribute__((swift_name("KotlinNativePlacement")))
@protocol USDKKotlinNativePlacement
@required
- (void *)doAllocSize:(int32_t)size align:(int32_t)align __attribute__((swift_name("doAlloc(size:align:)")));
- (void *)doAllocSize:(int64_t)size align_:(int32_t)align __attribute__((swift_name("doAlloc(size:align_:)")));
@end;

__attribute__((swift_name("KotlinAutofreeScope")))
@interface USDKKotlinAutofreeScope : USDKKotlinDeferScope <USDKKotlinNativePlacement>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void *)doAllocSize:(int64_t)size align_:(int32_t)align __attribute__((swift_name("doAlloc(size:align_:)")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientFeature")))
@protocol USDKKtor_client_coreHttpClientFeature
@required
- (void)installFeature:(id)feature scope:(USDKKtor_client_coreHttpClient *)scope __attribute__((swift_name("install(feature:scope:)")));
- (id)prepareBlock:(void (^)(id))block __attribute__((swift_name("prepare(block:)")));
@property (readonly) USDKKtor_utilsAttributeKey *key __attribute__((swift_name("key")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsAttributeKey")))
@interface USDKKtor_utilsAttributeKey : USDKBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreProxyConfig")))
@interface USDKKtor_client_coreProxyConfig : USDKBase
- (instancetype)initWithUrl:(USDKKtor_httpUrl *)url __attribute__((swift_name("init(url:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsPipelinePhase")))
@interface USDKKtor_utilsPipelinePhase : USDKBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinFunction")))
@protocol USDKKotlinFunction
@required
@end;

__attribute__((swift_name("KotlinSuspendFunction2")))
@protocol USDKKotlinSuspendFunction2 <USDKKotlinFunction>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl")))
@interface USDKKtor_httpUrl : USDKBase
- (instancetype)initWithProtocol:(USDKKtor_httpURLProtocol *)protocol host:(NSString *)host specifiedPort:(int32_t)specifiedPort encodedPath:(NSString *)encodedPath parameters:(id<USDKKtor_httpParameters>)parameters fragment:(NSString *)fragment user:(NSString * _Nullable)user password:(NSString * _Nullable)password trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("init(protocol:host:specifiedPort:encodedPath:parameters:fragment:user:password:trailingQuery:)"))) __attribute__((objc_designated_initializer));
- (USDKKtor_httpUrl *)doCopyProtocol:(USDKKtor_httpURLProtocol *)protocol host:(NSString *)host specifiedPort:(int32_t)specifiedPort encodedPath:(NSString *)encodedPath parameters:(id<USDKKtor_httpParameters>)parameters fragment:(NSString *)fragment user:(NSString * _Nullable)user password:(NSString * _Nullable)password trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("doCopy(protocol:host:specifiedPort:encodedPath:parameters:fragment:user:password:trailingQuery:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *encodedPath __attribute__((swift_name("encodedPath")));
@property (readonly) NSString *fragment __attribute__((swift_name("fragment")));
@property (readonly) NSString *host __attribute__((swift_name("host")));
@property (readonly) id<USDKKtor_httpParameters> parameters __attribute__((swift_name("parameters")));
@property (readonly) NSString * _Nullable password __attribute__((swift_name("password")));
@property (readonly) int32_t port __attribute__((swift_name("port")));
@property (readonly) USDKKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property (readonly) int32_t specifiedPort __attribute__((swift_name("specifiedPort")));
@property (readonly) BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property (readonly) NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol")))
@interface USDKKtor_httpURLProtocol : USDKBase
- (instancetype)initWithName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("init(name:defaultPort:)"))) __attribute__((objc_designated_initializer));
- (USDKKtor_httpURLProtocol *)doCopyName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("doCopy(name:defaultPort:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t defaultPort __attribute__((swift_name("defaultPort")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("Ktor_utilsStringValues")))
@protocol USDKKtor_utilsStringValues
@required
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<USDKKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (void)forEachBody:(void (^)(NSString *, NSArray<NSString *> *))body __attribute__((swift_name("forEach(body:)")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty_ __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@end;

__attribute__((swift_name("Ktor_httpParameters")))
@protocol USDKKtor_httpParameters <USDKKtor_utilsStringValues>
@required
@end;

__attribute__((swift_name("KotlinMapEntry")))
@protocol USDKKotlinMapEntry
@required
@property (readonly) id _Nullable key __attribute__((swift_name("key")));
@property (readonly) id _Nullable value_ __attribute__((swift_name("value_")));
@end;

#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
